<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Mdepartamento extends CI_Model
{
    public function num_rows()
    {
       return $this->db->count_all("departamento");
    }

    public function num_rows_find($txt_buscar)
    {   
       $this->db->like("departamento", $txt_buscar);
       $resultados= $this->db->count_all_results("departamento");
       return $resultados;

    }

    public function listar_paginacion($offset,$per_page)
    { 
       $this->db->order_by("d.id_departamento", "DESC");  
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("departamento d");

       return $resultados->result();  
    } 

    public function listar_paginacion_find($txt_buscar,$offset,$per_page)
    {
       $this->db->order_by("d.id_departamento", "DESC");
       $this->db->like("d.departamento", $txt_buscar); 
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("departamento d");

       return $resultados->result(); 
    }
 
	public function get_departamentos()
	{
       $this->db->select("*"); 
       $this->db->from("departamento d");
       $resultados = $this->db->get();
       return $resultados->result();
	}

	public function Reg_departamento($data)
	{
		   $this->db->insert("departamento",$data);
	}

	public function update_departamento($id_departamento)
	{
       $this->db->select("*"); 
       $this->db->from("departamento d");
       $this->db->where("d.id_departamento",$id_departamento);
       $result = $this->db->get();
       return $result->result();

	}

	public function Setting_departamento($data,$id_departamento)
	{
		   $this->db->where("id_departamento",$id_departamento);
       $this->db->update("departamento",$data);
	}

	public function get_departamento($id_departamento)
	{
      $this->db->select("*"); 
       $this->db->from("departamento d");
       $this->db->where("d.id_departamento",$id_departamento);
       $result = $this->db->get();
       return $result->result();
	}

  public function Delete_departamento($id_departamento)
	{
		   $this->db->where("id_departamento", $id_departamento);
		   $this->db->delete("departamento"); 
	}


  /* SECCION DE LOS SELECTS DEL SISTEMA */
   
}

?>


