<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Mcuenta_parametrizadas extends CI_Model
{
    public function num_rows()
    {
       return $this->db->count_all("cuenta_parametrizadas");
    }

    public function num_rows_find($txt_buscar)
    {   
       $this->db->like("cuenta_cp", $txt_buscar);
       $resultados= $this->db->count_all_results("cuenta_parametrizadas");
       return $resultados;

    }

    public function listar_paginacion($offset,$per_page)
    { 
       $this->db->order_by("c.id_cuenta_parametrizadas", "DESC");  
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("cuenta_parametrizadas c");

       return $resultados->result();  
    } 

    public function listar_paginacion_find($txt_buscar,$offset,$per_page)
    {
       $this->db->order_by("c.id_cuenta_parametrizadas", "DESC");
       $this->db->like("c.cuenta_cp", $txt_buscar); 
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("cuenta_parametrizadas c");

       return $resultados->result(); 
    }
 
	public function get_cuenta_parametrizadass()
	{
       $this->db->select("*"); 
       $this->db->from("cuenta_parametrizadas c");
       $resultados = $this->db->get();
       return $resultados->result();
	}

	public function Reg_cuenta_parametrizadas($data)
	{
		   $this->db->insert("cuenta_parametrizadas",$data);
	}

	public function update_cuenta_parametrizadas($id_cuenta_parametrizadas)
	{
       $this->db->select("*"); 
       $this->db->from("cuenta_parametrizadas c");
       $this->db->where("c.id_cuenta_parametrizadas",$id_cuenta_parametrizadas);
       $result = $this->db->get();
       return $result->result();

	}

	public function Setting_cuenta_parametrizadas($data,$id_cuenta_parametrizadas)
	{
		   $this->db->where("id_cuenta_parametrizadas",$id_cuenta_parametrizadas);
       $this->db->update("cuenta_parametrizadas",$data);
	}

	public function get_cuenta_parametrizadas($id_cuenta_parametrizadas)
	{
      $this->db->select("*"); 
       $this->db->from("cuenta_parametrizadas c");
       $this->db->where("c.id_cuenta_parametrizadas",$id_cuenta_parametrizadas);
       $result = $this->db->get();
       return $result->result();
	}

  public function Delete_cuenta_parametrizadas($id_cuenta_parametrizadas)
	{
		   $this->db->where("id_cuenta_parametrizadas", $id_cuenta_parametrizadas);
		   $this->db->delete("cuenta_parametrizadas"); 
	}


  /* SECCION DE LOS SELECTS DEL SISTEMA */
   
}

?>


