<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Mplan_cuentas extends CI_Model
{
    public function num_rows()
    {
       return $this->db->count_all("plan_cuenta");
    }

    public function num_rows_find($txt_buscar)
    {   
       $this->db->like("Codigo", $txt_buscar);
       $resultados= $this->db->count_all_results("plan_cuenta");
       return $resultados;
    }

    public function listar_paginacion($offset,$per_page)
    { 
       $this->db->order_by("p.Codigo", "ASC");
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("plan_cuenta p");

       return $resultados->result();  
    } 

    public function listar_paginacion_find($txt_buscar,$offset,$per_page)
    {
       $this->db->order_by("p.id_plan_cuenta", "DESC");
       
       $this->db->like("p.Codigo", $txt_buscar); 
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("plan_cuenta p");

       return $resultados->result(); 
    }
 
  	public function get_plan_cuentass()
  	{    
       $this->db->order_by("p.Codigo", "ASC");
       $resultados = $this->db->get("plan_cuenta p");
       return $resultados->result();
  	}

  	public function Reg_plan_cuentas($data)
  	{
  		 $this->db->insert("plan_cuenta",$data);
  	}

  	public function update_plan_cuentas($id_plan_cuentas)
  	{
       $this->db->select("*");
       $this->db->join("nivel_pc n", "n.id_nivel_pc = p.id_nivel");
       $this->db->join("moneda m", "m.id_moneda = p.id_moneda"); 
       $this->db->from("plan_cuentas p");
       $this->db->where("p.id_plan_cuentas",$id_plan_cuentas);
       $result = $this->db->get();
       return $result->result();
  	}

  	public function Setting_plan_cuentas($data,$id_plan_cuenta)
  	{
  		 $this->db->where("id_plan_cuenta",$id_plan_cuenta);
       $this->db->update("plan_cuenta",$data);
  	}

  	public function get_plan_cuentas($id_plan_cuenta)
  	{
       $this->db->select("*");
       $this->db->from("plan_cuenta p");
       $this->db->where("p.id_plan_cuenta",$id_plan_cuenta);
       $result = $this->db->get();
       return $result->result();
  	}

    public function Delete_plan_cuentas($id_plan_cuentas)
  	{
		   $this->db->where("id_plan_cuenta", $id_plan_cuentas);
		   $this->db->delete("plan_cuenta"); 
  	}


    /* SECCION DE LOS SELECTS DEL SISTEMA */
     
    public function get_nivels()
    {
      $this->db->select("*");
      $this->db->from("nivel_pc");
      $result = $this->db->get();
      return $result->result();
    }

   
    public function get_monedas()
    {
      $this->db->select("*");
      $this->db->from("moneda");
      $result = $this->db->get();
      return $result->result();
    }

    /* FUNCIONES DE REGISTRO POR IMPORTACIÓN */

    public function registrar_pc_import($data)
    {
      $this->db->insert("plan_cuenta",$data);
    }

    public function get_pc_codigo($Codigo)
    {
      $this->db->select("*");
      $this->db->from("plan_cuenta p");
      $this->db->where("p.Codigo",$Codigo);
      $result = $this->db->get();
      return $result->result();
    }

    public function buscar_plan_cuentas($txt_buscar)
    {
      $this->db->like("Codigo", $txt_buscar); 
      $this->db->or_like("Nombre", $txt_buscar);
      $resultados = $this->db->get("plan_cuenta");   
      return $resultados->result();
    }

    public function cuenta_parametrizada()
    {
      $this->db->select("*");
      $this->db->from("cuenta_parametrizadas cp");
      $result = $this->db->get();
      return $result->result();
    }

    public function update_cuentas_param($data,$id_pc)
    {
       $this->db->where("id_cuenta_parametrizadas",$id_pc);
       $this->db->update("cuenta_parametrizadas",$data);      
    }
 
}

?>


