<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Mestudiante extends CI_Model
{
    /*
    public function num_rows()
    {
       return $this->db->count_all("estudiante");
    }*/

	public function get_estudiantes_todos()
	{
       $this->db->select("*");
       $this->db->order_by("e.id_estudiante", "ASC");
       $this->db->from("estudiante e");
       $this->db->join("curso c", "c.id_curso = e.id_curso"); 
       $resultados = $this->db->get();
       return $resultados->result();
	}

	public function get_busqueda_estudiantes_todos($txt_buscar)
	{
       $this->db->order_by("e.id_estudiante", "DESC");
       $this->db->join("curso c", "c.id_curso = e.id_curso"); 
       $this->db->like("e.nombre_estudiante", $txt_buscar); 
       $resultados = $this->db->get("estudiante e");
       return $resultados->result(); 
	}
	
	public function get_estudiantes_todos_curso($id_curso)
	{
	   $this->db->order_by("e.id_estudiante", "ASC");
       $this->db->join("curso c", "c.id_curso = e.id_curso"); 
       $this->db->like("e.id_curso", $id_curso); 
       $resultados = $this->db->get("estudiante e");
       return $resultados->result();     
	}
	

    public function num_rows()
    {
       return $this->db->count_all("estudiante");
    }

    public function num_rows_find($txt_buscar)
    {   
       $this->db->like("nombre_estudiante", $txt_buscar);
       $resultados= $this->db->count_all_results("estudiante");
       return $resultados;

    }

    public function listar_paginacion($offset,$per_page)
    { 
       $this->db->order_by("e.id_estudiante", "DESC");
       $this->db->join("curso c", "c.id_curso = e.id_curso"); 
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("estudiante e");

       return $resultados->result();  
    } 

    public function listar_paginacion_find($txt_buscar,$offset,$per_page)
    {
       $this->db->order_by("e.id_estudiante", "DESC");
       $this->db->join("curso c", "c.id_curso = e.id_curso"); 
       $this->db->like("e.nombre_estudiante", $txt_buscar); 
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("estudiante e");

       return $resultados->result(); 
    }
 
	public function get_estudiantes()
	{
       $this->db->select("*");
       $this->db->join("curso c", "c.id_curso = e.id_curso"); 
       $this->db->from("estudiante e");
       $resultados = $this->db->get();
       return $resultados->result();
	}

 
	public function update_estudiante($id_estudiante)
	{
       $this->db->select("*");
       $this->db->join("curso c", "c.id_curso = e.id_curso"); 
       $this->db->from("estudiante e");
       $this->db->where("e.id_estudiante",$id_estudiante);
       $result = $this->db->get();
       return $result->result();
	}

	public function Setting_estudiante($data,$id_estudiante)
	{
	   $this->db->where("id_estudiante",$id_estudiante);
       $this->db->update("estudiante",$data);
	}

	public function get_estudiante($id_estudiante)
	{
       $this->db->select("*,c.certificado_curso");
       $this->db->join("curso c", "c.id_curso = e.id_curso"); 
       $this->db->from("estudiante e");
       $this->db->where("e.id_estudiante",$id_estudiante);
       $result = $this->db->get();
       return $result->result();
	}

    public function Delete_estudiante($id_estudiante)
	{
	   $this->db->where("id_estudiante", $id_estudiante);
	   $this->db->delete("estudiante"); 
	}


    /* SECCION DE LOS SELECTS DEL SISTEMA */
   
    public function get_cursos()
    {
      $this->db->select("*");
      $this->db->from("curso");
      $result = $this->db->get();
      return $result->result();
    }

	public function Reg_estudiante($data)
	{
	   $this->db->insert("estudiante",$data);
	}
	
	public function verificar_pago_estudiante($id_curso,$ci)
	{
	   $this->db->select("*");
       $this->db->from("estudiante u");
       $this->db->where("u.id_curso",$id_curso);
       $this->db->where("u.ci_estudiante",$ci);
       $resultados = $this->db->get();
       return $resultados->result();
	}
	
	public function get_estudiante_ver_ci($ci_verificar)
	{  
	   $this->db->select("*");
       $this->db->order_by("e.id_estudiante", "DESC");
       $this->db->from("estudiante e");
       $this->db->where("e.ci_estudiante",$ci_verificar);
       $this->db->join("curso c", "c.id_curso = e.id_curso"); 
       $resultados = $this->db->get();
       
       return $resultados->result();	    
	}
	
	public function get_estudiante_curso_impresion($id_estudiante,$id_curso)
	{
	   $this->db->select("*,c.certificado_curso");
       $this->db->from("estudiante e");
       $this->db->where("e.id_estudiante",$id_estudiante);
       $this->db->where("e.id_curso",$id_curso);
       $this->db->join("curso c", "c.id_curso = e.id_curso"); 
       $resultados = $this->db->get();
       
       return $resultados->result();	    
	}
	
	public function get_estudiantes_curso($id_curso)
	{
	   $this->db->select("*");
       $this->db->from("estudiante e");
       $this->db->where("e.id_curso",$id_curso);
       $this->db->join("curso c", "c.id_curso = e.id_curso"); 
       $resultados = $this->db->get();
       
       return $resultados->result();	    
	}
	
 
}

?>


