
<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Mlogin extends CI_Model
{
	public function verificar($email,$password)
	{
		    $this->db->select("*");
        $this->db->from("usuario");
        $this->db->where("email",$email);
        $this->db->where("password",$password);
        $resultados = $this->db->get();
        return $resultados->result();
	}

  public function get_user($id_usuario)
  {
        $this->db->select("*");
        $this->db->from("usuario");
        $this->db->where("id_usuario",$id_usuario);
        $resultados = $this->db->get();
        return $resultados->result();   
  }

}

?>


