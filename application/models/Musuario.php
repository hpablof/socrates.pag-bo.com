<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Musuario extends CI_Model
{
    public function num_rows()
    {
       return $this->db->count_all("usuario");
    }

    public function num_rows_find($txt_buscar)
    {   
       $this->db->like("alias", $txt_buscar);
       $resultados= $this->db->count_all_results("usuario");
       return $resultados;

    }

    public function listar_paginacion($offset,$per_page)
    { 
       $this->db->order_by("u.id_usuario", "DESC");
       $this->db->join("cargo_usuario c", "c.id_cargo_usuario = u.id_cargo_usuario");  
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("usuario u");

       return $resultados->result();  
    } 

    public function listar_paginacion_find($txt_buscar,$offset,$per_page)
    {
       $this->db->order_by("u.id_usuario", "DESC");
       $this->db->join("cargo_usuario c", "c.id_cargo_usuario = u.id_cargo_usuario");
       $this->db->like("u.alias", $txt_buscar); 
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("usuario u");

       return $resultados->result(); 
    }
 
	public function get_usuarios()
	{
       $this->db->select("*");
       $this->db->join("cargo_usuario c", "c.id_cargo_usuario = u.id_cargo_usuario"); 
       $this->db->from("usuario u");
       $resultados = $this->db->get();
       return $resultados->result();
	}

	public function Reg_usuario($data)
	{
		   $this->db->insert("usuario",$data);
	}

	public function update_usuario($id_usuario)
	{
       $this->db->select("*");
       $this->db->join("cargo_usuario c", "c.id_cargo_usuario = u.id_cargo_usuario"); 
       $this->db->from("usuario u");
       $this->db->where("u.id_usuario",$id_usuario);
       $result = $this->db->get();
       return $result->result();

	}

	public function Setting_usuario($data,$id_usuario)
	{
	   $this->db->where("id_usuario",$id_usuario);
       $this->db->update("usuario",$data);
	}

	public function get_usuario($id_usuario)
	{
       $this->db->select("*");
       $this->db->join("cargo_usuario c", "c.id_cargo_usuario = u.id_cargo_usuario"); 
       $this->db->from("usuario u");
       $this->db->where("u.id_usuario",$id_usuario);
       $result = $this->db->get();
       return $result->result();
	}

    public function Delete_usuario($id_usuario)
	{
	   $this->db->where("id_usuario", $id_usuario);
	   $this->db->delete("usuario"); 
	}


  /* SECCION DE LOS SELECTS DEL SISTEMA */
   
    public function get_cargo_usuarios()
    {
       $this->db->select("*");
       $this->db->from("cargo_usuario");
       $result = $this->db->get();
       return $result->result();
    }

 
}

?>


