<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Mcargo_usuario extends CI_Model
{
    public function num_rows()
    {
       return $this->db->count_all("cargo_usuario");
    }

    public function num_rows_find($txt_buscar)
    {   
       $this->db->like("cargo_usuario", $txt_buscar);
       $resultados= $this->db->count_all_results("cargo_usuario");
       return $resultados;

    }

    public function listar_paginacion($offset,$per_page)
    { 
       $this->db->order_by("c.id_cargo_usuario", "DESC");  
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("cargo_usuario c");

       return $resultados->result();  
    } 

    public function listar_paginacion_find($txt_buscar,$offset,$per_page)
    {
       $this->db->order_by("c.id_cargo_usuario", "DESC");
       $this->db->like("c.cargo_usuario", $txt_buscar); 
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("cargo_usuario c");

       return $resultados->result(); 
    }
 
	public function get_cargo_usuarios()
	{
       $this->db->select("*"); 
       $this->db->from("cargo_usuario c");
       $resultados = $this->db->get();
       return $resultados->result();
	}

	public function Reg_cargo_usuario($data)
	{
		   $this->db->insert("cargo_usuario",$data);
	}

	public function update_cargo_usuario($id_cargo_usuario)
	{
       $this->db->select("*"); 
       $this->db->from("cargo_usuario c");
       $this->db->where("c.id_cargo_usuario",$id_cargo_usuario);
       $result = $this->db->get();
       return $result->result();

	}

	public function Setting_cargo_usuario($data,$id_cargo_usuario)
	{
		   $this->db->where("id_cargo_usuario",$id_cargo_usuario);
       $this->db->update("cargo_usuario",$data);
	}

	public function get_cargo_usuario($id_cargo_usuario)
	{
      $this->db->select("*"); 
       $this->db->from("cargo_usuario c");
       $this->db->where("c.id_cargo_usuario",$id_cargo_usuario);
       $result = $this->db->get();
       return $result->result();
	}

  public function Delete_cargo_usuario($id_cargo_usuario)
	{
		   $this->db->where("id_cargo_usuario", $id_cargo_usuario);
		   $this->db->delete("cargo_usuario"); 
	}


  /* SECCION DE LOS SELECTS DEL SISTEMA */
   
}

?>


