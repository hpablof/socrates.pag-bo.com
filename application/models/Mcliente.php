<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Mcliente extends CI_Model
{
    public function num_rows($id_empresa,$id_cliente)
    {  
       $this->db->where("id_empresa",$id_empresa);
       return $this->db->count_all("cliente");
    }

    public function num_rows_find($txt_buscar,$id_empresa,$id_cliente)
    {   
       $this->db->like("cliente", $txt_buscar);
       $this->db->where("id_empresa",$id_empresa);
       $resultados= $this->db->count_all_results("cliente");
       return $resultados;

    }

    public function listar_paginacion($offset,$per_page,$id_empresa,$id_cliente)
    { 
       $this->db->order_by("c.id_cliente", "DESC"); 
       $this->db->where("c.id_empresa",$id_empresa); 
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("cliente c");

       return $resultados->result();  
    } 

    public function listar_paginacion_find($txt_buscar,$offset,$per_page,$id_empresa,$id_cliente)
    {
       $this->db->order_by("c.id_cliente", "DESC");
       $this->db->where("c.id_empresa",$id_empresa);
       $this->db->like("c.cliente", $txt_buscar); 
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("cliente c");

       return $resultados->result(); 
    }

    public function buscar_cliente($txt_buscar,$id_empresa)
    {
       $this->db->order_by("c.cliente", "ASC");
       $this->db->where("c.id_empresa",$id_empresa);
       $this->db->like("c.cliente", $txt_buscar); 
       $resultados = $this->db->get("cliente c");

       return $resultados->result();      
    }

 
	public function get_clientes()
	{
       $this->db->select("*"); 
       $this->db->from("cliente c");
       $resultados = $this->db->get();
       return $resultados->result();
	}

	public function Reg_cliente($data)
	{
		   $this->db->insert("cliente",$data);
	}

	public function update_cliente($id_cliente)
	{
       $this->db->select("*"); 
       $this->db->from("cliente c");
       $this->db->where("c.id_cliente",$id_cliente);
       $result = $this->db->get();
       return $result->result();

	}

	public function Setting_cliente($data,$id_cliente)
	{
		   $this->db->where("id_cliente",$id_cliente);
       $this->db->update("cliente",$data);
	}

	public function get_cliente($id_cliente)
	{
      $this->db->select("*"); 
       $this->db->from("cliente c");
       $this->db->where("c.id_cliente",$id_cliente);
       $result = $this->db->get();
       return $result->result();
	}

  public function Delete_cliente($id_cliente)
	{
		   $this->db->where("id_cliente", $id_cliente);
		   $this->db->delete("cliente"); 
	}


   
}

?>


