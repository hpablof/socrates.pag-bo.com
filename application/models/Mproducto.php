<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Mproducto extends CI_Model
{
    public function num_rows($id_empresa,$id_gestion)
    {
       $this->db->where("id_empresa",$id_empresa);
       $this->db->where("id_gestion",$id_gestion); 
       return $this->db->count_all("producto");

    }

    public function num_rows_find($txt_buscar,$id_empresa,$id_gestion)
    {  
       $this->db->where("id_empresa",$id_empresa);
       $this->db->where("id_gestion",$id_gestion);  
       $this->db->like("producto", $txt_buscar);
       $resultados= $this->db->count_all_results("producto");
       return $resultados;

    }

    public function listar_paginacion($offset,$per_page,$id_empresa,$id_gestion)
    { 
       $this->db->order_by("p.id_producto", "DESC");
       $this->db->join("area a", "a.id_area = p.id_area");
       $this->db->where("p.id_empresa",$id_empresa);
       $this->db->where("p.id_gestion",$id_gestion);  
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("producto p");

       return $resultados->result();  
    } 

    public function listar_paginacion_find($txt_buscar,$offset,$per_page,$id_empresa,$id_gestion)
    {
       $this->db->order_by("p.id_producto", "DESC");
       $this->db->join("area a", "a.id_area = p.id_area");
       $this->db->where("p.id_empresa",$id_empresa);
       $this->db->where("p.id_gestion",$id_gestion); 
       $this->db->like("p.producto", $txt_buscar); 
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("producto p");

       return $resultados->result(); 
    }

    public function buscar_producto($txt_buscar,$id_empresa,$id_gestion)
    {
       
       $this->db->order_by("p.id_producto", "DESC");
       $this->db->join("area a", "a.id_area = p.id_area");
       $this->db->where("p.id_empresa",$id_empresa);
       $this->db->where("p.id_gestion",$id_gestion); 
       $this->db->like("p.producto", $txt_buscar); 
       $resultados = $this->db->get("producto p");

       return $resultados->result();      
    }
 
  	public function get_productos()
  	{
         $this->db->select("*");
         $this->db->join("area a", "a.id_area = p.id_area"); 
         $this->db->from("producto p");
         $resultados = $this->db->get();
         return $resultados->result();
  	}

  	public function Reg_producto($data)
  	{
  		   $this->db->insert("producto",$data);
  	}

  	public function update_producto($id_producto)
  	{
         $this->db->select("*");
         $this->db->join("area a", "a.id_area = p.id_area"); 
         $this->db->from("producto p");
         $this->db->where("p.id_producto",$id_producto);
         $result = $this->db->get();
         return $result->result();

  	}

  	public function Setting_producto($data,$id_producto)
  	{
  		   $this->db->where("id_producto",$id_producto);
         $this->db->update("producto",$data);
  	}

  	public function get_producto($id_producto)
  	{
         $this->db->select("*");
         $this->db->join("area a", "a.id_area = p.id_area"); 
         $this->db->from("producto p");
         $this->db->where("p.id_producto",$id_producto);
         $result = $this->db->get();
         return $result->result();
  	}

    public function Delete_producto($id_producto)
  	{
  		   $this->db->where("id_producto", $id_producto);
  		   $this->db->delete("producto"); 
  	}


    /* SECCION DE LOS SELECTS DEL SISTEMA */
     
    public function get_areas($id_empresa)
    {
        $this->db->select("*");
        $this->db->where("a.id_empresa",$id_empresa);
        $this->db->from("area a");
        $result = $this->db->get();
        return $result->result();
    }

 
}

?>


