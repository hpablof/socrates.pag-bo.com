<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Mproveedor extends CI_Model
{
  public function num_rows($id_empresa,$id_gestion)
  {  
       $this->db->where("id_empresa",$id_empresa);
       $this->db->where("id_gestion",$id_gestion); 
       return $this->db->count_all("proveedor");
  }

  public function num_rows_find($txt_buscar,$id_empresa,$id_gestion)
  {  
       $this->db->where("id_empresa",$id_empresa);
       $this->db->where("id_gestion",$id_gestion);  
       $this->db->like("proveedor", $txt_buscar);
       $resultados= $this->db->count_all_results("proveedor");
       return $resultados;
  }

  public function listar_paginacion($offset,$per_page,$id_empresa,$id_gestion)
  { 
       $this->db->order_by("p.id_proveedor", "DESC");
       $this->db->where("p.id_empresa",$id_empresa);
       $this->db->where("p.id_gestion",$id_gestion);  
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("proveedor p");

       return $resultados->result();  
  } 

  public function listar_paginacion_find($txt_buscar,$offset,$per_page,$id_empresa,$id_gestion)
  {
       $this->db->order_by("p.id_proveedor", "DESC");
       $this->db->where("p.id_empresa",$id_empresa);
       $this->db->where("p.id_gestion",$id_gestion); 
       $this->db->like("p.proveedor", $txt_buscar); 
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("proveedor p");

       return $resultados->result(); 
  }

  public function buscar_proveedor($txt_buscar,$id_empresa,$id_gestion)
  {
       $this->db->order_by("p.id_proveedor", "DESC");
       $this->db->where("p.id_empresa",$id_empresa);
       $this->db->where("p.id_gestion",$id_gestion); 
       $this->db->like("p.proveedor", $txt_buscar); 
       $resultados = $this->db->get("proveedor p");

       return $resultados->result(); 
  }

	public function get_proveedors()
	{
       $this->db->select("*"); 
       $this->db->from("proveedor p");
       $resultados = $this->db->get();
       return $resultados->result();
	}

	public function Reg_proveedor($data)
	{
		   $this->db->insert("proveedor",$data);
	}

	public function update_proveedor($id_proveedor)
	{
       $this->db->select("*"); 
       $this->db->from("proveedor p");
       $this->db->where("p.id_proveedor",$id_proveedor);
       $result = $this->db->get();
       return $result->result();
	}

	public function Setting_proveedor($data,$id_proveedor)
	{
		   $this->db->where("id_proveedor",$id_proveedor);
       $this->db->update("proveedor",$data);
	}

	public function get_proveedor($id_proveedor)
	{
      $this->db->select("*"); 
       $this->db->from("proveedor p");
       $this->db->where("p.id_proveedor",$id_proveedor);
       $result = $this->db->get();
       return $result->result();
	}

  public function Delete_proveedor($id_proveedor)
	{
		   $this->db->where("id_proveedor", $id_proveedor);
		   $this->db->delete("proveedor"); 
	}


  /* SECCION DE LOS SELECTS DEL SISTEMA */
   
}

?>


