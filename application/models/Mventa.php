<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Mventa extends CI_Model
{
    public function num_rows($id_empresa,$id_gestion)
    {  
       $this->db->where("id_empresa",$id_empresa);
       $this->db->where("id_gestion",$id_gestion);
       return $this->db->count_all("venta");
    }

    public function num_rows_find($txt_buscar,$id_empresa,$id_gestion)
    {  
       $this->db->join("cliente cl", "cl.id_cliente = v.id_cliente");   
       $this->db->where("v.id_empresa",$id_empresa);
       $this->db->where("v.id_gestion",$id_gestion);
       $this->db->like("cl.cliente", $txt_buscar);
       $resultados= $this->db->count_all_results("venta v");
       return $resultados;

    }

    public function listar_paginacion($offset,$per_page,$id_empresa,$id_gestion)
    { 
       $this->db->order_by("v.id_venta", "DESC");
       $this->db->join("cliente cl", "cl.id_cliente = v.id_cliente");  
       $this->db->where("v.id_empresa",$id_empresa);
       $this->db->where("v.id_gestion",$id_gestion);

       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("venta v");

       return $resultados->result();  
    } 

    public function listar_paginacion_find($txt_buscar,$offset,$per_page,$id_empresa,$id_gestion)
    {
       $this->db->order_by("v.id_venta", "DESC");
       $this->db->join("cliente cl", "cl.id_cliente = v.id_cliente");   
       $this->db->where("v.id_empresa",$id_empresa);
       $this->db->where("v.id_gestion",$id_gestion);
       $this->db->like("cl.cliente", $txt_buscar);
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("venta v");

       return $resultados->result(); 
    }
 
  	public function Reg_venta($data)
  	{
  		   $this->db->insert("venta",$data);
  	}

    public function Reg_detalle_venta($data)
    {
         $this->db->insert("detalle_venta",$data);
    }

    public function get_ventas($codigo_venta)
    {
         $this->db->select("*");
         $this->db->join("detalle_venta dv", "dv.codigo_venta = v.codigo_venta"); 
         $this->db->join("producto p", "p.id_producto = dv.id_producto"); 
         $this->db->join("cliente cl", "cl.id_cliente = v.id_cliente"); 
         $this->db->where("v.codigo_venta",$codigo_venta);
         $this->db->from("venta v");
         $resultados = $this->db->get();
         return $resultados->result();
    }

    public function get_venta($id_venta)
    {
         $this->db->select("*");
         $this->db->join("detalle_venta dv", "dv.codigo_venta = v.codigo_venta"); 
         $this->db->join("producto p", "p.id_producto = dv.id_producto"); 
         $this->db->join("cliente cl", "cl.id_cliente = v.id_cliente"); 
         $this->db->where("v.id_venta",$id_venta);
         $this->db->from("venta v");
         $resultados = $this->db->get();
         return $resultados->result();
    }

    public function Delete_venta($codigo_venta)
    {

      $this->db->where("codigo_venta", $codigo_venta);
      $this->db->delete("detalle_venta"); 
 
      $this->db->where("codigo_venta", $codigo_venta);
      $this->db->delete("venta");     

    }

    public function get_venta_producto($id_producto)
    {
       $this->db->select("*");
       $this->db->from("detalle_venta dv");
       $this->db->where("dv.id_producto",$id_producto);
       $result = $this->db->get();
       return $result->result(); 
    }

    public function get_detalle_ventas($codigo_venta)
    {
       $this->db->select("*");
       $this->db->from("detalle_venta dv");
       $this->db->where("dv.codigo_venta",$codigo_venta);
       $result = $this->db->get();
       return $result->result(); 
    } 
 
    public function Delete_venta_lista($codigo_venta)
    {
         $this->db->where("codigo_venta", $codigo_venta);
         $this->db->delete("venta"); 
    }
 
    public function Delete_detalle_venta($codigo_venta)
    {
         $this->db->where("codigo_venta", $codigo_venta);
         $this->db->delete("detalle_venta");
    }


/*
    public function get_detalle_ventas()
    {
         $this->db->select("*");
         $this->db->join("producto p", "p.id_producto = d.id_producto"); 
         $this->db->from("detalle_venta d");
         $resultados = $this->db->get();
         return $resultados->result();
    }

  	public function update_detalle_venta($id_detalle_venta)
  	{
         $this->db->select("*");
         $this->db->join("producto p", "p.id_producto = d.id_producto"); 
         $this->db->from("detalle_venta d");
         $this->db->where("d.id_detalle_venta",$id_detalle_venta);
         $result = $this->db->get();
         return $result->result();

  	}

  	public function Setting_detalle_venta($data,$id_detalle_venta)
  	{
  		   $this->db->where("id_detalle_venta",$id_detalle_venta);
         $this->db->update("detalle_venta",$data);
  	}

  	public function get_detalle_venta($id_detalle_venta)
  	{
        $this->db->select("*");
         $this->db->join("producto p", "p.id_producto = d.id_producto"); 
         $this->db->from("detalle_venta d");
         $this->db->where("d.id_detalle_venta",$id_detalle_venta);
         $result = $this->db->get();
         return $result->result();
  	}

    public function Delete_detalle_venta($id_detalle_venta)
  	{
  		   $this->db->where("id_detalle_venta", $id_detalle_venta);
  		   $this->db->delete("detalle_venta"); 
  	}

*/
   

 
}

?>


