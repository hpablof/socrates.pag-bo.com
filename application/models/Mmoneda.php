<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Mmoneda extends CI_Model
{
    public function num_rows()
    {
       return $this->db->count_all("moneda");
    }

    public function num_rows_find($txt_buscar)
    {   
       $this->db->like("moneda", $txt_buscar);
       $resultados= $this->db->count_all_results("moneda");
       return $resultados;

    }

    public function listar_paginacion($offset,$per_page)
    { 
       $this->db->order_by("m.id_moneda", "DESC");  
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("moneda m");

       return $resultados->result();  
    } 

    public function listar_paginacion_find($txt_buscar,$offset,$per_page)
    {
       $this->db->order_by("m.id_moneda", "DESC");
       $this->db->like("m.moneda", $txt_buscar); 
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("moneda m");

       return $resultados->result(); 
    }
 
	public function get_monedas()
	{
       $this->db->select("*"); 
       $this->db->from("moneda m");
       $resultados = $this->db->get();
       return $resultados->result();
	}

	public function Reg_moneda($data)
	{
		   $this->db->insert("moneda",$data);
	}

	public function update_moneda($id_moneda)
	{
       $this->db->select("*"); 
       $this->db->from("moneda m");
       $this->db->where("m.id_moneda",$id_moneda);
       $result = $this->db->get();
       return $result->result();

	}

	public function Setting_moneda($data,$id_moneda)
	{
		   $this->db->where("id_moneda",$id_moneda);
       $this->db->update("moneda",$data);
	}

	public function get_moneda($id_moneda)
	{
      $this->db->select("*"); 
       $this->db->from("moneda m");
       $this->db->where("m.id_moneda",$id_moneda);
       $result = $this->db->get();
       return $result->result();
	}

  public function Delete_moneda($id_moneda)
	{
		   $this->db->where("id_moneda", $id_moneda);
		   $this->db->delete("moneda"); 
	}


  /* SECCION DE LOS SELECTS DEL SISTEMA */
   
}

?>


