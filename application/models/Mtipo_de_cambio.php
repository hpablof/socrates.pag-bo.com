<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Mtipo_de_cambio extends CI_Model
{
    public function num_rows()
    {
       return $this->db->count_all("tipo_de_cambio");
    }

    public function num_rows_find($txt_buscar)
    {   
       $this->db->like("fecha_tc", $txt_buscar);
       $resultados= $this->db->count_all_results("tipo_de_cambio");
       return $resultados;

    }

    public function listar_paginacion($offset,$per_page)
    { 
       $this->db->order_by("t.id_tipo_de_cambio", "DESC");  
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("tipo_de_cambio t");

       return $resultados->result();  
    } 

    public function listar_paginacion_find($txt_buscar,$offset,$per_page)
    {
       $this->db->order_by("t.id_tipo_de_cambio", "DESC");
       $this->db->like("t.fecha_tc", $txt_buscar); 
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("tipo_de_cambio t");

       return $resultados->result(); 
    }
 
	public function get_tipo_de_cambios()
	{
       $this->db->select("*"); 
       $this->db->from("tipo_de_cambio t");
       $resultados = $this->db->get();
       return $resultados->result();
	}

	public function Reg_tipo_de_cambio($data)
	{
		   $this->db->insert("tipo_de_cambio",$data);
	}

	public function update_tipo_de_cambio($id_tipo_de_cambio)
	{
       $this->db->select("*"); 
       $this->db->from("tipo_de_cambio t");
       $this->db->where("t.id_tipo_de_cambio",$id_tipo_de_cambio);
       $result = $this->db->get();
       return $result->result();

	}

	public function Setting_tipo_de_cambio($data,$id_tipo_de_cambio)
	{
		   $this->db->where("id_tipo_de_cambio",$id_tipo_de_cambio);
       $this->db->update("tipo_de_cambio",$data);
	}

	public function get_tipo_de_cambio($id_tipo_de_cambio)
	{
      $this->db->select("*"); 
       $this->db->from("tipo_de_cambio t");
       $this->db->where("t.id_tipo_de_cambio",$id_tipo_de_cambio);
       $result = $this->db->get();
       return $result->result();
	}

  public function Delete_tipo_de_cambio($id_tipo_de_cambio)
	{
		   $this->db->where("id_tipo_de_cambio", $id_tipo_de_cambio);
		   $this->db->delete("tipo_de_cambio"); 
	}


  /* SECCION DE LOS SELECTS DEL SISTEMA */
   
}

?>


