<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Majustes extends CI_Model
{
    public function num_rows()
    {
       return $this->db->count_all("ajustes");
    }

    public function num_rows_find($txt_buscar)
    {   
       $this->db->like("razon_social", $txt_buscar);
       $resultados= $this->db->count_all_results("ajustes");
       return $resultados;

    }

    public function listar_paginacion($offset,$per_page)
    { 
       $this->db->order_by("a.id_ajustes", "DESC");  
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("ajustes a");

       return $resultados->result();  
    } 

    public function listar_paginacion_find($txt_buscar,$offset,$per_page)
    {
       $this->db->order_by("a.id_ajustes", "DESC");
       $this->db->like("a.razon_social", $txt_buscar); 
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("ajustes a");

       return $resultados->result(); 
    }
 
	public function get_ajustess()
	{
       $this->db->select("*"); 
       $this->db->from("ajustes a");
       $resultados = $this->db->get();
       return $resultados->result();
	}

	public function Reg_ajustes($data)
	{
		   $this->db->insert("ajustes",$data);
	}

	public function update_ajustes($id_ajustes)
	{
       $this->db->select("*"); 
       $this->db->from("ajustes a");
       $this->db->where("a.id_ajustes",$id_ajustes);
       $result = $this->db->get();
       return $result->result();

	}

	public function Setting_ajustes($data,$id_ajustes)
	{
		   $this->db->where("id_ajustes",$id_ajustes);
       $this->db->update("ajustes",$data);
	}

	public function get_ajustes($id_ajustes)
	{
      $this->db->select("*"); 
       $this->db->from("ajustes a");
       $this->db->where("a.id_ajustes",$id_ajustes);
       $result = $this->db->get();
       return $result->result();
	}

  public function Delete_ajustes($id_ajustes)
	{
		   $this->db->where("id_ajustes", $id_ajustes);
		   $this->db->delete("ajustes"); 
	}


  /* SECCION DE LOS SELECTS DEL SISTEMA */
   
}

?>


