<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Mempresa extends CI_Model
{
    public function num_rows()
    {
       return $this->db->count_all("empresa");
    }

    public function num_rows_find($txt_buscar)
    {   
       $this->db->like("logo", $txt_buscar);
       $resultados= $this->db->count_all_results("empresa");
       return $resultados;

    }

    public function listar_paginacion($offset,$per_page)
    { 
       $this->db->order_by("e.id_empresa", "DESC");
       $this->db->join("rubro r", "r.id_rubro = e.id_rubro");
       $this->db->join("departamento d", "d.id_departamento = e.id_departamento");  
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("empresa e");

       return $resultados->result();  
    } 

    public function listar_paginacion_find($txt_buscar,$offset,$per_page)
    {
       $this->db->order_by("e.id_empresa", "DESC");
       $this->db->join("rubro r", "r.id_rubro = e.id_rubro");
       $this->db->join("departamento d", "d.id_departamento = e.id_departamento");
       $this->db->like("e.logo", $txt_buscar); 
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("empresa e");

       return $resultados->result(); 
    }
 
	public function get_empresas()
	{
       $this->db->select("*");
       $this->db->join("rubro r", "r.id_rubro = e.id_rubro");
       $this->db->join("departamento d", "d.id_departamento = e.id_departamento"); 
       $this->db->from("empresa e");
       $resultados = $this->db->get();
       return $resultados->result();
	}

	public function Reg_empresa($data)
	{
		   $this->db->insert("empresa",$data);
	}

	public function update_empresa($id_empresa)
	{
       $this->db->select("*");
       $this->db->join("rubro r", "r.id_rubro = e.id_rubro");
       $this->db->join("departamento d", "d.id_departamento = e.id_departamento"); 
       $this->db->from("empresa e");
       $this->db->where("e.id_empresa",$id_empresa);
       $result = $this->db->get();
       return $result->result();

	}

	public function Setting_empresa($data,$id_empresa)
	{
		   $this->db->where("id_empresa",$id_empresa);
       $this->db->update("empresa",$data);
	}

	public function get_empresa($id_empresa)
	{
      $this->db->select("*");
       $this->db->join("rubro r", "r.id_rubro = e.id_rubro");
       $this->db->join("departamento d", "d.id_departamento = e.id_departamento"); 
       $this->db->from("empresa e");
       $this->db->where("e.id_empresa",$id_empresa);
       $result = $this->db->get();
       return $result->result();
	}

  public function Delete_empresa($id_empresa)
	{
		   $this->db->where("id_empresa", $id_empresa);
		   $this->db->delete("empresa"); 
	}


  /* SECCION DE LOS SELECTS DEL SISTEMA */
   
  public function get_rubros()
  {
      $this->db->select("*");
      $this->db->from("rubro");
      $result = $this->db->get();
      return $result->result();
  }

 
  public function get_departamentos()
  {
      $this->db->select("*");
      $this->db->from("departamento");
      $result = $this->db->get();
      return $result->result();
  }

 
}

?>


