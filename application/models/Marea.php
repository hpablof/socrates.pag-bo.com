<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Marea extends CI_Model
{
    public function num_rows($id_empresa)
    {
       $this->db->where("id_empresa",$id_empresa);
       return $this->db->count_all("area");
    }

    public function num_rows_find($txt_buscar,$id_empresa)
    {  
       $this->db->where("id_empresa",$id_empresa);     
       $this->db->like("area", $txt_buscar);
       $resultados= $this->db->count_all_results("area");
       return $resultados;

    }

    public function listar_paginacion($offset,$per_page,$id_empresa)
    { 
       $this->db->order_by("a.id_area", "DESC");
       $this->db->where("a.id_empresa",$id_empresa);   
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("area a");
       return $resultados->result();  
    } 

    public function listar_paginacion_find($txt_buscar,$offset,$per_page,$id_empresa)
    {
       $this->db->order_by("a.id_area", "DESC");
       $this->db->where("a.id_empresa",$id_empresa);
       $this->db->like("a.area", $txt_buscar); 
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("area a");

       return $resultados->result(); 
    }
 
  	public function get_areas()
  	{
         $this->db->select("*"); 
         $this->db->from("area a");
         $resultados = $this->db->get();
         return $resultados->result();
  	}

  	public function Reg_area($data)
  	{
  		   $this->db->insert("area",$data);
  	}

  	public function update_area($id_area)
  	{
         $this->db->select("*"); 
         $this->db->from("area a");
         $this->db->where("a.id_area",$id_area);
         $result = $this->db->get();
         return $result->result();

  	}

  	public function Setting_area($data,$id_area)
  	{
  		   $this->db->where("id_area",$id_area);
         $this->db->update("area",$data);
  	}

  	public function get_area($id_area)
  	{
         $this->db->select("*"); 
         $this->db->from("area a");
         $this->db->where("a.id_area",$id_area);
         $result = $this->db->get();
         return $result->result();
  	}

    public function Delete_area($id_area)
  	{
  		   $this->db->where("id_area", $id_area);
  		   $this->db->delete("area"); 
  	}


  /* SECCION DE LOS SELECTS DEL SISTEMA */
   
}

?>


