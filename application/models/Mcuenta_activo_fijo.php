<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Mcuenta_activo_fijo extends CI_Model
{
    public function num_rows()
    {
       return $this->db->count_all("cuenta_activo_fijo");
    }

    public function num_rows_find($txt_buscar)
    {   
       $this->db->like("codigo_caf", $txt_buscar);
       $resultados= $this->db->count_all_results("cuenta_activo_fijo");
       return $resultados;

    }

    public function listar_paginacion($offset,$per_page)
    { 
       $this->db->order_by("c.id_cuenta_activo_fijo", "DESC");  
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("cuenta_activo_fijo c");

       return $resultados->result();  
    } 

    public function listar_paginacion_find($txt_buscar,$offset,$per_page)
    {
       $this->db->order_by("c.id_cuenta_activo_fijo", "DESC");
       $this->db->like("c.codigo_caf", $txt_buscar); 
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("cuenta_activo_fijo c");

       return $resultados->result(); 
    }
    
    
    public function buscar_activos_fijos($txt_buscar)
    {
      // $this->db->order_by("c.id_cuenta_activo_fijo", "DESC");
        
       $this->db->like("c.codigo_caf", $txt_buscar); 
       $this->db->or_like("c.cuenta_caf", $txt_buscar); 
       $this->db->or_like("c.codigo_cda", $txt_buscar); 
       $this->db->or_like("c.cuenta_cda", $txt_buscar); 
       $this->db->or_like("c.codigo_cdd", $txt_buscar); 
       $this->db->or_like("c.cuenta_cdd", $txt_buscar); 
       
       $resultados = $this->db->get("cuenta_activo_fijo c");

       return $resultados->result(); 
    }   
 
	public function get_cuenta_activo_fijos()
	{
       $this->db->select("*"); 
       $this->db->from("cuenta_activo_fijo c");
       $resultados = $this->db->get();
       return $resultados->result();
	}

	public function Reg_cuenta_activo_fijo($data)
	{
		   $this->db->insert("cuenta_activo_fijo",$data);
	}

	public function update_cuenta_activo_fijo($id_cuenta_activo_fijo)
	{
       $this->db->select("*"); 
       $this->db->from("cuenta_activo_fijo c");
       $this->db->where("c.id_cuenta_activo_fijo",$id_cuenta_activo_fijo);
       $result = $this->db->get();
       return $result->result();

	}

	public function Setting_cuenta_activo_fijo($data,$id_cuenta_activo_fijo)
	{
		   $this->db->where("id_cuenta_activo_fijo",$id_cuenta_activo_fijo);
       $this->db->update("cuenta_activo_fijo",$data);
	}

	public function get_cuenta_activo_fijo($id_cuenta_activo_fijo)
	{
      $this->db->select("*"); 
       $this->db->from("cuenta_activo_fijo c");
       $this->db->where("c.id_cuenta_activo_fijo",$id_cuenta_activo_fijo);
       $result = $this->db->get();
       return $result->result();
	}

  public function Delete_cuenta_activo_fijo($id_cuenta_activo_fijo)
	{
		   $this->db->where("id_cuenta_activo_fijo", $id_cuenta_activo_fijo);
		   $this->db->delete("cuenta_activo_fijo"); 
	}


  /* SECCION DE LOS SELECTS DEL SISTEMA */
   
}

?>


