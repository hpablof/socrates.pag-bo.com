<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Mrubro extends CI_Model
{
    public function num_rows()
    {
       return $this->db->count_all("rubro");
    }

    public function num_rows_find($txt_buscar)
    {   
       $this->db->like("rubro", $txt_buscar);
       $resultados= $this->db->count_all_results("rubro");
       return $resultados;

    }

    public function listar_paginacion($offset,$per_page)
    { 
       $this->db->order_by("r.id_rubro", "DESC");  
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("rubro r");

       return $resultados->result();  
    } 

    public function listar_paginacion_find($txt_buscar,$offset,$per_page)
    {
       $this->db->order_by("r.id_rubro", "DESC");
       $this->db->like("r.rubro", $txt_buscar); 
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("rubro r");

       return $resultados->result(); 
    }
 
	public function get_rubros()
	{
       $this->db->select("*"); 
       $this->db->from("rubro r");
       $resultados = $this->db->get();
       return $resultados->result();
	}

	public function Reg_rubro($data)
	{
		   $this->db->insert("rubro",$data);
	}

	public function update_rubro($id_rubro)
	{
       $this->db->select("*"); 
       $this->db->from("rubro r");
       $this->db->where("r.id_rubro",$id_rubro);
       $result = $this->db->get();
       return $result->result();

	}

	public function Setting_rubro($data,$id_rubro)
	{
		   $this->db->where("id_rubro",$id_rubro);
       $this->db->update("rubro",$data);
	}

	public function get_rubro($id_rubro)
	{
      $this->db->select("*"); 
       $this->db->from("rubro r");
       $this->db->where("r.id_rubro",$id_rubro);
       $result = $this->db->get();
       return $result->result();
	}

  public function Delete_rubro($id_rubro)
	{
		   $this->db->where("id_rubro", $id_rubro);
		   $this->db->delete("rubro"); 
	}


  /* SECCION DE LOS SELECTS DEL SISTEMA */
   
}

?>


