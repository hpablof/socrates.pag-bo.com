<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Mrazon_social extends CI_Model
{
    public function num_rows()
    {
       return $this->db->count_all("razon_social");
    }

    public function num_rows_find($txt_buscar)
    {   
       $this->db->like("nit", $txt_buscar);
       $resultados= $this->db->count_all_results("razon_social");
       return $resultados;
    }

    public function listar_paginacion($offset,$per_page)
    { 
       $this->db->order_by("r.id_razon_social", "DESC");  
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("razon_social r");

       return $resultados->result();  
    } 

    public function listar_paginacion_find($txt_buscar,$offset,$per_page)
    {
       $this->db->order_by("r.id_razon_social", "DESC");
       $this->db->like("r.nit", $txt_buscar); 
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("razon_social r");

       return $resultados->result(); 
    }
 
	public function get_razon_socials()
	{
       $this->db->select("*"); 
       $this->db->from("razon_social r");
       $resultados = $this->db->get();
       return $resultados->result();
	}

	public function Reg_razon_social($data)
	{
		   $this->db->insert("razon_social",$data);
	}

	public function update_razon_social($id_razon_social)
	{
       $this->db->select("*"); 
       $this->db->from("razon_social r");
       $this->db->where("r.id_razon_social",$id_razon_social);
       $result = $this->db->get();
       return $result->result();

	}

	public function Setting_razon_social($data,$id_razon_social)
	{
		   $this->db->where("id_razon_social",$id_razon_social);
       $this->db->update("razon_social",$data);
	}

	public function get_razon_social($id_razon_social)
	{
       $this->db->select("*"); 
       $this->db->from("razon_social r");
       $this->db->where("r.id_razon_social",$id_razon_social);
       $result = $this->db->get();
       return $result->result();
	}

  public function Delete_razon_social($id_razon_social)
	{
		   $this->db->where("id_razon_social", $id_razon_social);
		   $this->db->delete("razon_social"); 
	}

  public function getVerNit($nit)
  {
       $this->db->like("r.nit", $nit); 
       $resultados = $this->db->get("razon_social r");
       return $resultados->result(); 
  }


  /* SECCION DE LOS SELECTS DEL SISTEMA */
   
}

?>


