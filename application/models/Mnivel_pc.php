<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Mnivel_pc extends CI_Model
{
  public function num_rows()
  {
     return $this->db->count_all("nivel_pc");
  }

  public function num_rows_find($txt_buscar)
  {   
     $this->db->like("nivel_pc", $txt_buscar);
     $resultados= $this->db->count_all_results("nivel_pc");
     return $resultados;

  }

  public function listar_paginacion($offset,$per_page)
  { 
     $this->db->order_by("n.id_nivel_pc", "DESC");  
     $this->db->limit($per_page, $offset);
     $resultados = $this->db->get("nivel_pc n");

     return $resultados->result();  
  } 

  public function listar_paginacion_find($txt_buscar,$offset,$per_page)
  {
     $this->db->order_by("n.id_nivel_pc", "DESC");
     $this->db->like("n.nivel_pc", $txt_buscar); 
     $this->db->limit($per_page, $offset);
     $resultados = $this->db->get("nivel_pc n");

     return $resultados->result(); 
  }
 
	public function get_nivel_pcs()
	{
       $this->db->select("*"); 
       $this->db->from("nivel_pc");
       $resultados = $this->db->get();
       return $resultados->result();
	}

	public function Reg_nivel_pc($data)
	{
		   $this->db->insert("nivel_pc",$data);
	}

	public function update_nivel_pc($id_nivel_pc)
	{
       $this->db->select("*"); 
       $this->db->from("nivel_pc n");
       $this->db->where("n.id_nivel_pc",$id_nivel_pc);
       $result = $this->db->get();
       return $result->result();
	}

	public function Setting_nivel_pc($data,$id_nivel_pc)
	{
		   $this->db->where("id_nivel_pc",$id_nivel_pc);
       $this->db->update("nivel_pc",$data);
	}

	public function get_nivel_pc($id_nivel_pc)
	{
       $this->db->select("*"); 
       $this->db->from("nivel_pc n");
       $this->db->where("n.id_nivel_pc",$id_nivel_pc);
       $result = $this->db->get();
       return $result->result();
	}

  public function Delete_nivel_pc($id_nivel_pc)
	{
		   $this->db->where("id_nivel_pc", $id_nivel_pc);
		   $this->db->delete("nivel_pc"); 
	}


  /* SECCION DE LOS SELECTS DEL SISTEMA */
   
}

?>


