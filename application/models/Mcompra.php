<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Mcompra extends CI_Model
{
    public function num_rows($id_empresa,$id_gestion)
    {  
       $this->db->where("id_empresa",$id_empresa);
       $this->db->where("id_gestion",$id_gestion); 
       return $this->db->count_all("compra");
    }

    public function num_rows_find($txt_buscar,$id_empresa,$id_gestion)
    {   
       $this->db->join("proveedor pr", "pr.id_proveedor = c.id_proveedor"); 
       $this->db->where("c.id_empresa",$id_empresa);
       $this->db->where("c.id_gestion",$id_gestion);
       $this->db->like("pr.proveedor", $txt_buscar);
       $resultados= $this->db->count_all_results("compra c");
       return $resultados;
    }

    public function listar_paginacion($offset,$per_page,$id_empresa,$id_gestion)
    { 
       $this->db->order_by("c.id_compra", "DESC"); 
       $this->db->join("proveedor pr", "pr.id_proveedor = c.id_proveedor");       
       $this->db->where("c.id_empresa",$id_empresa);
       $this->db->where("c.id_gestion",$id_gestion);
       
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("compra c");

       return $resultados->result();  
    } 

    public function listar_paginacion_find($txt_buscar,$offset,$per_page,$id_empresa,$id_gestion)
    {
       $this->db->order_by("c.id_compra", "DESC");
       $this->db->join("proveedor pr", "pr.id_proveedor = c.id_proveedor");
       $this->db->where("c.id_empresa",$id_empresa);
       $this->db->where("c.id_gestion",$id_gestion);
       $this->db->like("pr.proveedor", $txt_buscar); 
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("compra c");

       return $resultados->result(); 
    }
 
    public function Reg_compra($data)
    {
       $this->db->insert("compra",$data);
    }

  	public function Reg_detalle_compra($data)
  	{
  		 $this->db->insert("detalle_compra",$data);
  	}

    public function get_compra($id_compra)
    {
       $this->db->select("*");
       $this->db->join("proveedor p", "p.id_proveedor = c.id_proveedor"); 
       $this->db->join("detalle_compra dc", "dc.codigo_compra = c.codigo_compra");
       $this->db->join("producto pr", "pr.id_producto = dc.id_producto");  
       $this->db->from("compra c");
       $this->db->where("c.id_compra",$id_compra);
       $result = $this->db->get();
       return $result->result();
    }

    function get_detalle_compra($codigo_compra)
    {
       $this->db->select("*");
       $this->db->join("producto pr", "pr.id_producto = dc.id_producto");  
       $this->db->from("detalle_compra dc");
       $this->db->where("dc.codigo_compra",$codigo_compra);
       $result = $this->db->get();
       return $result->result();     
    }

    function Delete_compra($codigo_compra)
    {
       $this->db->where("codigo_compra", $codigo_compra);
       $this->db->delete("compra"); 
    }

    function Delete_detalle_compra($codigo_compra)
    {
       $this->db->where("codigo_compra", $codigo_compra);
       $this->db->delete("detalle_compra"); 
    }

    function get_compra_producto($id_producto)
    {
       $this->db->select("*");
       $this->db->from("detalle_compra dc");
       $this->db->where("dc.id_producto",$id_producto);
       $result = $this->db->get();
       return $result->result();          
    }



/*
  public function get_detalle_compra()
  {
       $this->db->select("*");
       $this->db->join("producto p", "p.id_producto = d.id_producto"); 
       $this->db->from("detalle_compra d");
       $resultados = $this->db->get();
       return $resultados->result();
  }

    public function update_detalle_compra($id_detalle_compra)
  {
       $this->db->select("*");
       $this->db->join("producto p", "p.id_producto = d.id_producto"); 
       $this->db->from("detalle_compra d");
       $this->db->where("d.id_detalle_compra",$id_detalle_compra);
       $result = $this->db->get();
       return $result->result();

  }

  public function Setting_detalle_compra($data,$id_detalle_compra)
  {
       $this->db->where("id_detalle_compra",$id_detalle_compra);
       $this->db->update("detalle_compra",$data);
  }

  public function get_detalle_compra($id_detalle_compra)
  {
       $this->db->select("*");
       $this->db->join("producto p", "p.id_producto = d.id_producto"); 
       $this->db->from("detalle_compra d");
       $this->db->where("d.id_detalle_compra",$id_detalle_compra);
       $result = $this->db->get();
       return $result->result();
  }

  public function Delete_detalle_compra($id_detalle_compra)
  {
       $this->db->where("id_detalle_compra", $id_detalle_compra);
       $this->db->delete("detalle_compra"); 
  }
 */
}

?>


