<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Mcurso extends CI_Model
{
    public function num_rows()
    {
       return $this->db->count_all("curso");
    }

    public function num_rows_find($txt_buscar)
    {   
       $this->db->like("portada_curso", $txt_buscar);
       $resultados= $this->db->count_all_results("curso");
       return $resultados;

    }

    public function listar_paginacion($offset,$per_page)
    { 
       $this->db->order_by("c.id_curso", "DESC");  
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("curso c");

       return $resultados->result();  
    } 

    public function listar_paginacion_find($txt_buscar,$offset,$per_page)
    {
       $this->db->order_by("c.id_curso", "DESC");
       $this->db->like("c.portada_curso", $txt_buscar); 
       $this->db->limit($per_page, $offset);
       $resultados = $this->db->get("curso c");

       return $resultados->result(); 
    }
 
	public function get_cursos()
	{
       $this->db->select("*"); 
       $this->db->from("curso c");
       $resultados = $this->db->get();
       return $resultados->result();
	}

	public function Reg_curso($data)
	{
		$this->db->insert("curso",$data);
	}

	public function update_curso($id_curso)
	{
       $this->db->select("*"); 
       $this->db->from("curso c");
       $this->db->where("c.id_curso",$id_curso);
       $result = $this->db->get();
       return $result->result();
	}

	public function Setting_curso($data,$id_curso)
	{
	   $this->db->where("id_curso",$id_curso);
       $this->db->update("curso",$data);
	}

	public function get_curso($id_curso)
	{
       $this->db->select("*"); 
       $this->db->from("curso c");
       $this->db->where("c.id_curso",$id_curso);
       $result = $this->db->get();
       return $result->result();
	}
	
	
	public function get_cursos_activos()
	{
       $this->db->select("*"); 
       $this->db->from("curso c");
       //$this->db->where("c.estado_curso",1);
       $result = $this->db->get();
       return $result->result();
	}	

    public function Delete_curso($id_curso)
	{
	   $this->db->where("id_curso", $id_curso);
	   $this->db->delete("curso"); 
	}


  /* SECCION DE LOS SELECTS DEL SISTEMA */
   
}

?>


