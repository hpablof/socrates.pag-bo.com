
<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Cdepartamento extends CI_Controller 
{   
	public function __construct()
    {
        parent::__construct();
        $this->load->model("Mdepartamento");
    }

    public function index()
    {    
        $page = 1;
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mdepartamento->num_rows(); 
        $total_pages = ceil($numrows/$per_page);
      
        $data = array(
            "reload" => "<?php echo base_url(); ?>Cdepartamento/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "departamentos" => $this->Mdepartamento->listar_paginacion($offset,$per_page)
            );
        
        $this->load->view("assets/Header");  
        $this->load->view("departamento/VML_departamento",$data);
        $this->load->view("assets/Footer"); 

    } 

    public function paginacion()
    {   
        $page = $_POST["page"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mdepartamento->num_rows();

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "reload" => "<?php echo base_url(); ?>Cdepartamento/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "departamentos" => $this->Mdepartamento->listar_paginacion($offset,$per_page)
            );
 
        $this->load->view("departamento/VL_departamento",$data); 
    

    }

    public function paginacion_find()
    {
        $page = $_POST["page"];
        $txt_buscar = $_POST["txt_buscar"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mdepartamento->num_rows_find($txt_buscar);

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "txt_buscar" => $txt_buscar,
            "reload" => "<?php echo base_url(); ?>departamento/paginacion_find",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "departamentos" => $this->Mdepartamento->listar_paginacion_find($txt_buscar,$offset,$per_page)
            );

         $this->load->view("departamento/VLB_departamento",$data);   
    }
 
    public function R_departamento()
    {
          $this->load->view("departamento/VR_departamento");

		  
    } 

    public function G_departamento()
    {
         $id_usuario = trim($_POST["id_usuario"]); 
         $fecha = date("Y-m-d");
         $hora = date("H:i:s");
      
         $departamentos =  json_decode($_POST["lista"]);

      foreach($departamentos as $departamento)
      { 
        $departamento = $departamento->departamento; 
        
        $data = array( 
        "departamento" => $departamento, 
        "fecha" => $fecha, 
        "hora" => $hora, 
        "id_usuario" => $id_usuario
            ); 

      $this->Mdepartamento->Reg_departamento($data);
 
      }

     
      
      ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script>
           
       setTimeout(function(){ $("#panel_modal_resultado_registrar").html(""); },1000);
       setTimeout(function(){ $("#myModal_Register").modal("hide"); },2000);
       setTimeout(function(){ location.reload(); },3000);

      </script>
      
      <?php

    } 

    public function VD_departamento()
    {
    	$id_departamento = trim($_POST["id_departamento"]);
    	$data = array("departamentos" => $this->Mdepartamento->get_departamento($id_departamento));
        
		  $this->load->view("departamento/VD_departamento",$data); 
    }

    public function VE_departamento()
    {
    	$id_departamento = trim($_POST["id_departamento"]);
    	$data = array("departamentos" => $this->Mdepartamento->get_departamento($id_departamento),);
        
		$this->load->view("departamento/VE_departamento",$data); 
    }

    public function VU_departamento()
    {
            $cont_up=0;
            $id_departamento = trim($_POST["id_departamento"]);
            $departamento = trim($_POST["departamento"]);

            if($departamento!="")
            {
                $data = array("departamento" => $departamento); 
                $this->Mdepartamento->Setting_departamento($data,$id_departamento);
                $cont_up++;
            }
    

            if($cont_up>0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡Edicion Correcta!</strong></h4> 
                Informacion Cambiada Correctamente
             
              </div>

              <script>
                setTimeout(function(){ $("#panel_modal_respuesta_editar").html(""); },1000);
                setTimeout(function(){ $("#myModal_Update").modal("hide"); },2000);
                setTimeout(function(){ location.reload(); },3000); 
              </script>

              <?php 
            }

            if($cont_up==0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
                Informacion sin cambios en la base de datos
             
              </div>
              <?php      
            }
  
 
    }

    public function VB_departamento()
    {
    	$id_departamento = trim($_POST["id_departamento"]);

    	$this->Mdepartamento-> Delete_departamento($id_departamento);

      ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Eliminación Correcta!</strong></h4> 
          Informacion Borrada Definitivamente
       
        </div>
      <?php    
    }

} 

?>
