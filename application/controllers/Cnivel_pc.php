
<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Cnivel_pc extends CI_Controller 
{   
	public function __construct()
    {
        parent::__construct();
        $this->load->model("Mnivel_pc");
    }

    public function index()
    {    
        $page = 1;
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mnivel_pc->num_rows(); 
        $total_pages = ceil($numrows/$per_page);
      
        $data = array(
            "reload" => "<?php echo base_url(); ?>Cnivel_pc/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "nivel_pcs" => $this->Mnivel_pc->listar_paginacion($offset,$per_page)
            );
 
        $this->load->view("nivel_pc/VML_nivel_pc",$data);
    } 

    public function paginacion()
    {   
        $page = $_POST["page"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mnivel_pc->num_rows();

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "reload" => "<?php echo base_url(); ?>Cnivel_pc/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "nivel_pcs" => $this->Mnivel_pc->listar_paginacion($offset,$per_page)
            );
 
        $this->load->view("nivel_pc/VL_nivel_pc",$data); 
    }

    public function paginacion_find()
    {
        $page = $_POST["page"];
        $txt_buscar = $_POST["txt_buscar"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mnivel_pc->num_rows_find($txt_buscar);

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "txt_buscar" => $txt_buscar,
            "reload" => "<?php echo base_url(); ?>nivel_pc/paginacion_find",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "nivel_pcs" => $this->Mnivel_pc->listar_paginacion_find($txt_buscar,$offset,$per_page)
            );

         $this->load->view("nivel_pc/VLB_nivel_pc",$data);   
    }
 
    public function R_nivel_pc()
    {
       $this->load->view("nivel_pc/VR_nivel_pc");
    } 

    public function G_nivel_pc()
    {
         $id_usuario = trim($_POST["id_usuario"]); 
         $fecha = date("Y-m-d");
         $hora = date("H:i:s");
      
         $nivel_pcs =  json_decode($_POST["lista"]);

         foreach($nivel_pcs as $n_pc)
         { 
            $nivel_pc = $n_pc->nivel_pc; 
            $nombre_pc = $n_pc->nombre_pc;
 
            $data = array( 
            "nivel_pc" => $nivel_pc, 
            "nombre_pc" => $nombre_pc,
            "fecha" => $fecha, 
            "hora" => $hora, 
            "id_usuario" => $id_usuario
             ); 

            $this->Mnivel_pc->Reg_nivel_pc($data);
 
          }
 
          ?>
          <p class="transparent"> espacio </p>
          <div class="alert alert-info alert-dismissable">
           
           <button type="button" class="close">&times;</button>

            <h4><strong>¡Registro Correcto!</strong></h4> 
            Informacion Almacenada Correctamente
     
          </div>

          <script>
           setTimeout(function(){ $("#panel_modal_resultado_registrar_nivel_pc").html(""); },1000);
           setTimeout(function(){ $("#myModal_Register_nivel_pc").modal("hide"); },2000);
           setTimeout(function(){ cargar_datos_nivel_pc(1); },3000);
          </script>
      
      <?php

    } 

    public function VD_nivel_pc()
    {
      $id_nivel_pc = trim($_POST["id_nivel_pc"]);
    	$data = array("nivel_pcs" => $this->Mnivel_pc->get_nivel_pc($id_nivel_pc));
        
		  $this->load->view("nivel_pc/VD_nivel_pc",$data);
    }

    public function VE_nivel_pc()
    {
    	$id_nivel_pc = trim($_POST["id_nivel_pc"]);
    	$data = array("nivel_pcs" => $this->Mnivel_pc->get_nivel_pc($id_nivel_pc),);
        
		$this->load->view("nivel_pc/VE_nivel_pc",$data); 
    }

    public function VU_nivel_pc()
    {
            $cont_up=0;
            $id_nivel_pc = trim($_POST["id_nivel_pc"]);
            $nivel_pc = trim($_POST["nivel_pc"]);
            $nombre_pc = trim($_POST["nombre_pc"]);

            if($nivel_pc!="")
            {
                $data = array("nivel_pc" => $nivel_pc); 
                $this->Mnivel_pc->Setting_nivel_pc($data,$id_nivel_pc);
                $cont_up++;
            }

            if($nombre_pc!="")
            {
                $data = array("nombre_pc" => $nombre_pc); 
                $this->Mnivel_pc->Setting_nivel_pc($data,$id_nivel_pc);
                $cont_up++;
            }   

            if($cont_up>0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡Edicion Correcta!</strong></h4> 
                Informacion Cambiada Correctamente
             
              </div>

              <script>
                setTimeout(function(){ $("#panel_modal_respuesta_editar_nivel_pc").html(""); },1000);
                setTimeout(function(){ $("#myModal_Update_nivel_pc").modal("hide"); },2000);
                setTimeout(function(){ btn_menu_nivel_pc(); },3000); 
              </script>

              <?php 
            }

            if($cont_up==0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
                Informacion sin cambios en la base de datos
             
              </div>
              <?php      
            }
  
 
    }

    public function VB_nivel_pc()
    {
    	$id_nivel_pc = trim($_POST["id_nivel_pc"]);

    	$this->Mnivel_pc-> Delete_nivel_pc($id_nivel_pc);

      ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Eliminación Correcta!</strong></h4> 
          Informacion Borrada Definitivamente
       
        </div>
      <?php    
    }

} 

?>
