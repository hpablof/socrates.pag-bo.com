
<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Cajustes extends CI_Controller 
{   
	public function __construct()
    {
        parent::__construct();
        $this->load->model("Majustes");
    }

    public function index()
    {    
        $page = 1;
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Majustes->num_rows(); 
        $total_pages = ceil($numrows/$per_page);
      
        $data = array(
            "reload" => "<?php echo base_url(); ?>Cajustes/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "ajustess" => $this->Majustes->listar_paginacion($offset,$per_page)
            );
        
        $this->load->view("assets/Header");  
        $this->load->view("ajustes/VML_ajustes",$data);
        $this->load->view("assets/Footer"); 

    } 

    public function paginacion()
    {   
        $page = $_POST["page"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Majustes->num_rows();

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "reload" => "<?php echo base_url(); ?>Cajustes/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "ajustess" => $this->Majustes->listar_paginacion($offset,$per_page)
            );
 
        $this->load->view("ajustes/VL_ajustes",$data); 
    

    }

    public function paginacion_find()
    {
        $page = $_POST["page"];
        $txt_buscar = $_POST["txt_buscar"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Majustes->num_rows_find($txt_buscar);

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "txt_buscar" => $txt_buscar,
            "reload" => "<?php echo base_url(); ?>ajustes/paginacion_find",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "ajustess" => $this->Majustes->listar_paginacion_find($txt_buscar,$offset,$per_page)
            );

         $this->load->view("ajustes/VLB_ajustes",$data);   
    }
 
    public function R_ajustes()
    {
          $this->load->view("ajustes/VR_ajustes");

		  
    } 

    public function G_ajustes()
    {
          $razon_social = trim($_POST["razon_social"]);
          $descripcion = trim($_POST["descripcion"]);
          $celular = trim($_POST["celular"]);
          $whatsapp = trim($_POST["whatsapp"]);
          $direccion = trim($_POST["direccion"]);
          
          $portada = $_FILES["portada"]["name"];
          $temp = $_FILES["portada"]["tmp_name"];
          move_uploaded_file($temp, "assets/multimedia/portadas/".$portada);

          
          $id_usuario = trim($_POST["id_usuario"]); 
          $fecha = date("Y-m-d");
          $hora = date("H:i:s");

      
        
          $data = array( 
          "razon_social" => $razon_social, 
          "descripcion" => $descripcion, 
          "celular" => $celular, 
          "whatsapp" => $whatsapp, 
          "direccion" => $direccion, 
          "portada" => $portada, 
          "fecha" => $fecha, 
          "hora" => $hora, 
          "id_usuario" => $id_usuario
            ); 

      $this->Majustes->Reg_ajustes($data);
      
      ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script>
           
       setTimeout(function(){ $("#panel_modal_resultado_registrar").html(""); },1000);
       setTimeout(function(){ $("#myModal_Register").modal("hide"); },2000);
       setTimeout(function(){ location.reload(); },3000);

      </script>
      
      <?php

    } 

    public function VD_ajustes()
    {
    	$id_ajustes = trim($_POST["id_ajustes"]);
    	$data = array("ajustess" => $this->Majustes->get_ajustes($id_ajustes));
        
		  $this->load->view("ajustes/VD_ajustes",$data); 
    }

    public function VE_ajustes()
    {
    	$id_ajustes = trim($_POST["id_ajustes"]);
    	$data = array("ajustess" => $this->Majustes->get_ajustes($id_ajustes),);
        
		$this->load->view("ajustes/VE_ajustes",$data); 
    }

    public function VU_ajustes()
    {
            $cont_up=0;
            $id_ajustes = trim($_POST["id_ajustes"]);
            $razon_social = trim($_POST["razon_social"]);
            $descripcion = trim($_POST["descripcion"]);
            $celular = trim($_POST["celular"]);
            $whatsapp = trim($_POST["whatsapp"]);
            $direccion = trim($_POST["direccion"]);

            $portada = trim($_FILES["portada"]["name"]); 
            $temp = $_FILES["portada"]["tmp_name"];
            move_uploaded_file($temp, "assets/multimedia/portadas/".$portada);

            

            if($razon_social!="")
            {
                $data = array("razon_social" => $razon_social); 
                $this->Majustes->Setting_ajustes($data,$id_ajustes);
                $cont_up++;
            }
    

            if($descripcion!="")
            {
                $data = array("descripcion" => $descripcion); 
                $this->Majustes->Setting_ajustes($data,$id_ajustes);
                $cont_up++;
            }
    

            if($celular!="")
            {
                $data = array("celular" => $celular); 
                $this->Majustes->Setting_ajustes($data,$id_ajustes);
                $cont_up++;
            }
    

            if($whatsapp!="")
            {
                $data = array("whatsapp" => $whatsapp); 
                $this->Majustes->Setting_ajustes($data,$id_ajustes);
                $cont_up++;
            }
    

            if($direccion!="")
            {
                $data = array("direccion" => $direccion); 
                $this->Majustes->Setting_ajustes($data,$id_ajustes);
                $cont_up++;
            }
    

            if($portada!="")
            {
                $data = array("portada" => $portada); 
                $this->Majustes->Setting_ajustes($data,$id_ajustes);
                $cont_up++;
            }
    

            if($cont_up>0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡Edicion Correcta!</strong></h4> 
                Informacion Cambiada Correctamente
             
              </div>

              <script>
                setTimeout(function(){ $("#panel_modal_respuesta_editar").html(""); },1000);
                setTimeout(function(){ $("#myModal_Update").modal("hide"); },2000);
                setTimeout(function(){ location.reload(); },3000); 
              </script>

              <?php 
            }

            if($cont_up==0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
                Informacion sin cambios en la base de datos
             
              </div>
              <?php      
            }
  
 
    }

    public function VB_ajustes()
    {
    	$id_ajustes = trim($_POST["id_ajustes"]);

    	$this->Majustes-> Delete_ajustes($id_ajustes);

      ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Eliminación Correcta!</strong></h4> 
          Informacion Borrada Definitivamente
       
        </div>
      <?php    
    }

} 

?>
