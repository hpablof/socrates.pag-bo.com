
<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Cplan_cuentas extends CI_Controller 
{   
	  public function __construct()
    {
        parent::__construct();
        $this->load->model("Mplan_cuentas");
        $this->load->model("Mcuenta_activo_fijo");
    }

    public function index()
    {    
        $page = 1;
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mplan_cuentas->num_rows(); 
        $total_pages = ceil($numrows/$per_page);
      
        $data = array(
            "reload" => "<?php echo base_url(); ?>Cplan_cuentas/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "plan_cuentass" => $this->Mplan_cuentas->listar_paginacion($offset,$per_page)
            );
 
        $this->load->view("plan_cuentas/VML_plan_cuentas",$data);
    } 

    public function paginacion()
    {   
        $page = $_POST["page"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mplan_cuentas->num_rows();

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "reload" => "<?php echo base_url(); ?>Cplan_cuentas/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "plan_cuentass" => $this->Mplan_cuentas->listar_paginacion($offset,$per_page)
            );
 
        $this->load->view("plan_cuentas/VL_plan_cuentas",$data); 
 
    }

    public function paginacion_find()
    {
        $page = $_POST["page"];
        $txt_buscar = $_POST["txt_buscar"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mplan_cuentas->num_rows_find($txt_buscar);
        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "txt_buscar" => $txt_buscar,
            "reload" => "<?php echo base_url(); ?>plan_cuentas/paginacion_find",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "plan_cuentass" => $this->Mplan_cuentas->listar_paginacion_find($txt_buscar,$offset,$per_page)
            );

         $this->load->view("plan_cuentas/VLB_plan_cuentas",$data);   
    }
 
    public function R_plan_cuentas_select()
    {
      $id_plan_cuenta = $_POST['id_plan_cuenta'];
      $lista_pc = $this->Mplan_cuentas->get_plan_cuentas($id_plan_cuenta);

      $codigo_pc = $lista_pc[0]->Codigo;
      $digito = substr($codigo_pc, -1, 1);
      $suma_digito = $digito+1;
      $nuevo_codigo = substr($codigo_pc,0,-1);

      $codigo_pc = $nuevo_codigo."".$suma_digito;

      $data = array(
      "codigo_pc"=>$codigo_pc,
      "nivels"=>$this->Mplan_cuentas->get_nivels(),
      "monedas"=>$this->Mplan_cuentas->get_monedas());
      $this->load->view("plan_cuentas/VR_plan_cuentas_select",$data);
    
    } 

    public function R_plan_cuentas()
    {
      $data = array(
      "nivels"=>$this->Mplan_cuentas->get_nivels(),
      "monedas"=>$this->Mplan_cuentas->get_monedas());
      $this->load->view("plan_cuentas/VR_plan_cuentas",$data); 
    }

    public function G_plan_cuentas()
    {
      $id_usuario = trim($_POST["id_usuario"]); 
      $fecha = date("Y-m-d");
      $hora = date("H:i:s");
      
      $plan_cuentass =  json_decode($_POST["lista"]);

      foreach($plan_cuentass as $plan_cuentas)
      { 
        $Codigo = $plan_cuentas->Codigo;
        $Nombre = $plan_cuentas->Nombre;
        $Nivel = $plan_cuentas->Nivel;
        $Moneda = $plan_cuentas->Moneda;
        
        $NIT = $plan_cuentas->NIT;
        $Glosa = $plan_cuentas->Glosa;
        $Cod_SIAT = $plan_cuentas->Cod_SIAT;
        $Cod_Flujo_SIAT = $plan_cuentas->Cod_Flujo_SIAT;
        $Cod_Evolucion_Pat = $plan_cuentas->Cod_Evolucion_Pat;
 
        $data = array( 
        "Codigo" => $Codigo,
        "Nombre" => $Nombre,
        "Nivel" => $Nivel,
        "Moneda" => $Moneda,
        "NIT" => $NIT,
        "Glosa" => $Glosa,
        "Cod_SIAT" => $Cod_SIAT,
        "Cod_Flujo_SIAT" => $Cod_Flujo_SIAT,
        "Cod_Evolucion_Pat" => $Cod_Evolucion_Pat, 
        "fecha_pc" => $fecha, 
        "hora_pc" => $hora, 
        "id_usuario" => $id_usuario
        ); 

        $this->Mplan_cuentas->Reg_plan_cuentas($data);
      }
 
      ?>
       <p class="transparent"> espacio </p>
       <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
       </div>

      <script>
           
       setTimeout(function(){ $("#panel_modal_resultado_registrar_plan_cuentas").html(""); },1000);
       setTimeout(function(){ $("#myModal_Register_plan_cuentas").modal("hide"); },2000);
       setTimeout(function(){ cargar_datos_plan_cuentas(1); },3000);

      </script>
      
      <?php

    } 
    
    public function G_plan_cuentas_select()
    {
      $id_usuario = trim($_POST["id_usuario"]); 
      $fecha = date("Y-m-d");
      $hora = date("H:i:s");
      
      $plan_cuentass =  json_decode($_POST["lista"]);

      foreach($plan_cuentass as $plan_cuentas)
      { 
        $Codigo = $plan_cuentas->Codigo;
        $Nombre = $plan_cuentas->Nombre;
        $Nivel = $plan_cuentas->Nivel;
        $Moneda = $plan_cuentas->Moneda;
        $NIT = $plan_cuentas->NIT;
        $Glosa = $plan_cuentas->Glosa;
        $Cod_SIAT = $plan_cuentas->Cod_SIAT;
        $Cod_Flujo_SIAT = $plan_cuentas->Cod_Flujo_SIAT;
        $Cod_Evolucion_Pat = $plan_cuentas->Cod_Evolucion_Pat;
 
        $data = array( 
        "Codigo" => $Codigo,
        "Nombre" => $Nombre,
        "Nivel" => $Nivel,
        "Moneda" => $Moneda,
        "NIT" => $NIT,
        "Glosa" => $Glosa,
        "Cod_SIAT" => $Cod_SIAT,
        "Cod_Flujo_SIAT" => $Cod_Flujo_SIAT,
        "Cod_Evolucion_Pat" => $Cod_Evolucion_Pat, 
        "fecha_pc" => $fecha, 
        "hora_pc" => $hora, 
        "id_usuario" => $id_usuario
        ); 

        $this->Mplan_cuentas->Reg_plan_cuentas($data);
      }
 
      ?>
       <p class="transparent"> espacio </p>
       <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
       </div>

      <script>
           
       setTimeout(function(){ 
        $("#panel_modal_resultado_registrar_plan_cuentas_select").html(""); },1000);

       setTimeout(function(){ 
        $("#myModal_Register_plan_cuentas_select").modal("hide"); },2000);
        
       setTimeout(function(){ btn_menu_plan_cuentas_asignacion(); }, 2500);

      </script>
      
      <?php
    }

    public function VD_plan_cuentas()
    {
    	$id_plan_cuentas = trim($_POST["id_plan_cuentas"]);
    	$data = array("plan_cuentass" => $this->Mplan_cuentas->get_plan_cuentas($id_plan_cuentas));
        
		  $this->load->view("plan_cuentas/VD_plan_cuentas",$data); 
    }

    public function VE_plan_cuentas()
    {
    	$id_plan_cuentas = trim($_POST["id_plan_cuentas"]);
    	$data = array("plan_cuentass" => $this->Mplan_cuentas->get_plan_cuentas($id_plan_cuentas),"nivels" => $this->Mplan_cuentas->get_nivels(),"monedas" => $this->Mplan_cuentas->get_monedas(),);
        
		  $this->load->view("plan_cuentas/VE_plan_cuentas",$data); 
    }

    public function VU_plan_cuentas()
    {
        $cont_up=0;

        $id_plan_cuentas = trim($_POST['id_plan_cuenta']);
        $Codigo = trim($_POST['Codigo']);
        $Nombre = trim($_POST['Nombre']);
        $Nivel = trim($_POST['Nivel']);
        $Moneda = trim($_POST['Moneda']);
        $NIT = trim($_POST['NIT']);
        $Glosa = trim($_POST['Glosa']);
        $Cod_SIAT = trim($_POST['Cod_SIAT']);
        $Cod_Flujo_SIAT = trim($_POST['Cod_Flujo_SIAT']);
        $Cod_Evolucion_Pat = trim($_POST['Cod_Evolucion_Pat']);

        if($Codigo!="")
        {
            $data = array("Codigo" => $Codigo); 
            $this->Mplan_cuentas->Setting_plan_cuentas($data,$id_plan_cuentas);
            $cont_up++;
        }

        if($Nombre!="")
        {
            $data = array("Nombre" => $Nombre); 
            $this->Mplan_cuentas->Setting_plan_cuentas($data,$id_plan_cuentas);
            $cont_up++;
        }

        if($Nivel!="")
        {
            $data = array("Nivel" => $Nivel); 
            $this->Mplan_cuentas->Setting_plan_cuentas($data,$id_plan_cuentas);
            $cont_up++;
        }

        if($Moneda!="")
        {
            $data = array("Moneda" => $Moneda); 
            $this->Mplan_cuentas->Setting_plan_cuentas($data,$id_plan_cuentas);
            $cont_up++;
        }

        if($idcta!="")
        {
            $data = array("idcta" => $idcta); 
            $this->Mplan_cuentas->Setting_plan_cuentas($data,$id_plan_cuentas);
            $cont_up++;
        }

        if($NIT!="")
        {
            $data = array("NIT" => $NIT); 
            $this->Mplan_cuentas->Setting_plan_cuentas($data,$id_plan_cuentas);
            $cont_up++;
        }

        if($Glosa!="")
        {
            $data = array("Glosa" => $Glosa); 
            $this->Mplan_cuentas->Setting_plan_cuentas($data,$id_plan_cuentas);
            $cont_up++;
        }


        if($Cod_SIAT!="")
        {
            $data = array("Cod_SIAT" => $Cod_SIAT); 
            $this->Mplan_cuentas->Setting_plan_cuentas($data,$id_plan_cuentas);
            $cont_up++;
        }

        if($Cod_Flujo_SIAT!="")
        {
            $data = array("Cod_Flujo_SIAT" => $Cod_Flujo_SIAT); 
            $this->Mplan_cuentas->Setting_plan_cuentas($data,$id_plan_cuentas);
            $cont_up++;
        }

        if($Cod_Evolucion_Pat!="")
        {
            $data = array("Cod_Evolucion_Pat" => $Cod_Evolucion_Pat); 
            $this->Mplan_cuentas->Setting_plan_cuentas($data,$id_plan_cuentas);
            $cont_up++;
        }

        if($cont_up>0)
        {
          ?>
          <p class="transparent"> espacio </p>
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close">&times;</button>

            <h4><strong>¡Edicion Correcta!</strong></h4> 
            Informacion Cambiada Correctamente
         
          </div>

          <script>
            setTimeout(function(){ $("#panel_modal_respuesta_editar_plan_cuentas").html(""); },1000);
            setTimeout(function(){ $("#myModal_Update_plan_cuentas").modal("hide"); },2000);
            setTimeout(function(){ cargar_datos_plan_cuentas(1); },3000); 
          </script>

          <?php 
        }

        if($cont_up==0)
        {
          ?>
          <p class="transparent"> espacio </p>
          <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close">&times;</button>

            <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
            Informacion sin cambios en la base de datos
         
          </div>
          <?php      
        }

    }
    
    public function VU_plan_cuentas_select()
    {
        $cont_up=0;

        $id_plan_cuentas = trim($_POST['id_plan_cuenta']);
        $Codigo = trim($_POST['Codigo']);
        $Nombre = trim($_POST['Nombre']);
        $Nivel = trim($_POST['Nivel']);
        $Moneda = trim($_POST['Moneda']);
        
        $NIT = trim($_POST['NIT']);
        $Glosa = trim($_POST['Glosa']);
        $Cod_SIAT = trim($_POST['Cod_SIAT']);
        $Cod_Flujo_SIAT = trim($_POST['Cod_Flujo_SIAT']);
        $Cod_Evolucion_Pat = trim($_POST['Cod_Evolucion_Pat']);

        if($Codigo!="")
        {
            $data = array("Codigo" => $Codigo); 
            $this->Mplan_cuentas->Setting_plan_cuentas($data,$id_plan_cuentas);
            $cont_up++;
        }

        if($Nombre!="")
        {
            $data = array("Nombre" => $Nombre); 
            $this->Mplan_cuentas->Setting_plan_cuentas($data,$id_plan_cuentas);
            $cont_up++;
        }

        if($Nivel!="")
        {
            $data = array("Nivel" => $Nivel); 
            $this->Mplan_cuentas->Setting_plan_cuentas($data,$id_plan_cuentas);
            $cont_up++;
        }

        if($Moneda!="")
        {
            $data = array("Moneda" => $Moneda); 
            $this->Mplan_cuentas->Setting_plan_cuentas($data,$id_plan_cuentas);
            $cont_up++;
        }

        if($NIT!="")
        {
            $data = array("NIT" => $NIT); 
            $this->Mplan_cuentas->Setting_plan_cuentas($data,$id_plan_cuentas);
            $cont_up++;
        }


        if($Glosa!="")
        {
            $data = array("Glosa" => $Glosa); 
            $this->Mplan_cuentas->Setting_plan_cuentas($data,$id_plan_cuentas);
            $cont_up++;
        }


        if($Cod_SIAT!="")
        {
            $data = array("Cod_SIAT" => $Cod_SIAT); 
            $this->Mplan_cuentas->Setting_plan_cuentas($data,$id_plan_cuentas);
            $cont_up++;
        }


        if($Cod_Flujo_SIAT!="")
        {
            $data = array("Cod_Flujo_SIAT" => $Cod_Flujo_SIAT); 
            $this->Mplan_cuentas->Setting_plan_cuentas($data,$id_plan_cuentas);
            $cont_up++;
        }


        if($Cod_Evolucion_Pat!="")
        {
            $data = array("Cod_Evolucion_Pat" => $Cod_Evolucion_Pat); 
            $this->Mplan_cuentas->Setting_plan_cuentas($data,$id_plan_cuentas);
            $cont_up++;
        }

 
        if($cont_up>0)
        {
          ?>

          <p class="transparent"> espacio </p>
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close">&times;</button>

            <h4><strong>¡Edicion Correcta!</strong></h4> 
            Informacion Cambiada Correctamente
         
          </div>

          <script>
          
            setTimeout(function(){ $("#panel_modal_respuesta_editar_plan_cuentas_select").html(""); },1000);

            setTimeout(function(){ $("#myModal_Update_plan_cuentas_select").modal("hide"); },2000);
          
          </script>

          <?php 
        }

        if($cont_up==0)
        {
          ?>
          <p class="transparent"> espacio </p>
          <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close">&times;</button>

            <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
            Informacion sin cambios en la base de datos
         
          </div>
          <?php      
        }

    }

    public function VB_plan_cuentas()
    {
      $id_plan_cuentas = trim($_POST["id_plan_cuentas"]);
    	$this->Mplan_cuentas-> Delete_plan_cuentas($id_plan_cuentas);

      ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Eliminación Correcta!</strong></h4> 
          Informacion Borrada Definitivamente
       
        </div>
       <?php    
    }

    public function VPC_asignacion()
    {
      $data = array('plan_cuentas' => $this->Mplan_cuentas->get_plan_cuentass(),
      'niveles' => $this->Mplan_cuentas->get_nivels());
      
      $this->load->view("plan_cuentas/VC_plan_cuentas",$data); 
    
    }

    public function VR_cts_especiales()
    {
      $data = array('opcion' => 'cuentas_especiales', 
      'cuenta_param'=> $this->Mplan_cuentas->cuenta_parametrizada());
      $this->load->view("plan_cuentas/VO_plan_cuentas",$data); 
    }

    public function VR_cts_retenciones()
    {
      $data = array('opcion' => 'cuentas_retenciales');
      $this->load->view("plan_cuentas/VO_plan_cuentas",$data);        
    }

    public function Subir_plan_cuentas()
    {
       /*
       echo "ingreso aqui XD ";
       echo " - "; echo $archivo_pc = $_FILES["archivo_pc"]["name"];
       echo " - "; echo $temp = $_FILES["archivo_pc"]["tmp_name"];
       
       move_uploaded_file($temp, "assets/multimedia/documentos/plan_cuentas/".$archivo_pc);*/

       /*$archivo = $_FILES['archivo_pc'];
       $extension = pathinfo($archivo['name'], PATHINFO_EXTENSION);
 
       $nombre = "plan_de_cuentas.txt";

       if (move_uploaded_file($archivo['tmp_name'], "assets/multimedia/documentos/plan_cuentas/".$nombre)) {
       
           echo $nombre;
        
        } else {
            echo 0;
        }*/


        $filePath = "assets/multimedia/documentos/plan_cuentas/plan_de_cuentas.txt";
        $delimiter = ";";

        $fecha_pc = date('Y-m-d');
        $hora_pc = date('H:i:s');

        $file = new SplFileObject($filePath);
        while (!$file->eof()) 
        {
               $line = $file->fgetcsv($delimiter);

               echo $Codigo = $line[0];
               echo " - "; echo $Nombre = $line[1];
               echo " - "; echo $Nivel = $line[2];
               echo " - "; echo $Moneda = $line[3];
               echo " - "; echo $idcta = $line[4];
               echo " - "; echo $NIT = $line[5];
               echo " - "; echo $Glosa = $line[6];
               echo " - "; echo $Cod_SIAT = $line[7];
               echo " - "; echo $Cod_Flujo_SIAT = $line[8];
               
               $data = array(
               'Codigo' => $Codigo,
               'Nombre' => $Nombre, 
               'Nivel' => $Nivel,
               'Moneda' => $Moneda,
               'NIT' => $NIT,
               'Glosa' => $Glosa,
               'Cod_SIAT' => $Cod_SIAT,
               'Cod_Flujo_SIAT' => $Cod_Flujo_SIAT,
               'fecha_pc' => $fecha_pc,
               'hora_pc' => $hora_pc,
               'id_usuario' => 1
               );
             
               echo "</br>";

               $this->Mplan_cuentas->registrar_pc_import($data);        
        }

    }

    public function VFiltro_plan_cuentas()
    { 
      $select_nivel = trim($_POST['select_nivel']);
      $data = array('opcion' => 'listar_filtro_pc', 
      "select_nivel"=>$select_nivel, 
      'plan_cuentas' => $this->Mplan_cuentas->get_plan_cuentass());

      $this->load->view("plan_cuentas/VO_plan_cuentas",$data);   
    }

    public function VFiltro_plan_cuentas_numeros()
    {
       $select_nivel = trim($_POST['select_nivel']);
       $data = array('opcion' => 'listar_filtro_pc_numero', 
       "select_nivel"=>$select_nivel, 
       'plan_cuentas' => $this->Mplan_cuentas->get_plan_cuentass());

       $this->load->view("plan_cuentas/VO_plan_cuentas",$data);      
    }

    public function VImprimir_PC()
    {  
       $data = array('opcion' => 'imprimir_pc', 
       'plan_cuentas' => $this->Mplan_cuentas->get_plan_cuentass());
 
       $this->load->view("plan_cuentas/VO_plan_cuentas",$data); 

    }

    public function Verificar_Codigo_PC()
    {
       $Codigo = trim($_POST['Codigo']);
       $lista_codigos = $this->Mplan_cuentas->get_pc_codigo($Codigo);

       if(sizeof($lista_codigos)>0)
       {
        echo "<label style='color:red;'> CODIGO EXISTENTE </label>";
       }
       else{ }

       ?>
     <script type="text/javascript">
       btn_validar_nivel();
     </script>
       <?php

    }

    public function Buscar_PC_select()
    {
       $txt_buscar = trim($_POST['txt_buscar']);
       $panel_resp = trim($_POST['panel_resp']);
       $id_pc = trim($_POST['id_pc']);
       
       $data = array('opcion' => 'Buscar_pc_select', 
       'plan_cuentas' => $this->Mplan_cuentas->buscar_plan_cuentas($txt_buscar)
       ,"txt_buscar" => $txt_buscar,
       "panel_resp" => $panel_resp,
       "id_pc" => $id_pc
       );
 
       $this->load->view("plan_cuentas/VO_plan_cuentas",$data);  

    }

    public function Asignar_pc_select()
    {
       $id_pc = trim($_POST['id_pc']); 
       $Codigo = trim($_POST['Codigo']); 
       $Nombre = trim($_POST['Nombre']); 
       $panel_resp = trim($_POST['panel_resp']); 

       $data = array(
        'codigo_cp' => $Codigo,
        'plan_cuenta_cp' => $Nombre,
       );  

       $this->Mplan_cuentas->update_cuentas_param($data,$id_pc);
       ?>
       <script type="text/javascript">
         $("#<?php echo $panel_resp; ?>").html("");
         
         setTimeout(function(){
          btn_recargar_cuentas_parametrizadas();
         },0);
         
       </script>
       <?php
    }

    public function Recargar_cts_param()
    {
      $data = array('opcion' => 'cuentas_especiales_recargada', 
      'cuenta_param'=> $this->Mplan_cuentas->cuenta_parametrizada());
      $this->load->view("plan_cuentas/VO_plan_cuentas",$data); 
    }
    
    public function V_cuenta_activo_fijo()
    {
      $data = array('opcion' => 'formulario_activo_fijo', "cuentas_fijas" => $this->Mcuenta_activo_fijo->get_cuenta_activo_fijos());
      
      $this->load->view("plan_cuentas/VO_activo_fijo",$data); 
    }
    
    public function VR_cuenta_activo_fijo()
    {
        $data = array('opcion' => 'registro_cuenta_activo_fijo');
         
        $this->load->view("plan_cuentas/VO_activo_fijo",$data);
    }
    
    public function VG_cuenta_activo_fijo()
    {
        $codigo_caf = trim($_POST['codigo_caf']);
        $cuenta_caf = trim($_POST['cuenta_caf']);
        $codigo_cda = trim($_POST['codigo_cda']);
        $cuenta_cda = trim($_POST['cuenta_cda']);
        $codigo_cdd = trim($_POST['codigo_cdd']);
        $cuenta_cdd = trim($_POST['cuenta_cdd']);
        $porcentaje_af = trim($_POST['porcentaje_af']);
        
        $id_usuario = trim($_POST["id_usuario"]); 
        $fecha = date("Y-m-d");
        $hora = date("H:i:s");
        
        $data = array( 
        "codigo_caf" => $codigo_caf, 
        "cuenta_caf" => $cuenta_caf, 
        "codigo_cda" => $codigo_cda, 
        "cuenta_cda" => $cuenta_cda, 
        "codigo_cdd" => $codigo_cdd, 
        "cuenta_cdd" => $cuenta_cdd, 
        "porcentaje_af" => $porcentaje_af, 
        "fecha_caf" => $fecha, 
        "hora_caf" => $hora, 
        "id_usuario" => $id_usuario
            ); 

        $this->Mcuenta_activo_fijo->Reg_cuenta_activo_fijo($data);
        ?>

          <p class="transparent"> espacio </p>
          <div class="alert alert-info alert-dismissable">
            <button type="button" class="close">&times;</button>
    
            <h4><strong>¡Registro Correcto!</strong></h4> 
            Informacion Almacenada Correctamente
         
          </div>
    
          <script>
               
           setTimeout(function(){ $("#panel_resultado_registro_cuentas_activo_fijo").html(""); },1000);
           setTimeout(function(){ $("#myModal_Registro_Cuentas_Activo_Fijo").modal("hide"); },2000);
           
    
          </script>
      
        <?php
      
    }
    
    public function VBuscar_cuenta_activo_fijo()
    {
        $txt_buscar = trim($_POST['txt_buscar']);
        $panel_activo_fijo = trim($_POST['panel_activo_fijo']);
        $code_id_caf =  trim($_POST['code_id_caf']);
        $id_caf = trim($_POST['id_caf']);
        
        $data = array("opcion" => "lista_busqueda_activos_fijos", 'plan_cuentas' => $this->Mplan_cuentas->buscar_plan_cuentas($txt_buscar), 
        "txt_buscar" =>  $txt_buscar, "panel_activo_fijo" => $panel_activo_fijo, "code_id_caf" => $code_id_caf, "id_caf" => $id_caf);
        
        $this->load->view("plan_cuentas/VO_activo_fijo",$data); 
    }
} 

?>
