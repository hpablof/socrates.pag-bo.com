
<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Clogin extends CI_Controller 
{   
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Mlogin");
        $this->load->model("Mplan_cuentas");
    }

    public function index()
    {
        $this->load->view("assets/Header_login");
        $this->load->view("pagina/Carrusel");
        $this->load->view("pagina/Contenido");
        $this->load->view("assets/Footer"); 

    }

    public function Acceso()
    {   
        $this->load->view("assets/Header_login");
        $this->load->view("assets/Login");
        $this->load->view("assets/Footer"); 

    }
    
    public function Menu()
    {   
        $this->load->view("assets/Header");
        $this->load->view("assets/Menu");
        $this->load->view("assets/Footer"); 

    }
    public function validar()
    {
         $email = trim($_POST["email"]);
         $password = trim($_POST["password"]);
 
         $user_model =  $this->Mlogin->verificar($email,$password);

         $size = sizeof($user_model);

         if ($size>0) 
         {

          $id_usuario = $user_model[0]->id_usuario;
          $email = $user_model[0]->email;
          $password = $user_model[0]->password;
        
          $data_session = array
          (
             "id_usuario" => $id_usuario,
             "email"      => $email,
             "password"   => $password
    
          );

          $this->session->set_userdata($data_session); 
 
          ?>
          <script type="text/javascript">
          location.href="<?php echo base_url(); ?>Clogin/menu";
          </script>
          <?php

 
         }
         else{
              echo "Cuenta Error !! Volver a Intentar X( ";
              
         }

    }



    public function perfil()
    {
        $id_usuario = $this->session->userdata("id_usuario");
        $data = array("perfil" => $this->Mlogin->get_user($id_usuario));

        $this->load->view("assets/Header");
        $this->load->view("assets/Perfil",$data);
        $this->load->view("assets/Footer"); 
        
        
    }

    public function salir()
    {
      $this->session->sess_destroy(); ?>

      <script type="text/javascript">
      location.href="<?php echo base_url(); ?>Clogin/";
      </script>
    
    <?php
    }

    public function actualizar_pc()
    {
      $plan_c = $this->Mplan_cuentas->get_plan_cuentass();
     // print_r($plan_c);

      foreach ($plan_c as $pc) 
      {
        $nivel = $pc->Nivel;

        if($nivel==5)
        {
          echo $codigo = $pc->Codigo;
          echo " - "; echo $nivel;
          echo "</br>";

        }
       
      }
     
    }
    
    public function Formulario()
    {
        echo "Formulario Socrates XD";
    }

 
} 

?>
