<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Cursos extends CI_Controller 
{   
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Mestudiante");
        $this->load->model("Mcurso");
    }

    public function index()
    {   
        $data = array("opcion" => "listar_cursos", "cursos" => $this->Mcurso->get_cursos_activos());
        
        $this->load->view("assets/Header_login");
        $this->load->view("assets/Cursos",$data);
        $this->load->view("assets/Footer"); 
    }
    
    public function Formulario()
    {
        $id_curso = trim($_GET['id']);
        
        $data = array("opcion" => "formulario_curso", "curso" => $this->Mcurso->get_curso($id_curso));
        
        $this->load->view("assets/Header_login");
        $this->load->view("assets/Cursos",$data);
        $this->load->view("assets/Footer"); 
    }

    public function Verificacion()
    {   
        $data = array("opcion" => "formulario_verificacion_curso");
        
        $this->load->view("assets/Header_login");
        $this->load->view("assets/Cursos",$data);
        $this->load->view("assets/Footer"); 
    }
    
    public function Verificar_Participante()
    {
        $ci_verificar = trim($_POST['ci_verificar']);
        
        $verf = $this->Mestudiante->get_estudiante_ver_ci($ci_verificar);
 
        if($verf[0]->ci_estudiante!="")
        {
 
          ?>
           
           <div class="alert alert-info alert-dismissable">

            <h4><strong>¡Usted se Encuentra Registrado Correctamente!</strong></h4> 
            <hr>
            <h4>Curso : <strong> <?php echo $verf[0]->curso; ?></strong></h4> 
            <h4>Participante : <strong> <?php echo $verf[0]->nombre_estudiante; echo $verf[0]->ci_estudiante; ?></strong></h4>
         
          </div>
      
          <?php
        }
        else{
        
        ?>
           <div class="alert alert-danger alert-dismissable">

            <h4><strong>¡Usted No se Encuentra Registrado en los Cursos!</strong></h4> 
 
          </div>
          
        <?php  
        }
    }
        
   
    

 
 
} 

?>