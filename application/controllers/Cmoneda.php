
<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Cmoneda extends CI_Controller 
{   
	  public function __construct()
    {
        parent::__construct();
        $this->load->model("Mmoneda");
    }

    public function index()
    {    
        $page = 1;
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mmoneda->num_rows(); 
        $total_pages = ceil($numrows/$per_page);
      
        $data = array(
            "reload" => "<?php echo base_url(); ?>Cmoneda/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "monedas" => $this->Mmoneda->listar_paginacion($offset,$per_page)
            );
 
        $this->load->view("moneda/VML_moneda",$data);
 
    } 

    public function paginacion()
    {   
        $page = $_POST["page"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mmoneda->num_rows();

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "reload" => "<?php echo base_url(); ?>Cmoneda/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "monedas" => $this->Mmoneda->listar_paginacion($offset,$per_page)
            );
 
        $this->load->view("moneda/VL_moneda",$data); 
    

    }

    public function paginacion_find()
    {
        $page = $_POST["page"];
        $txt_buscar = $_POST["txt_buscar"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mmoneda->num_rows_find($txt_buscar);

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "txt_buscar" => $txt_buscar,
            "reload" => "<?php echo base_url(); ?>moneda/paginacion_find",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "monedas" => $this->Mmoneda->listar_paginacion_find($txt_buscar,$offset,$per_page)
            );

         $this->load->view("moneda/VLB_moneda",$data);   
    }
 
    public function R_moneda()
    {
        $this->load->view("moneda/VR_moneda");
    } 

    public function G_moneda()
    {
         $id_usuario = trim($_POST["id_usuario"]); 
         $fecha = date("Y-m-d");
         $hora = date("H:i:s");
      
         $monedas =  json_decode($_POST["lista"]);

         foreach($monedas as $moneda)
         { 
           $moneda = $moneda->moneda; 
          
           $data = array( 
           "moneda" => $moneda, 
           "fecha" => $fecha, 
           "hora" => $hora, 
           "id_usuario" => $id_usuario ); 

           $this->Mmoneda->Reg_moneda($data);
   
         }
 
      ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script>
           
       setTimeout(function(){ $("#panel_modal_resultado_registrar_moneda").html(""); },1000);
       setTimeout(function(){ $("#myModal_Register_moneda").modal("hide"); },2000);
       setTimeout(function(){ cargar_datos_moneda(1); },3000);

      </script>
      
      <?php

    } 

    public function VD_moneda()
    {
    	$id_moneda = trim($_POST["id_moneda"]);
    	$data = array("monedas" => $this->Mmoneda->get_moneda($id_moneda)); 
		  $this->load->view("moneda/VD_moneda",$data); 
    }

    public function VE_moneda()
    {
    	$id_moneda = trim($_POST["id_moneda"]);
    	$data = array("monedas" => $this->Mmoneda->get_moneda($id_moneda),);

		  $this->load->view("moneda/VE_moneda",$data); 
    }

    public function VU_moneda()
    {
            $cont_up=0;
            $id_moneda = trim($_POST["id_moneda"]);
            $moneda = trim($_POST["moneda"]);

            if($moneda!="")
            {
                $data = array("moneda" => $moneda); 
                $this->Mmoneda->Setting_moneda($data,$id_moneda);
                $cont_up++;
            }
 
            if($cont_up>0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡Edicion Correcta!</strong></h4> 
                Informacion Cambiada Correctamente
             
              </div>

              <script>
                setTimeout(function(){ $("#panel_modal_respuesta_editar_moneda").html(""); },1000);
                setTimeout(function(){ $("#myModal_Update_moneda").modal("hide"); },2000);
                setTimeout(function(){ cargar_datos_moneda(1);; },3000);
              </script>

              <?php 
            }

            if($cont_up==0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
                Informacion sin cambios en la base de datos
             
              </div>
              <?php      
            }
  
 
    }

    public function VB_moneda()
    {
    	$id_moneda = trim($_POST["id_moneda"]);

    	$this->Mmoneda-> Delete_moneda($id_moneda);

      ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Eliminación Correcta!</strong></h4> 
          Informacion Borrada Definitivamente
       
        </div>
      <?php    
    }

} 

?>
