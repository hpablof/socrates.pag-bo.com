
<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Crazon_social extends CI_Controller 
{   
	public function __construct()
    {
        parent::__construct();
        $this->load->model("Mrazon_social");
    }

    public function index()
    {    
        $page = 1;
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mrazon_social->num_rows(); 
        $total_pages = ceil($numrows/$per_page);
      
        $data = array(
            "reload" => "<?php echo base_url(); ?>Crazon_social/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "razon_socials" => $this->Mrazon_social->listar_paginacion($offset,$per_page)
            );

        $this->load->view("razon_social/VML_razon_social",$data);
    } 

    public function paginacion()
    {   
        $page = $_POST["page"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mrazon_social->num_rows();

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "reload" => "<?php echo base_url(); ?>Crazon_social/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "razon_socials" => $this->Mrazon_social->listar_paginacion($offset,$per_page)
            );
 
        $this->load->view("razon_social/VL_razon_social",$data); 
    

    }

    public function paginacion_find()
    {
        $page = $_POST["page"];
        $txt_buscar = $_POST["txt_buscar"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mrazon_social->num_rows_find($txt_buscar);

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "txt_buscar" => $txt_buscar,
            "reload" => "<?php echo base_url(); ?>razon_social/paginacion_find",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "razon_socials" => $this->Mrazon_social->listar_paginacion_find($txt_buscar,$offset,$per_page)
            );

         $this->load->view("razon_social/VLB_razon_social",$data);   
    }
 
    public function R_razon_social()
    {
          $this->load->view("razon_social/VR_razon_social");
    } 

    public function G_razon_social()
    {
         $id_usuario = trim($_POST["id_usuario"]); 
         $fecha = date("Y-m-d");
         $hora = date("H:i:s");
      
         $razon_socials =  json_decode($_POST["lista"]);

      foreach($razon_socials as $razon_social)
      { 
        $nit = $razon_social->nit; 
        $razon_social = $razon_social->razon_social; 
        
        $data = array( 
        "nit" => $nit, 
        "razon_social" => $razon_social, 
        "fecha_rs" => $fecha, 
        "hora_rs" => $hora, 
        "id_usuario" => $id_usuario
            ); 

      $this->Mrazon_social->Reg_razon_social($data);
 
      }

     
      
      ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script>
           
       setTimeout(function(){ $("#panel_modal_resultado_registrar").html(""); },1000);
       setTimeout(function(){ $("#myModal_Register").modal("hide"); },2000);
       setTimeout(function(){ location.reload(); },3000);

      </script>
      
      <?php

    } 

    public function VD_razon_social()
    {
    	$id_razon_social = trim($_POST["id_razon_social"]);
    	$data = array("razon_socials" => $this->Mrazon_social->get_razon_social($id_razon_social));
        
		  $this->load->view("razon_social/VD_razon_social",$data); 
    }

    public function VE_razon_social()
    {
    	$id_razon_social = trim($_POST["id_razon_social"]);
    	$data = array("razon_socials" => $this->Mrazon_social->get_razon_social($id_razon_social),);
        
		$this->load->view("razon_social/VE_razon_social",$data); 
    }

    public function VU_razon_social()
    {
            $cont_up=0;
            $id_razon_social = trim($_POST["id_razon_social"]);
            $nit = trim($_POST["nit"]);
            $razon_social = trim($_POST["razon_social"]);

            if($nit!="")
            {
                $data = array("nit" => $nit); 
                $this->Mrazon_social->Setting_razon_social($data,$id_razon_social);
                $cont_up++;
            }
    

            if($razon_social!="")
            {
                $data = array("razon_social" => $razon_social); 
                $this->Mrazon_social->Setting_razon_social($data,$id_razon_social);
                $cont_up++;
            }
    

            if($cont_up>0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡Edicion Correcta!</strong></h4> 
                Informacion Cambiada Correctamente
             
              </div>

              <script>
                setTimeout(function(){ $("#panel_modal_respuesta_editar").html(""); },1000);
                setTimeout(function(){ $("#myModal_Update").modal("hide"); },2000);
                setTimeout(function(){ location.reload(); },3000); 
              </script>

              <?php 
            }

            if($cont_up==0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
                Informacion sin cambios en la base de datos
             
              </div>
              <?php      
            }
  
 
    }

    public function VB_razon_social()
    {
    	$id_razon_social = trim($_POST["id_razon_social"]);

    	$this->Mrazon_social-> Delete_razon_social($id_razon_social);

      ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Eliminación Correcta!</strong></h4> 
          Informacion Borrada Definitivamente
       
        </div>
      <?php    
    }

    public function Verificar_razon_social()
    {
      $nit = trim($_POST["nit_ver"]);
      $ver = $this->Mrazon_social->getVerNit($nit);

      if(sizeof($ver)>0)
      {
        echo "<label style='color:red;'> El nit ya se encuentra registrado </label>";
      }

    }

    public function Upload_razon_social()
    {
      $data = array("opcion" => "panel_subir_rs",);
      $this->load->view("razon_social/VO_razon_social",$data);  
    }

    public function Subir_razon_social()
    {
        echo " SUBIR RAZON SOCIAL </br> </br>";

        $filePath = "assets/multimedia/documentos/razon_social/razones_sociales.txt";
        $delimiter = ";";

        echo $fecha_rs = date('Y-m-d');
        echo " - "; echo $hora_rs = date('H:i:s');

        echo " - "; echo $id_usuario = trim($_POST["id_usuario"]);

        echo "</br>"; 
  
        $file = new SplFileObject($filePath);
        while (!$file->eof()) 
        {
              $line = $file->fgetcsv($delimiter);

              echo $razon_social = $line[0];
              echo " - "; echo $nit = $line[1];
              echo "</br>";

              $data = array( 
              "nit" => $nit, 
              "razon_social" => $razon_social, 
              "fecha_rs" => $fecha_rs, 
              "hora_rs" => $hora_rs, 
              "id_usuario" => $id_usuario
              ); 

              $this->Mrazon_social->Reg_razon_social($data);
                       
        }

        ?>
        <script>
             
         setTimeout(function(){ $("#panel_resp_upload_rs").html(""); },1000);
         setTimeout(function(){ $("#myModal_Upload_RS").modal("hide"); },2000);
         setTimeout(function(){ location.reload(); },3000);

        </script>
        <?php

    }

} 

?>
