<?php
defined("BASEPATH") OR exit("No direct script access allowed");
 
class Cestudiante extends CI_Controller 
{   
	public function __construct()
    {
        parent::__construct();
        $this->load->model("Mestudiante");
        $this->load->model("Mcurso");
    }

    public function index()
    {    
        $page = 1;
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mestudiante->num_rows(); 
        $total_pages = ceil($numrows/$per_page); 
 
        $data = array(
            "reload" => "<?php echo base_url(); ?>Cestudiante/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "estudiantes" => $this->Mestudiante->get_estudiantes_todos(),
            "cursos" => $this->Mcurso->get_cursos(),
            "numrows" => $numrows
            );
        
          
        $this->load->view("estudiante/VML_estudiante",$data);
    } 

    public function paginacion()
    {   
        $page = $_POST["page"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mestudiante->num_rows();

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "reload" => "<?php echo base_url(); ?>Cestudiante/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "estudiantes" => $this->Mestudiante->get_estudiantes_todos()
            );
 
        $this->load->view("estudiante/VL_estudiante",$data); 
    }

    public function paginacion_find()
    {
        $page = $_POST["page"];
        $txt_buscar = $_POST["txt_buscar"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mestudiante->num_rows_find($txt_buscar);

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "txt_buscar" => $txt_buscar,
            "reload" => "<?php echo base_url(); ?>estudiante/paginacion_find",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "estudiantes" => $this->Mestudiante->get_busqueda_estudiantes_todos($txt_buscar)
            );

         $this->load->view("estudiante/VLB_estudiante",$data);   
    }

    public function Filtro_Cursos()
    {
        $page = $_POST["page"];
        $id_curso = $_POST["id_curso"];
 
        $data = array("estudiantes" => $this->Mestudiante->get_estudiantes_todos_curso($id_curso));
        $this->load->view("estudiante/VL_estudiante",$data);
    }
    
    public function R_estudiante()
    {
          $data = array(
          "cursos"=>$this->Mestudiante->get_cursos());
          $this->load->view("estudiante/VR_estudiante",$data);

    } 

    public function G_estudiante()
    {
         $id_usuario = trim($_POST["id_usuario"]); 
         $fecha = date("Y-m-d");
         $hora = date("H:i:s");
      
         $estudiantes =  json_decode($_POST["lista"]);

      foreach($estudiantes as $estudiante)
      { 
        $nombre_estudiante = $estudiante->nombre_estudiante; 
        $razon_social = $estudiante->razon_social; 
        $ci_nit_estudiante = $estudiante->ci_nit_estudiante; 
        $email_estudiante = $estudiante->email_estudiante; 
        $celular_estudiante = $estudiante->celular_estudiante; 
        $tipo_de_pago = $estudiante->tipo_de_pago; 
        $codigo_num_transferencia = $estudiante->codigo_num_transferencia; 
        $id_curso = $estudiante->id_curso; 
        
        $data = array( 
        "nombre_estudiante" => $nombre_estudiante, 
        "razon_social" => $razon_social, 
        "ci_nit_estudiante" => $ci_nit_estudiante, 
        "email_estudiante" => $email_estudiante, 
        "celular_estudiante" => $celular_estudiante, 
        "tipo_de_pago" => $tipo_de_pago, 
        "codigo_num_transferencia" => $codigo_num_transferencia, 
        "id_curso" => $id_curso, 
        "fecha" => $fecha, 
        "hora" => $hora, 
        "id_usuario" => $id_usuario
            ); 

      $this->Mestudiante->Reg_estudiante($data);
 
      }

     
      
      ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script>
           
       setTimeout(function(){ $("#panel_modal_resultado_registrar").html(""); },1000);
       setTimeout(function(){ $("#myModal_Register").modal("hide"); },2000);
       setTimeout(function(){ location.reload(); },3000);

      </script>
      
      <?php

    } 

    public function VD_estudiante()
    {
    	$id_estudiante = trim($_POST["id_estudiante"]);
    	$data = array("estudiantes" => $this->Mestudiante->get_estudiante($id_estudiante));
        
	    $this->load->view("estudiante/VD_estudiante",$data); 
    }

    public function VE_estudiante()
    {
    	$id_estudiante = trim($_POST["id_estudiante"]);
    	$data = array("estudiantes" => $this->Mestudiante->get_estudiante($id_estudiante),"cursos" => $this->Mestudiante->get_cursos(),);
        
		$this->load->view("estudiante/VE_estudiante",$data); 
    }

    public function VU_estudiante()
    {
            $cont_up=0;
            $id_estudiante = trim($_POST["id_estudiante"]);
            $nombre_estudiante = trim($_POST["nombre_estudiante"]);
            $razon_social = trim($_POST["razon_social"]);
            $ci_nit_estudiante = trim($_POST["ci_nit_estudiante"]);
            $email_estudiante = trim($_POST["email_estudiante"]);
            $celular_estudiante = trim($_POST["celular_estudiante"]);
            $tipo_de_pago = trim($_POST["tipo_de_pago"]);
            $codigo_num_transferencia = trim($_POST["codigo_num_transferencia"]);
            $id_curso = trim($_POST["id_curso"]);
            
            $estado_estudiante = trim($_POST["estado_estudiante"]);
            $estado_certificado = trim($_POST["estado_certificado"]);
            
		    $factura = trim($_POST["factura_estudiante"]);

            if($estado_certificado!="")
            {
                $data = array("estado_certificado" => $estado_certificado); 
                $this->Mestudiante->Setting_estudiante($data,$id_estudiante);
                $cont_up++;
            }
            
            if($estado_estudiante!="")
            {
                $data = array("estado_estudiante" => $estado_estudiante); 
                $this->Mestudiante->Setting_estudiante($data,$id_estudiante);
                $cont_up++;
            }

            if($factura!="")
            {
                $data = array("factura" => $factura); 
                $this->Mestudiante->Setting_estudiante($data,$id_estudiante);
                $cont_up++;
            }
    
    
            if($nombre_estudiante!="")
            {
                $data = array("nombre_estudiante" => $nombre_estudiante); 
                $this->Mestudiante->Setting_estudiante($data,$id_estudiante);
                $cont_up++;
            }
    

            if($razon_social!="")
            {
                $data = array("razon_social" => $razon_social); 
                $this->Mestudiante->Setting_estudiante($data,$id_estudiante);
                $cont_up++;
            }
    

            if($ci_nit_estudiante!="")
            {
                $data = array("ci_nit_estudiante" => $ci_nit_estudiante); 
                $this->Mestudiante->Setting_estudiante($data,$id_estudiante);
                $cont_up++;
            }
    

            if($email_estudiante!="")
            {
                $data = array("email_estudiante" => $email_estudiante); 
                $this->Mestudiante->Setting_estudiante($data,$id_estudiante);
                $cont_up++;
            }
    

            if($celular_estudiante!="")
            {
                $data = array("celular_estudiante" => $celular_estudiante); 
                $this->Mestudiante->Setting_estudiante($data,$id_estudiante);
                $cont_up++;
            }
    

            if($tipo_de_pago!="")
            {
                $data = array("tipo_de_pago" => $tipo_de_pago); 
                $this->Mestudiante->Setting_estudiante($data,$id_estudiante);
                $cont_up++;
            }
    

            if($codigo_num_transferencia!="")
            {
                $data = array("codigo_num_transferencia" => $codigo_num_transferencia); 
                $this->Mestudiante->Setting_estudiante($data,$id_estudiante);
                $cont_up++;
            }
    

            if($id_curso!="")
            {
                $data = array("id_curso" => $id_curso); 
                $this->Mestudiante->Setting_estudiante($data,$id_estudiante);
                $cont_up++;
            }
    

            if($cont_up>0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡Edicion Correcta!</strong></h4> 
                Informacion Cambiada Correctamente
             
              </div>

              <script>
                setTimeout(function(){ $("#panel_modal_respuesta_editar").html(""); },1000);
                setTimeout(function(){ $("#myModal_Update").modal("hide"); },2000);
                setTimeout(function(){ cargar_datos(1); },3000);
              </script>

              <?php 
            }

            if($cont_up==0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
                Informacion sin cambios en la base de datos
             
              </div>
              <?php      
            }
  
 
    }

    public function VB_estudiante()
    {
    	$id_estudiante = trim($_POST["id_estudiante"]);

    	$this->Mestudiante-> Delete_estudiante($id_estudiante);

      ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Eliminación Correcta!</strong></h4> 
          Informacion Borrada Definitivamente
       
        </div>
      <?php    
    }
    
    public function VO_impresion_estudiante()
    {
        $data = array("opcion" => "vista_variables_participantes");
        
        $this->load->view("estudiante/VO_estudiante",$data); 
    }
    
    public function VO_certificado_estudiante()
    {
       $id_estudiante = trim($_GET['IDE']);
       $id_curso = trim($_GET['IDC']);

       $data = array(
       "estudiante" => $this->Mestudiante->get_estudiante_curso_impresion($id_estudiante,$id_curso),
       );
       
       $this->load->view("estudiante/VI_certificado",$data); 
       
    }
    
    public function Formulario_impresion()
    {
        $id_estudiante = trim($_POST['id_estudiante']);
        $id_curso = trim($_POST['id_curso']);
        
        $data = array("opcion" => "formulario_impresion", "estudiantes" => $this->Mestudiante->get_estudiante_curso_impresion($id_estudiante,$id_curso));
        $this->load->view("estudiante/VO_estudiante",$data); 
    }
    
    public function Impresion_Certificados()
    {
         echo "codigo curso : "; echo $id_curso = trim($_POST['id_curso']);
         
         $estudiantes = $this->Mestudiante->get_estudiantes_curso($id_curso); 
         
         foreach($estudiantes as $est)
         {
             $nombre_estudiante = $est->nombre_estudiante;  
             
             $id_estudiante = $est->id_estudiante;  
             $id_curso = $est->id_curso; 
             
            ?>
            <script>
 
               setTimeout(function(){
                
                var url_cert = url_p+"Cestudiante/VO_certificado_estudiante_todos?IDE=<?php echo $id_estudiante; ?>&IDC=<?php echo $id_curso; ?>";
                window.open(url_cert, '_blank'); 
                
               },1000);
                
            </script>
            <?php
             
         }
    }
    
    public function VO_certificado_estudiante_todos()
    {
       $id_estudiante = trim($_GET['IDE']);
       $id_curso = trim($_GET['IDC']);
          
       $data = array("estudiante" => $this->Mestudiante->get_estudiante_curso_impresion($id_estudiante,$id_curso));
       
       $this->load->view("estudiante/VI_certificado",$data); 
       
    }

} 

?>
