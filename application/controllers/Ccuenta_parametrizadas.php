
<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Ccuenta_parametrizadas extends CI_Controller 
{   
	public function __construct()
    {
        parent::__construct();
        $this->load->model("Mcuenta_parametrizadas");
    }

    public function index()
    {    
        $page = 1;
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mcuenta_parametrizadas->num_rows(); 
        $total_pages = ceil($numrows/$per_page);
      
        $data = array(
            "reload" => "<?php echo base_url(); ?>Ccuenta_parametrizadas/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "cuenta_parametrizadass" => $this->Mcuenta_parametrizadas->listar_paginacion($offset,$per_page)
            );
        
        $this->load->view("assets/Header");  
        $this->load->view("cuenta_parametrizadas/VML_cuenta_parametrizadas",$data);
        $this->load->view("assets/Footer"); 

    } 

    public function paginacion()
    {   
        $page = $_POST["page"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mcuenta_parametrizadas->num_rows();

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "reload" => "<?php echo base_url(); ?>Ccuenta_parametrizadas/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "cuenta_parametrizadass" => $this->Mcuenta_parametrizadas->listar_paginacion($offset,$per_page)
            );
 
        $this->load->view("cuenta_parametrizadas/VL_cuenta_parametrizadas",$data); 
    

    }

    public function paginacion_find()
    {
        $page = $_POST["page"];
        $txt_buscar = $_POST["txt_buscar"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mcuenta_parametrizadas->num_rows_find($txt_buscar);

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "txt_buscar" => $txt_buscar,
            "reload" => "<?php echo base_url(); ?>cuenta_parametrizadas/paginacion_find",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "cuenta_parametrizadass" => $this->Mcuenta_parametrizadas->listar_paginacion_find($txt_buscar,$offset,$per_page)
            );

         $this->load->view("cuenta_parametrizadas/VLB_cuenta_parametrizadas",$data);   
    }
 
    public function R_cuenta_parametrizadas()
    {
          $this->load->view("cuenta_parametrizadas/VR_cuenta_parametrizadas");

		  
    } 

    public function G_cuenta_parametrizadas()
    {
          $cuenta_cp = trim($_POST["cuenta_cp"]);
          $codigo_cp = trim($_POST["codigo_cp"]);
          $plan_cuenta_cp = trim($_POST["plan_cuenta_cp"]);
          $id_usuario = trim($_POST["id_usuario"]); 
          $fecha = date("Y-m-d");
          $hora = date("H:i:s");
 
          $data = array( 
          "cuenta_cp" => $cuenta_cp, 
          "codigo_cp" => $codigo_cp, 
          "plan_cuenta_cp" => $plan_cuenta_cp, 
          "fecha_cp" => $fecha, 
          "hora_cp" => $hora, 
          "id_usuario" => $id_usuario
            ); 

      $this->Mcuenta_parametrizadas->Reg_cuenta_parametrizadas($data);
      
      ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script>
           
       setTimeout(function(){ $("#panel_modal_resultado_registrar").html(""); },1000);
       setTimeout(function(){ $("#myModal_Register").modal("hide"); },2000);
       setTimeout(function(){ location.reload(); },3000);

      </script>
      
      <?php

    } 

    public function VD_cuenta_parametrizadas()
    {
    	$id_cuenta_parametrizadas = trim($_POST["id_cuenta_parametrizadas"]);
    	$data = array("cuenta_parametrizadass" => $this->Mcuenta_parametrizadas->get_cuenta_parametrizadas($id_cuenta_parametrizadas));
        
		  $this->load->view("cuenta_parametrizadas/VD_cuenta_parametrizadas",$data); 
    }

    public function VE_cuenta_parametrizadas()
    {
    	$id_cuenta_parametrizadas = trim($_POST["id_cuenta_parametrizadas"]);
    	$data = array("cuenta_parametrizadass" => $this->Mcuenta_parametrizadas->get_cuenta_parametrizadas($id_cuenta_parametrizadas),);
        
		$this->load->view("cuenta_parametrizadas/VE_cuenta_parametrizadas",$data); 
    }

    public function VU_cuenta_parametrizadas()
    {
            $cont_up=0;
            $id_cuenta_parametrizadas = trim($_POST["id_cuenta_parametrizadas"]);
            $cuenta_cp = trim($_POST["cuenta_cp"]);
            $codigo_cp = trim($_POST["codigo_cp"]);
            $plan_cuenta_cp = trim($_POST["plan_cuenta_cp"]);

            if($cuenta_cp!="")
            {
                $data = array("cuenta_cp" => $cuenta_cp); 
                $this->Mcuenta_parametrizadas->Setting_cuenta_parametrizadas($data,$id_cuenta_parametrizadas);
                $cont_up++;
            }
    

            if($codigo_cp!="")
            {
                $data = array("codigo_cp" => $codigo_cp); 
                $this->Mcuenta_parametrizadas->Setting_cuenta_parametrizadas($data,$id_cuenta_parametrizadas);
                $cont_up++;
            }
    

            if($plan_cuenta_cp!="")
            {
                $data = array("plan_cuenta_cp" => $plan_cuenta_cp); 
                $this->Mcuenta_parametrizadas->Setting_cuenta_parametrizadas($data,$id_cuenta_parametrizadas);
                $cont_up++;
            }
    

            if($cont_up>0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡Edicion Correcta!</strong></h4> 
                Informacion Cambiada Correctamente
             
              </div>

              <script>
                setTimeout(function(){ $("#panel_modal_respuesta_editar").html(""); },1000);
                setTimeout(function(){ $("#myModal_Update").modal("hide"); },2000);
                setTimeout(function(){ location.reload(); },3000); 
              </script>

              <?php 
            }

            if($cont_up==0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
                Informacion sin cambios en la base de datos
             
              </div>
              <?php      
            }
  
 
    }

    public function VB_cuenta_parametrizadas()
    {
    	$id_cuenta_parametrizadas = trim($_POST["id_cuenta_parametrizadas"]);

    	$this->Mcuenta_parametrizadas-> Delete_cuenta_parametrizadas($id_cuenta_parametrizadas);

      ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Eliminación Correcta!</strong></h4> 
          Informacion Borrada Definitivamente
       
        </div>
      <?php    
    }

} 

?>
