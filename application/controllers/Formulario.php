<?php

defined("BASEPATH") OR exit("No direct script access allowed");

class Formulario extends CI_Controller 
{   
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Mestudiante");
    }

    public function index()
    {    
        $this->load->view("assets/Header_login");
        $this->load->view("assets/Formulario");
        $this->load->view("assets/Footer"); 
    }
    
    public function Registrar()
    {
        $nombre_completo = trim($_POST['nombre_completo']);
        $ci = trim($_POST['ci']);
        $razon_social = trim($_POST['razon_social']);
        $ci_nit = trim($_POST['ci_nit']);
        $email = trim($_POST['email']);
        $celular = trim($_POST['celular']);
        $opcion_tipo_pago = trim($_POST['opcion_tipo_pago']);
        $codigo_transferencia = trim($_POST['codigo_transferencia']);
        $id_curso = trim($_POST['id_curso']);
        $importe = trim($_POST['importe']);
        
        $fecha = date("Y-m-d");
        $hora = date("H:i:s");
        
        if($nombre_completo!="" && $razon_social!="" && $ci!="" && $ci_nit!="" && $opcion_tipo_pago!="" && $codigo_transferencia!="")
        {
            
            $cont_est = $this->Mestudiante->verificar_pago_estudiante($id_curso,$ci);
            
            if(sizeof($cont_est)==0)
            {
                   $data = array
                    (
                      "nombre_estudiante" => $nombre_completo,
                      "razon_social" => $razon_social,
                      "ci_estudiante" => $ci,
                      "ci_nit_estudiante" => $ci_nit,
                      "email_estudiante" => $email,
                      "celular_estudiante" => $celular,
                      "tipo_de_pago" => $opcion_tipo_pago,
                      "codigo_num_transferencia" => $codigo_transferencia,
                      "importe_estudiante" => $importe,
                      "id_curso" => $id_curso,
                      "fecha_est" => $fecha,
                      "hora_est" => $hora,
            
                    );
                    
                    $this->Mestudiante->Reg_estudiante($data); 
                    
                    ?>
                      <p class="transparent"> espacio </p>
                      <div class="alert alert-info alert-dismissable">
                        
                        <h4><strong>¡Registro Correcto de Estudiante!</strong></h4> 
                            Informacion Almacenada Correctamente
                     
                      </div>
                
                      <script>
                           
                       setTimeout(function(){ $("#panel_resp_formulario_curso").html(""); },2000);
                       setTimeout(function(){ location.reload(); },3000);
                
                      </script>
                    <?php
            }
            
            else
            {

                ?>
                  <p class="transparent"> espacio </p>
                  <div class="alert alert-warning alert-dismissable">

                    <h4><strong>¡Registro Duplicado!</strong></h4> 
                        Usted ya realizo el registro de la inscripcion al curso </br>
                        cualquier error comuniquese con el administrador
                        
                  </div>
            
                  <script>
                       
                   setTimeout(function(){ $("#panel_resp_formulario_curso").html(""); },5000);
                   //setTimeout(function(){ location.reload(); },3000);
            
                  </script>
                <?php
                    
            }
            
 
            
        }
        
        else
        {
            ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-danger alert-dismissable">
                
        
                <h4><strong>¡ Error en el Registro!</strong></h4> 
                    Debe llenar todos los campos del formulario para registrarse
             
              </div>
        
            <?php           
        }
  
    }
    
    
    public function VML_estudiantes()
    {
         $data = array("estudiantes"=>$this->Mestudiante->get_estudiantes());
         
         $this->load->view("estudiante/VML_estudiante",$data);
         
    }
    

 
 
} 

?>