
<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Cproveedor extends CI_Controller 
{   
	public function __construct()
    {
        parent::__construct();
        $this->load->model("Mproveedor");
    }

    public function index()
    {    
        $page = 1;
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        $id_empresa = trim($_POST['id_empresa']);
        $id_gestion = trim($_POST['id_gestion']);
        
        $numrows = $this->Mproveedor->num_rows($id_empresa,$id_gestion); 
        $total_pages = ceil($numrows/$per_page);
      
        $data = array(
            "reload" => "Cproveedor/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "proveedors" => $this->Mproveedor->listar_paginacion($offset,$per_page,$id_empresa,$id_gestion)
            );
        
        $this->load->view("proveedor/VML_proveedor",$data);

    } 

    public function paginacion()
    {   
        $page = $_POST["page"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;

        $id_empresa = trim($_POST['id_empresa']);
        $id_gestion = trim($_POST['id_gestion']);

        $numrows = $this->Mproveedor->num_rows($id_empresa,$id_gestion); 

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "reload" => "Cproveedor/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "proveedors" => $this->Mproveedor->listar_paginacion($offset,$per_page,$id_empresa,$id_gestion)
            );
 
        $this->load->view("proveedor/VL_proveedor",$data); 
 
    }

    public function paginacion_find()
    {
        $page = $_POST["page"];
        $txt_buscar = $_POST["txt_buscar"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        $id_empresa = trim($_POST['id_empresa']);
        $id_gestion = trim($_POST['id_gestion']);
        
        $numrows = $this->Mproveedor->num_rows_find($txt_buscar,$id_empresa,$id_gestion);

        $total_pages = ceil($numrows/$per_page);
 
        $data = array
        (
            "txt_buscar" => $txt_buscar,
            "reload" => "proveedor/paginacion_find",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "proveedors" => $this->Mproveedor->listar_paginacion_find($txt_buscar,$offset,$per_page,$id_empresa,$id_gestion)
        );

        $this->load->view("proveedor/VLB_proveedor",$data);   
    }
 
    public function R_proveedor()
    {
        $this->load->view("proveedor/VR_proveedor");	  
    } 

    public function G_proveedor()
    {
         $id_usuario = trim($_POST["id_usuario"]); 
         $fecha = date("Y-m-d");
         $hora = date("H:i:s");

         $id_empresa = trim($_POST["id_empresa"]); 
         $id_gestion = trim($_POST["id_gestion"]); 
      
         $proveedors =  json_decode($_POST["lista"]);

      foreach($proveedors as $prov)
      { 
         $proveedor = $prov->proveedor; 
         $encargado = $prov->encargado; 
         $telefonos = $prov->telefonos; 
         $celulares = $prov->celulares; 
         $whatsapp = $prov->whatsapp; 
         $direccion = $prov->direccion; 
         $correo = $prov->correo; 
        
         $data = array( 
         "proveedor" => $proveedor, 
         "encargado" => $encargado, 
         "telefonos" => $telefonos, 
         "celulares" => $celulares, 
         "whatsapp" => $whatsapp, 
         "direccion" => $direccion, 
         "correo" => $correo, 
         "fecha_pr" => $fecha, 
         "hora_pr" => $hora, 
         "id_usuario" => $id_usuario,
         "id_empresa" => $id_empresa,
         "id_gestion" => $id_gestion
            ); 

        $this->Mproveedor->Reg_proveedor($data); 
 
      }

   
      ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script>
           
       setTimeout(function(){ $("#panel_modal_resultado_registrar").html(""); },1000);
       setTimeout(function(){ $("#myModal_Register").modal("hide"); },2000);
       setTimeout(function(){ cargar_datos(1); },3000);

      </script>
      
      <?php
      
    } 

    public function VD_proveedor()
    {
    	$id_proveedor = trim($_POST["id_proveedor"]);
    	$data = array("proveedors" => $this->Mproveedor->get_proveedor($id_proveedor));
        
		  $this->load->view("proveedor/VD_proveedor",$data); 
    }

    public function VE_proveedor()
    {
    	$id_proveedor = trim($_POST["id_proveedor"]);
    	$data = array("proveedors" => $this->Mproveedor->get_proveedor($id_proveedor),);
        
		$this->load->view("proveedor/VE_proveedor",$data); 
    }

    public function VU_proveedor()
    {
            $cont_up=0;
            $id_proveedor = trim($_POST["id_proveedor"]);
            $proveedor = trim($_POST["proveedor"]);
            $encargado = trim($_POST["encargado"]);
            $telefonos = trim($_POST["telefonos"]);
            $celulares = trim($_POST["celulares"]);
            $whatsapp = trim($_POST["whatsapp"]);
            $direccion = trim($_POST["direccion"]);
            $correo = trim($_POST["correo"]);

            if($proveedor!="")
            {
                $data = array("proveedor" => $proveedor); 
                $this->Mproveedor->Setting_proveedor($data,$id_proveedor);
                $cont_up++;
            }
    

            if($encargado!="")
            {
                $data = array("encargado" => $encargado); 
                $this->Mproveedor->Setting_proveedor($data,$id_proveedor);
                $cont_up++;
            }
    

            if($telefonos!="")
            {
                $data = array("telefonos" => $telefonos); 
                $this->Mproveedor->Setting_proveedor($data,$id_proveedor);
                $cont_up++;
            }
    

            if($celulares!="")
            {
                $data = array("celulares" => $celulares); 
                $this->Mproveedor->Setting_proveedor($data,$id_proveedor);
                $cont_up++;
            }
    

            if($whatsapp!="")
            {
                $data = array("whatsapp" => $whatsapp); 
                $this->Mproveedor->Setting_proveedor($data,$id_proveedor);
                $cont_up++;
            }
    

            if($direccion!="")
            {
                $data = array("direccion" => $direccion); 
                $this->Mproveedor->Setting_proveedor($data,$id_proveedor);
                $cont_up++;
            }
    

            if($correo!="")
            {
                $data = array("correo" => $correo); 
                $this->Mproveedor->Setting_proveedor($data,$id_proveedor);
                $cont_up++;
            }
    

            if($cont_up>0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡Edicion Correcta!</strong></h4> 
                Informacion Cambiada Correctamente
             
              </div>

              <script>
                setTimeout(function(){ $("#panel_modal_respuesta_editar").html(""); },1000);
                setTimeout(function(){ $("#myModal_Update").modal("hide"); },2000);
                setTimeout(function(){ cargar_datos(1); },3000);
              </script>

              <?php 
            }

            if($cont_up==0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
                Informacion sin cambios en la base de datos
             
              </div>
              <?php      
            }
  
 
    }

    public function VB_proveedor()
    {
    	$id_proveedor = trim($_POST["id_proveedor"]);

    	$this->Mproveedor-> Delete_proveedor($id_proveedor);

      ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Eliminación Correcta!</strong></h4> 
          Informacion Borrada Definitivamente
       
        </div>
      <?php    
    }

} 

?>
