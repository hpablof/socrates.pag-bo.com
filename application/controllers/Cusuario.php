
<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Cusuario extends CI_Controller 
{   
	public function __construct()
    {
        parent::__construct();
        $this->load->model("Musuario");
    }

    public function index()
    {    
        $page = 1;
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Musuario->num_rows(); 
        $total_pages = ceil($numrows/$per_page);
      
        $data = array(
            "reload" => "<?php echo base_url(); ?>Cusuario/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "usuarios" => $this->Musuario->listar_paginacion($offset,$per_page)
            );
        
        $this->load->view("assets/Header");  
        $this->load->view("usuario/VML_usuario",$data);
        $this->load->view("assets/Footer"); 

    } 

    public function paginacion()
    {   
        $page = $_POST["page"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Musuario->num_rows();

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "reload" => "<?php echo base_url(); ?>Cusuario/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "usuarios" => $this->Musuario->listar_paginacion($offset,$per_page)
            );
 
        $this->load->view("usuario/VL_usuario",$data); 
    

    }

    public function paginacion_find()
    {
        $page = $_POST["page"];
        $txt_buscar = $_POST["txt_buscar"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Musuario->num_rows_find($txt_buscar);

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "txt_buscar" => $txt_buscar,
            "reload" => "<?php echo base_url(); ?>usuario/paginacion_find",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "usuarios" => $this->Musuario->listar_paginacion_find($txt_buscar,$offset,$per_page)
            );

         $this->load->view("usuario/VLB_usuario",$data);   
    }
 
    public function R_usuario()
    {
          $data = array(
          "cargo_usuarios"=>$this->Musuario->get_cargo_usuarios());
          $this->load->view("usuario/VR_usuario",$data);  
    } 

    public function G_usuario()
    {
         $id_usuario = trim($_POST["id_usuario"]); 
         $fecha = date("Y-m-d");
         $hora = date("H:i:s");
      
         $usuarios =  json_decode($_POST["lista"]);

      foreach($usuarios as $usuario)
      { 
        $alias = $usuario->alias; 
        $email = $usuario->email; 
        $password = $usuario->password; 
        $id_cargo_usuario = $usuario->id_cargo_usuario; 
        $nombres = $usuario->nombres; 
        $apellidos = $usuario->apellidos; 
        $ci = $usuario->ci; 

        
        $data = array( 
        "alias" => $alias, 
        "email" => $email, 
        "password" => $password, 
        "id_cargo_usuario" => $id_cargo_usuario, 
        "nombres" => $nombres, 
        "apellidos" => $apellidos, 
        "ci" => $ci ); 

        $this->Musuario->Reg_usuario($data);
 
      }

     
      
      ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script>
 
       setTimeout(function(){ $("#panel_modal_resultado_registrar").html(""); },1000);
       setTimeout(function(){ $("#myModal_Register").modal("hide"); },2000);
       setTimeout(function(){ location.reload(); },3000);
 
      </script>
      
      <?php

    } 

    public function VD_usuario()
    {
    	$id_usuario = trim($_POST["id_usuario"]);
    	$data = array("usuarios" => $this->Musuario->get_usuario($id_usuario));
        
		  $this->load->view("usuario/VD_usuario",$data); 
    }

    public function VE_usuario()
    {
    	$id_usuario = trim($_POST["id_usuario"]);
    	$data = array("usuarios" => $this->Musuario->get_usuario($id_usuario),"cargo_usuarios" => $this->Musuario->get_cargo_usuarios(),);
        
		$this->load->view("usuario/VE_usuario",$data); 
    }

    public function VU_usuario()
    {
            $cont_up=0;
            $id_usuario = trim($_POST["id_usuario"]);
            $alias = trim($_POST["alias"]);
            $email = trim($_POST["email"]);
            $password = trim($_POST["password"]);
            $id_cargo_usuario = trim($_POST["id_cargo_usuario"]);
            $nombres = trim($_POST["nombres"]);
            $apellidos = trim($_POST["apellidos"]);
            $ci = trim($_POST["ci"]);

            if($alias!="")
            {
                $data = array("alias" => $alias); 
                $this->Musuario->Setting_usuario($data,$id_usuario);
                $cont_up++;
            }
    

            if($email!="")
            {
                $data = array("email" => $email); 
                $this->Musuario->Setting_usuario($data,$id_usuario);
                $cont_up++;
            }
    

            if($password!="")
            {
                $data = array("password" => $password); 
                $this->Musuario->Setting_usuario($data,$id_usuario);
                $cont_up++;
            }
    

            if($id_cargo_usuario!="")
            {
                $data = array("id_cargo_usuario" => $id_cargo_usuario); 
                $this->Musuario->Setting_usuario($data,$id_usuario);
                $cont_up++;
            }
    

            if($nombres!="")
            {
                $data = array("nombres" => $nombres); 
                $this->Musuario->Setting_usuario($data,$id_usuario);
                $cont_up++;
            }
    

            if($apellidos!="")
            {
                $data = array("apellidos" => $apellidos); 
                $this->Musuario->Setting_usuario($data,$id_usuario);
                $cont_up++;
            }
    

            if($ci!="")
            {
                $data = array("ci" => $ci); 
                $this->Musuario->Setting_usuario($data,$id_usuario);
                $cont_up++;
            }
    

            if($cont_up>0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡Edicion Correcta!</strong></h4> 
                Informacion Cambiada Correctamente
             
              </div>

              <script>
                setTimeout(function(){ $("#panel_modal_respuesta_editar").html(""); },1000);
                setTimeout(function(){ $("#myModal_Update").modal("hide"); },2000);
                setTimeout(function(){ location.reload(); },3000); 
              </script>

              <?php 
            }

            if($cont_up==0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
                Informacion sin cambios en la base de datos
             
              </div>
              <?php      
            }
  
 
    }

    public function VB_usuario()
    {
    	$id_usuario = trim($_POST["id_usuario"]);

    	$this->Musuario-> Delete_usuario($id_usuario);

      ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Eliminación Correcta!</strong></h4> 
          Informacion Borrada Definitivamente
       
        </div>
      <?php    
    }

} 

?>
