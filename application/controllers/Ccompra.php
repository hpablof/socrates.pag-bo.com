
<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Ccompra extends CI_Controller 
{   
	public function __construct()
    {
        parent::__construct();
        $this->load->model("Mcompra");
        $this->load->model("Mproveedor");
        $this->load->model("Mproducto");

    }

    public function index()
    {    
        $page = 1;
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;

        $id_empresa = trim($_POST['id_empresa']);
        $id_gestion = trim($_POST['id_gestion']);
        
        $numrows = $this->Mcompra->num_rows($id_empresa,$id_gestion); 
        $total_pages = ceil($numrows/$per_page);
      
        $data = array(
            "reload" => "Ccompra/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "compras" => $this->Mcompra->listar_paginacion($offset,$per_page,$id_empresa,$id_gestion)
            );
        
        //print_r($data);

        $this->load->view("compra/VML_compra",$data);
    } 

    public function paginacion()
    {   
        $page = $_POST["page"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;

        $id_empresa = trim($_POST['id_empresa']);
        $id_gestion = trim($_POST['id_gestion']);
        
        $numrows = $this->Mcompra->num_rows($id_empresa,$id_gestion);

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "reload" => "Ccompra/paginacion",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "compras" => $this->Mcompra->listar_paginacion($offset,$per_page,$id_empresa,$id_gestion)
            );
 
        $this->load->view("compra/VL_compra",$data); 
    }

    public function paginacion_find()
    {
        $page = trim($_POST["page"]);
        $txt_buscar = trim($_POST["txt_buscar"]);
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1)*$per_page;

        $id_empresa = trim($_POST['id_empresa']);
        $id_gestion = trim($_POST['id_gestion']);

        $numrows = $this->Mcompra->num_rows_find($txt_buscar,$id_empresa,$id_gestion);

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "txt_buscar" => $txt_buscar,
            "reload" => "Ccompra/paginacion_find",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "compras" => $this->Mcompra->listar_paginacion_find($txt_buscar,$offset,$per_page,$id_empresa,$id_gestion)
            );

         $this->load->view("compra/VLB_compra",$data);   
    }
 
    public function R_compra()
    {
          $this->load->view("compra/VR_compra");  
    } 

    public function G_compra()
    {
         $id_usuario = trim($_POST["id_usuario"]);
         $id_empresa = trim($_POST["id_empresa"]);
         $id_gestion = trim($_POST["id_gestion"]);
         $id_proveedor = trim($_POST["id_proveedor"]);

         $total_compra = trim($_POST['total_compra']);
         $pago_compra = trim($_POST['pago_compra']);
         $cambio_compra = trim($_POST['cambio_compra']);


         $fecha_compra = date("Y-m-d");
         $hora_compra = date("H:i:s");
         $codigo_compra = "C".date("YmdHis");
          

         $compras =  json_decode($_POST["lista"]);

      foreach($compras as $dcompra)
      { 
         $id_producto = $dcompra->id_producto; 
         $producto = $dcompra->producto; 
         $cantidad = $dcompra->cantidad; 
         $precio_compra = $dcompra->precio_compra; 
         $precio_venta = $dcompra->precio_venta; 
         $subtotal_compra = $dcompra->subtotal_compra;
         $fecha_vencimiento = $dcompra->fecha_vencimiento; 
         
         $data_detalle = array
         ( 
          "id_producto" => $id_producto,     
          "codigo_compra" => $codigo_compra,   
          "cantidad_dc" => $cantidad,     
          "precio_dc" => $precio_compra,   
          "precio_venta_dc" => $precio_venta,     
          "subtotal_dc" => $subtotal_compra,     
          "id_usuario" => $id_usuario,  
          "fecha_dc" => $fecha_compra,    
          "hora_dc" => $hora_compra,     
          "id_gestion" => $id_gestion,  
          "id_empresa" => $id_empresa 
         ); 

         $this->Mcompra->Reg_detalle_compra($data_detalle);

         $prod = $this->Mproducto->get_producto($id_producto);
         $cantidad_p = $prod[0]->cantidad;

         $suma_compra = $cantidad_p+$cantidad;
         $data_prod = array(
         "cantidad"  => $suma_compra, 
         "precio_compra"  => $precio_compra, 
         "precio_venta"  => $precio_venta);

         $this->Mproducto->Setting_producto($data_prod,$id_producto);
 
      }

         $data_compra = array
         ( 
         "codigo_compra" => $codigo_compra,   
         "id_proveedor" => $id_proveedor,    
         "total_compra" => $total_compra,    
         "pago_compra" => $pago_compra,     
         "cambio_compra" => $cambio_compra,     
         "fecha_compra" => $fecha_compra,    
         "hora_compra" => $hora_compra,     
         "id_gestion" => $id_gestion,  
         "id_empresa" => $id_empresa,  
         "id_usuario" => $id_usuario 
         );
    
        $this->Mcompra->Reg_compra($data_compra);
 
      ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script>
           
       setTimeout(function(){ $("#panel_modal_resultado_registrar").html(""); },1000);
       setTimeout(function(){ $("#myModal_Register").modal("hide"); },2000);
       setTimeout(function(){ cargar_datos(1); },3000);

      </script>
      
      <?php
       
    } 

    public function VD_compra()
    {
    	$id_compra = trim($_POST["id_compra"]);
      $data = array("compras" => $this->Mcompra->get_compra($id_compra));
		  $this->load->view("compra/VD_compra",$data); 

    }

    public function VE_compra()
    {
    	$id_compra = trim($_POST["id_compra"]);
      $data = array("compras" => $this->Mcompra->get_compra($id_compra));
		  $this->load->view("compra/VE_compra",$data); 
    }

    public function VU_compra()
    {
            $cont_up=0;
            $id_detalle_compra = trim($_POST["id_detalle_compra"]);
            $id_producto = trim($_POST["id_producto"]);
            $codigo_compra = trim($_POST["codigo_compra"]);
            $cantidad_dc = trim($_POST["cantidad_dc"]);
            $precio_dc = trim($_POST["precio_dc"]);
            $precio_venta_dc = trim($_POST["precio_venta_dc"]);
            $subtotal_dc = trim($_POST["subtotal_dc"]);

            if($id_producto!="")
            {
                $data = array("id_producto" => $id_producto); 
                $this->Mdetalle_compra->Setting_detalle_compra($data,$id_detalle_compra);
                $cont_up++;
            }
    

            if($codigo_compra!="")
            {
                $data = array("codigo_compra" => $codigo_compra); 
                $this->Mdetalle_compra->Setting_detalle_compra($data,$id_detalle_compra);
                $cont_up++;
            }
    

            if($cantidad_dc!="")
            {
                $data = array("cantidad_dc" => $cantidad_dc); 
                $this->Mdetalle_compra->Setting_detalle_compra($data,$id_detalle_compra);
                $cont_up++;
            }
    

            if($precio_dc!="")
            {
                $data = array("precio_dc" => $precio_dc); 
                $this->Mdetalle_compra->Setting_detalle_compra($data,$id_detalle_compra);
                $cont_up++;
            }
    

            if($precio_venta_dc!="")
            {
                $data = array("precio_venta_dc" => $precio_venta_dc); 
                $this->Mdetalle_compra->Setting_detalle_compra($data,$id_detalle_compra);
                $cont_up++;
            }
    

            if($subtotal_dc!="")
            {
                $data = array("subtotal_dc" => $subtotal_dc); 
                $this->Mdetalle_compra->Setting_detalle_compra($data,$id_detalle_compra);
                $cont_up++;
            }
    

            if($cont_up>0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡Edicion Correcta!</strong></h4> 
                Informacion Cambiada Correctamente
             
              </div>

              <script>
                setTimeout(function(){ $("#panel_modal_respuesta_editar").html(""); },1000);
                setTimeout(function(){ $("#myModal_Update").modal("hide"); },2000);
                setTimeout(function(){ cargar_datos(1); },3000); 
              </script>

              <?php 
            }

            if($cont_up==0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
                Informacion sin cambios en la base de datos
             
              </div>
              <?php      
            }
 
    }

    public function VB_compra()
    {
      $codigo_compra = trim($_POST["codigo_compra"]); 
      $detalle_compra = $this->Mcompra->get_detalle_compra($codigo_compra);
      
      foreach ($detalle_compra as $dc) 
      {
         $id_producto = $dc->id_producto;
         $cantidad_dc = $dc->cantidad_dc;

         $prod = $this->Mproducto->get_producto($id_producto);
         $cantidad_stock = $prod[0]->cantidad;

         if($cantidad_stock>=$cantidad_dc)
         {
           $resta_stock = $cantidad_stock-$cantidad_dc;
           $data_prod = array("cantidad"  => $resta_stock);
           $this->Mproducto->Setting_producto($data_prod,$id_producto);

         }
         else{
           $resta_stock = 0;
           $data_prod = array("cantidad"  => $resta_stock);
           $this->Mproducto->Setting_producto($data_prod,$id_producto);
         } 

      }
 
    	$this->Mcompra-> Delete_compra($codigo_compra);
      $this->Mcompra-> Delete_detalle_compra($codigo_compra);
 
      ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Eliminación Correcta!</strong></h4> 
          Informacion Borrada Definitivamente
       
        </div>
      <?php
        
    }


    public function VB_proveedor()
    {
      $txt_buscar = trim($_POST['txt_buscar']);
      $id_empresa = trim($_POST['id_empresa']);
      $id_gestion = trim($_POST['id_gestion']);

      $data = array("opcion"=>"listar_proveedor", "proveedors" => $this->Mproveedor->buscar_proveedor($txt_buscar,$id_empresa,$id_gestion), "txt_buscar" => $txt_buscar );
        
      $this->load->view("compra/VO_compra",$data);       
         
    }

    public function VB_producto()
    {
      $txt_buscar = trim($_POST['txt_buscar']);
      $id_empresa = trim($_POST['id_empresa']);
      $id_gestion = trim($_POST['id_gestion']);

      $data = array("opcion"=>"listar_productos", "productos" => $this->Mproducto->buscar_producto($txt_buscar,$id_empresa,$id_gestion), "txt_buscar" => $txt_buscar );
        
      $this->load->view("compra/VO_compra",$data);      
         
    }

    /* FUNCIONES DE PROVEEDOR */

    public function R_proveedor()
    {
      $data = array("opcion"=>"mostrar_proveedor");
      $this->load->view("compra/VO_compra",$data);      
    }

    public function G_proveedor()
    {
      $proveedor = trim($_POST['proveedor']);
      $encargado = trim($_POST['encargado']);
      $telefonos = trim($_POST['telefonos']);
      $celulares = trim($_POST['celulares']);
      $whatsapp  = trim($_POST['whatsapp']);
      $direccion = trim($_POST['direccion']);
      $correo    = trim($_POST['correo']);

      $id_empresa = trim($_POST['id_empresa']);
      $id_gestion = trim($_POST['id_gestion']);
      $id_usuario = trim($_POST['id_usuario']);

      $fecha = date('Y-m-d');
      $hora = date('H:i:s');

      $data = array( 
      "proveedor" => $proveedor, 
      "encargado" => $encargado, 
      "telefonos" => $telefonos, 
      "celulares" => $celulares, 
      "whatsapp" => $whatsapp, 
      "direccion" => $direccion, 
      "correo" => $correo, 
      "fecha_pr" => $fecha, 
      "hora_pr" => $hora, 
      "id_usuario" => $id_usuario,
      "id_empresa" => $id_empresa,
      "id_gestion" => $id_gestion
        ); 

      $this->Mproveedor->Reg_proveedor($data);

      ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script type="text/javascript">
         setTimeout(function()
         {
           $("#myModal_Register_Proveedor").modal('hide'); 
         },2000);
         
      </script>

      <?php
    
      

    }


    /* FUNCIONES DE PRODUCTO */

    public function R_producto()
    {
      $id_empresa = trim($_POST['id_empresa']);
      $id_gestion = trim($_POST['id_gestion']);

      $data = array("opcion"=>"mostrar_producto",
      "areas"=>$this->Mproducto->get_areas($id_empresa, $id_gestion));

      $this->load->view("compra/VO_compra",$data);     
    }

    public function G_producto()
    {

      $id_empresa = trim($_POST["id_empresa"]);
      $id_gestion = trim($_POST["id_gestion"]);
      $producto = trim($_POST["producto"]);

      $temp = $_FILES["portada"]["tmp_name"];

      $portada = $_FILES['portada'];
      $extension = pathinfo($portada['name'], PATHINFO_EXTENSION);
      $time = time();
      $logo_producto = "p-$time.$extension";

      move_uploaded_file($temp, "assets/multimedia/portadas/".$logo_producto);

      $codigo_bar = trim($_POST["codigo_bar"]);
      $cantidad = trim($_POST["cantidad"]);
      $precio_compra = trim($_POST["precio_compra"]);
      $precio_venta = trim($_POST["precio_venta"]);
      $descripcion = trim($_POST["descripcion"]);
      $id_area = trim($_POST["id_area"]);
      $id_usuario = trim($_POST["id_usuario"]); 
      $fecha = date("Y-m-d");
      $hora = date("H:i:s");

      $data = array( 
      "producto" => $producto, 
      "portada" => $logo_producto, 
      "codigo_bar" => $codigo_bar, 
      "cantidad" => $cantidad, 
      "precio_compra" => $precio_compra, 
      "precio_venta" => $precio_venta, 
      "descripcion" => $descripcion, 
      "id_area" => $id_area, 
      "fecha_p" => $fecha, 
      "hora_p" => $hora, 
      "id_usuario" => $id_usuario,
      "id_empresa" => $id_empresa,
      "id_gestion" => $id_gestion
        ); 

      $this->Mproducto->Reg_producto($data);

      ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script>
           
       setTimeout(function(){ $("#panel_modal_resultado_registrar_Producto").html(""); },1500);
       setTimeout(function(){ $("#myModal_Register_Producto").modal("hide"); },2000);
        
      </script>
      <?php          

    }

} 

?>
