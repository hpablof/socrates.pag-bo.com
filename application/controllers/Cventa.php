
<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Cventa extends CI_Controller 
{   
	  public function __construct()
    {
      parent::__construct();
      $this->load->model("Mventa");
      $this->load->model("Mcliente");
      $this->load->model("Mproducto");
    }

    public function index()
    {    
      $page = 1;
      $per_page = 10;  
      $adjacents  = 4; 
      $offset = ($page - 1) * $per_page;

      $id_empresa = trim($_POST['id_empresa']);
      $id_gestion = trim($_POST['id_gestion']);
        
      $numrows = $this->Mventa->num_rows($id_empresa,$id_gestion); 
      $total_pages = ceil($numrows/$per_page);
      
      $data = array(
            "reload" => "Cventa/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "ventas" => $this->Mventa->listar_paginacion($offset,$per_page,$id_empresa,$id_gestion)
            );

      $this->load->view("venta/VML_venta",$data);

    } 
 
    public function paginacion()
    {   
      $page = $_POST["page"];
      $per_page = 10;  
      $adjacents  = 4; 
      $offset = ($page - 1) * $per_page;

      $id_empresa = trim($_POST['id_empresa']);
      $id_gestion = trim($_POST['id_gestion']);
        
      $numrows = $this->Mventa->num_rows($id_empresa,$id_gestion); 

      $total_pages = ceil($numrows/$per_page);
 
      $data = array(
            "reload" => "Cventa/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "ventas" => $this->Mventa->listar_paginacion($offset,$per_page,$id_empresa,$id_gestion)
            );
 
      $this->load->view("venta/VL_venta",$data); 

    }

    public function paginacion_find()
    {
      $page = $_POST["page"];
      $txt_buscar = trim($_POST["txt_buscar"]);
      $per_page = 10;  
      $adjacents  = 4; 
      $offset = ($page - 1) * $per_page;
        
      $id_empresa = trim($_POST['id_empresa']);
      $id_gestion = trim($_POST['id_gestion']);

      $numrows = $this->Mventa->num_rows_find($txt_buscar,$id_empresa,$id_gestion);
       
      $total_pages = ceil($numrows/$per_page);
 
      $data = array(
            "txt_buscar" => $txt_buscar,
            "reload" => "Cventa/paginacion_find",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "ventas" => $this->Mventa->listar_paginacion_find($txt_buscar,$offset,$per_page,$id_empresa,$id_gestion)
            );

      $this->load->view("venta/VLB_venta",$data);   
    } 
 
    public function R_detalle_venta()
    {
      $this->load->view("venta/VR_venta");
    } 

    public function G_venta()
    {
      $id_usuario = trim($_POST["id_usuario"]);
      $id_empresa = trim($_POST["id_empresa"]); 
      $id_gestion = trim($_POST["id_gestion"]);  
      $id_cliente = trim($_POST["id_cliente"]);

      $total_venta = trim($_POST["total_venta"]);
      $pago_venta = trim($_POST["pago_venta"]);
      $cambio_venta = trim($_POST["cambio_venta"]);
      
      $codigo_venta = "V".date("YmdHis");
      $fecha_venta = date("Y-m-d");
      $hora_venta = date("H:i:s");
    
      $ventas =  json_decode($_POST["lista"]);

      foreach($ventas as $dventa)
      { 
        $id_producto = $dventa->id_producto; 
        $cantidad_venta = $dventa->cantidad_venta; 
        $precio_venta = $dventa->precio_venta; 
        $subtotal_venta = $dventa->subtotal_venta; 

        $data = array( 
        "id_producto" => $id_producto,
        "codigo_venta" => $codigo_venta, 
        "cantidad_dv" => $cantidad_venta, 
        "precio_venta_dv" => $precio_venta, 
        "subtotal_dv" => $subtotal_venta, 
        "fecha_dv" => $fecha_venta, 
        "hora_dv" => $hora_venta, 
        "id_usuario" => $id_usuario,
        "id_empresa" => $id_empresa,
        "id_gestion" => $id_gestion
        ); 

        $this->Mventa->Reg_detalle_venta($data);
      }

      $dat_venta = array
      (
        "codigo_venta" => $codigo_venta,  
        "id_cliente" => $id_cliente,  
        "total_venta" => $total_venta,   
        "pago_venta" => $pago_venta,  
        "cambio_venta" => $cambio_venta,  
        "estado_venta" => 1,  
        "fecha_venta" => $fecha_venta,   
        "hora_venta" => $hora_venta,  
        "id_usuario" => $id_usuario,
        "id_empresa" => $id_empresa,
        "id_gestion" => $id_gestion
      );

      $this->Mventa->Reg_venta($dat_venta);
     
      ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script>
         
       setTimeout(function(){ $("#panel_modal_resultado_registrar").html(""); },1000);
       setTimeout(function(){ $("#myModal_Register").modal("hide"); },2000);
       setTimeout(function(){ btn_imprimir_venta('<?php echo $codigo_venta; ?>'); },2500);
       setTimeout(function(){ cargar_datos(1); },3000);
       
      </script>
      
      <?php

    } 

    public function VD_venta()
    {
    	$id_venta = trim($_POST["id_venta"]);
    	$data = array("ventas" => $this->Mventa->get_venta($id_venta));
        
		  $this->load->view("venta/VD_venta",$data); 
    }

    public function VE_detalle_venta()
    {
      $id_venta = trim($_POST["id_venta"]);
      $data = array("ventas" => $this->Mventa->get_venta($id_venta));
        
      $this->load->view("venta/VE_venta",$data); 
    }

    public function VU_detalle_venta()
    {
      $cont_up=0;
      $id_detalle_venta = trim($_POST["id_detalle_venta"]);
      $id_producto = trim($_POST["id_producto"]);
      $cantidad_dv = trim($_POST["cantidad_dv"]);
      $precio_venta_dv = trim($_POST["precio_venta_dv"]);
      $subtotal_dv = trim($_POST["subtotal_dv"]);

      if($id_producto!="")
      {
          $data = array("id_producto" => $id_producto); 
          $this->Mdetalle_venta->Setting_detalle_venta($data,$id_detalle_venta);
          $cont_up++;
      }


      if($cantidad_dv!="")
      {
          $data = array("cantidad_dv" => $cantidad_dv); 
          $this->Mdetalle_venta->Setting_detalle_venta($data,$id_detalle_venta);
          $cont_up++;
      }


      if($precio_venta_dv!="")
      {
          $data = array("precio_venta_dv" => $precio_venta_dv); 
          $this->Mdetalle_venta->Setting_detalle_venta($data,$id_detalle_venta);
          $cont_up++;
      }


      if($subtotal_dv!="")
      {
          $data = array("subtotal_dv" => $subtotal_dv); 
          $this->Mdetalle_venta->Setting_detalle_venta($data,$id_detalle_venta);
          $cont_up++;
      }


      if($cont_up>0)
      {
        ?>
        <p class="transparent"> espacio </p>
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Edicion Correcta!</strong></h4> 
          Informacion Cambiada Correctamente
       
        </div>

        <script>
          setTimeout(function(){ $("#panel_modal_respuesta_editar").html(""); },1000);
          setTimeout(function(){ $("#myModal_Update").modal("hide"); },2000);
          setTimeout(function(){ cargar_datos(1); },3000); 
        </script>

        <?php 
      }

      if($cont_up==0)
      {
        ?>
        <p class="transparent"> espacio </p>
        <div class="alert alert-warning alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
          Informacion sin cambios en la base de datos
       
        </div>
        <?php      
      }
  
    }

    public function VB_venta()
    {
    	
      $codigo_venta = trim($_POST["codigo_venta"]);

      $detalle_venta = $this->Mventa->get_detalle_ventas($codigo_venta);

      foreach ($detalle_venta as $dv) 
      {
        $id_producto = $dv->id_producto;
        $cantidad_dv = $dv->cantidad_dv;  

        $prod = $this->Mproducto->get_producto($id_producto);
        $stock_actual = $prod[0]->cantidad; echo "</br>";

        $suma_stock = $stock_actual+$cantidad_dv;
        $data_prod = array("cantidad"  => $suma_stock);
        $this->Mproducto->Setting_producto($data_prod,$id_producto); 

      }
 
      $this->Mventa->Delete_venta_lista($codigo_venta);
      $this->Mventa->Delete_detalle_venta($codigo_venta);

      ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Eliminación Correcta!</strong></h4> 
          Informacion Borrada Definitivamente
       
        </div>
        
        <script type="text/javascript">

          setTimeout(function(){ $("#panel_modal_eliminar").html(""); },1000);
          setTimeout(function(){ $("#myModal_Delete").modal("show"); },2000);
          setTimeout(function(){ cargar_datos(1); },3000);

        </script>

      <?php     
    }

    public function VB_cliente()
    {
       
      $txt_buscar = trim($_POST['txt_buscar']);
      $id_empresa = trim($_POST['id_empresa']);

      $data = array("opcion"=>"listar_clientes", "txt_buscar" => $txt_buscar, "clientes" => $this->Mcliente->buscar_cliente($txt_buscar,$id_empresa));

      $this->load->view("venta/VO_venta",$data); 

    }

    public function VB_producto_venta()
    {
      $txt_buscar = trim($_POST['txt_buscar']);
      $id_empresa = trim($_POST['id_empresa']);
      $id_gestion = trim($_POST['id_gestion']);
     
      $data = array("opcion"=>"listar_productos", "productos" => $this->Mproducto->buscar_producto($txt_buscar,$id_empresa,$id_gestion), "txt_buscar" => $txt_buscar );
 
      $this->load->view("venta/VO_venta",$data);   
         
    }

    public function recibo_venta()
    {
      $codigo_venta = trim($_GET['codigo_venta']);

      $data = array("opcion"=>"recibo_venta", 
      "ventas" => $this->Mventa->get_ventas($codigo_venta));
  
      $this->load->view("venta/VO_venta",$data);     

    }

    /* FUNCIONES DE CLIENTE */

    public function R_cliente()
    {
      $data = array("opcion"=>"mostrar_cliente");
      $this->load->view("venta/VO_venta",$data); 
    }

    public function G_cliente()
    {

     $id_usuario = trim($_POST["id_usuario"]); 
     $id_empresa = trim($_POST["id_empresa"]);  
     $id_gestion = trim($_POST["id_gestion"]);  

     $fecha = date("Y-m-d");
     $hora = date("H:i:s");

     $cliente = trim($_POST["cliente"]); 
     $ci_nit = trim($_POST["ci_nit"]); 
     $celular_cl = trim($_POST["celular_cl"]); 
     $whatsapp_cl = trim($_POST["whatsapp_cl"]); 
     $direccion_cl = trim($_POST["direccion_cl"]); 
     $correo_cl = trim($_POST["correo_cl"]); 


     $data = array( 
      "cliente" => $cliente, 
      "ci_nit" => $ci_nit, 
      "celular_cl" => $celular_cl, 
      "whatsapp_cl" => $whatsapp_cl, 
      "direccion_cl" => $direccion_cl, 
      "correo_cl" => $correo_cl, 
      "fecha_cl" => $fecha, 
      "hora_cl" => $hora, 
      "id_usuario" => $id_usuario,
      "id_empresa" => $id_empresa,
      "id_gestion" => $id_gestion
          ); 

     $this->Mcliente->Reg_cliente($data);

     ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script>
           
       setTimeout(function(){ $("#panel_modal_resultado_registrar_cliente").html(""); },1500);
       setTimeout(function(){ $("#myModal_Register_Cliente").modal("hide"); },2000);
        
      </script>
     <?php

    }
} 

?>
