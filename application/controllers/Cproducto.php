
<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Cproducto extends CI_Controller 
{   
	  public function __construct()
    {
        parent::__construct();
        $this->load->model("Mproducto");
        $this->load->model("Marea");  
        /*$this->load->model("Mcompra"); 
        $this->load->model("Mventa"); */
    }

    public function index()
    {    
        $page = 1;
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        $id_empresa = trim($_POST['id_empresa']);
        $id_gestion = trim($_POST['id_gestion']);
        
        $numrows = $this->Mproducto->num_rows($id_empresa,$id_gestion); 
        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "reload" => "Cproducto/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "productos" => $this->Mproducto->listar_paginacion($offset,$per_page,$id_empresa,$id_gestion)
        );

        $this->load->view("producto/VML_producto",$data);
 
    } 

    public function paginacion()
    {   
        $page = $_POST["page"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;

        $id_empresa = trim($_POST['id_empresa']);
        $id_gestion = trim($_POST['id_gestion']);
        
        $numrows = $this->Mproducto->num_rows($id_empresa,$id_gestion);

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "reload" => "<?php echo base_url(); ?>Cproducto/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "productos" => $this->Mproducto->listar_paginacion($offset,$per_page,$id_empresa,$id_gestion)
            );
 
        $this->load->view("producto/VL_producto",$data); 
 
    }

    public function paginacion_find()
    {
        $page = $_POST["page"];
        $txt_buscar = $_POST["txt_buscar"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;

        $id_empresa = trim($_POST['id_empresa']);
        $id_gestion = trim($_POST['id_gestion']);
        
        $numrows = $this->Mproducto->num_rows_find($txt_buscar,$id_empresa,$id_gestion);

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "txt_buscar" => $txt_buscar,
            "reload" => "producto/paginacion_find",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "productos" => $this->Mproducto->listar_paginacion_find($txt_buscar,$offset,$per_page,$id_empresa,$id_gestion)
            );

         $this->load->view("producto/VLB_producto",$data);   
    }
 
    public function R_producto()
    {     
          $id_empresa = trim($_POST['id_empresa']);
          $id_gestion = trim($_POST['id_gestion']);

          $data = array(
          "areas"=>$this->Mproducto->get_areas($id_empresa,$id_gestion));
          $this->load->view("producto/VR_producto",$data);
    } 

    public function G_producto()
    {
          $id_empresa = trim($_POST["id_empresa"]);
          $id_gestion = trim($_POST["id_gestion"]);
          $producto = trim($_POST["producto"]);
 
          $temp = $_FILES["portada"]["tmp_name"];

          $portada = $_FILES['portada'];
          $extension = pathinfo($portada['name'], PATHINFO_EXTENSION);
          $time = time();
          $logo_producto = "p-$time.$extension";

          move_uploaded_file($temp, "assets/multimedia/portadas/".$logo_producto);

          $codigo_bar = trim($_POST["codigo_bar"]);
          $cantidad = trim($_POST["cantidad"]);
          $precio_compra = trim($_POST["precio_compra"]);
          $precio_venta = trim($_POST["precio_venta"]);
          $descripcion = trim($_POST["descripcion"]);
          $id_area = trim($_POST["id_area"]);
          $id_usuario = trim($_POST["id_usuario"]); 
          $fecha = date("Y-m-d");
          $hora = date("H:i:s");

          $data = array( 
          "producto" => $producto, 
          "portada" => $logo_producto, 
          "codigo_bar" => $codigo_bar, 
          "cantidad" => $cantidad, 
          "precio_compra" => $precio_compra, 
          "precio_venta" => $precio_venta, 
          "descripcion" => $descripcion, 
          "id_area" => $id_area, 
          "fecha_p" => $fecha, 
          "hora_p" => $hora, 
          "id_usuario" => $id_usuario,
          "id_empresa" => $id_empresa,
          "id_gestion" => $id_gestion
            ); 
          
          $this->Mproducto->Reg_producto($data);
      
      ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script>
           
       setTimeout(function(){ $("#panel_modal_resultado_registrar").html(""); },1000);
       setTimeout(function(){ $("#myModal_Register").modal("hide"); },2000);
       setTimeout(function(){ cargar_datos(1); },2500);

      </script>
      <?php
     
    } 

    public function VD_producto()
    {
    	$id_producto = trim($_POST["id_producto"]);
    	$data = array("productos" => $this->Mproducto->get_producto($id_producto));
        
		  $this->load->view("producto/VD_producto",$data); 
    }

    public function VE_producto()
    {
    	$id_producto = trim($_POST["id_producto"]);
      $id_empresa = trim($_POST["id_empresa"]);

    	$data = array("productos" => $this->Mproducto->get_producto($id_producto),
        "areas" => $this->Mproducto->get_areas($id_empresa),);
        
		  $this->load->view("producto/VE_producto",$data); 
    }

    public function VU_producto()
    {
            $cont_up=0;
            $id_producto = trim($_POST["id_producto"]);
            $producto = trim($_POST["producto"]);

            $portada = trim($_FILES["portada"]["name"]); 
            $temp = $_FILES["portada"]["tmp_name"];
            move_uploaded_file($temp, "assets/multimedia/portadas/".$portada);

            
            $codigo_bar = trim($_POST["codigo_bar"]);
            $cantidad = trim($_POST["cantidad"]);
            $precio_compra = trim($_POST["precio_compra"]);
            $precio_venta = trim($_POST["precio_venta"]);
            $descripcion = trim($_POST["descripcion"]);
            $id_area = trim($_POST["id_area"]);

            if($producto!="")
            {
                $data = array("producto" => $producto); 
                $this->Mproducto->Setting_producto($data,$id_producto);
                $cont_up++;
            }
    

            if($portada!="")
            {
                $data = array("portada" => $portada); 
                $this->Mproducto->Setting_producto($data,$id_producto);
                $cont_up++;
            }
    

            if($codigo_bar!="")
            {
                $data = array("codigo_bar" => $codigo_bar); 
                $this->Mproducto->Setting_producto($data,$id_producto);
                $cont_up++;
            }
    

            if($cantidad!="")
            {
                $data = array("cantidad" => $cantidad); 
                $this->Mproducto->Setting_producto($data,$id_producto);
                $cont_up++;
            }
    

            if($precio_compra!="")
            {
                $data = array("precio_compra" => $precio_compra); 
                $this->Mproducto->Setting_producto($data,$id_producto);
                $cont_up++;
            }
    

            if($precio_venta!="")
            {
                $data = array("precio_venta" => $precio_venta); 
                $this->Mproducto->Setting_producto($data,$id_producto);
                $cont_up++;
            }
    

            if($descripcion!="")
            {
                $data = array("descripcion" => $descripcion); 
                $this->Mproducto->Setting_producto($data,$id_producto);
                $cont_up++;
            }
    

            if($id_area!="")
            {
                $data = array("id_area" => $id_area); 
                $this->Mproducto->Setting_producto($data,$id_producto);
                $cont_up++;
            }
    

            if($cont_up>0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡Edicion Correcta!</strong></h4> 
                Informacion Cambiada Correctamente
             
              </div>

              <script>
                setTimeout(function(){ $("#panel_modal_respuesta_editar").html(""); },1000);
                setTimeout(function(){ $("#myModal_Update").modal("hide"); },2000);
                setTimeout(function(){ location.reload(); },3000); 
              </script>

              <?php 
            }

            if($cont_up==0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
                Informacion sin cambios en la base de datos
             
              </div>
              <?php      
            }
  
 
    }

    public function VB_producto()
    {
    	$id_producto = trim($_POST["id_producto"]);

      $dc = $this->Mcompra->get_compra_producto($id_producto);
      $dv = $this->Mventa->get_venta_producto($id_producto);

      $cont_prod = 0;

      if(sizeof($dc)>0){ $cont_prod++; }

      if(sizeof($dv)>0){ $cont_prod++; }

      if($cont_prod>0)
      {
        
      ?>
        <div class="alert alert-warning alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡No Se Puede Eliminar!</strong></h4> 
           El producto contiene ventas oh compras que eliminar
       
        </div>

      <?php 

      }

      if($cont_prod==0){
        $this->Mproducto-> Delete_producto($id_producto);
      ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Eliminación Correcta!</strong></h4> 
          Informacion Borrada Definitivamente
       
        </div>
        <script type="text/javascript">

          setTimeout(function(){ $("#panel_modal_eliminar").html(""); },1000);
          setTimeout(function(){ $("#myModal_Delete").modal("show"); },2000);
          setTimeout(function(){ location.reload(); },1000);
          
        </script>        
      <?php 
      }

    } 

} 

?>
