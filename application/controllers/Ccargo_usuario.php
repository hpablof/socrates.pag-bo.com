
<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Ccargo_usuario extends CI_Controller 
{   
	public function __construct()
    {
        parent::__construct();
        $this->load->model("Mcargo_usuario");
    }

    public function index()
    {    
        $page = 1;
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mcargo_usuario->num_rows(); 
        $total_pages = ceil($numrows/$per_page);
      
        $data = array(
            "reload" => "<?php echo base_url(); ?>Ccargo_usuario/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "cargo_usuarios" => $this->Mcargo_usuario->listar_paginacion($offset,$per_page)
            );
        
        $this->load->view("assets/Header");  
        $this->load->view("cargo_usuario/VML_cargo_usuario",$data);
        $this->load->view("assets/Footer"); 

    } 

    public function paginacion()
    {   
        $page = $_POST["page"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mcargo_usuario->num_rows();

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "reload" => "<?php echo base_url(); ?>Ccargo_usuario/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "cargo_usuarios" => $this->Mcargo_usuario->listar_paginacion($offset,$per_page)
            );
 
        $this->load->view("cargo_usuario/VL_cargo_usuario",$data); 
    

    }

    public function paginacion_find()
    {
        $page = $_POST["page"];
        $txt_buscar = $_POST["txt_buscar"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mcargo_usuario->num_rows_find($txt_buscar);

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "txt_buscar" => $txt_buscar,
            "reload" => "<?php echo base_url(); ?>cargo_usuario/paginacion_find",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "cargo_usuarios" => $this->Mcargo_usuario->listar_paginacion_find($txt_buscar,$offset,$per_page)
            );

         $this->load->view("cargo_usuario/VLB_cargo_usuario",$data);   
    }
 
    public function R_cargo_usuario()
    {
          $this->load->view("cargo_usuario/VR_cargo_usuario");

		  
    } 

    public function G_cargo_usuario()
    {
          $cargo_usuario = trim($_POST["cargo_usuario"]);
          $id_usuario = trim($_POST["id_usuario"]); 
          $fecha = date("Y-m-d");
          $hora = date("H:i:s");

      
        
          $data = array( 
          "cargo_usuario" => $cargo_usuario, 
          "fecha" => $fecha, 
          "hora" => $hora, 
          "id_usuario" => $id_usuario
            ); 

      $this->Mcargo_usuario->Reg_cargo_usuario($data);
      
      ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script>
           
       setTimeout(function(){ $("#panel_modal_resultado_registrar").html(""); },1000);
       setTimeout(function(){ $("#myModal_Register").modal("hide"); },2000);
       setTimeout(function(){ location.reload(); },3000);

      </script>
      
      <?php

    } 

    public function VD_cargo_usuario()
    {
    	$id_cargo_usuario = trim($_POST["id_cargo_usuario"]);
    	$data = array("cargo_usuarios" => $this->Mcargo_usuario->get_cargo_usuario($id_cargo_usuario));
        
		  $this->load->view("cargo_usuario/VD_cargo_usuario",$data); 
    }

    public function VE_cargo_usuario()
    {
    	$id_cargo_usuario = trim($_POST["id_cargo_usuario"]);
    	$data = array("cargo_usuarios" => $this->Mcargo_usuario->get_cargo_usuario($id_cargo_usuario),);
        
		$this->load->view("cargo_usuario/VE_cargo_usuario",$data); 
    }

    public function VU_cargo_usuario()
    {
            $cont_up=0;
            $id_cargo_usuario = trim($_POST["id_cargo_usuario"]);
            $cargo_usuario = trim($_POST["cargo_usuario"]);

            if($cargo_usuario!="")
            {
                $data = array("cargo_usuario" => $cargo_usuario); 
                $this->Mcargo_usuario->Setting_cargo_usuario($data,$id_cargo_usuario);
                $cont_up++;
            }
    

            if($cont_up>0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡Edicion Correcta!</strong></h4> 
                Informacion Cambiada Correctamente
             
              </div>

              <script>
                setTimeout(function(){ $("#panel_modal_respuesta_editar").html(""); },1000);
                setTimeout(function(){ $("#myModal_Update").modal("hide"); },2000);
                setTimeout(function(){ location.reload(); },3000); 
              </script>

              <?php 
            }

            if($cont_up==0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
                Informacion sin cambios en la base de datos
             
              </div>
              <?php      
            }
  
 
    }

    public function VB_cargo_usuario()
    {
    	$id_cargo_usuario = trim($_POST["id_cargo_usuario"]);

    	$this->Mcargo_usuario-> Delete_cargo_usuario($id_cargo_usuario);

      ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Eliminación Correcta!</strong></h4> 
          Informacion Borrada Definitivamente
       
        </div>
      <?php    
    }

} 

?>
