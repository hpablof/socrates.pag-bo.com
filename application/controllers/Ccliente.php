
<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Ccliente extends CI_Controller 
{   
	public function __construct()
    {
        parent::__construct();
        $this->load->model("Mcliente");
    }

    public function index()
    {    
        $page = 1;
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $id_empresa = trim($_POST['id_empresa']);
        $id_gestion = trim($_POST['id_gestion']);

        $numrows = $this->Mcliente->num_rows($id_empresa,$id_gestion); 
        $total_pages = ceil($numrows/$per_page);
      
        $data = array(
            "reload" => "Ccliente/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "clientes" => $this->Mcliente->listar_paginacion($offset,$per_page,
            $id_empresa,$id_gestion)
            );
 
        $this->load->view("cliente/VML_cliente",$data);
 
    } 
 
    public function paginacion()
    {   
        $page = $_POST["page"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;

        $id_empresa = trim($_POST['id_empresa']);
        $id_gestion = trim($_POST['id_gestion']);

        $numrows = $this->Mcliente->num_rows($id_empresa,$id_gestion); 
        $total_pages = ceil($numrows/$per_page);
      
        $data = array(
            "reload" => "Ccliente/paginacion",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "clientes" => $this->Mcliente->listar_paginacion($offset,$per_page,$id_empresa,$id_gestion)
            );
 
        $this->load->view("cliente/VL_cliente",$data); 
    }
  

    public function paginacion_find()
    {
        $page = $_POST["page"];
        $txt_buscar = $_POST["txt_buscar"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;

        $id_empresa = trim($_POST['id_empresa']);
        $id_gestion = trim($_POST['id_gestion']);
        
        $numrows = $this->Mcliente->num_rows_find($txt_buscar,$id_empresa,$id_gestion);

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "txt_buscar" => $txt_buscar,
            "reload" => "cliente/paginacion_find",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "clientes" => $this->Mcliente->listar_paginacion_find($txt_buscar,$offset,$per_page,$id_empresa,$id_gestion)
            );

         $this->load->view("cliente/VLB_cliente",$data);   
    }
 
    public function R_cliente()
    {
         $this->load->view("cliente/VR_cliente");  
    } 

    public function G_cliente()
    {
      $id_usuario = trim($_POST["id_usuario"]); 
      $id_empresa = trim($_POST["id_empresa"]);  
      $id_gestion = trim($_POST["id_gestion"]);  

      $fecha = date("Y-m-d");
      $hora = date("H:i:s");
  
      $clientes =  json_decode($_POST["lista"]);

      foreach($clientes as $cl)
      { 
        $cliente = $cl->cliente; 
        $ci_nit = $cl->ci_nit; 
        $celular_cl = $cl->celular_cl; 
        $whatsapp_cl = $cl->whatsapp_cl; 
        $direccion_cl = $cl->direccion_cl; 
        $correo_cl = $cl->correo_cl; 

        $data = array( 
        "cliente" => $cliente, 
        "ci_nit" => $ci_nit, 
        "celular_cl" => $celular_cl, 
        "whatsapp_cl" => $whatsapp_cl, 
        "direccion_cl" => $direccion_cl, 
        "correo_cl" => $correo_cl, 
        "fecha_cl" => $fecha, 
        "hora_cl" => $hora, 
        "id_usuario" => $id_usuario,
        "id_empresa" => $id_empresa,
        "id_gestion" => $id_gestion
        ); 

        $this->Mcliente->Reg_cliente($data);
 
      }

      ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script>
          
       setTimeout(function(){$("#panel_modal_resultado_registrar").html("");},1000);
       setTimeout(function(){$("#myModal_Register").modal("hide");},2000);
       setTimeout(function(){cargar_datos(1);},3000);
    
      </script>
      
      <?php

    } 

    public function VD_cliente()
    {
    	$id_cliente = trim($_POST["id_cliente"]);
    	$data = array("clientes" => $this->Mcliente->get_cliente($id_cliente));
        
		  $this->load->view("cliente/VD_cliente",$data); 
    }

    public function VE_cliente()
    {
    	$id_cliente = trim($_POST["id_cliente"]);
    	$data = array("clientes" => $this->Mcliente->get_cliente($id_cliente),);
        
		$this->load->view("cliente/VE_cliente",$data); 
    }

    public function VU_cliente()
    {
            $cont_up=0;
            $id_cliente = trim($_POST["id_cliente"]);
            $cliente = trim($_POST["cliente"]);
            $ci_nit = trim($_POST["ci_nit"]);
            $celular_cl = trim($_POST["celular_cl"]);
            $whatsapp_cl = trim($_POST["whatsapp_cl"]);
            $direccion_cl = trim($_POST["direccion_cl"]);
            $correo_cl = trim($_POST["correo_cl"]);

            if($cliente!="")
            {
                $data = array("cliente" => $cliente); 
                $this->Mcliente->Setting_cliente($data,$id_cliente);
                $cont_up++;
            }
    

            if($ci_nit!="")
            {
                $data = array("ci_nit" => $ci_nit); 
                $this->Mcliente->Setting_cliente($data,$id_cliente);
                $cont_up++;
            }
    

            if($celular_cl!="")
            {
                $data = array("celular_cl" => $celular_cl); 
                $this->Mcliente->Setting_cliente($data,$id_cliente);
                $cont_up++;
            }
    

            if($whatsapp_cl!="")
            {
                $data = array("whatsapp_cl" => $whatsapp_cl); 
                $this->Mcliente->Setting_cliente($data,$id_cliente);
                $cont_up++;
            }
    

            if($direccion_cl!="")
            {
                $data = array("direccion_cl" => $direccion_cl); 
                $this->Mcliente->Setting_cliente($data,$id_cliente);
                $cont_up++;
            }
    

            if($correo_cl!="")
            {
                $data = array("correo_cl" => $correo_cl); 
                $this->Mcliente->Setting_cliente($data,$id_cliente);
                $cont_up++;
            }
    

            if($cont_up>0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡Edicion Correcta!</strong></h4> 
                Informacion Cambiada Correctamente
             
              </div>

              <script>
                setTimeout(function(){ $("#panel_modal_respuesta_editar").html(""); },1000);
                setTimeout(function(){ $("#myModal_Update").modal("hide"); },2000);
                setTimeout(function(){ cargar_datos(1); },3000); 
              </script>

              <?php 
            }

            if($cont_up==0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
                Informacion sin cambios en la base de datos
             
              </div>
              <?php      
            }
  
 
    }

    public function VB_cliente()
    {
    	$id_cliente = trim($_POST["id_cliente"]);

    	$this->Mcliente-> Delete_cliente($id_cliente);

      ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Eliminación Correcta!</strong></h4> 
          Informacion Borrada Definitivamente
       
        </div>
      <?php    
    }

} 

?>
