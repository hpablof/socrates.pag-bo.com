<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Carea extends CI_Controller 
{   
	public function __construct()
    {
        parent::__construct();
        $this->load->model("Marea");
    }

    public function index()
    {   
        $page = 1;
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;

        $id_empresa = trim($_POST['id_empresa']);
        $id_gestion = trim($_POST['id_gestion']); 
        
        $numrows = $this->Marea->num_rows($id_empresa); 
        $total_pages = ceil($numrows/$per_page);
      
        $data = array(
            "reload" => "Carea/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "areas" => $this->Marea->listar_paginacion($offset,$per_page,$id_empresa)
        );

        $this->load->view("area/VML_area",$data);
    } 

    public function paginacion()
    {   
        $page = $_POST["page"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $id_empresa = trim($_POST['id_empresa']);
        $numrows = $this->Marea->num_rows($id_empresa,$id_gestion); 
        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "reload" => "Carea/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "areas" => $this->Marea->listar_paginacion($offset,$per_page,$id_empresa)
            );
 
        $this->load->view("area/VL_area",$data); 
    }

    public function paginacion_find()
    {
        $page = $_POST["page"];
        $txt_buscar = $_POST["txt_buscar"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $id_empresa = trim($_POST['id_empresa']);
        $numrows = $this->Marea->num_rows_find($txt_buscar,$id_empresa);
        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "txt_buscar" => $txt_buscar,
            "reload" => "area/paginacion_find",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "areas" => $this->Marea->listar_paginacion_find($txt_buscar,$offset,$per_page,$id_empresa)
            );

         $this->load->view("area/VLB_area",$data);   
    }
 
    public function R_area()
    {
         $this->load->view("area/VR_area");
    } 

    public function G_area()
    {
         $id_usuario = trim($_POST["id_usuario"]); 
         $fecha = date("Y-m-d");
         $hora = date("H:i:s");
         $id_empresa = trim($_POST['id_empresa']);
         $id_gestion = trim($_POST['id_gestion']);
      
         $areas =  json_decode($_POST["lista"]);

         foreach($areas as $area_o)
         { 
            $area = $area_o->area; 
            
            $data = array( 
            "area" => $area, 
            "fecha_area" => $fecha, 
            "hora_area" => $hora, 
            "id_usuario" => $id_usuario,
            "id_empresa" => $id_empresa,
            "id_gestion" => $id_gestion
            ); 

            $this->Marea->Reg_area($data);
          }
 
      ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script>
           
       setTimeout(function(){ $("#panel_modal_resultado_registrar").html(""); },1000);
       setTimeout(function(){ $("#myModal_Register").modal("hide"); },2000);
       setTimeout(function(){ cargar_datos(1); },3000);

      </script>
      
      <?php
      
    } 

    public function VD_area()
    {
    	$id_area = trim($_POST["id_area"]);
    	$data = array("areas" => $this->Marea->get_area($id_area));
        
		  $this->load->view("area/VD_area",$data); 
    }

    public function VE_area()
    {
    	$id_area = trim($_POST["id_area"]);
    	$data = array("areas" => $this->Marea->get_area($id_area),);
        
		$this->load->view("area/VE_area",$data); 
    }

    public function VU_area()
    {
            $cont_up=0;
            $id_area = trim($_POST["id_area"]);
            $area = trim($_POST["area"]);

            if($area!="")
            {
                $data = array("area" => $area); 
                $this->Marea->Setting_area($data,$id_area);
                $cont_up++;
            }
    
            if($cont_up>0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡Edicion Correcta!</strong></h4> 
                Informacion Cambiada Correctamente
             
              </div>

              <script>
                setTimeout(function(){ $("#panel_modal_respuesta_editar").html(""); },1000);
                setTimeout(function(){ $("#myModal_Update").modal("hide"); },2000);
                setTimeout(function(){ cargar_datos(1); },3000); 
              </script>

              <?php 
            }

            if($cont_up==0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
                Informacion sin cambios en la base de datos
             
              </div>
              <?php      
            }
    }

    public function VB_area()
    {
    	$id_area = trim($_POST["id_area"]);

    	$this->Marea-> Delete_area($id_area);

      ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Eliminación Correcta!</strong></h4> 
          Informacion Borrada Definitivamente
       
        </div>
      <?php    
    }

} 

?>
