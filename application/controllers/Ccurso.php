<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Ccurso extends CI_Controller 
{   
	public function __construct()
    {
        parent::__construct();
        $this->load->model("Mcurso");
    }

    public function index()
    {    
        $page = 1;
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mcurso->num_rows(); 
        $total_pages = ceil($numrows/$per_page);
      
        $data = array(
            "reload" => "<?php echo base_url(); ?>Ccurso/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "cursos" => $this->Mcurso->listar_paginacion($offset,$per_page)
            );
        
        $this->load->view("curso/VML_curso",$data);
    } 

    public function paginacion()
    {   
        $page = $_POST["page"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mcurso->num_rows();
        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "reload" => "<?php echo base_url(); ?>Ccurso/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "cursos" => $this->Mcurso->listar_paginacion($offset,$per_page)
            );
 
        $this->load->view("curso/VL_curso",$data); 
    }

    public function paginacion_find()
    {
        $page = $_POST["page"];
        $txt_buscar = $_POST["txt_buscar"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mcurso->num_rows_find($txt_buscar);

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "txt_buscar" => $txt_buscar,
            "reload" => "<?php echo base_url(); ?>curso/paginacion_find",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "cursos" => $this->Mcurso->listar_paginacion_find($txt_buscar,$offset,$per_page)
            );

         $this->load->view("curso/VLB_curso",$data);   
    }
 
    public function R_curso()
    {
          $this->load->view("curso/VR_curso");
    } 

    public function G_curso()
    {
          $portada_curso = $_FILES["portada_curso"]["name"];
          $temp = $_FILES["portada_curso"]["tmp_name"];
          move_uploaded_file($temp, "assets/multimedia/curso/".$portada_curso);

          $curso = trim($_POST["curso"]);
          $descripcion_curso = trim($_POST["descripcion_curso"]);
          $costo_curso = trim($_POST["costo_curso"]);
          $fecha_curso = trim($_POST["fecha_curso"]);
          $duracion_curso = trim($_POST["duracion_curso"]);
          
          $se_certifica_curso = trim($_POST["se_certifica_curso"]);
          $participa_curso = trim($_POST["participa_curso"]);
          $horas_curso = trim($_POST["horas_curso"]);
          $fecha_del_evento = trim($_POST["fecha_del_evento"]);
            
          $certificado_curso = $_FILES["certificado_curso"]["name"];
          $temp = $_FILES["certificado_curso"]["tmp_name"];
          move_uploaded_file($temp, "assets/multimedia/certificado/".$certificado_curso);

          $id_usuario = trim($_POST["id_usuario"]); 
          $fecha = date("Y-m-d");
          $hora = date("H:i:s");

          $data = array( 
          "portada_curso" => $portada_curso, 
          "curso" => $curso, 
          "descripcion_curso" => $descripcion_curso, 
          "costo_curso" => $costo_curso, 
          "fecha_curso" => $fecha_curso, 
          "duracion_curso" => $duracion_curso, 
          "certificado_curso" => $certificado_curso, 
          "estado_curso" => 1,
          "se_certifica_curso" => $se_certifica_curso, 
          "participa_curso" => $participa_curso,
          "horas_curso" => $horas_curso,
          "fecha_del_evento" => $fecha_del_evento,

          "fecha_cur" => $fecha, 
          "hora_cur" => $hora, 
          "id_usuario" => $id_usuario
            ); 
 
          $this->Mcurso->Reg_curso($data); 
      
      ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script>
           
       setTimeout(function(){ $("#panel_modal_resultado_registrar").html(""); },1000);
       setTimeout(function(){ $("#myModal_Register").modal("hide"); },2000);
       setTimeout(function(){ cargar_datos(1); },3000); 

      </script>
      
      <?php

    } 

    public function VD_curso()
    {
    	$id_curso = trim($_POST["id_curso"]);
    	$data = array("cursos" => $this->Mcurso->get_curso($id_curso));
        
		  $this->load->view("curso/VD_curso",$data); 
    }

    public function VE_curso()
    {
    	$id_curso = trim($_POST["id_curso"]);
    	$data = array("cursos" => $this->Mcurso->get_curso($id_curso),);
        
		$this->load->view("curso/VE_curso",$data); 
    }

    public function VU_curso()
    {
            $cont_up=0;
            $id_curso = trim($_POST["id_curso"]);

            $portada_curso = trim($_FILES["portada_curso"]["name"]); 
            $temp = $_FILES["portada_curso"]["tmp_name"];
            move_uploaded_file($temp, "assets/multimedia/curso/".$portada_curso);

            $curso = trim($_POST["curso"]);
            $descripcion_curso = trim($_POST["descripcion_curso"]);
            $costo_curso = trim($_POST["costo_curso"]);
            $fecha_curso = trim($_POST["fecha_curso"]);
            $duracion_curso = trim($_POST["duracion_curso"]);
            $estado_curso = trim($_POST["estado_curso"]);
            
            $se_certifica_curso = trim($_POST["se_certifica_curso"]);
            $participa_curso = trim($_POST["participa_curso"]);
            $horas_curso = trim($_POST["horas_curso"]);
            $fecha_del_evento = trim($_POST["fecha_del_evento"]);
            
            $certificado_curso = trim($_FILES["certificado_curso"]["name"]); 
            $temp = $_FILES["certificado_curso"]["tmp_name"];
            move_uploaded_file($temp, "assets/multimedia/certificado/".$certificado_curso);
 
 
            if($fecha_del_evento!="")
            {
                $data = array("fecha_del_evento" => $fecha_del_evento); 
                $this->Mcurso->Setting_curso($data,$id_curso);
                $cont_up++;
            }
            
            if($horas_curso!="")
            {
                $data = array("horas_curso" => $horas_curso); 
                $this->Mcurso->Setting_curso($data,$id_curso);
                $cont_up++;
            }
            
            if($participa_curso!="")
            {
                $data = array("participa_curso" => $participa_curso); 
                $this->Mcurso->Setting_curso($data,$id_curso);
                $cont_up++;
            }
            
            if($se_certifica_curso!="")
            {
                $data = array("se_certifica_curso" => $se_certifica_curso); 
                $this->Mcurso->Setting_curso($data,$id_curso);
                $cont_up++;
            }
            

            if($portada_curso!="")
            {
                $data = array("portada_curso" => $portada_curso); 
                $this->Mcurso->Setting_curso($data,$id_curso);
                $cont_up++;
            }

            if($estado_curso!="")
            {
                $data = array("estado_curso" => $estado_curso); 
                $this->Mcurso->Setting_curso($data,$id_curso);
                $cont_up++;
            }
            
            if($curso!="")
            {
                $data = array("curso" => $curso); 
                $this->Mcurso->Setting_curso($data,$id_curso);
                $cont_up++;
            }
    
            if($descripcion_curso!="")
            {
                $data = array("descripcion_curso" => $descripcion_curso); 
                $this->Mcurso->Setting_curso($data,$id_curso);
                $cont_up++;
            }
    
            if($costo_curso!="")
            {
                $data = array("costo_curso" => $costo_curso); 
                $this->Mcurso->Setting_curso($data,$id_curso);
                $cont_up++;
            }
    
            if($fecha_curso!="")
            {
                $data = array("fecha_curso" => $fecha_curso); 
                $this->Mcurso->Setting_curso($data,$id_curso);
                $cont_up++;
            }
    
            if($duracion_curso!="")
            {
                $data = array("duracion_curso" => $duracion_curso); 
                $this->Mcurso->Setting_curso($data,$id_curso);
                $cont_up++;
            }
    

            if($certificado_curso!="")
            {
                $data = array("certificado_curso" => $certificado_curso); 
                $this->Mcurso->Setting_curso($data,$id_curso);
                $cont_up++;
            }
    

            if($cont_up>0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡Edicion Correcta!</strong></h4> 
                Informacion Cambiada Correctamente
             
              </div>

              <script>
                setTimeout(function(){ $("#panel_modal_respuesta_editar").html(""); },1000);
                setTimeout(function(){ $("#myModal_Update").modal("hide"); },2000);
                setTimeout(function(){ cargar_datos(1); },3000); 
              </script>

              <?php 
            }

            if($cont_up==0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
                Informacion sin cambios en la base de datos
             
              </div>
              <?php      
            }
  
 
    }

    public function VB_curso()
    {
    	$id_curso = trim($_POST["id_curso"]);

    	$this->Mcurso-> Delete_curso($id_curso);

      ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Eliminación Correcta!</strong></h4> 
          Informacion Borrada Definitivamente
       
        </div>
      <?php    
    }
 
    
} 

?>
