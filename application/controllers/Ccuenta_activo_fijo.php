
<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Ccuenta_activo_fijo extends CI_Controller 
{   
	public function __construct()
    {
        parent::__construct();
        $this->load->model("Mcuenta_activo_fijo");
    }

    public function index()
    {    
        $page = 1;
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mcuenta_activo_fijo->num_rows(); 
        $total_pages = ceil($numrows/$per_page);
      
        $data = array(
            "reload" => "<?php echo base_url(); ?>Ccuenta_activo_fijo/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "cuenta_activo_fijos" => $this->Mcuenta_activo_fijo->listar_paginacion($offset,$per_page)
            );
        
        $this->load->view("assets/Header");  
        $this->load->view("cuenta_activo_fijo/VML_cuenta_activo_fijo",$data);
        $this->load->view("assets/Footer"); 

    } 

    public function paginacion()
    {   
        $page = $_POST["page"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mcuenta_activo_fijo->num_rows();

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "reload" => "<?php echo base_url(); ?>Ccuenta_activo_fijo/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "cuenta_activo_fijos" => $this->Mcuenta_activo_fijo->listar_paginacion($offset,$per_page)
            );
 
        $this->load->view("cuenta_activo_fijo/VL_cuenta_activo_fijo",$data); 
    

    }

    public function paginacion_find()
    {
        $page = $_POST["page"];
        $txt_buscar = $_POST["txt_buscar"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mcuenta_activo_fijo->num_rows_find($txt_buscar);

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "txt_buscar" => $txt_buscar,
            "reload" => "<?php echo base_url(); ?>cuenta_activo_fijo/paginacion_find",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "cuenta_activo_fijos" => $this->Mcuenta_activo_fijo->listar_paginacion_find($txt_buscar,$offset,$per_page)
            );

         $this->load->view("cuenta_activo_fijo/VLB_cuenta_activo_fijo",$data);   
    }
 
    public function R_cuenta_activo_fijo()
    {
          $this->load->view("cuenta_activo_fijo/VR_cuenta_activo_fijo");

		  
    } 

    public function G_cuenta_activo_fijo()
    {
         $id_usuario = trim($_POST["id_usuario"]); 
         $fecha = date("Y-m-d");
         $hora = date("H:i:s");
      
         $cuenta_activo_fijos =  json_decode($_POST["lista"]);

      foreach($cuenta_activo_fijos as $cuenta_activo_fijo)
      { 
        $codigo_caf = $cuenta_activo_fijo->codigo_caf; 
        $cuenta_caf = $cuenta_activo_fijo->cuenta_caf; 
        $codigo_cda = $cuenta_activo_fijo->codigo_cda; 
        $cuenta_cda = $cuenta_activo_fijo->cuenta_cda; 
        $codigo_cdd = $cuenta_activo_fijo->codigo_cdd; 
        $cuenta_cdd = $cuenta_activo_fijo->cuenta_cdd; 
        $porcentaje_af = $cuenta_activo_fijo->porcentaje_af; 
        
        $data = array( 
        "codigo_caf" => $codigo_caf, 
        "cuenta_caf" => $cuenta_caf, 
        "codigo_cda" => $codigo_cda, 
        "cuenta_cda" => $cuenta_cda, 
        "codigo_cdd" => $codigo_cdd, 
        "cuenta_cdd" => $cuenta_cdd, 
        "porcentaje_af" => $porcentaje_af, 
        "fecha_caf" => $fecha, 
        "hora_caf" => $hora, 
        "id_usuario" => $id_usuario
            ); 

        $this->Mcuenta_activo_fijo->Reg_cuenta_activo_fijo($data);
 
      }

     
      
      ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script>
           
       setTimeout(function(){ $("#panel_modal_resultado_registrar").html(""); },1000);
       setTimeout(function(){ $("#myModal_Register").modal("hide"); },2000);
       setTimeout(function(){ location.reload(); },3000);

      </script>
      
      <?php

    } 

    public function VD_cuenta_activo_fijo()
    {
    	$id_cuenta_activo_fijo = trim($_POST["id_cuenta_activo_fijo"]);
    	$data = array("cuenta_activo_fijos" => $this->Mcuenta_activo_fijo->get_cuenta_activo_fijo($id_cuenta_activo_fijo));
        
		  $this->load->view("cuenta_activo_fijo/VD_cuenta_activo_fijo",$data); 
    }

    public function VE_cuenta_activo_fijo()
    {
    	$id_cuenta_activo_fijo = trim($_POST["id_cuenta_activo_fijo"]);
    	$data = array("cuenta_activo_fijos" => $this->Mcuenta_activo_fijo->get_cuenta_activo_fijo($id_cuenta_activo_fijo),);
        
		$this->load->view("cuenta_activo_fijo/VE_cuenta_activo_fijo",$data); 
    }

    public function VU_cuenta_activo_fijo()
    {
            $cont_up=0;
            $id_cuenta_activo_fijo = trim($_POST["id_cuenta_activo_fijo"]);
            $codigo_caf = trim($_POST["codigo_caf"]);
            $cuenta_caf = trim($_POST["cuenta_caf"]);
            $codigo_cda = trim($_POST["codigo_cda"]);
            $cuenta_cda = trim($_POST["cuenta_cda"]);
            $codigo_cdd = trim($_POST["codigo_cdd"]);
            $cuenta_cdd = trim($_POST["cuenta_cdd"]);
            $porcentaje_af = trim($_POST["porcentaje_af"]);

            if($codigo_caf!="")
            {
                $data = array("codigo_caf" => $codigo_caf); 
                $this->Mcuenta_activo_fijo->Setting_cuenta_activo_fijo($data,$id_cuenta_activo_fijo);
                $cont_up++;
            }
    

            if($cuenta_caf!="")
            {
                $data = array("cuenta_caf" => $cuenta_caf); 
                $this->Mcuenta_activo_fijo->Setting_cuenta_activo_fijo($data,$id_cuenta_activo_fijo);
                $cont_up++;
            }
    

            if($codigo_cda!="")
            {
                $data = array("codigo_cda" => $codigo_cda); 
                $this->Mcuenta_activo_fijo->Setting_cuenta_activo_fijo($data,$id_cuenta_activo_fijo);
                $cont_up++;
            }
    

            if($cuenta_cda!="")
            {
                $data = array("cuenta_cda" => $cuenta_cda); 
                $this->Mcuenta_activo_fijo->Setting_cuenta_activo_fijo($data,$id_cuenta_activo_fijo);
                $cont_up++;
            }
    

            if($codigo_cdd!="")
            {
                $data = array("codigo_cdd" => $codigo_cdd); 
                $this->Mcuenta_activo_fijo->Setting_cuenta_activo_fijo($data,$id_cuenta_activo_fijo);
                $cont_up++;
            }
    

            if($cuenta_cdd!="")
            {
                $data = array("cuenta_cdd" => $cuenta_cdd); 
                $this->Mcuenta_activo_fijo->Setting_cuenta_activo_fijo($data,$id_cuenta_activo_fijo);
                $cont_up++;
            }
    

            if($porcentaje_af!="")
            {
                $data = array("porcentaje_af" => $porcentaje_af); 
                $this->Mcuenta_activo_fijo->Setting_cuenta_activo_fijo($data,$id_cuenta_activo_fijo);
                $cont_up++;
            }
    

            if($cont_up>0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡Edicion Correcta!</strong></h4> 
                Informacion Cambiada Correctamente
             
              </div>

              <script>
                setTimeout(function(){ $("#panel_modal_respuesta_editar").html(""); },1000);
                setTimeout(function(){ $("#myModal_Update").modal("hide"); },2000);
                setTimeout(function(){ location.reload(); },3000); 
              </script>

              <?php 
            }

            if($cont_up==0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
                Informacion sin cambios en la base de datos
             
              </div>
              <?php      
            }
  
 
    }

    public function VB_cuenta_activo_fijo()
    {
    	$id_cuenta_activo_fijo = trim($_POST["id_cuenta_activo_fijo"]);

    	$this->Mcuenta_activo_fijo-> Delete_cuenta_activo_fijo($id_cuenta_activo_fijo);

      ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Eliminación Correcta!</strong></h4> 
          Informacion Borrada Definitivamente
       
        </div>
      <?php    
    }

} 

?>
