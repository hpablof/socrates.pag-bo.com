
<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Crubro extends CI_Controller 
{   
	public function __construct()
    {
        parent::__construct();
        $this->load->model("Mrubro");
    }

    public function index()
    {    
        $page = 1;
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mrubro->num_rows(); 
        $total_pages = ceil($numrows/$per_page);
      
        $data = array(
            "reload" => "<?php echo base_url(); ?>Crubro/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "rubros" => $this->Mrubro->listar_paginacion($offset,$per_page)
            );

        $this->load->view("rubro/VML_rubro",$data);
    } 

    public function paginacion()
    {   
        $page = $_POST["page"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mrubro->num_rows();

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "reload" => "<?php echo base_url(); ?>Crubro/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "rubros" => $this->Mrubro->listar_paginacion($offset,$per_page)
            );
 
        $this->load->view("rubro/VL_rubro",$data); 
    

    }

    public function paginacion_find()
    {
        $page = $_POST["page"];
        $txt_buscar = $_POST["txt_buscar"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mrubro->num_rows_find($txt_buscar);

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "txt_buscar" => $txt_buscar,
            "reload" => "<?php echo base_url(); ?>rubro/paginacion_find",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "rubros" => $this->Mrubro->listar_paginacion_find($txt_buscar,$offset,$per_page)
            );

         $this->load->view("rubro/VLB_rubro",$data);   
    }
 
    public function R_rubro()
    {
          $this->load->view("rubro/VR_rubro");

		  
    } 

    public function G_rubro()
    {
         $id_usuario = trim($_POST["id_usuario"]); 
         $fecha = date("Y-m-d");
         $hora = date("H:i:s");
      
         $rubros =  json_decode($_POST["lista"]);

      foreach($rubros as $rubro)
      { 
        $rubro = $rubro->rubro; 
        
        $data = array( 
        "rubro" => $rubro, 
        "fecha" => $fecha, 
        "hora" => $hora, 
        "id_usuario" => $id_usuario
            ); 

      $this->Mrubro->Reg_rubro($data);
 
      }

     
      
      ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script>
           
       setTimeout(function(){ $("#panel_modal_resultado_registrar").html(""); },1000);
       setTimeout(function(){ $("#myModal_Register").modal("hide"); },2000);
       setTimeout(function(){ location.reload(); },3000);

      </script>
      
      <?php

    } 

    public function VD_rubro()
    {
    	$id_rubro = trim($_POST["id_rubro"]);
    	$data = array("rubros" => $this->Mrubro->get_rubro($id_rubro));
        
		  $this->load->view("rubro/VD_rubro",$data); 
    }

    public function VE_rubro()
    {
    	$id_rubro = trim($_POST["id_rubro"]);
    	$data = array("rubros" => $this->Mrubro->get_rubro($id_rubro),);
        
		$this->load->view("rubro/VE_rubro",$data); 
    }

    public function VU_rubro()
    {
            $cont_up=0;
            $id_rubro = trim($_POST["id_rubro"]);
            $rubro = trim($_POST["rubro"]);

            if($rubro!="")
            {
                $data = array("rubro" => $rubro); 
                $this->Mrubro->Setting_rubro($data,$id_rubro);
                $cont_up++;
            }
    

            if($cont_up>0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡Edicion Correcta!</strong></h4> 
                Informacion Cambiada Correctamente
             
              </div>

              <script>
                setTimeout(function(){ $("#panel_modal_respuesta_editar").html(""); },1000);
                setTimeout(function(){ $("#myModal_Update").modal("hide"); },2000);
                setTimeout(function(){ location.reload(); },3000); 
              </script>

              <?php 
            }

            if($cont_up==0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
                Informacion sin cambios en la base de datos
             
              </div>
              <?php      
            }
  
 
    }

    public function VB_rubro()
    {
    	$id_rubro = trim($_POST["id_rubro"]);

    	$this->Mrubro-> Delete_rubro($id_rubro);

      ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Eliminación Correcta!</strong></h4> 
          Informacion Borrada Definitivamente
       
        </div>
      <?php    
    }

} 

?>
