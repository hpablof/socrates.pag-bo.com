
<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Cempresa extends CI_Controller 
{   
	public function __construct()
    {
        parent::__construct();
        $this->load->model("Mempresa");
    }

    public function index()
    {    
        $page = 1;
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mempresa->num_rows(); 
        $total_pages = ceil($numrows/$per_page);
      
        $data = array(
            "reload" => "<?php echo base_url(); ?>Cempresa/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "empresas" => $this->Mempresa->listar_paginacion($offset,$per_page)
            );
        
        
        $this->load->view("empresa/VML_empresa",$data);
        

    } 

    public function paginacion()
    {   
        $page = $_POST["page"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mempresa->num_rows();

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "reload" => "<?php echo base_url(); ?>Cempresa/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "empresas" => $this->Mempresa->listar_paginacion($offset,$per_page)
            );
 
        $this->load->view("empresa/VL_empresa",$data); 
    

    }

    public function paginacion_find()
    {
        $page = $_POST["page"];
        $txt_buscar = $_POST["txt_buscar"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mempresa->num_rows_find($txt_buscar);

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "txt_buscar" => $txt_buscar,
            "reload" => "<?php echo base_url(); ?>empresa/paginacion_find",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "empresas" => $this->Mempresa->listar_paginacion_find($txt_buscar,$offset,$per_page)
            );

         $this->load->view("empresa/VLB_empresa",$data);   
    }
 
    public function R_empresa()
    {
          $data = array(
          "rubros"=>$this->Mempresa->get_rubros(),
          "departamentos"=>$this->Mempresa->get_departamentos());
          $this->load->view("empresa/VR_empresa",$data);
    } 

    public function G_empresa()
    {
          
          $logo = $_FILES["logo"]["name"];
          $temp = $_FILES["logo"]["tmp_name"];
          move_uploaded_file($temp, "assets/multimedia/portadas/".$logo);

          
          $razon_social = trim($_POST["razon_social"]);
          $encargado = trim($_POST["encargado"]);
          $id_rubro = trim($_POST["id_rubro"]);
          $nit_empresa = trim($_POST["nit_empresa"]);
          $ci_encargado = trim($_POST["ci_encargado"]);
          $id_departamento = trim($_POST["id_departamento"]);
          $email = trim($_POST["email"]);
          $password = trim($_POST["password"]);
          $direccion = trim($_POST["direccion"]);
          $telefono = trim($_POST["telefono"]);
          $celular = trim($_POST["celular"]);
          $whatsapp = trim($_POST["whatsapp"]);
          $id_usuario = trim($_POST["id_usuario"]); 
          $fecha = date("Y-m-d");
          $hora = date("H:i:s");

      
        
          $data = array( 
          "logo" => $logo, 
          "razon_social" => $razon_social, 
          "encargado" => $encargado, 
          "id_rubro" => $id_rubro, 
          "nit_empresa" => $nit_empresa, 
          "ci_encargado" => $ci_encargado, 
          "id_departamento" => $id_departamento, 
          "email" => $email, 
          "password" => $password, 
          "direccion" => $direccion, 
          "telefono" => $telefono, 
          "celular" => $celular, 
          "whatsapp" => $whatsapp, 
          "fecha" => $fecha, 
          "hora" => $hora, 
          "id_usuario" => $id_usuario
            ); 

      $this->Mempresa->Reg_empresa($data);
      
      ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script>
       /*    
       setTimeout(function(){ $("#panel_modal_resultado_registrar").html(""); },1000);
       setTimeout(function(){ $("#myModal_Register").modal("hide"); },2000);
       setTimeout(function(){ cargar_datos(1); },3000);
       */
      </script>
      
      <?php

    } 

    public function VD_empresa()
    {
    	$id_empresa = trim($_POST["id_empresa"]);
    	$data = array("empresas" => $this->Mempresa->get_empresa($id_empresa));
        
		  $this->load->view("empresa/VD_empresa",$data); 
    }

    public function VE_empresa()
    {
    	$id_empresa = trim($_POST["id_empresa"]);
    	$data = array("empresas" => $this->Mempresa->get_empresa($id_empresa),"rubros" => $this->Mempresa->get_rubros(),"departamentos" => $this->Mempresa->get_departamentos(),);
        
		$this->load->view("empresa/VE_empresa",$data); 
    }

    public function VU_empresa()
    {
            $cont_up=0;
            $id_empresa = trim($_POST["id_empresa"]);

            $logo = trim($_FILES["logo"]["name"]); 
            $temp = $_FILES["logo"]["tmp_name"];
            move_uploaded_file($temp, "assets/multimedia/portadas/".$logo);

            
            $razon_social = trim($_POST["razon_social"]);
            $encargado = trim($_POST["encargado"]);
            $id_rubro = trim($_POST["id_rubro"]);
            $nit_empresa = trim($_POST["nit_empresa"]);
            $ci_encargado = trim($_POST["ci_encargado"]);
            $id_departamento = trim($_POST["id_departamento"]);
            $email = trim($_POST["email"]);
            $password = trim($_POST["password"]);
            $direccion = trim($_POST["direccion"]);
            $telefono = trim($_POST["telefono"]);
            $celular = trim($_POST["celular"]);
            $whatsapp = trim($_POST["whatsapp"]);

            if($logo!="")
            {
                $data = array("logo" => $logo); 
                $this->Mempresa->Setting_empresa($data,$id_empresa);
                $cont_up++;
            }
    

            if($razon_social!="")
            {
                $data = array("razon_social" => $razon_social); 
                $this->Mempresa->Setting_empresa($data,$id_empresa);
                $cont_up++;
            }
    

            if($encargado!="")
            {
                $data = array("encargado" => $encargado); 
                $this->Mempresa->Setting_empresa($data,$id_empresa);
                $cont_up++;
            }
    

            if($id_rubro!="")
            {
                $data = array("id_rubro" => $id_rubro); 
                $this->Mempresa->Setting_empresa($data,$id_empresa);
                $cont_up++;
            }
    

            if($nit_empresa!="")
            {
                $data = array("nit_empresa" => $nit_empresa); 
                $this->Mempresa->Setting_empresa($data,$id_empresa);
                $cont_up++;
            }
    

            if($ci_encargado!="")
            {
                $data = array("ci_encargado" => $ci_encargado); 
                $this->Mempresa->Setting_empresa($data,$id_empresa);
                $cont_up++;
            }
    

            if($id_departamento!="")
            {
                $data = array("id_departamento" => $id_departamento); 
                $this->Mempresa->Setting_empresa($data,$id_empresa);
                $cont_up++;
            }
    

            if($email!="")
            {
                $data = array("email" => $email); 
                $this->Mempresa->Setting_empresa($data,$id_empresa);
                $cont_up++;
            }
    

            if($password!="")
            {
                $data = array("password" => $password); 
                $this->Mempresa->Setting_empresa($data,$id_empresa);
                $cont_up++;
            }
    

            if($direccion!="")
            {
                $data = array("direccion" => $direccion); 
                $this->Mempresa->Setting_empresa($data,$id_empresa);
                $cont_up++;
            }
    

            if($telefono!="")
            {
                $data = array("telefono" => $telefono); 
                $this->Mempresa->Setting_empresa($data,$id_empresa);
                $cont_up++;
            }
    

            if($celular!="")
            {
                $data = array("celular" => $celular); 
                $this->Mempresa->Setting_empresa($data,$id_empresa);
                $cont_up++;
            }
    

            if($whatsapp!="")
            {
                $data = array("whatsapp" => $whatsapp); 
                $this->Mempresa->Setting_empresa($data,$id_empresa);
                $cont_up++;
            }
    

            if($cont_up>0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡Edicion Correcta!</strong></h4> 
                Informacion Cambiada Correctamente
             
              </div>

              <script>
                setTimeout(function(){ $("#panel_modal_respuesta_editar").html(""); },1000);
                setTimeout(function(){ $("#myModal_Update").modal("hide"); },2000);
                setTimeout(function(){ cargar_datos(1); },3000); 
              </script>

              <?php 
            }

            if($cont_up==0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
                Informacion sin cambios en la base de datos
             
              </div>
              <?php      
            }
  
 
    }

    public function VB_empresa()
    {
    	$id_empresa = trim($_POST["id_empresa"]);

    	$this->Mempresa-> Delete_empresa($id_empresa);

      ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Eliminación Correcta!</strong></h4> 
          Informacion Borrada Definitivamente
       
        </div>
      <?php    
    }

} 

?>
