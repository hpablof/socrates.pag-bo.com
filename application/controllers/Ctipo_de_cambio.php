
<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Ctipo_de_cambio extends CI_Controller 
{   
	public function __construct()
    {
        parent::__construct();
        $this->load->model("Mtipo_de_cambio");
    }

    public function index()
    {    
        $page = 1;
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mtipo_de_cambio->num_rows(); 
        $total_pages = ceil($numrows/$per_page);
      
        $data = array(
            "reload" => "<?php echo base_url(); ?>Ctipo_de_cambio/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "tipo_de_cambios" => $this->Mtipo_de_cambio->listar_paginacion($offset,$per_page)
            );
        
        $this->load->view("assets/Header");  
        $this->load->view("tipo_de_cambio/VML_tipo_de_cambio",$data);
        $this->load->view("assets/Footer"); 

    } 

    public function paginacion()
    {   
        $page = $_POST["page"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mtipo_de_cambio->num_rows();

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "reload" => "<?php echo base_url(); ?>Ctipo_de_cambio/",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "tipo_de_cambios" => $this->Mtipo_de_cambio->listar_paginacion($offset,$per_page)
            );
 
        $this->load->view("tipo_de_cambio/VL_tipo_de_cambio",$data); 
    

    }

    public function paginacion_find()
    {
        $page = $_POST["page"];
        $txt_buscar = $_POST["txt_buscar"];
        $per_page = 10;  
        $adjacents  = 4; 
        $offset = ($page - 1) * $per_page;
        
        $numrows = $this->Mtipo_de_cambio->num_rows_find($txt_buscar);

        $total_pages = ceil($numrows/$per_page);
 
        $data = array(
            "txt_buscar" => $txt_buscar,
            "reload" => "<?php echo base_url(); ?>tipo_de_cambio/paginacion_find",
            "page" => $page,
            "total_pages" => $total_pages,
            "adjacents" => $adjacents,
            "tipo_de_cambios" => $this->Mtipo_de_cambio->listar_paginacion_find($txt_buscar,$offset,$per_page)
            );

         $this->load->view("tipo_de_cambio/VLB_tipo_de_cambio",$data);   
    }
 
    public function R_tipo_de_cambio()
    {
          $this->load->view("tipo_de_cambio/VR_tipo_de_cambio"); 
    } 

    public function G_tipo_de_cambio()
    {
         $id_usuario = trim($_POST["id_usuario"]); 
         $fecha = date("Y-m-d");
         $hora = date("H:i:s");
      
         $tipo_de_cambios =  json_decode($_POST["lista"]);

      foreach($tipo_de_cambios as $tipo_de_cambio)
      { 
        $fecha_tc = $tipo_de_cambio->fecha_tc; 
        $dolar_tc = $tipo_de_cambio->dolar_tc; 
        $ufv_tc = $tipo_de_cambio->ufv_tc; 
        
        $data = array( 
        "fecha_tc" => $fecha_tc, 
        "dolar_tc" => $dolar_tc, 
        "ufv_tc" => $ufv_tc, 
        "fecha_rtc" => $fecha, 
        "hora_rtc" => $hora, 
        "id_usuario" => $id_usuario
            ); 

      $this->Mtipo_de_cambio->Reg_tipo_de_cambio($data);
 
      }

     
      
      ?>
      <p class="transparent"> espacio </p>
      <div class="alert alert-info alert-dismissable">
        <button type="button" class="close">&times;</button>

        <h4><strong>¡Registro Correcto!</strong></h4> 
        Informacion Almacenada Correctamente
     
      </div>

      <script>
           
       setTimeout(function(){ $("#panel_modal_resultado_registrar").html(""); },1000);
       setTimeout(function(){ $("#myModal_Register").modal("hide"); },2000);
       setTimeout(function(){ location.reload(); },3000);

      </script>
      
      <?php

    } 

    public function VD_tipo_de_cambio()
    {
    	$id_tipo_de_cambio = trim($_POST["id_tipo_de_cambio"]);
    	$data = array("tipo_de_cambios" => $this->Mtipo_de_cambio->get_tipo_de_cambio($id_tipo_de_cambio));
        
		  $this->load->view("tipo_de_cambio/VD_tipo_de_cambio",$data); 
    }

    public function VE_tipo_de_cambio()
    {
    	$id_tipo_de_cambio = trim($_POST["id_tipo_de_cambio"]);
    	$data = array("tipo_de_cambios" => $this->Mtipo_de_cambio->get_tipo_de_cambio($id_tipo_de_cambio),);
        
		$this->load->view("tipo_de_cambio/VE_tipo_de_cambio",$data); 
    }

    public function VU_tipo_de_cambio()
    {
            $cont_up=0;
            $id_tipo_de_cambio = trim($_POST["id_tipo_de_cambio"]);
            $fecha_tc = trim($_POST["fecha_tc"]);
            $dolar_tc = trim($_POST["dolar_tc"]);
            $ufv_tc = trim($_POST["ufv_tc"]);

            if($fecha_tc!="")
            {
                $data = array("fecha_tc" => $fecha_tc); 
                $this->Mtipo_de_cambio->Setting_tipo_de_cambio($data,$id_tipo_de_cambio);
                $cont_up++;
            }
    

            if($dolar_tc!="")
            {
                $data = array("dolar_tc" => $dolar_tc); 
                $this->Mtipo_de_cambio->Setting_tipo_de_cambio($data,$id_tipo_de_cambio);
                $cont_up++;
            }
    

            if($ufv_tc!="")
            {
                $data = array("ufv_tc" => $ufv_tc); 
                $this->Mtipo_de_cambio->Setting_tipo_de_cambio($data,$id_tipo_de_cambio);
                $cont_up++;
            }
    

            if($cont_up>0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡Edicion Correcta!</strong></h4> 
                Informacion Cambiada Correctamente
             
              </div>

              <script>
                setTimeout(function(){ $("#panel_modal_respuesta_editar").html(""); },1000);
                setTimeout(function(){ $("#myModal_Update").modal("hide"); },2000);
                setTimeout(function(){ location.reload(); },3000); 
              </script>

              <?php 
            }

            if($cont_up==0)
            {
              ?>
              <p class="transparent"> espacio </p>
              <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close">&times;</button>

                <h4><strong>¡No Se Realizo Edicion!</strong></h4> 
                Informacion sin cambios en la base de datos
             
              </div>
              <?php      
            }
  
 
    }

    public function VB_tipo_de_cambio()
    {
    	$id_tipo_de_cambio = trim($_POST["id_tipo_de_cambio"]);

    	$this->Mtipo_de_cambio-> Delete_tipo_de_cambio($id_tipo_de_cambio);

      ?>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close">&times;</button>

          <h4><strong>¡Eliminación Correcta!</strong></h4> 
          Informacion Borrada Definitivamente
       
        </div>
      <?php    
    }

} 

?>
