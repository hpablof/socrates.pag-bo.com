

   <table class="table table-bordered table-condensed table-hover table-striped">
     <thead>
      <tr>
        <th class="hidden-lg hidden-md hidden-sm"> </th>
        <th> rubro </th>
       <th class="hidden-xs"> </th>

      </tr>

     </thead>
     <tbody>
     <?php 
     foreach($rubros as $ob_rubro)
     {
        $id_rubro = $ob_rubro->id_rubro;
        $rubro = $ob_rubro->rubro;
     ?>
       <tr>

        <td class="col-xs-1 hidden-lg hidden-md hidden-sm"> <center>
             <a href="#" onclick="btn_submenu_xs('<?php echo $id_rubro; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones-xs" id="panel_opciones_xs<?php echo $id_rubro; ?>">

              <div align="right" class="panel_cerrar_botones-xs" >
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu_xs('<?php echo $id_rubro; ?>');"> X </buttom>
              </div>
              <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_examinar_rubro('<?php echo $id_rubro; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_editar_rubro('<?php echo $id_rubro; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line-xs">

               <button class="btn btn-danger btn-md" onclick="btn_borrar_rubro('<?php echo $id_rubro; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 

             </div>

          </center>
          </td>
     
        <td> <?php echo $rubro; ?> </td>
        <td class="col-lg-1 hidden-xs"> <center>
             <a href="#" onclick="btn_submenu('<?php echo $id_rubro; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones" id="panel_opciones_<?php echo $id_rubro; ?>">

              <div align="right" class="panel_cerrar_botones">
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu('<?php echo $id_rubro; ?>');"> x </buttom>
              </div>
              <hr class="btn_line">

               <button class="btn btn-default btn-xs" onclick="btn_examinar_rubro('<?php echo $id_rubro; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line">

               <button class="btn btn-default btn-xs" onclick="btn_editar_rubro('<?php echo $id_rubro; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line">

               <button class="btn btn-danger btn-xs" onclick="btn_borrar_rubro('<?php echo $id_rubro; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 
             
             </div>

          </center>
          </td>

       </tr>
     <?php
     } 
     ?>
     </tbody>
 
 <!-- Final del table -->
 </table>
 <?php require "paginacion.php"; ?>

