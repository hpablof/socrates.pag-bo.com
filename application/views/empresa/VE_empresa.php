
<div class="row">
<form id="formulario" enctype="multipart/form-data">

 <?php 
         $id_empresa = $empresas[0]->id_empresa;
         $logo = $empresas[0]->logo;
         $razon_social = $empresas[0]->razon_social;
         $encargado = $empresas[0]->encargado;
         $rubro = $empresas[0]->rubro;
         $nit_empresa = $empresas[0]->nit_empresa;
         $ci_encargado = $empresas[0]->ci_encargado;
         $departamento = $empresas[0]->departamento;
         $email = $empresas[0]->email;
         $password = $empresas[0]->password;
         $direccion = $empresas[0]->direccion;
         $telefono = $empresas[0]->telefono;
         $celular = $empresas[0]->celular;
         $whatsapp = $empresas[0]->whatsapp;
         ?>
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
         <input type="hidden" id="id_empresa_edicion" name="id_empresa_edicion" value="<?php echo $id_empresa; ?>" >
	     <center>
          <label> logo </label></br> 

	      <img id="img_resp_logo" src="<?php echo base_url(); ?>assets/multimedia/portadas/<?php echo $logo; ?>" style="width: 80%; height: 300px; margin-bottom: 1.5%; border:1px solid #086A87;">
	 
	      <span class="btn btn-success btn-file"> 
	      <span class="glyphicon glyphicon-picture"></span> Subir logo
	      
	      <input type="file" class="form-control" name="logo" 
	      id="logo" onchange="previsualizar();">

	      </span> 
	    </center>

	      <div id="panel_resp_logo"></div> 
 
	  <!-- Final del div panel campo -->
	  </div>
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	   <label> razon social </label></br> 
       <input type="text" class="form-control" name="razon_social" 
       id="razon_social" maxlength="200" onkeyup="validador_campo('razon_social','panel_resp_razon_social','');" 
       onkeypress="return valida_letras(event);" placeholder="* Escriba su razon social" value="<?php echo $razon_social; ?>" >
       <div id="panel_resp_razon_social"></div>

        <!-- Final del div panel campo -->
       </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	   <label> encargado </label></br> 
     <input type="text" class="form-control" name="encargado" 
     id="encargado" maxlength="200" onkeyup="validador_campo('encargado','panel_resp_encargado','5');" 
     onkeypress="return valida_letras(event);" placeholder="* Escriba su encargado" value="<?php echo $encargado; ?>" >
     <div id="panel_resp_encargado"></div>

        <!-- Final del div panel campo -->
       </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	   
        <label> rubro : <span id="rubro"> <?php echo $rubro; ?></span> </label></br> 
        <input type="hidden" name="id_rubro" id="id_rubro" >
 
        <ul class="nav navbar-nav" style="width:100%; ">
         
        <li class="dropdown" style="width:100%;">
          
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="background:transparent; color:#2E4053; width:100%; border:1px solid #2E4053; padding: 1%;" id="select_rubro" > rubro <span class="caret"></span> </a>

        <ul class="dropdown-menu" id="select-form" style="border:1px solid #bdc3c7;">
        <?php

         foreach($rubros as $rubro)
         {
            $id_rubro = $rubro->id_rubro;
            $rubro_n = $rubro->rubro;
            ?>
            <li> <a href="#" onclick="select('<?php echo $id_rubro; ?>','<?php echo $rubro_n; ?>','rubro');"> <?php echo $rubro_n; ?> </a> </li> 
            <?php
          } ?>
         </ul>

         </li>
        </ul> 
         
        <div id="panel_resp_rubro"></div>

    <!-- Final del div panel campo -->
    </div>
    
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	   <label> nit empresa </label></br> 
     <input type="text" class="form-control" name="nit_empresa" 
     id="nit_empresa" maxlength="200" onkeyup="validador_campo('nit_empresa','panel_resp_nit_empresa','5');" 
     onkeypress="return valida_letras(event);" placeholder="* Escriba su nit empresa" value="<?php echo $nit_empresa; ?>" >
     <div id="panel_resp_nit_empresa"></div>

        <!-- Final del div panel campo -->
       </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	   <label> ci encargado </label></br> 
     <input type="text" class="form-control" name="ci_encargado" 
     id="ci_encargado" maxlength="200" onkeyup="validador_campo('ci_encargado','panel_resp_ci_encargado','5');" 
     onkeypress="return valida_letras(event);" placeholder="* Escriba su ci" value="<?php echo $ci_encargado; ?>" >
     <div id="panel_resp_ci_encargado"></div>

        <!-- Final del div panel campo -->
       </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">

        <label> departamento : <span id="departamento"> <?php echo $departamento; ?></span> </label></br> 
        <input type="hidden" name="id_departamento" id="id_departamento" >
 
        <ul class="nav navbar-nav" style="width:100%; ">
         
        <li class="dropdown" style="width:100%;">
          
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="background:transparent; color:#2E4053; width:100%; border:1px solid #2E4053; padding: 1%;" id="select_departamento" > departamento <span class="caret"></span> </a>

        <ul class="dropdown-menu" id="select-form" style="border:1px solid #bdc3c7;">
        <?php

         foreach($departamentos as $departamento)
         {
            $id_departamento = $departamento->id_departamento;
            $departamento_n = $departamento->departamento;
            ?>
            <li> <a href="#" onclick="select('<?php echo $id_departamento; ?>','<?php echo $departamento_n; ?>','departamento');"> <?php echo $departamento_n; ?> </a> </li> 
            <?php
          } ?>
         </ul>

         </li>
        </ul> 
         
        <div id="panel_resp_departamento"></div>

    <!-- Final del div panel campo -->
    </div>
    
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	    <label> email </label></br> 
        <input type="text" class="form-control" name="email" 
        id="email" maxlength="300" onkeyup="validador_correo('email','5','panel_resp_email');" 
        onkeypress="return valida_ambos(event);" placeholder="* Escriba su email" value="<?php echo $email; ?>" >
        <div id="panel_resp_email"></div>

        <!-- Final del div panel campo -->
       </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	    <label> password </label></br> 
        <input type="text" class="form-control" name="password" 
        id="password" maxlength="10" onkeyup="validador_campo('password','panel_resp_password','5');" 
        onkeypress="return valida_ambos(event);" placeholder="* Escriba su password" value="<?php echo $password; ?>">
        <div id="panel_resp_password"></div>

      <!-- Final del div panel campo -->
      </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	   <label> direccion </label></br> 
       <input type="text" class="form-control" name="direccion" 
       id="direccion" maxlength="200" onkeyup="validador_campo('direccion','panel_resp_direccion','5');" 
       onkeypress="return valida_letras(event);" placeholder="* Escriba su direccion" value="<?php echo $direccion; ?>" >
       <div id="panel_resp_direccion"></div>

        <!-- Final del div panel campo -->
       </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	   <label> telefono </label></br> 
       <input type="text" class="form-control" name="telefono" 
       id="telefono" maxlength="10" onkeyup="validador_campo('telefono','panel_resp_telefono','5');" 
       onkeypress="return valida_numeros(event);" placeholder="* Escriba su telefono" value="<?php echo $telefono; ?>" >
       <div id="panel_resp_telefono"></div>

      <!-- Final del div panel campo -->
      </div>

  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	   <label> celular </label></br> 
       <input type="text" class="form-control" name="celular" 
       id="celular" maxlength="10" onkeyup="validador_campo('celular','panel_resp_celular','5');" 
       onkeypress="return valida_numeros(event);" placeholder="* Escriba su celular" value="<?php echo $celular; ?>" >
       <div id="panel_resp_celular"></div>

       <!-- Final del div panel campo -->
       </div>

  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	   <label> whatsapp </label></br> 
       <input type="text" class="form-control" name="whatsapp" 
       id="whatsapp" maxlength="10" onkeyup="validador_campo('whatsapp','panel_resp_whatsapp','5');" 
       onkeypress="return valida_numeros(event);" placeholder="* Escriba su whatsapp" value="<?php echo $whatsapp; ?>" >
       <div id="panel_resp_whatsapp"></div>

       <!-- Final del div panel campo -->
       </div>

  
<!-- -------------------------------------------------------------------- -->
 
</form>
<!-- Final del div row -->
</div>
 

