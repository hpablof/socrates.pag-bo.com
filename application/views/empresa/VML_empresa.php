
<div class="componente_menu" >

  <h4 class="titulo_menu" > System /empresa /menu </h4>
  <script type="text/javascript" src="<?php echo base_url(); ?>scripts/C_empresa.js"></script>
 
  <div class="row">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="panel_buscador">
     
     <table class="table table-bordered table-condensed table-hover table-striped">
     <tr>
      
      <td> 
       <input type="text" class="form-control" id="txt_buscar" placeholder="* Buscar empresa" onkeyup="buscar_datos(1);"> 
      </td>
      
      <td>
        <button class="btn btn-default btn-md" onclick="buscar_datos(1);" > 
        <span class="glyphicon glyphicon-search"></span>
        Buscar </button>
      
      </td>

      <td align="right">
       <button class="btn btn-default btn-md" onclick="btn_nuevo_empresa();" > 
       <span class="glyphicon glyphicon-file"></span>
       Nuevo empresa </button>
      </td>

     </tr>
     
     </table>

     <div id="panel_resultado_busqueda"> </div>

   </div>

   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="panel_listado">
   <div id="panel_paginacion_datos" class="table-responsive">
   
   <table class="table table-bordered table-condensed table-hover table-striped">
     <thead>
      <tr>
          
        <th class="hidden-lg hidden-md hidden-sm"> </th>
            <th> # </th>
    		<th> logo </th>
    		<th> razon social </th>
    		<th> encargado </th>
    		<th> rubro </th>
    		<th> nit </th>
    		<th> ci </th>
    		<th> emitido </th>
    		<th> email </th>
    		<th> password </th>
    		<th> direccion </th>
    		<th> telefono </th>
    		<th> celular </th>
    		<th> whatsapp </th>
       <th class="hidden-xs"> </th>

      </tr>

     </thead>
     <tbody>
     <?php 
     foreach($empresas as $ob_empresa)
     {
     	    $id_empresa = $ob_empresa->id_empresa;
		    $logo = $ob_empresa->logo;
		    $razon_social = $ob_empresa->razon_social;
		    $encargado = $ob_empresa->encargado;
		    $rubro = $ob_empresa->rubro;
		    $nit_empresa = $ob_empresa->nit_empresa;
		    $ci_encargado = $ob_empresa->ci_encargado;
		    $departamento = $ob_empresa->departamento;
		    $email = $ob_empresa->email;
		    $password = $ob_empresa->password;
		    $direccion = $ob_empresa->direccion;
		    $telefono = $ob_empresa->telefono;
		    $celular = $ob_empresa->celular;
		    $whatsapp = $ob_empresa->whatsapp;
     ?>
       <tr>

        <td class="col-xs-1 hidden-lg hidden-md hidden-sm"> <center>
             <a href="javascript:void();" onclick="btn_submenu_xs('<?php echo $id_empresa; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones-xs" id="panel_opciones_xs<?php echo $id_empresa; ?>">

              <div align="right" class="panel_cerrar_botones-xs" >
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu_xs('<?php echo $id_empresa; ?>');"> X </buttom>
              </div>
              <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_examinar_empresa('<?php echo $id_empresa; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_editar_empresa('<?php echo $id_empresa; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line-xs">

               <button class="btn btn-danger btn-md" onclick="btn_borrar_empresa('<?php echo $id_empresa; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 

             </div>

          </center>
          </td>
            <td> <?php echo $id_empresa; ?> </td>
		    <td> <img src="<?php echo base_url(); ?>assets/multimedia/portadas/<?php echo $logo; ?>" style="width:50px; height:50px;" > </td>
		    <td> <?php echo $razon_social; ?> </td>
		    <td> <?php echo $encargado; ?> </td>
		    <td> <?php echo $rubro; ?> </td>
		    <td> <?php echo $nit_empresa; ?> </td>
		    <td> <?php echo $ci_encargado; ?> </td>
		    <td> <?php echo $departamento; ?> </td>
		    <td> <?php echo $email; ?> </td>
		    <td> <?php echo $password; ?> </td>
		    <td> <?php echo $direccion; ?> </td>
		    <td> <?php echo $telefono; ?> </td>
		    <td> <?php echo $celular; ?> </td>
		    <td> <?php echo $whatsapp; ?> </td>
            
            <td class="col-lg-1 hidden-xs"> <center>
             <a href="javascript:void();" onclick="btn_submenu('<?php echo $id_empresa; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones" id="panel_opciones_<?php echo $id_empresa; ?>">

              <div align="right" class="panel_cerrar_botones">
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu('<?php echo $id_empresa; ?>');"> x </buttom>
              </div>

              <hr class="btn_line">

	   	         <button class="btn btn-default btn-xs" onclick="btn_asignar_empresa('<?php echo $id_empresa; ?>','<?php echo $razon_social; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-check"></span> 
	   	         asignar </button> 
	   	         
              <hr class="btn_line">

	   	         <button class="btn btn-default btn-xs" onclick="btn_examinar_empresa('<?php echo $id_empresa; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-eye-open"></span> 
	   	         examinar </button> 
	   	       
               <hr class="btn_line">

	   	         <button class="btn btn-default btn-xs" onclick="btn_editar_empresa('<?php echo $id_empresa; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
	   	           editar </button> 

               <hr class="btn_line">

	   	         <button class="btn btn-danger btn-xs" onclick="btn_borrar_empresa('<?php echo $id_empresa; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-trash"></span> 
	   	         borrar </button> 
             
             </div>

          </center>
	   	    </td>

       </tr>
     <?php
     } 
     ?>
     </tbody>
 
 <!-- Final del table -->
 </table>

 <?php require "paginacion.php"; ?>

 </div>
 <!-- Final del panel blanco -->
 </div>

 <!-- Final del div panel -->
 </div>



<!-- Final del div row -->
</div>

<?php require "VM_empresa.php"; ?>
 

