
<div class="row">
<form id="formulario" enctype="multipart/form-data">

	<!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
         <center>
        <label> logo </label></br> 

	      <img id="img_resp_logo" src="" style="width: 80%; height: 350px; margin-bottom: 1.5%; border:1px solid #086A87;"> </br>
	 
	      <span class="btn btn-success btn-file"> 
	      <span class="glyphicon glyphicon-picture"></span> Subir logo
	      
	      <input type="file" class="form-control" name="logo" 
	      id="logo" onchange="previsualizar();">

	      </span> </center>

	      <div id="panel_resp_logo"></div> 
 
	  <!-- Final del div panel campo -->
	  </div>

	  
	<!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="panel_campo">
        <label> razon social </label></br> 
	     <input type="text" class="form-control" name="razon_social" 
	     id="razon_social" maxlength="200" onkeyup="validador_campo('razon_social','panel_resp_razon_social','');" 
	     onkeypress="return valida_letras(event);" placeholder="* Escriba su razon social">
	     
	     <div id="panel_resp_razon_social"></div>

	  <!-- Final del div panel campo -->
	  </div>

	  
	<!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="panel_campo">
        <label> encargado </label></br> 
	     <input type="text" class="form-control" name="encargado" 
	     id="encargado" maxlength="200" onkeyup="validador_campo('encargado','panel_resp_encargado','5');" 
	     onkeypress="return valida_letras(event);" placeholder="* Escriba su encargado">
	     
	     <div id="panel_resp_encargado"></div>

	  <!-- Final del div panel campo -->
	  </div>

	  
	<!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="panel_campo">
         
        <label> rubro : <span id="rubro"> </span> </label></br> 
        <input type="hidden" name="id_rubro" id="id_rubro" >
 
        <ul class="nav navbar-nav" style="width:100%; ">
         
        <li class="dropdown" style="width:100%;">
          
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="background:transparent; color:#2E4053; width:100%; border:1px solid #2E4053; padding: 1%;" id="rubro" > rubro <span class="caret"></span> </a>

        <ul class="dropdown-menu" id="select-form" style="border:1px solid #2E4053;">
        <?php

         foreach($rubros as $rubro)
         {
            $id_rubro = $rubro->id_rubro;
            $rubro_n = $rubro->rubro;
            ?>
            <li> <a href="#" onclick="select('<?php echo $id_rubro; ?>','<?php echo $rubro_n; ?>','rubro');"> <?php echo $rubro_n; ?> </a> </li> 
            <?php
          } ?>
         </ul>

         </li>
        </ul> 
         
        <div id="panel_resp_rubro"></div>

    <!-- Final del div panel campo -->
    </div>
    
	<!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="panel_campo">
        <label> nit empresa </label></br> 
	     <input type="text" class="form-control" name="nit_empresa" 
	     id="nit_empresa" maxlength="200" onkeyup="validador_campo('nit_empresa','panel_resp_nit_empresa','5');" 
	     onkeypress="return valida_letras(event);" placeholder="* Escriba su nit">
	     
	     <div id="panel_resp_nit_empresa"></div>

	  <!-- Final del div panel campo -->
	  </div>

	  
	<!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="panel_campo">
        <label> ci encargado </label></br> 
	     <input type="text" class="form-control" name="ci_encargado" 
	     id="ci_encargado" maxlength="200" onkeyup="validador_campo('ci_encargado','panel_resp_ci_encargado','5');" 
	     onkeypress="return valida_letras(event);" placeholder="* Escriba su ci">
	     
	     <div id="panel_resp_ci_encargado"></div>

	  <!-- Final del div panel campo -->
	  </div>

	  
	<!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="panel_campo">
         
        <label> emitido : <span id="departamento"> </span> </label></br> 
        <input type="hidden" name="id_departamento" id="id_departamento" >
 
        <ul class="nav navbar-nav" style="width:100%; ">
         
        <li class="dropdown" style="width:100%;">
          
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="background:transparent; color:#2E4053; width:100%; border:1px solid #2E4053; padding: 1%;" id="departamento" > departamento <span class="caret"></span> </a>

        <ul class="dropdown-menu" id="select-form" style="border:1px solid #2E4053;">
        <?php

         foreach($departamentos as $departamento)
         {
            $id_departamento = $departamento->id_departamento;
            $departamento_n = $departamento->departamento;
            ?>
            <li> <a href="#" onclick="select('<?php echo $id_departamento; ?>','<?php echo $departamento_n; ?>','departamento');"> <?php echo $departamento_n; ?> </a> </li> 
            <?php
          } ?>
         </ul>

         </li>
        </ul> 
         
        <div id="panel_resp_departamento"></div>

    <!-- Final del div panel campo -->
    </div>

    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
    </div>
    
	<!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="panel_campo">
         <label> email </label></br> 
	      <input type="text" class="form-control" name="email" 
	      id="email" maxlength="300" onkeyup="validador_correo('email','5','panel_resp_email');" 
	      onkeypress="return valida_ambos(event);" placeholder="* Escriba su email">
	      <div id="panel_resp_email"></div>

	  <!-- Final del div panel campo -->
	  </div>
	  
	<!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="panel_campo">
         <label> password </label></br> 
	      <input type="text" class="form-control" name="password" 
	      id="password" maxlength="10" onkeyup="validador_campo('password','panel_resp_password','5');" 
	      onkeypress="return valida_ambos(event);" placeholder="* Escriba su password">
	      <div id="panel_resp_password"></div>

	  <!-- Final del div panel campo -->
	  </div>
	  
	<!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="panel_campo">
        <label> direccion </label></br> 
	     <input type="text" class="form-control" name="direccion" 
	     id="direccion" maxlength="200" onkeyup="validador_campo('direccion','panel_resp_direccion','5');" 
	     onkeypress="return valida_letras(event);" placeholder="* Escriba su direccion">
	     
	     <div id="panel_resp_direccion"></div>

	  <!-- Final del div panel campo -->
	  </div>

	  
	<!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="panel_campo">
         <label> telefono </label></br> 
	       <input type="text" class="form-control" name="telefono" 
	       id="telefono" maxlength="10" onkeyup="validador_campo('telefono','panel_resp_telefono','5');" 
	       onkeypress="return valida_numeros(event);" placeholder="* Escriba su telefono">
	       <div id="panel_resp_telefono"></div>

	  <!-- Final del div panel campo -->
	  </div>
	   
	<!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="panel_campo">
         <label> celular </label></br> 
	       <input type="text" class="form-control" name="celular" 
	       id="celular" maxlength="10" onkeyup="validador_campo('celular','panel_resp_celular','5');" 
	       onkeypress="return valida_numeros(event);" placeholder="* Escriba su celular">
	       <div id="panel_resp_celular"></div>

	  <!-- Final del div panel campo -->
	  </div>
	   
	<!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="panel_campo">
         <label> whatsapp </label></br> 
	       <input type="text" class="form-control" name="whatsapp" 
	       id="whatsapp" maxlength="10" onkeyup="validador_campo('whatsapp','panel_resp_whatsapp','5');" 
	       onkeypress="return valida_numeros(event);" placeholder="* Escriba su whatsapp">
	       <div id="panel_resp_whatsapp"></div>

	  <!-- Final del div panel campo -->
	  </div>
	   

<!-- Final del formulario -->
</form>

<!-- Final del div row -->
</div>
       

