
<!DOCTYPE html>
<html>
<head>
  <title> Socrates </title>
  
  <meta name="viewport" content="initial-scale=1, maximum-scale=1">
  <meta charset="utf-8">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-theme.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/theme.css">

  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js" ></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.js" ></script> 
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/validador.js" ></script> 
 
<!-- INSERCION DEL MENU DE NAVEGACION --> 

 <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" style="border-radius:0px; background:transparent;">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar" style="background:white;" ></span>
        <span class="icon-bar" style="background:white;" ></span>
        <span class="icon-bar" style="background:white;" ></span>
      </button>
      
      <a class="navbar-brand" href="<?php echo base_url(); ?>" style="color:white; text-shadow:0px; padding: 0px; margin: 0px;">   
       
       <img src="https://socrates.pag-bo.com/assets/multimedia/iconos/logo_socrates.png" style="width: 150px; height: 50px; background:;">

      </a>

    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
         <li> <a href="https://socrates.pag-bo.com/Cursos" style="text-shadow:none; color:white;"> Cursos </a> </li>
         <li> <a href="https://socrates.pag-bo.com/Clogin/Acceso" style="text-shadow:none; color:white;"> Acceso </a> </li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
    
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

</head>

<body>
 
  

