<script type="text/javascript">
	//btn_submenu_menu('contabilidad');
</script>

<style>
    .panel_area_sistema
    {
      width:100%;
      height: 550px;
      overflow-y:auto;
      background:white;
      border: 1px solid #060817;
      margin-bottom:0.5%;
       
    }
    .panel_footer_sistema
    {
        max-width:100%;
        width:100%;
        overflow-x:scroll;
        overflow-y:none;
        background:;
        height: 50px;
    }
    
    .horizontal-scroll-contenedor
    {
        width: auto;
        height: 25px;
        padding: 0px;
        white-space: nowrap;
        background:;
        margin:0px;
    }
    
    .horizontal-scroll-contenedor div
    {
        width: 100px;
        height: 20px;
        padding: 0;
        display: inline-block;
        background:blue;
        margin:0px;
    }
    
    
</style>
<div class="row" style="margin:0px; ">
 
 <input type="hidden" id="modal_select_actual" value="menu_select_principal">
 
 
  <!-- PANEL PLAN DE CUENTAS  -->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:0px; background:white;" id="menu_select_principal">
          
      <div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-top: 1%;">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
    
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
          <div class="item active">
            <img src="<?php echo base_url(); ?>assets/multimedia/iconos/slider_1.jpg " style="width: 100%; height: 500px;" >
          </div>
    
          <div class="item">
            <img src="<?php echo base_url(); ?>assets/multimedia/iconos/slider_2.jpg " style="width: 100%; height: 500px;" >
          </div>
    
          <div class="item">
            <img src="<?php echo base_url(); ?>assets/multimedia/iconos/slider_3.jpg " style="width: 100%; height: 500px;" >
          </div>
        </div>
    
        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    
    </div>
  
     
  </div>

 <!-- AREA DE INVENTARIOS  -->
 
 <!-- PANEL EMPRESAS  -->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:0px; display:none; " id="menu_empresa">

     <div id="header_empresa">
       <table style="width:100%; background:#060817; color:white; cursor:pointer;">
          <tr> 
           <td style="width:90%;" > EMPRESAS  </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_empresa')"; > <span class="glyphicon glyphicon-minus"> </span> </td>
           <td style="width:3.3%;" > <span class="glyphicon glyphicon-new-window"> </span> </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_empresa')"; > <span class="glyphicon glyphicon-remove"> </span> </td>
          </tr> 
       </table>            
         
     </div>
     
     <div  class="panel_area_sistema" id="contenedor_empresas" style="width:100%;">
         
     </div>
     
  </div>

 <!-- PANEL NITS EMPRESAS  -->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:0px; display:none; " id="menu_nits_empresas">

     <div id="header_nits_empresas">
       <table style="width:100%; background:#060817; color:white; cursor:pointer;">
          <tr> 
           <td style="width:90%;" > NITS EMPRESAS  </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_nits_empresas')"; > <span class="glyphicon glyphicon-minus"> </span> </td>
           <td style="width:3.3%;" > <span class="glyphicon glyphicon-new-window"> </span> </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_nits_empresas')"; > <span class="glyphicon glyphicon-remove"> </span> </td>
          </tr> 
       </table>            
         
     </div>
     
     <div  class="panel_area_sistema" id="contenedor_nits_empresas" style="width:100%;">
         
     </div>
     
  </div>

 <!-- PANEL RUBROS  -->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:0px; display:none; " id="menu_rubro">

     <div id="header_rubro">
       <table style="width:100%; background:#060817; color:white; cursor:pointer;">
          <tr> 
           <td style="width:90%;" > RUBROS  </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_rubro')"; > <span class="glyphicon glyphicon-minus"> </span> </td>
           <td style="width:3.3%;" > <span class="glyphicon glyphicon-new-window"> </span> </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_nits_empresas')"; > <span class="glyphicon glyphicon-remove"> </span> </td>
          </tr> 
       </table>            
         
     </div>
     
     <div  class="panel_area_sistema" id="contenedor_rubro" style="width:100%;">
         
     </div>
     
  </div>


 <!-- PANEL PRODUCTOS  -->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:0px; display:none; " id="menu_productos">

     <div id="header_empresa">
       <table style="width:100%; background:#060817; color:white; cursor:pointer;">
          <tr> 
           <td style="width:90%;" > PRODUCTOS  </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_productos')"; > <span class="glyphicon glyphicon-minus"> </span> </td>
           <td style="width:3.3%;" > <span class="glyphicon glyphicon-new-window"> </span> </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_productos')"; > <span class="glyphicon glyphicon-remove"> </span> </td>
          </tr> 
       </table>            
         
     </div>
     
     <div  class="panel_area_sistema" id="contenedor_productos" style="width:100%;">
         
     </div>
     
  </div>


 <!-- PANEL AREAS DE PRODUCTOS -->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:0px; display:none; " id="menu_area_productos">

     <div id="header_empresa">
       <table style="width:100%; background:#060817; color:white; cursor:pointer;">
          <tr> 
           <td style="width:90%;" > AREA PRODUCTOS  </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_area_productos')"; > <span class="glyphicon glyphicon-minus"> </span> </td>
           <td style="width:3.3%;" > <span class="glyphicon glyphicon-new-window"> </span> </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_area_productos')"; > <span class="glyphicon glyphicon-remove"> </span> </td>
          </tr> 
       </table>            
         
     </div>
     
     <div  class="panel_area_sistema" id="contenedor_area_productos" style="width:100%;">
         
     </div>
     
  </div> 
  
 <!-- PANEL PROVEEDORES -->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:0px; display:none; " id="menu_proveedor">

     <div id="header_empresa">
       <table style="width:100%; background:#060817; color:white; cursor:pointer;">
          <tr> 
           <td style="width:90%;" > PROVEEDORES </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_proveedor')"; > <span class="glyphicon glyphicon-minus"> </span> </td>
           <td style="width:3.3%;" > <span class="glyphicon glyphicon-new-window"> </span> </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_proveedor')"; > <span class="glyphicon glyphicon-remove"> </span> </td>
          </tr> 
       </table>             
     </div>
     
     <div  class="panel_area_sistema" id="contenedor_proveedor" style="width:100%;">
         
     </div>
     
  </div> 

 <!-- PANEL COMPRAS -->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:0px; display:none; " id="menu_compra">

     <div id="header_empresa">
       <table style="width:100%; background:#060817; color:white; cursor:pointer;">
          <tr> 
           <td style="width:90%;" > COMPRAS  </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_compra')"; > <span class="glyphicon glyphicon-minus"> </span> </td>
           <td style="width:3.3%;" > <span class="glyphicon glyphicon-new-window"> </span> </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_compra')"; > <span class="glyphicon glyphicon-remove"> </span> </td>
          </tr> 
       </table>             
     </div>
     
     <div  class="panel_area_sistema" id="contenedor_compra" style="width:100%;">
         
     </div>
     
  </div> 

 <!-- PANEL CLIENTES -->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:0px; display:none; " id="menu_cliente">

     <div id="header_empresa">
       <table style="width:100%; background:#060817; color:white; cursor:pointer;">
          <tr> 
           <td style="width:90%;" > CLIENTE  </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_cliente')"; > <span class="glyphicon glyphicon-minus"> </span> </td>
           <td style="width:3.3%;" > <span class="glyphicon glyphicon-new-window"> </span> </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_cliente')"; > <span class="glyphicon glyphicon-remove"> </span> </td>
          </tr> 
       </table>             
     </div>
     
     <div  class="panel_area_sistema" id="contenedor_cliente" style="width:100%;">
         
     </div>
     
  </div> 

 <!-- PANEL CLIENTES -->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:0px; display:none; " id="menu_venta">

     <div id="header_empresa">
       <table style="width:100%; background:#060817; color:white; cursor:pointer;">
          <tr> 
           <td style="width:90%;" > VENTAS  </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_venta')"; > <span class="glyphicon glyphicon-minus"> </span> </td>
           <td style="width:3.3%;" > <span class="glyphicon glyphicon-new-window"> </span> </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_venta')"; > <span class="glyphicon glyphicon-remove"> </span> </td>
          </tr> 
       </table>             
     </div>
     
     <div  class="panel_area_sistema" id="contenedor_venta" style="width:100%;">
         
     </div>
     
  </div> 

 <!-- AREA CONTABLE  -->
 <!-- -------------------------------------------------------------------------------------------------------------------------------------------------------- -->
 
 <!-- PANEL MONEDA  -->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:0px; display:none; " id="menu_moneda">

     <div id="header_niveles">
       <table style="width:100%; background:#060817; color:white; cursor:pointer;">
          <tr> 
           <td style="width:90%;" > MONEDA  </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_moneda')"; > <span class="glyphicon glyphicon-minus"> </span> </td>
           <td style="width:3.3%;" > <span class="glyphicon glyphicon-new-window"> </span> </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_moneda')"; > <span class="glyphicon glyphicon-remove"> </span> </td>
          </tr> 
       </table>            
         
     </div>
     
     <div  class="panel_area_sistema" id="contenedor_monedas" style="width:100%;">
         
     </div>
     
  </div>
  
 <!-- PANEL PARAMETRIZACION  -->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:0px; display:none; " id="menu_parametrizacion">

     <div id="header_niveles">
       <table style="width:100%; background:#060817; color:white; cursor:pointer;">
          <tr> 
           <td style="width:90%;" > PARAMETRIZACION  </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_niveles')"; > <span class="glyphicon glyphicon-minus"> </span> </td>
           <td style="width:3.3%;" > <span class="glyphicon glyphicon-new-window"> </span> </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_niveles')"; > <span class="glyphicon glyphicon-remove"> </span> </td>
          </tr> 
       </table>            
         
     </div>
     
     <div  class="panel_area_sistema" id="contenedor_parametrizacion" style="width:100%;">
         
     </div>
     
  </div>
  
  
 <!-- PANEL NIVELES  -->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:0px; display:none; " id="menu_niveles">

     <div id="header_niveles">
       <table style="width:100%; background:#060817; color:white; cursor:pointer;">
          <tr> 
           <td style="width:90%;" > NIVELES  </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_niveles')"; > <span class="glyphicon glyphicon-minus"> </span> </td>
           <td style="width:3.3%;" > <span class="glyphicon glyphicon-new-window"> </span> </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_niveles')"; > <span class="glyphicon glyphicon-remove"> </span> </td>
          </tr> 
       </table>            
         
     </div>
     
     <div  class="panel_area_sistema" id="contenedor_niveles" style="width:100%;">
         
     </div>
     
  </div>
  
 <!-- PANEL PLAN DE CUENTAS  -->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:0px; display:none; " id="menu_plan_de_cuentas">

     <div id="header_plan_de_cuentas">
       <table style="width:100%; background:#060817; color:white; cursor:pointer;">
          <tr> 
           <td style="width:90%;" > PLAN DE CUENTAS  </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_plan_de_cuentas')"; > <span class="glyphicon glyphicon-minus"> </span> </td>
           <td style="width:3.3%;" > <span class="glyphicon glyphicon-new-window"> </span> </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_plan_de_cuentas')"; > <span class="glyphicon glyphicon-remove"> </span> </td>
          </tr> 
       </table>            
         
     </div>
     
     <div  class="panel_area_sistema" id="contenedor_plan_de_cuentas" style="width:100%;">
     </div>
 
  </div>
  
  <!-- PANEL AREA DE SEMINARIOS Y CURSOS  -->  

  <!-- PANEL PARTICIPANTE  -->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:0px; display:none; " id="menu_curso">

     <div id="header_rubro">
       <table style="width:100%; background:#060817; color:white; cursor:pointer;">
          <tr> 
           <td style="width:90%;" > RUBROS  </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_curso')"; > <span class="glyphicon glyphicon-minus"> </span> </td>
           <td style="width:3.3%;" > <span class="glyphicon glyphicon-new-window"> </span> </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_curso')"; > <span class="glyphicon glyphicon-remove"> </span> </td>
          </tr> 
       </table>            
         
     </div>
     
     <div  class="panel_area_sistema" id="contenedor_curso" style="width:100%;">
         
     </div>
     
  </div>
  
  <!-- PANEL PARTICIPANTE  -->
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:0px; display:none; " id="menu_participante">

     <div id="header_rubro">
       <table style="width:100%; background:#060817; color:white; cursor:pointer;">
          <tr> 
           <td style="width:90%;" > RUBROS  </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_participante')"; > <span class="glyphicon glyphicon-minus"> </span> </td>
           <td style="width:3.3%;" > <span class="glyphicon glyphicon-new-window"> </span> </td>
           <td style="width:3.3%;" onclick = "btn_ocultar('menu_nits_empresas')"; > <span class="glyphicon glyphicon-remove"> </span> </td>
          </tr> 
       </table>            
         
     </div>
     
     <div  class="panel_area_sistema" id="contenedor_participante" style="width:100%;">
         
     </div>
     
  </div>
 
 
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin:0px; overflow-x:scroll;">
      
      
     <div class="panel_footer_sistema" id="panel_socrates_lista_empresas">
     
            <div class="horizontal-scroll-contenedor">
            	
            	<div style="width:200px;"> 
            	  
            	  <table style="width:100%; background:#060817; color:white; margin:0px;">
                   <tr> 
                    <td style="width:400px;" id="footer_empresas" onclick="btn_mostrar('menu_plan_de_cuentas');"> Plan de Cuentas  </td>
                    <td style="width:20px;"  id="footer_empresas" > <span class="glyphicon glyphicon-minus"> </span> </td>
                    <td style="width:20px;"  id="footer_empresas" > <span class="glyphicon glyphicon-new-window"> </span> </td>
                    <td style="width:20px; border-right: 2px solid white;" id="footer_empresas" > <span class="glyphicon glyphicon-remove"> </span> </td>
                   </tr>
                  </table>
                  
            	</div>
            	
            	<div style="width:200px;"> 
            	  
            	  <table style="width:100%; background:#060817; color:white; margin:0px;">
                   <tr> 
                    <td style="width:400px;" id="footer_empresas" > Parametrizaci��n  </td>
                    <td style="width:20px;"  id="footer_empresas" > <span class="glyphicon glyphicon-minus"> </span> </td>
                    <td style="width:20px;"  id="footer_empresas" > <span class="glyphicon glyphicon-new-window"> </span> </td>
                    <td style="width:20px; border-right: 2px solid white;" id="footer_empresas" > <span class="glyphicon glyphicon-remove"> </span> </td>
                   </tr>
                  </table>
                  
            	</div>

            	<div style="width:200px;"> 
            	  
            	  <table style="width:100%; background:#060817; color:white; margin:0px;">
                   <tr> 
                    <td style="width:400px;" id="footer_empresas" > Niveles </td>
                    <td style="width:20px;"  id="footer_empresas" > <span class="glyphicon glyphicon-minus"> </span> </td>
                    <td style="width:20px;"  id="footer_empresas" > <span class="glyphicon glyphicon-new-window"> </span> </td>
                    <td style="width:20px; border-right: 2px solid white;" id="footer_empresas" > <span class="glyphicon glyphicon-remove"> </span> </td>
                   </tr>
                  </table>
                  
            	</div> 

            	<div style="width:200px;"> 
            	  
            	  <table style="width:100%; background:#060817; color:white; margin:0px;">
                   <tr> 
                    <td style="width:400px;" id="footer_empresas" > Monedas </td>
                    <td style="width:20px;"  id="footer_empresas" > <span class="glyphicon glyphicon-minus"> </span> </td>
                    <td style="width:20px;"  id="footer_empresas" > <span class="glyphicon glyphicon-new-window"> </span> </td>
                    <td style="width:20px; border-right: 2px solid white;" id="footer_empresas" > <span class="glyphicon glyphicon-remove"> </span> </td>
                   </tr>
                  </table>
                  
            	</div>             	
            	
            	
            </div>      
       
     </div> 
  
  </div>
  
<!-- FINAL DEL DIV ROW  -->
</div>

</body>




