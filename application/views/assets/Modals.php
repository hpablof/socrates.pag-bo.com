<style type="text/css">

  .modal_componente 
  {
    left: 25%;
    top: 0%;
    width: 550px;

    min-width: 300px;
    min-height: 300px;

    height: 585px;
    max-width: 100%;
    max-height: 600px;
 
    right: 0px; 
 
    padding: 10px 10px 10px 10px;
    border: 1px solid #bdc3c7;
    border-top: 1px solid transparent;
    background: white;
    
    display: block;
    margin-top: 0%;
    margin-right: 0%;
    float: left;
    left: 0px;
    display: none;
    position: relative;
    word-wrap: break-word;
  
  }


</style>

<script type="text/javascript">
  
  $(document).ready(function() 
  {
 
      $('#modal_moneda').draggable({ containment: "#panel_moneda" });
      $("#modal_moneda").resizable({
          handles: 'n, e, s, w, ne, se, sw, nw'
      });

     /* ------------------------------------------------------------------------- */

      $('#modal_nivel').draggable({ containment: "#panel_moneda" });
      $("#modal_nivel").resizable({
          handles: 'n, e, s, w, ne, se, sw, nw'
      }); 


     /* ------------------------------------------------------------------------- */

      $('#modal_plan_cuentas').draggable({ containment: "#panel_moneda" });
      $("#modal_plan_cuentas").resizable({
          handles: 'n, e, s, w, ne, se, sw, nw'
      }); 

  });


</script>

<div class="row" id="panel_principal_modals" style="margin: 0px; padding: 0px; height: 700px; background: #2c3e50;">


<!-- INICIO DEL MODAL DE MONEDA -->	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="panel_moneda" style="margin: 0px; padding: 0px; height: 700px; background: #bdc3c7;">


       <div id="modal_moneda" class="modal_componente"> 

          <div class="modal_header" align="right" style="margin: 0px; margin-top: -10px; margin-right: -10px; background: #1B2631; color:white; margin-left: -10px;">
            
            <table style="width: 100px; background: ;">
              <tr>
                
                <td style="padding: 1%;" align="right">
                  <i class="fa fa-window-minimize" aria-hidden="true" style="font-size: 17px;" onclick="btn_ocultar('modal_moneda')"; ></i>
                </td>

                <td style="padding: 1%;" align="right">
                  <i class="fas fa-window-maximize" aria-hidden="true" style="font-size: 17px;" onclick="btn_ampliar('modal_moneda');" ></i>
                </td>
                
                <td style="padding: 1%;" align="right">
                  <i class="fa fa-window-close" aria-hidden="true" style="font-size: 17px;" onclick="btn_ocultar('modal_moneda');" ></i>
                </td>

              </tr>
            </table>
          </div>
         
	        <div class="modal_body">
	        	 <div id="contenedor_moneda"> 
	        	 </div>
	        </div>

	        <div class="modal_footer">
	        	
	        </div>

        <!-- FINAL DEL MODAL -->
        </div>
	        
 
       <div id="modal_nivel" class="modal_componente"> 

          <div class="modal_header" align="right" style="margin: 0px; margin-top: -10px; margin-right: -10px; background: #1B2631; color:white; margin-left: -10px;">
            
            <table style="width: 100px; background: ;">
              <tr>
                
                <td style="padding: 1%;" align="right">
                  <i class="fa fa-window-minimize" aria-hidden="true" style="font-size: 17px;" onclick="btn_ocultar('modal_nivel')"; ></i>
                </td>

                <td style="padding: 1%;" align="right">
                  <i class="fas fa-window-maximize" aria-hidden="true" style="font-size: 17px;" onclick="btn_ampliar('modal_nivel');" ></i>
                </td>
                
                <td style="padding: 1%;" align="right">
                  <i class="fa fa-window-close" aria-hidden="true" style="font-size: 17px;" onclick="btn_ocultar('modal_nivel');" ></i>
                </td>

              </tr>
            </table>
          </div>
         
	        <div class="modal_body">

	             <div id="contenedor_nivel"> 
	        	 </div>
	        	
	        </div>

	        <div class="modal_footer">
	        	
	        </div>

        <!-- FINAL DEL MODAL -->
        </div>


 
       <div id="modal_plan_cuentas" class="modal_componente"> 

          <div class="modal_header" align="right" style="margin: 0px; margin-top: -10px; margin-right: -10px; background: #1B2631; color:white; margin-left: -10px;">
            
            <table style="width: 100px; background: ;">
              <tr>
                
                <td style="padding: 1%;" align="right">
                  <i class="fa fa-window-minimize" aria-hidden="true" style="font-size: 17px;" onclick="btn_ocultar('modal_plan_cuentas')"; ></i>
                </td>

                <td style="padding: 1%;" align="right">
                  <i class="fas fa-window-maximize" aria-hidden="true" style="font-size: 17px;" onclick="btn_ampliar('modal_plan_cuentas');" ></i>
                </td>
                
                <td style="padding: 1%;" align="right">
                  <i class="fa fa-window-close" aria-hidden="true" style="font-size: 17px;" onclick="btn_ocultar('modal_plan_cuentas');" ></i>
                </td>

              </tr>
            </table>
          </div>
         
          <div class="modal_body">

               <div id="contenedor_plan_cuentas"> 
             </div>
            
          </div>

          <div class="modal_footer">
            
          </div>

        <!-- FINAL DEL MODAL -->
        </div>

	        
    <!-- FINAL DEL COMPONENTE MODAL MONEDA --> 
	</div>



<!-- FINAL DEL ROW DE COMPOSICION -->	
</div>