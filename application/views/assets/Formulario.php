
<div class="row" >
  
  <script src="https://socrates.pag-bo.com/scripts/Formulario.js"></script>
  
  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" >
      
  </div>    

  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" >
    <div style="background:white; margin:2%; padding:4%;">      
     
     <h4 style="margin:0px; text-align:center;"> Registro de Participantes del Curso </h4>
     <h5 style="margin:0px; text-align:center; color:#0098D1; font-weight:bold;"> ELABORACIÓN Y PRESENTACIÓN DE ESTADOS FINANCIEROS COMPUTARIZADOS 2020</h5>
     <p style="margin:0px; text-align:center; margin-bottom:2%; font-size:10px;"> * Debes llenar todos los campos para el registro correcto </p>
     <hr>
     
     <div class="row" >
         
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             
             <label> Nombre Completo </label>
             <input type="text" class="form-control" id="nombre_completo" placeholder="* Nombre Completo" autocomplete="off" style="text-transform:uppercase;" maxlength="200">
             <div id="panel_resp_nombre_completo"></div>
             
         </div>

         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             
             <label> CI </label>
             <input type="text" class="form-control" id="ci" placeholder="* ci " autocomplete="off" style="text-transform:uppercase;" maxlength="15">
             <div id="panel_resp_ci"></div>
             
         </div>
         
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             
             <label>Razón Social </label>
             <input type="text" class="form-control" id="razon_social" placeholder="* Razón Social" autocomplete="off" style="text-transform:uppercase;" maxlength="200">
             <div id="panel_resp_razon_social"></div>
             
         </div>
         
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             
             <label> NIT </label>
             <input type="text" class="form-control" id="ci_nit" placeholder="* nit" autocomplete="off" style="text-transform:uppercase;" maxlength="15">
             <div id="panel_resp_ci_nit"></div>
             
         </div>
         
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             
             <label> Email </label>
             <input type="text" class="form-control" id="email" placeholder="* email" autocomplete="off" style="text-transform:;" maxlength="200">
             <div id="panel_resp_email"></div>
         </div>    

         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             
             <label> Celular / Whatsapp </label>
             <input type="text" class="form-control" id="celular" placeholder="* numero" autocomplete="off" style="text-transform:;" maxlength="8" >
             <div id="panel_resp_celular"></div>
         </div>  
         
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             
             <label> Seleccione Cómo realizo el Pago </label>
             <select class="form-control" id="opcion_tipo_pago" onchange="btn_selec_codigo_transferencia();">
                 <option value=""> Seleccione </option>
                 <option value="1" > Banco Union </option>
                 <option value="2" > Banco Mercantil </option>
                 <option value="3" > Banco Bisa </option>
                 <option value="4" > Pago en Efectivo </option>
                 
             </select>
             <div id="panel_resp_tipo_pago"></div>
         </div>
         
                      
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             
             <label> codigo/numero de transferencia </label>
             <input type="text" class="form-control" id="codigo_transferencia" placeholder="* numero o codigo de transferencia" autocomplete="off" style="text-transform:uppercase;" maxlength="50">
             <div id="panel_resp_codigo_transferencia"></div>
             
         </div>

         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             
             <div id="panel_resp_formulario_curso"> </div>
             
             <center style="margin-top:2%;">
             <button class="btn btn-info btn-lg" onclick="btn_registrar_estudiante_curso();"> <span class="glyphicon glyphicon-check"></span> ENVIAR </button>
             
             </center>
             
         </div>
     
     <!-- FINAL DEL ROW DEL FORMULARIO -->         
     </div>   
     
  </div> 
  <!-- FINAL DEL COMPONENTE DE REGISTRO -->   
  </div>  
  
 </div>    
 