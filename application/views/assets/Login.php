
<script type="text/javascript" src="<?php echo base_url(); ?>scripts/C_login.js"></script>

<div class="row" style="margin:0px; padding:0px;">
 
  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="margin:0px; padding:0px; ">
    <div style="background:white; margin:3%; padding:2%;" >
      <h4 align="center"> Bienvenido al Sistema Socrates </h4>

      <label> Email </label>
      <input type="text" class="form-control" id="email" placeholder="* Email"  autocomplete="off">
      </br>

      <label> Password </label>
      <input type="password" class="form-control" id="password" placeholder="* Password"  autocomplete="off">
      </br>

      <label> Confirme su Password </label>
      <input type="password" class="form-control" id="confir_password" placeholder="* Confirme su Password"  autocomplete="off" onchange="btn_login();">
      </br>

      <hr>
      <div id="panel_respuesta_login"> </div>
          
          <center> 
            <button class="btn btn-info btn-md"  onclick="btn_login();" > <span class="glyphicon glyphicon-circle-arrow-right"></span> INGRESAR </button> 
          </center>
          
    </div>
  </div>


  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" >
    
          <div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-top: 1%;">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
              <div class="item active">
                <img src="https://socrates.pag-bo.com/assets/multimedia/iconos/slider_1.jpg" style="width: 100%; height: 450px;" >
              </div>

              <div class="item">
                <img src="https://socrates.pag-bo.com/assets/multimedia/iconos/slider_2.jpg" style="width: 100%; height: 450px;" >
              </div>

              <div class="item">
                <img src="https://socrates.pag-bo.com/assets/multimedia/iconos/slider_3.png" style="width: 100%; height: 450px;" >
              </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>

        </div>

</div>

</body>

