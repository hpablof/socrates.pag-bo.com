<!DOCTYPE html>
<html>
<head>
  <title> Socrates </title>

  <meta name="viewport" content="initial-scale=1, maximum-scale=1">
  <meta charset="utf-8">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-theme.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/theme.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.structure.css">

  <link href="<?php echo base_url(); ?>assets/fontawesome/css/fontawesome.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/fontawesome/css/brands.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/fontawesome/css/solid.css" rel="stylesheet">

  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js" ></script>

  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.js" ></script> 
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/validador.js" ></script> 

  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui.js" ></script> 

  <script type="text/javascript" src="<?php echo base_url(); ?>scripts/C_menu.js" ></script> 
  
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js" ></script>

 
<!-- INSERCION DEL MENU DE NAVEGACION --> 

 <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" style="border-radius:0px; background:transparent;">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar" style="background:white;" ></span>
        <span class="icon-bar" style="background:white;" ></span>
        <span class="icon-bar" style="background:white;" ></span>
      </button>

      <a class="navbar-brand" href="<?php echo base_url(); ?>Clogin/menu" style="color:white; text-shadow:0px; padding: 0px; margin: 0px;">   
            <img src="https://socrates.pag-bo.com/assets/multimedia/iconos/logo_socrates.png" style="width: 150px; height: 50px; ">
       </a>

    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">

        <li> <a href="javascript:void;" onclick="btn_menu_responsivo();" > <span style="color: #ecf0f1; border:1px solid #ecf0f1; padding:5px;" class="glyphicon glyphicon-align-justify"> </span>  </a> </li>
        <li> <a href="javascript:void;" style="color:white; text-shadow:none;">   Empresa :  <label id="empresa_select" style="box-shadow:none;"> sin asignar </label> 
              <input type="text" id="id_empresa_select" style="color:black;" value="1">
              <input type="text" id="id_gestion_select" style="color:black;" value="4">
            </a> 
        </li>
        
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo base_url(); ?>Clogin/perfil" style="color: white; background: transparent; font-size:12px;"> <span class="glyphicon glyphicon-wrench" ></span> Soporte </a></li>
        <li><a href="<?php echo base_url(); ?>Clogin/perfil" style="color: white; background: transparent; font-size:12px;"> <span class="glyphicon glyphicon-user" ></span> Perfil </a></li>

        <li><a href="<?php echo base_url(); ?>Clogin/salir" style="color: white; background: transparent; font-size:12px;"> <span class="glyphicon glyphicon-log-in" ></span> Salir </a></li>
    
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

</head>

<body>

<style type="text/css">
    
    .componente_menu_links
    {
        padding: 0%;
        display: none;
        margin-top: 0%;
        background: #12102B;
    }
    
    .componente_menu_links h6
    {
      border-bottom: 1px solid #bdc3c7;
      padding: 3%;
    }
 
    .link_menu
    {
        width: 100%;
        text-decoration: none;
        font-weight: bold;
        font-size: 12px;
        color: white;
        padding-left:5%;
        
    }

    .link_menu:hover
    { 
       text-decoration: none;
       color:white;
 
    }

    .link_menu:focus
    { 
       text-decoration: none;
       color:white;
 
    }   
    .componente_menu_sublinks
    {
        padding: 0%;
        display: none;
        margin-top: 0%;
        background: #ecf0f1; 
        padding-left:2.5%;
        color:white;
    }
    
    .link_submenu
    {
       text-decoration: none;
       color: #060817; 
       padding-left: 10%;
    }
    
    .link_submenu:hover
    { 
       text-decoration: none;
       color: #060817;
 
    }

    .link_submenu:focus
    { 
       text-decoration: none;
       color: #060817;
 
    }
    
</style>

<script type="text/javascript">
    var cont_menu=0;
    var cont_submenu=0;

    function btn_submenu_menu(area)
    {  
        if (cont_menu==0)
        {
          $('#menu_'+area).show(500);
          cont_menu=1;  
        }
        else 
        {
          $('#menu_'+area).hide(500);  
          cont_menu=0;
        } 
    } 
    
    function btn_submenu_sublinks(subarea)
    {
        if (cont_submenu==0)
        {
          $('#submenu_'+subarea).show(500);
          cont_submenu=1;  
        }
        else 
        {
          $('#submenu_'+subarea).hide(500);  
          cont_submenu=0;
        }        
    }
    
    var cont_resp=0
    function btn_menu_responsivo()
    {   

        
        if(cont_resp==0)
        {
         $("#panel_menu_socrates").css("width","0%");
         $("#panel_menu_socrates").css("display","none");
         $("#panel_contenido_socrates").css("width","100%"); 
         cont_resp=1;
        }
        else
        {
         $("#panel_menu_socrates").css("width","17%");
         $("#panel_menu_socrates").css("display","block");
         $("#panel_contenido_socrates").css("width","83%"); 
         cont_resp=0;            
        }
    }
    
</script>

 <div class="row" style="margin:0px; padding:0px;">
  
  <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 hidden-xs" style="height: auto;" id="panel_menu_socrates"> 
    <input type="hidden" id="id_usuario_session" value="1"> 
    <div class="row">
      <div class="col-lg-12" style="padding: 0%; color:white; margin-top: 1%; width: 100%;">

       <ul class="menu" >

          <li class="submenu" > 
            <a class="link" href="javascript:void();" >___________ Modulos ___________</a> 
          </li>

          <li class="submenu" > 
            <h6 style="margin:0px; padding:0px; padding-left: 5%;" onclick="btn_empresa_menu();" > <span class="glyphicon glyphicon-compressed" style="margin-right: 5%;"></span> EMPRESAS </h6> 
          </li>

          <li class="submenu" > 
            <h6 style="margin:0px; padding:0px; padding-left: 5%;" onclick="btn_rubros_menu();" >  <span class="glyphicon glyphicon-book" style="margin-right: 5%;"></span>  RUBROS </h6>
          </li>

          
          <li class="submenu" onclick="btn_submenu_menu('datos');" > 
            <a class="link" href="javascript:void();" > <span class="glyphicon glyphicon-shopping-cart"> </span> Inventarios  </a> 
    
            <div class="componente_menu_links" id="menu_datos">
              
              <h6 onclick="btn_productos_menu();" > <a class="link_menu" href="javascript:void();"> <span class="glyphicon glyphicon-record"></span>  Productos </a></h6>

              <h6 onclick="btn_area_productos_menu();" > <a class="link_menu" href="javascript:void();"> <span class="glyphicon glyphicon-record"></span>  Categorias </a></h6>

              <h6 onclick="btn_proveedor_menu();" > <a class="link_menu" href="javascript:void();"> <span class="glyphicon glyphicon-record"></span>  Proveedores </a></h6>

               <h6 onclick="btn_compra_menu();" > <a class="link_menu" href="javascript:void();"> <span class="glyphicon glyphicon-record"></span>  Compras </a></h6>

              <h6 onclick="btn_cliente_menu();" > <a class="link_menu" href="javascript:void();"> <span class="glyphicon glyphicon-record"></span>   Clientes </a></h6>

              <h6 onclick="btn_venta_menu();"> <a class="link_menu" href="javascript:void();"> <span class="glyphicon glyphicon-record"></span>  Ventas </a></h6>

              <h6> <a class="link_menu" href="<?php echo base_url(); ?>Cempresa/"> <span class="glyphicon glyphicon-record"></span>  Servicios </a></h6>
              
    
            </div>
    
          </li>

          <li class="submenu"  > 
            <a class="link" href="javascript:void();" onclick="btn_submenu_menu('contable');" > <span class="glyphicon glyphicon-share"> </span> Contabilidad  </a> 
    
            <div class="componente_menu_links" id="menu_contable">

              <h6 onclick="btn_submenu_sublinks('plan_empresas');" > <a class="link_menu" href="javascript:void();" > 
              <span class="glyphicon glyphicon-record"></span>  Lista de Empresas </a></h6>
                
                <div class="componente_menu_sublinks" id="submenu_plan_empresas">
                    <h6> <a class="link_submenu" href="javascript:void();" onclick="btn_empresa_menu();" > - Empresas </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();" onclick="btn_menu_nits_empresas();" > - Nits Empresas </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();"> - Consultora </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();"> - Tipo de Cambio </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();" onclick="btn_menu_rubro();" > - Rubro </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();"> - Departamentos </a></h6>
                </div> 
                
              <h6 onclick="btn_submenu_sublinks('plan_cuentas');" > <a class="link_menu" href="javascript:void();" > 
              <span class="glyphicon glyphicon-record"></span>  Plan de Cuentas </a></h6>
                
                <div class="componente_menu_sublinks" id="submenu_plan_cuentas">
                    <h6> <a class="link_submenu" href="javascript:void();" onclick="btn_menu_plan_cuentas_asignacion();" > - Plan de Cuentas </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();" onclick="btn_menu_parametrizacion();" > - Parametrizacion </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();" onclick="btn_menu_nivel_pc();" > - Niveles </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();" onclick="btn_menu_monedas();" > - Monedas </a></h6>
                </div>    

              <h6 onclick="btn_submenu_sublinks('comprobantes');" > <a class="link_menu" href="javascript:void();" > 
              <span class="glyphicon glyphicon-record"></span>  Comprobantes - Libro Mayor </a></h6>
                
                <div class="componente_menu_sublinks" id="submenu_comprobantes">
                    <h6> <a class="link_submenu" href="javascript:void();"> - Libro Mayor  </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();"> - Libro Diario </a></h6>                  

                    <h6> <a class="link_submenu" href="javascript:void();"> - Ingresos </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();"> - Egresos </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();"> - Diarios </a></h6>
                    
                    <h6> <a class="link_submenu" href="javascript:void();"> - Ajustes </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();"> - Importar Desde Excel </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();"> - Exportar a Excel </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();"> - Enlace Contable </a></h6>
                    
                </div> 

              <h6 onclick="btn_submenu_sublinks('activo_fijo');" > <a class="link_menu" href="javascript:void();" > 
              <span class="glyphicon glyphicon-record"></span>  Activos Fijo </a></h6>
                
                <div class="componente_menu_sublinks" id="submenu_activo_fijo">
                    <h6> <a class="link_submenu" href="javascript:void();"> - Parametrizacion A.F. </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();"> - Crear Items A.F. </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();"> - Bajas A.F. </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();"> - Reportes A.F. </a></h6>
    
                </div> 

              <h6 onclick="btn_submenu_sublinks('libro_de_compras');" > <a class="link_menu" href="javascript:void();" > 
              <span class="glyphicon glyphicon-record"></span>  LCV - Bancarización </a></h6>
                
                <div class="componente_menu_sublinks" id="submenu_libro_de_compras">
                    <h6> <a class="link_submenu" href="javascript:void();"> - Compras </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();"> - Ventas </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();"> - Bancarizacion Compras </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();"> - Bancarizacion Ventas </a></h6>
                    
                    <h6> <a class="link_submenu" href="javascript:void();"> - Importar Libro de Compras  </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();"> - Importar Libro de Ventas </a></h6>

    
                </div> 

              <h6 onclick="btn_submenu_sublinks('presupuestos');" > <a class="link_menu" href="javascript:void();" > 
              <span class="glyphicon glyphicon-record"></span>  Presupuestos </a></h6>
                
                <div class="componente_menu_sublinks" id="submenu_presupuestos">
                    <h6> <a class="link_submenu" href="javascript:void();"> - Presupuestos </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();"> - Ejecucion Presupuestaria  </a></h6>

                </div> 

              <h6 onclick="btn_submenu_sublinks('impuestos');" > <a class="link_menu" href="javascript:void();" > 
              <span class="glyphicon glyphicon-record"></span>  Impuestos IVA-IT </a></h6>
                
                <div class="componente_menu_sublinks" id="submenu_impuestos">
                    <h6> <a class="link_submenu" href="javascript:void();"> - Compensación IVA </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();"> - Compensación IT  </a></h6>

                </div> 


              <h6 onclick="btn_submenu_sublinks('estados_financieros');" > <a class="link_menu" href="javascript:void();" > 
              <span class="glyphicon glyphicon-record"></span> Estados Financieros </a></h6>
                
                <div class="componente_menu_sublinks" id="submenu_estados_financieros">

                      <h6> <a class="link_submenu" href="javascript:void();" >  Carátula </a></h6>
                      <h6> <a class="link_submenu" href="javascript:void();" >  Balance de Apertura </a></h6>
                      <h6> <a class="link_submenu" href="javascript:void();" >  Balance General </a></h6>
                      <h6> <a class="link_submenu" href="javascript:void();" >  Estado de Resultados </a></h6>
                      <h6> <a class="link_submenu" href="javascript:void();" >  Determinación del IUE </a></h6> 
                      <h6> <a class="link_submenu" href="javascript:void();" >  Resultados Acumulados </a></h6> 
                      <h6> <a class="link_submenu" href="javascript:void();" >  Evolución del Patrimonio </a></h6>
                      <h6> <a class="link_submenu" href="javascript:void();" >  Balance Comparativo </a></h6>
                      <h6> <a class="link_submenu" href="javascript:void();" >  Flujo de Efectivo </a></h6>  
                      <h6> <a class="link_submenu" href="javascript:void();" >  Suma y Saldos </a></h6> 
                      <h6> <a class="link_submenu" href="javascript:void();" >  Notas a los EE. FF. </a></h6> 
                      
                </div> 

              <h6 onclick="btn_submenu_sublinks('proyectos');" > <a class="link_menu" href="javascript:void();" > 
              <span class="glyphicon glyphicon-record"></span>  Proyectos </a></h6>
                
                <div class="componente_menu_sublinks" id="submenu_proyectos">
                    <h6> <a class="link_submenu" href="javascript:void();"> - Crear Proyectos </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();"> - Reportes de Proyectos  </a></h6>

                </div> 
                
              <h6 onclick="btn_submenu_sublinks('anexos_itc');" > <a class="link_menu" href="javascript:void();" > 
              <span class="glyphicon glyphicon-record"></span>  Anexos ITC  </a></h6>
                
                <div class="componente_menu_sublinks" id="submenu_anexos_itc">

                      <h6> <a class="link_submenu" href="javascript:void();"> Anexo 1 </a></h6>
                      <h6> <a class="link_submenu" href="javascript:void();"> Anexo 2 </a></h6>
                      <h6> <a class="link_submenu" href="javascript:void();"> Anexo 3 </a></h6>
                      <h6> <a class="link_submenu" href="javascript:void();"> Anexo 4 </a></h6>
                      <h6> <a class="link_submenu" href="javascript:void();"> Anexo 5 </a></h6> 

                      <h6> <a class="link_submenu" href="javascript:void();"> Anexo 6 </a></h6>
                      <h6> <a class="link_submenu" href="javascript:void();"> Anexo 7 </a></h6>
                      <h6> <a class="link_submenu" href="javascript:void();"> Anexo 8 </a></h6>
                      <h6> <a class="link_submenu" href="javascript:void();"> Anexo 9 </a></h6>
                      <h6> <a class="link_submenu" href="javascript:void();"> Anexo 10 </a></h6> 

                      <h6> <a class="link_submenu" href="javascript:void();"> Anexo 11 </a></h6>
                      <h6> <a class="link_submenu" href="javascript:void();"> Anexo 12 </a></h6>
                      <h6> <a class="link_submenu" href="javascript:void();"> Anexo 13 </a></h6>
                      <h6> <a class="link_submenu" href="javascript:void();"> Anexo 14 </a></h6>
                      <h6> <a class="link_submenu" href="javascript:void();"> Anexo 15 </a></h6> 
                     
                </div> 
                
                 
              <h6 onclick="btn_submenu_sublinks('conciliacion_bancaria');" > <a class="link_menu" href="javascript:void();" > 
              <span class="glyphicon glyphicon-record"></span>  Concilación Bancaria  </a></h6>
                
                <div class="componente_menu_sublinks" id="submenu_conciliacion_bancaria">
                    <h6> <a class="link_submenu" href="javascript:void();"> - Bancos </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();"> - Libro de Bancos  </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();"> - Conciliación Bancaria  </a></h6>
 
                </div> 

            </div>
    
          </li>


          <li class="submenu" > 
            <a class="link" onclick="btn_submenu_menu('recursos_humanos');"  href="javascript:void();" > <span class="glyphicon glyphicon-briefcase"> </span> Recursos Humanos  </a> 
    
            <div class="componente_menu_links" id="menu_recursos_humanos">

              <h6 onclick="btn_submenu_sublinks('planilla_sueldos');" > <a class="link_menu" href="javascript:void();" > 
              <span class="glyphicon glyphicon-record"></span> PLANILLA DE SUELDOS </a></h6>
                
                <div class="componente_menu_sublinks" id="submenu_planilla_sueldos">
                    
                    <h6> <a class="link_submenu" href="javascript:void();" style="text-transform:lowercase; padding-left:0%;"> - AREAS DE TRABAJO    </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();" style="text-transform:lowercase; padding-left:0%;"> - KARDEX DEL PERSONAL </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();" style="text-transform:lowercase; padding-left:0%;"> - PLANILLA DE RETROACTIVOS </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();" style="text-transform:lowercase; padding-left:0%;"> - PLANILLA DE EXTRAS AL PERSONAL </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();" style="text-transform:lowercase; padding-left:0%;"> - PLANILLA DE AGUINALDOS </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();" style="text-transform:lowercase; padding-left:0%;"> - PLANILLA DE INDEMINIZACIÓN </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();" style="text-transform:lowercase; padding-left:0%;"> - PLANILLA DE PRIMAS </a></h6>
 
                </div>
                

              <h6 onclick="btn_submenu_sublinks('planilla_descuento');" > <a class="link_menu" href="javascript:void();" > 
              <span class="glyphicon glyphicon-record"></span> PLANILLA DE DESCUENTOS AL PERSONAL </a></h6>
                
                <div class="componente_menu_sublinks" id="submenu_planilla_descuento">
                    <h6> <a class="link_submenu" href="javascript:void();" style="text-transform:lowercase; padding-left:0%;"> - PLANILLA DE ANTICIPOS AL PERSONAL </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();" style="text-transform:lowercase; padding-left:0%;"> - PLANILLA DE CUENTAS POR COBRAR AL PERSONAL </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();" style="text-transform:lowercase; padding-left:0%;"> - PLANILLA DE PRESTAMOS AL PERSONAL </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();" style="text-transform:lowercase; padding-left:0%;"> - PLANILLA RC IVA - TRIBUTARIA Y FORM. 608 </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();" style="text-transform:lowercase; padding-left:0%;"> - PLANILLA APORTES AFP FUTURO </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();" style="text-transform:lowercase; padding-left:0%;"> - PLANILLA APORTES AFP PREVISION </a></h6>
 
                </div>
 
              <h6 onclick="btn_submenu_sublinks('otras_planillas');" > <a class="link_menu" href="javascript:void();" > 
              <span class="glyphicon glyphicon-record"></span> OTRAS PLANILLAS </a></h6>
                
                <div class="componente_menu_sublinks" id="submenu_otras_planillas">
                    <h6> <a class="link_submenu" href="javascript:void();" style="text-transform:lowercase; padding-left:0%;"> - PLANILLA PARA MANDAR AL BANCO </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();" style="text-transform:lowercase; padding-left:0%;"> - PLANILLA MINISTERIO DE TRABAJO </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();" style="text-transform:lowercase; padding-left:0%;"> - PLANILLA DE BAJAS </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();" style="text-transform:lowercase; padding-left:0%;"> - FINIQUITOS PAGADOS </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();" style="text-transform:lowercase; padding-left:0%;"> - PLANILLA CONTROL DE VACACIONES </a></h6>
                    <h6> <a class="link_submenu" href="javascript:void();" style="text-transform:lowercase; padding-left:0%;"> - CUMPLEAÑOS </a></h6>
 
                </div>
                
                
    
            </div>
    
          </li>

          <li class="submenu" onclick="btn_submenu_menu('recursos_cursos');" > 
            <a class="link" href="javascript:void();" > <span class="glyphicon glyphicon-book"> </span> Cursos - Seminarios </a> 
    
            <div class="componente_menu_links" id="menu_recursos_cursos">
              
              <h6> <a class="link_submenu" onclick="btn_menu_curso();" href="javascript:void();" style="color:white;" >  Cursos  </a></h6>
              
              <h6 style="cursor:pointer;" onclick="btn_menu_estudiantes();" > <a class="link_submenu" href="javascript:void();" style="color:white;" >  Participantes </a></h6>
              
            </div>
    
          </li>
          
       </ul>              


      </div>
 
    </div>
       
  </div>

  <div class="col-lg-10 col-md-8 col-sm-9 col-xs-12" style=" padding: 0px; margin:0px; padding-left: 0.3%; padding-top: 0.3%;" id="panel_contenido_socrates">

