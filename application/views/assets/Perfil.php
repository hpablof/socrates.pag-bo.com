

<?php
 
   $email=$perfil[0]->email;
   $alias=$perfil[0]->alias;
   $nombres=$perfil[0]->nombres;
   $apellidos=$perfil[0]->apellidos; 
   $ci=$perfil[0]->ci;
 
?>

<div class="row">
  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"></div>
  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <br>
    <div>
      <table class="table table-bordered table-hover table-striped">
        <thead>
        <tr>
          <th> PERFIL DEL USUARIO</th>
        </tr>
        </thead>
        
        <tr>
           <td align="center"> <img src="<?php echo base_url(); ?>/assets/multimedia/iconos/icono_user.png" style="width: 85%; height: 250px;"> </td> 
        </tr>

        <tr>
          <td> EMAIL : <?php echo $email; ?></td>
        </tr>

        <tr>
          <td> ALIAS : <?php echo $alias; ?></td>
        </tr>

        <tr>
          <td> NOMBRES : <?php echo $nombres; ?></td>
        </tr>

        <tr>
          <td> APELLIDOS : <?php echo $apellidos; ?></td>
        </tr>

        <tr>
          <td> CI : <?php echo $ci; ?></td>
        </tr>


      </table>  
    </div>

  </div>
</div>
 

