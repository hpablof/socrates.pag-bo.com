
<!DOCTYPE html>
<html>
<head>
  <title> Empresa </title>

  <meta name="viewport" content="initial-scale=1, maximum-scale=1">
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-theme.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/theme.css">

  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js" ></script>

  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.js" ></script> 
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/validador.js" ></script> 


<!-- INSERCION DEL MENU DE NAVEGACION --> 

 <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" style="border-radius:0px; background:transparent;">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar" style="background:white;" ></span>
        <span class="icon-bar" style="background:white;" ></span>
        <span class="icon-bar" style="background:white;" ></span>
      </button>

      <a class="navbar-brand" href="#" style="color:white; text-shadow:0px;">  Sistema </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav hidden-lg hidden-md hidden-sm">
 
      </ul>
      
      <ul class="nav navbar-nav navbar-right">

        <li><a href="<?php echo base_url(); ?>Clogin/perfil" style="color: white; background: transparent;"> <span class="glyphicon glyphicon-user" ></span> Perfil </a></li>

        <li><a href="<?php echo base_url(); ?>Clogin/salir" style="color: white; background: transparent;"> <span class="glyphicon glyphicon-log-in" ></span> Salir </a></li>
    
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

</head>

<body>

<style type="text/css">
    
    .componente_menu_links
    {
        padding: 0%;
        display: none;
        margin-top: 0%;
        background: #2E3E51;
    }
    
    .componente_menu_links h6
    {
      border-bottom: 1px solid #bdc3c7;
      padding: 3%;
    }
 
    .link_menu
    {
        width: 100%;
        text-decoration: none;
        font-weight: bold;
        font-size: 13px;
        color: white;
        
    }

    .link_menu:hover
    { 
       text-decoration: none;
       color:white;
 
    }

</style>

<script type="text/javascript">
    var cont_menu=0;

    function btn_submenu_menu(area)
    {  
        if (cont_menu==0)
        {
          $('#menu_'+area).show(500);
          cont_menu=1;  
        }
        else 
        {
          $('#menu_'+area).hide(500);  
          cont_menu=0;
        } 
    } 
</script>

 <div class="row" style="margin:0px; padding:0px;">
  
  <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 hidden-xs"> 
    <input type="text" id="id_usuario_session" value="1">
    <input type="text" id="id_empresa_select" value="1">
    <input type="text" id="id_gestion" value="1">   
    
    <div class="row">
      <div class="col-lg-12" style="padding: 0%; color:white; margin-top: 1%; width: 100%;">
      <ul class="menu">

                  <li class="submenu" onclick="btn_submenu_menu('anexos');" > 
                    <a class="link" href="javascript:void();" > <span class="glyphicon glyphicon-cog"> </span> Empresas </a> 

                      <div class="componente_menu_links" id="menu_anexos">
 
                        <h6> <a class="link_menu" href="<?php echo base_url(); ?>Cempresa/"> <span class="glyphicon glyphicon-home"> </span> empresa </a> </h6>
                   
                        <h6> <a class="link_menu" href="<?php echo base_url(); ?>Cgestion/"> <span class="glyphicon glyphicon-home"> </span> gestion </a> </h6>

                      </div>

                  </li>

                  <li class="submenu" onclick="btn_submenu_menu('almacen');" > 
                    <a class="link" href="javascript:void();" > <span class="glyphicon glyphicon-cog"> </span> Almacen </a> 

                      <div class="componente_menu_links" id="menu_almacen">
 
                        <h6> <a class="link_menu" href="<?php echo base_url(); ?>Cproducto/"> producto </a> </h6>

                        <h6> <a class="link_menu" href="<?php echo base_url(); ?>Carea/"> area </a> </h6>

                        <h6> <a class="link_menu" href="<?php echo base_url(); ?>Cmedida/"> medida </a> </h6>

                        <h6> <a class="link_menu" href="<?php echo base_url(); ?>Cproveedor/"> proveedor </a> </h6>

                        <h6> <a class="link_menu" href="<?php echo base_url(); ?>Ccompra/"> compra </a> </h6>

                      </div>

                   </li>
 

                  <li class="submenu" onclick="btn_submenu_menu('ventas');" > 
                    <a class="link" href="javascript:void();" > <span class="glyphicon glyphicon-cog"> </span> Salidas </a> 

                      <div class="componente_menu_links" id="menu_ventas">
 
                        <h6> <a class="link_menu" href="<?php echo base_url(); ?>Cdetalle_venta/"> ventas </a> </h6>

                        <h6> <a class="link_menu" href="<?php echo base_url(); ?>Ccliente/"> cliente </a> </h6>

                      </div>

                   </li>

 
                  <li class="submenu" onclick="btn_submenu_menu('salidas');" > 
                    <a class="link" href="javascript:void();" > <span class="glyphicon glyphicon-cog"> </span> Salidas </a> 

                      <div class="componente_menu_links" id="menu_salidas">
 
                        <h6> <a class="link_menu" href="<?php echo base_url(); ?>Cusuario/"> usuario </a> </h6>

                        <h6> <a class="link_menu" href="<?php echo base_url(); ?>Ccargo/"> cargo </a> </h6>

                        <h6> <a class="link_menu" href="<?php echo base_url(); ?>Cpermiso/"> permiso </a> </h6>

                      </div>

                   </li>

                   
                   
 
                   
 
                   

             
     </ul>

      </div>
 
    </div>
       
  </div>

  <div class="col-lg-10 col-md-8 col-sm-9 col-xs-12" style="height:;">

