<?php
 
 if($opcion=="listar_cursos")
 {
 ?>
 <div class="row" style="background:white;">
     
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
         
         <h3 style="text-align:center; font-weight:bold; color:#2980b9;"> LISTA DE CURSOS DEL SISTEMA SOCRATES </h3>
         
         <div class="row">
 
         <?php 
         
            foreach($cursos as $cursos_ob)
            {
                $id_curso = $cursos_ob->id_curso;
    		    $portada_curso = $cursos_ob->portada_curso;
    		    $curso = $cursos_ob->curso;
    		    $descripcion_curso = $cursos_ob->descripcion_curso;
    		    $costo_curso = $cursos_ob->costo_curso;
    		    $fecha_curso = $cursos_ob->fecha_curso;
    		    $duracion_curso = $cursos_ob->duracion_curso;
    		    $certificado_curso = $cursos_ob->certificado_curso;
    		    $estado_curso = $cursos_ob->estado_curso;
                ?>
                <div class="col-lg-4 col-md-4 col-sm-12 col-12" >
                 
                  <div style="margin:1%; padding:1%;"> 
                   
                   <img src="<?php echo base_url(); ?>assets/multimedia/curso/<?php echo $portada_curso; ?>" style="width:100%; height:450px;">
                   <h4> <?php echo $curso; ?> </h4>
                   </hr>
                   
                   <center>
                       <button class="btn btn-info btn-md"  onclick="btn_ver_curso('<?php echo $id_curso; ?>');"> VER CURSO </button>
                       <?php if($estado_curso==1)
                       {
                         ?>
                         <button class="btn btn-info btn-md" onclick="btn_ver_formulario('<?php echo $id_curso; ?>');"> FORMULARIO </button>
                         <?php
                       } else{} ?>
                       
                   </center>
                   
                  </div>
                  
                </div>     
                <?php
                
            }
            
         ?>
        </div> 
 
     </div>
     
 </div>
 
 <script type="text/javascript">
 
   function btn_ver_formulario(id_curso)
   {   
       window.open('https://socrates.pag-bo.com/Cursos/Formulario?id='+id_curso); 
   }
 
 </script>
 
 <?php
 }
 
 if($opcion=="formulario_curso")
 {
  ?>
   <script src="https://socrates.pag-bo.com/scripts/Formulario.js"></script>
   
   <div class="row" style="background:white;">
     
     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" > </div> 
     
     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" > 
     
     <?php  
        $id_curso = $curso[0]->id_curso;
        $titulo_curso = $curso[0]->curso;
        $costo_curso = $curso[0]->costo_curso;
     ?>

     <div style="background:white; margin:2%; padding:4%; border:1px solid #bdc3c7; box-shadow: 1px  1px 15px 1px #bdc3c7; margin-bottom:5%;">      
     
     <h4 style="margin:0px; text-align:center;"> Registro de Participantes del Curso </h4>
     <h5 style="margin:0px; text-align:center; color:#0098D1; font-weight:bold;">  <?php echo $titulo_curso; ?></h5>
     <p style="margin:0px; text-align:center; margin-bottom:2%; font-size:10px;"> * Debes llenar todos los campos para el registro correcto </p>
   
     
     <input type="hidden" value="<?php echo $id_curso; ?>" id="id_curso" name="id_curso" >
     
     <div class="row" >
         
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             
             <label> Nombre Completo </label>
             <input type="text" class="form-control" id="nombre_completo" placeholder="* Nombre Completo" autocomplete="off" style="text-transform:uppercase;" maxlength="200">
             <div id="panel_resp_nombre_completo"></div>
             
         </div>

         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             
             <label> CI </label>
             <input type="text" class="form-control" id="ci" placeholder="* ci " autocomplete="off" style="text-transform:uppercase;" maxlength="15">
             <div id="panel_resp_ci"></div>
             
         </div>
         
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             
             <label>Razón Social </label>
             <input type="text" class="form-control" id="razon_social" placeholder="* Razón Social" autocomplete="off" style="text-transform:uppercase;" maxlength="200">
             <div id="panel_resp_razon_social"></div>
             
         </div>
         
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             
             <label> NIT </label>
             <input type="text" class="form-control" id="ci_nit" placeholder="* nit" autocomplete="off" style="text-transform:uppercase;" maxlength="15">
             <div id="panel_resp_ci_nit"></div>
             
         </div>
         
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             
             <label> Email </label>
             <input type="text" class="form-control" id="email" placeholder="* email" autocomplete="off" style="text-transform:;" maxlength="200">
             <div id="panel_resp_email"></div>
         </div>    

         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
             
             <label> Celular / Whatsapp </label>
             <input type="text" class="form-control" id="celular" placeholder="* numero" autocomplete="off" style="text-transform:;" maxlength="8" >
             <div id="panel_resp_celular"></div>
         </div>  

         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
             
             <label> Importe Cancelado </label>
             <input type="text" class="form-control" id="importe" placeholder="*  0.00" autocomplete="off" style="text-transform:;" maxlength="3" >
             <div id="panel_resp_importe"></div>
         </div> 
         
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             
             <label> Seleccione Cómo realizo el Pago </label>
             <select class="form-control" id="opcion_tipo_pago" onchange="btn_selec_codigo_transferencia();">
                 <option value=""> Seleccione </option>
                 <option value="1" > Banco Union </option>
                 <option value="2" > Banco Mercantil </option>
                 <option value="3" > Banco Bisa </option>
                 <option value="5" > Tigo Money </option>
                 <option value="4" > Pago en Efectivo </option>
                 
             </select>
             <div id="panel_resp_tipo_pago"></div>
         </div>
         
                      
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             
             <label> codigo/numero de transferencia </label>
             <input type="text" class="form-control" id="codigo_transferencia" placeholder="* numero o codigo de transferencia" autocomplete="off" style="text-transform:uppercase;" maxlength="50">
             <div id="panel_resp_codigo_transferencia"></div>
             
         </div>

         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             
             <hr>
             <center>
             Instructor : <label> Lic. Juan Vargas Buendia </label> </br>
             INVERSION : <label>  <?php echo $costo_curso; echo " Bs."; ?> </label> </br>
             </center>
             
             
             <div id="panel_resp_formulario_curso"> </div>
             
             <center style="margin-top:2%;">
             <button class="btn btn-info btn-lg" onclick="btn_registrar_estudiante_curso();"> <span class="glyphicon glyphicon-check"></span> ENVIAR </button>
             
             </center>
             
         </div>
     
     <!-- FINAL DEL ROW DEL FORMULARIO -->         
     </div> 
     
      
     </div> 
     
   
   </div>      
  <?php
 }
 
 if($opcion=="formulario_verificacion_curso")
 {
  ?>
   <div class="row">
 
       <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
      </div> 

       <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            
           <div style="background:white; margin:1%; padding:1%;">
               
                <h4 align="center" > INGRESE TU CI PARA VERIFICAR SU INSCRIPCION AL CURSO  </h4>
                <hr>

                    <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                         <label> Escriba su Ci </label>
                         <input type="text" class="form-control" id="ci_verificar" placeholder="* CI PARTICIPANTE" maxlength="10">
                         <div class="panel_resp_ci_ver"></div>
                         <hr>
                      </div> 
                      
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div id="panel_resp_verificacion_participante"></div>
                      </div> 
                      
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" align="center">
                         <buttton class="btn btn-info" onclick="btn_verificar_participante();" > ENVIAR </buttton>
                      </div> 
                    
           </div>
           
           </div>
           
      </div> 
      
   </div>
   <script>
       
       function btn_verificar_participante()
       {
         var ci_verificar = $("#ci_verificar").val();
         
         var ob = { ci_verificar:ci_verificar };
        
         $.ajax({
            type: "POST",
            url: "https://socrates.pag-bo.com/Cursos/Verificar_Participante",
            data: ob,
            beforeSend: function(objeto)
            {
              $("#panel_resp_verificacion_participante").html("<div class='cargando'>  </div>");
            },
            success:function(data)
            {
              $("#panel_resp_verificacion_participante").html(data);
              
              setTimeout(function()
              {
                $("#ci_verificar").val("");
                $("#panel_resp_verificacion_participante").html(""); 
                  
              },10000);
            }
         });
    
       }
       
   </script>
  <?php
 }
 
 

?>