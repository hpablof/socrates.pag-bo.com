<a href="javascript:void();" onclick="cargar_datos_nivel_pc(1);"> Listar Datos  </a>
    <table class="table table-bordered table-condensed table-hover table-striped">
     <thead>
     
      <tr>
        <th class="hidden-lg hidden-md hidden-sm"> </th>
        <th class="col-lg-1"> nivel pc </th>
        <th> nombre pc </th>
        <th class="hidden-xs"> </th>
      </tr>

     </thead>
     <tbody>
     <?php 
     foreach($nivel_pcs as $ob_nivel_pc)
     {
        $id_nivel_pc = $ob_nivel_pc->id_nivel_pc;
        $nivel_pc = $ob_nivel_pc->nivel_pc;
        $nombre_pc = $ob_nivel_pc->nombre_pc;
     ?>
       <tr>

        <td class="col-xs-1 hidden-lg hidden-md hidden-sm"> <center>
             <a href="#" onclick="btn_submenu_nivel_pc_xs('<?php echo $id_nivel_pc; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones-xs" id="panel_opciones_n_xs_<?php echo $id_nivel_pc; ?>">

              <div align="right" class="panel_cerrar_botones-xs" >
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu_nivel_pc_xs('<?php echo $id_nivel_pc; ?>');"> X </buttom>
              </div>
              <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_examinar_nivel_pc('<?php echo $id_nivel_pc; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_editar_nivel_pc('<?php echo $id_nivel_pc; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line-xs">

               <button class="btn btn-danger btn-md" onclick="btn_borrar_nivel_pc('<?php echo $id_nivel_pc; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 

             </div>

          </center>
        </td>
        
        <td align="center" ><?php echo $nivel_pc; ?></td>
        <td> <?php echo $nombre_pc; ?> </td>
        
        <td class="col-lg-1 hidden-xs"> <center>
             <a href="javascript:void(0);" onclick="btn_submenu_nivel_pc('<?php echo $id_nivel_pc; ?>');">
              <span class="glyphicon glyphicon-align-justify"> </span> </a>
                
             <div class="btn_opciones" id="panel_opciones_n_<?php echo $id_nivel_pc; ?>">

              <div align="right" class="panel_cerrar_botones">
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu_nivel_pc('<?php echo $id_nivel_pc; ?>');"> x </buttom>
              </div>
              <hr class="btn_line">

               <button class="btn btn-default btn-xs" onclick="btn_examinar_nivel_pc('<?php echo $id_nivel_pc; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line">

               <button class="btn btn-default btn-xs" onclick="btn_editar_nivel_pc('<?php echo $id_nivel_pc; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line">

               <button class="btn btn-danger btn-xs" onclick="btn_borrar_nivel_pc('<?php echo $id_nivel_pc; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 
             
             </div>

          </center>
          </td>

       </tr>
     <?php
     } 
     ?>
     </tbody>
 
 <!-- Final del table -->
 </table>
 
 <?php require "paginacion_busqueda.php"; ?>

