
<div class="componente_menu" >

  <h4 class="titulo_menu" > System /nivel pc /menu </h4>
  <script type="text/javascript" src="<?php echo base_url(); ?>/scripts/C_nivel_pc.js"></script>
 
  <div class="row">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="panel_buscador">
     
     <table class="table table-bordered table-condensed table-hover table-striped">
     <tr>
      
      <td> 
       <input type="text" class="form-control" id="txt_buscar_nivel_pc" placeholder="* Buscar nivel pc" onkeyup="buscar_datos_nivel_pc(1);"> 
      </td>
      
      <td>
        <button class="btn btn-default btn-md" onclick="buscar_datos_nivel_pc(1);" > 
        <span class="glyphicon glyphicon-search"></span>
        Buscar </button>
      
      </td>

      <td align="right">
       <button class="btn btn-default btn-md" onclick="btn_nuevo_nivel_pc();" > 
       <span class="glyphicon glyphicon-file"></span>
       Nuevo nivel pc </button>
      </td>

     </tr>
     
     </table>

     <div id="panel_resultado_busqueda"> </div>

   </div>

   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="panel_listado">
   
   <div id="panel_paginacion_datos_nivel_pc" class="table-responsive">
   
   <table class="table table-bordered table-condensed table-hover table-striped">
     <thead>
     
      <tr>
        <th class="hidden-lg hidden-md hidden-sm"> </th>
    		<th class="col-lg-1"> nivel pc </th>
        <th> nombre pc </th>
        <th class="hidden-xs"> </th>
      </tr>

     </thead>
     <tbody>
     <?php 
     foreach($nivel_pcs as $ob_nivel_pc)
     {
     	  $id_nivel_pc = $ob_nivel_pc->id_nivel_pc;
		    $nivel_pc = $ob_nivel_pc->nivel_pc;
        $nombre_pc = $ob_nivel_pc->nombre_pc;
     ?>
       <tr>

        <td class="col-xs-1 hidden-lg hidden-md hidden-sm"> <center>
             <a href="#" onclick="btn_submenu_nivel_pc_xs('<?php echo $id_nivel_pc; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones-xs" id="panel_opciones_n_xs_<?php echo $id_nivel_pc; ?>">

              <div align="right" class="panel_cerrar_botones-xs" >
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu_nivel_pc_xs('<?php echo $id_nivel_pc; ?>');"> X </buttom>
              </div>
              <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_examinar_nivel_pc('<?php echo $id_nivel_pc; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_editar_nivel_pc('<?php echo $id_nivel_pc; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line-xs">

               <button class="btn btn-danger btn-md" onclick="btn_borrar_nivel_pc('<?php echo $id_nivel_pc; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 

             </div>

          </center>
        </td>
        
        <td align="center" ><?php echo $nivel_pc; ?></td>
		    <td> <?php echo $nombre_pc; ?> </td>
        
        <td class="col-lg-1 hidden-xs"> <center>
             <a href="javascript:void(0);" onclick="btn_submenu_nivel_pc('<?php echo $id_nivel_pc; ?>');">
              <span class="glyphicon glyphicon-align-justify"> </span> </a>
                
             <div class="btn_opciones" id="panel_opciones_n_<?php echo $id_nivel_pc; ?>">

              <div align="right" class="panel_cerrar_botones">
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu_nivel_pc('<?php echo $id_nivel_pc; ?>');"> x </buttom>
              </div>
              <hr class="btn_line">

	   	         <button class="btn btn-default btn-xs" onclick="btn_examinar_nivel_pc('<?php echo $id_nivel_pc; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
	   	         examinar </button> 
	   	       
               <hr class="btn_line">

	   	         <button class="btn btn-default btn-xs" onclick="btn_editar_nivel_pc('<?php echo $id_nivel_pc; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
	   	           editar </button> 

               <hr class="btn_line">

	   	         <button class="btn btn-danger btn-xs" onclick="btn_borrar_nivel_pc('<?php echo $id_nivel_pc; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
	   	         borrar </button> 
             
             </div>

          </center>
	   	    </td>

       </tr>
     <?php
     } 
     ?>
     </tbody>
 
 <!-- Final del table -->
 </table>

 <?php require "paginacion.php"; ?>

 </div>
 <!-- Final del panel blanco -->
 </div>

 <!-- Final del div panel -->
 </div>



<!-- Final del div row -->
</div>

<?php require "VM_nivel_pc.php"; ?>


<script type="text/javascript">
  
var estado_nivel_pc = 0;

function btn_submenu_nivel_pc(id)
{
  
  
  if(estado_nivel_pc==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_n_"+id).css("display","block");
    estado_nivel_pc++;
  }

  else{
  if(estado_nivel_pc>0)
  { //alert(id+" - "+estado);
    estado_nivel_pc=0;
    
    $("#panel_opciones_n_"+id).css("display","none");
  }
  } 

}

function btn_submenu_nivel_pc_xs(id)
{
  if(estado_nivel_pc==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_n_xs_"+id).css("display","block");
    estado_nivel_pc++;
  }

  else{
  if(estado_nivel_pc>0)
  { //alert(id+" - "+estado);
    estado_nivel_pc=0;
    
    $("#panel_opciones_n_xs_"+id).css("display","none");
  }
  } 

}



</script>
 

