<div class="row">
 
 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel">
  <div class="row">

  <!-- -------------------------------------------------------------------- -->
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="panel_campo">
        
       <label> Codigo </label></br> 
       <input type="text" class="form-control" name="Codigo" 
       id="Codigo" maxlength="100"
       onkeyup="return valida_codigo_plan_cuentas();" placeholder="Codigo">
     
       <div id="panel_resp_Codigo"></div>

      <!-- Final del div panel campo -->
      </div>

  <!-- -------------------------------------------------------------------- -->
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="panel_campo">
        
       <label> Nombre </label></br> 
       <input type="text" class="form-control" name="Nombre" 
       id="Nombre" maxlength="200" onkeyup="validador_campo('Nombre','panel_resp_Nombre','5');" 
       onkeypress="return valida_letras(event);" placeholder="Escriba el Nombre">
     
       <div id="panel_resp_Nombre"></div>

      <!-- Final del div panel campo -->
      </div>

  <!-- -------------------------------------------------------------------- -->
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="panel_campo">
        
       <label> Nivel </label></br> 
       <input type="text" class="form-control" name="Nivel" 
       id="Nivel" maxlength="100" onkeyup="validador_campo('Nivel','panel_resp_Nivel','5');" 
       onkeypress="return valida_letras(event);" placeholder="Nivel">
     
       <div id="panel_resp_Nivel"></div>

      <!-- Final del div panel campo -->
      </div>

	<!-- -------------------------------------------------------------------- -->
	    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	    <label> Moneda </label></br> 
      <input type="text" class="form-control" name="Moneda" 
       id="Moneda" maxlength="50" onkeyup="validador_campo('Moneda','panel_resp_Moneda','5');" 
       onkeypress="return valida_letras(event);" placeholder="Moneda">
     
       <div id="panel_resp_Moneda"></div>

      <!-- Final del div panel campo -->
      </div>
  
	 
	<!-- -------------------------------------------------------------------- -->
	    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	   
        <label> NIT :  </label></br> 
        <input type="text" class="form-control" name="NIT" id="NIT" placeholder="NIT">
 
        <div id="panel_resp_NIT"></div>

      <!-- Final del div panel campo -->
      </div>
    
	<!-- -------------------------------------------------------------------- -->
	    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	     <label> Glosa </label></br> 
       <input type="text" class="form-control" name="Glosa" 
       id="Glosa" maxlength="20" onkeyup="validador_campo('Glosa','panel_resp_Glosa','5');" 
       onkeypress="return valida_letras(event);" placeholder="* Escriba su Glosa">
     
       <div id="panel_resp_Glosa"></div>

      <!-- Final del div panel campo -->
      </div>
  
	<!-- -------------------------------------------------------------------- -->
	    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	     <label>  Cod. SIAT </label></br> 
       <input type="text" class="form-control" name="Cod_SIAT" 
       id="Cod_SIAT" maxlength="100" onkeyup="validador_campo('Cod_SIAT','panel_resp_Cod_SIAT','5');" 
       onkeypress="return valida_letras(event);" placeholder="Cod SIAT">
     
       <div id="panel_resp_Cod_SIAT"></div>

      <!-- Final del div panel campo -->
      </div>
  
	<!-- -------------------------------------------------------------------- -->
	    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	     <label> Cod. Flujo SIAT </label></br> 
       <input type="text" class="form-control" name="Cod_Flujo_SIAT" 
       id="Cod_Flujo_SIAT" maxlength="100" onkeyup="validador_campo('Cod_Flujo_SIAT','panel_resp_Cod_Flujo_SIAT','5');" 
       onkeypress="return valida_letras(event);" placeholder="Cod. Flujo SIAT">
     
       <div id="panel_resp_Cod_Flujo_SIAT"></div>

      <!-- Final del div panel campo -->
      </div>
  
	<!-- -------------------------------------------------------------------- -->
	    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	     <label> Cod. Evolucion Pat </label></br> 
       <input type="text" class="form-control" name="Cod_Evolucion_Pat" 
       id="Cod_Evolucion_Pat" maxlength="200" onkeyup="validador_campo('Cod_Evolucion_Pat','panel_resp_Cod_Evolucion_Pat','5');" 
       onkeypress="return valida_letras(event);" placeholder="Cod. Evolucion Pat">
     
       <div id="panel_resp_Cod_Evolucion_Pat"></div>

      <!-- Final del div panel campo -->
      </div>
  
	<!-- -------------------------------------------------------------------- -->
 
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      </br>
      <center> 
       <button class="btn btn-default" onclick="btn_agregar_carrito_plan_cuentas();" > Agregar </button> 
      </center>
    </div>

<!-- -------------------------------------------------------------------- -->

  <!-- Final del div row panel -->
  </div>

<!-- -------------------------------------------------------------------- -->  

 <!-- Final del div panel -->
 </div>

<!-- -------------------------------------------------------------------- -->

 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_listado">
    <h4 align="center"> LISTADO DE DATOS A REGISTRAR </h4>
    <div id="panel_listado_plan_cuentas_carrito" style="overflow-y:scroll; max-height:350px;" >
      
    </div>
 </div>

 <!-- Final del div panel listado -->
 </div>

<!-- Final del div row -->
</div>
       

