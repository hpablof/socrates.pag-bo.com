<script type="text/javascript" src="https://socrates.pag-bo.com/scripts/C_plan_cuentas.js"></script>

<div class="row" style="margin: 0px; padding: 0px;">


 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin: 0px; padding: 0px;">
    
 </div>

 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin: 0px; padding: 0px;">
 	  
 	  <div class="table-responsive"> 
 	  <table class="table table-bordered table-condensed table-hover">
 	  	<tr>
 	  		<!--<td> <button class="btn btn-info btn-sm" style="background: blue; color: white; width: 100%; font-weight: bold;" > <span></span>  PLAN DE CUENTAS </button> </td>--> 
 	  		
        <td> <button class="btn btn-success btn-sm" onclick="btn_modal_cts_especiales();" style="background: #9b59b6; color: white; width: 100%; font-weight: bold;"> <span></span> PARAMETRIZACION DE CUENTAS </button> </td>

     	  		<td> <button class="btn btn-primary btn-sm" onclick="btn_modal_cts_activo_fijo();" style="background: orange; color: white; width: 100%; font-weight: bold; border:1px solid yellow;"> <span></span> CUENTAS DE ACTIVO FIJO </button> </td>

           <td align="right"> <button class="btn btn-success btn-sm" onclick="btn_importar_pc_modal();" style="background: green; color: white; width: 100%; font-weight: bold;" > <span class="glyphicon glyphicon-file"></span> IMPORTAR DE EXCEL </button> </td>

           <td> <button class="btn btn-default btn-sm" style="width: 100%; background: #3498db; color:white; text-shadow: none;" > <span class="glyphicon glyphicon-file"></span> EXPORTAR EXCEL </button> </td>
 
           <td> <button class="btn btn-default btn-sm" style="width: 100%; background: #bdc3c7; color:black; text-shadow: none;" onclick="btn_imprimir_pc();"> <span class="glyphicon glyphicon-print"></span> IMPRIMIR </button> </td>        
     
     
 	  	</tr>
 	  	<tr>
 	  		<td colspan="5"> 
              <select class="form-control" id="select_niveles" onchange="btn_filtro_pc();">
              	<option value=""> Listar Todas </option>
                <?php //print_r($niveles); 
                    foreach ($niveles as $niv) 
                    {
                      $id_nivel_pc = $niv->id_nivel_pc;
                      $nivel_pc = $niv->nivel_pc;
                      $nombre_pc = $niv->nombre_pc;

                      ?>
                      <option value="<?php echo $id_nivel_pc; ?>"> <?php echo $nivel_pc; echo " "; echo $nombre_pc; ?> </option> 
                      <?php
                    }
                ?>
              </select>
 	  		</td>
 	  	</tr>
 	  </table>
    </div>

    <div class="table-responsive">

      <table class="table table-bordered table-hover">
       <tr>
         <!--<td> <button class="btn btn-default btn-xs" style="width: 100%;" >  Agregar </button> </td>
         <td> <button class="btn btn-default btn-xs" style="width: 100%;" >  Detallar </button> </td>
         <td> <button class="btn btn-default btn-xs" style="width: 100%;" >  Modificar </button> </td>
         <td> <button class="btn btn-default btn-xs" style="width: 100%;" >  Eliminar </button> </td>
 
         -->
 
        
       </tr>        
      </table>

      <div id="panel_resp_impresion_pc"></div>

    </div>

    <div style="width: 100%; overflow-x: scroll; overflow-y: scroll; max-height: 450px; cursor: pointer;">
     
     <div id="panel_listar_pc_filtro">
 	   <table style="font-size: 11px; width: 100%;" class="table table-bordered table-hover">
        
        <tr>
        	<th onclick="btn_filtro_pc_lista(1);" style="cursor: pointer;"> 1 </br> + </th>
        	<th onclick="btn_filtro_pc_lista(2);" style="cursor: pointer;"> 2 </br> + </th>
        	<th onclick="btn_filtro_pc_lista(3);" style="cursor: pointer;"> 3 </br> + </th>
          <th onclick="btn_filtro_pc_lista(4);" style="cursor: pointer;"> 4 </br> + </th>

          <th onclick="btn_filtro_pc_lista(5);" style="cursor: pointer;"> 5 </br> + </th>

        	<th> Codigo </th>
        	<th> Plan de Cuentas </th>
        	<th> Nivel </th>
        	<th> Moneda </th>
           
        	<th> Nit </th>
        	<th> Glosa </th>
        	<th> Cod Siat </th>
          <th> Cod Flujo SIAT </th>
          <th> Cod Evolucion PAT </th>
          <!-- <th> </th> -->
          <th style="width: 20px; background: #003366; text-align: center; text-transform: uppercase;"> Agre</br>gar </th>
          <th style="width: 20px; background: green; text-align: center; text-transform: uppercase;"> Modi</br>ficar </th>
          <th style="width: 20px; background: red; text-align: center; text-transform: uppercase;"> Elimi</br>nar </th>
        </tr>
 
      <?php
      
      foreach ($plan_cuentas as $pc) 
   	  {
        $id_plan_cuenta_select = $pc-> id_plan_cuenta;
        $Codigo = $pc-> Codigo;
        $Nombre = $pc-> Nombre;
        $Nivel = $pc-> Nivel;
        $Moneda = $pc-> Moneda;
        
        $NIT = $pc-> NIT;
        $Glosa = $pc-> Glosa;
        $Cod_SIAT = $pc-> Cod_SIAT;
        $Cod_Flujo_SIAT = $pc-> Cod_Flujo_SIAT;
        $Cod_Evolucion_Pat = $pc-> Cod_Evolucion_Pat;

        if($Nivel==1){ echo "<tr style='background: #8DD3F7;  font-weight:bold;'> "; }
        if($Nivel==2){ echo "<tr style='background: #AFFFA6;'> "; }
        if($Nivel==3){ echo "<tr style='background: #F9D1F0;'> "; }
        if($Nivel==4){ echo "<tr style='background: #FFF0B8;'> "; }

      ?>
        
          <td> - </td>
          <td> - </td>
          <td> - </td>
          <td> - </td>
          <td> - </td>

          <td style="font-weight: bold;"> <?php echo $Codigo; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Nombre; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Nivel; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Moneda; ?> </td>
 
          <td style="font-weight: bold;"> <?php echo $NIT; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Glosa; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Cod_SIAT; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Cod_Flujo_SIAT; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Cod_Evolucion_Pat; ?> </td>

         <!--
         <td style="background: white;">
            <button class="btn btn-default btn-xs" onclick="myModal_Opciones_PC_Select('<?php //echo $id_plan_cuenta_select; ?>');" > <span class="glyphicon glyphicon-align-justify"></span> </button>
          </td> -->

          <td style="background: white;  text-align: center;">
            <button class="btn btn-info btn-sm" style="background: #003366; color:white; border:1px solid #003366;" onclick="btn_nuevo_plan_cuentas_modal_select('<?php echo $id_plan_cuenta_select; ?>');" >  <span class="glyphicon glyphicon-plus"></span> </button>
          </td>

           <td style="background: white;  text-align: center;">
            <button class="btn btn-success btn-sm" style="background: green; color:white;" onclick="btn_editar_plan_cuentas_modal_select('<?php echo $id_plan_cuenta_select; ?>');">  <span class="glyphicon glyphicon-pencil"></span> </button>
          </td>

          <td style="background: white;  text-align: center;">
            <button class="btn btn-danger btn-sm" style="background: red; color:white;" onclick="btn_eliminar_plan_cuentas_modal_select('<?php echo $id_plan_cuenta_select; ?>');" > <span class="glyphicon glyphicon-trash"></span></button>
          </td>

        </tr>
      <?php                   
 
      }

      ?>
 
 	   </table>
     <!-- FINAL DEL DIV DE FILTRO -->
     </div>


 	  </div> 

 </div>



</div>


<!-- LISTADO DE MODALS DE MANEJO PARA EL PLAN DE CUENTAS -->


<!-- Modal -->
<div id="myModal_Insertar_PC" class="modal fade" role="dialog">
  <div class="modal-dialog" >

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Subir Plan de Cuentas </h4>
      </div>
      <div class="modal-body">
         
         <div id="panel_modal_subir_plan_cuentas"> 
           
           <form id="formulario_PC" enctype="multipart/form-data" >
           
             <table class="table table-bordered">
              <tr>
                <td> <input type="file" name="archivo_pc" id="archivo_pc" > </td>
                
                <td> <button type="button" class="btn btn-default btn-xs" onclick="btn_subir_archivo_pc();"> Subir </button> </td>
                
                <td> <button class="btn btn-default btn-xs"> Importar </button> </td>
                
              </tr>
             </table>

           </form>

         </div>

         <div id="panel_modal_reusltado_subir_plan_cuentas"> 

         </div>

      </div>
      <div class="modal-footer">
 
        <button type="button" class="btn btn-danger" data-dismiss="modal">
         <span class="glyphicon glyphicon-remove"></span>
         Cerrar </button>

      </div>
    </div>

  </div>
</div>


<!-- Modal -->
<div id="myModal_Opciones_PC" class="modal fade" role="dialog">
  <div class="modal-dialog modal-xs" >

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Opcion PC </h4>
      </div>
      <div class="modal-body">
         
         <div id="panel_modal_subir_plan_cuentas"> 
          <input type="hidden" name="id_plan_cuenta_select" id="id_plan_cuenta_select">
          
          <table class="table table-bordered">
            <tr>
 
             <td align="center"> <button class="btn btn-default btn-md" onclick="btn_nuevo_plan_cuentas_modal_select();"> 
              Agregar Cuenta </button> </td>

             <!-- <td align="center"> <button class="btn btn-default btn-md"> 
              Agregar Analiticos </button> </td> -->

              <td align="center"> <button class="btn btn-default btn-md" onclick="btn_editar_plan_cuentas_modal_select();"> Editar Cuenta </button> </td>

              <td align="center"> <button class="btn btn-danger btn-md" onclick="btn_eliminar_plan_cuentas_modal_select();"> Eliminar Cuenta </button> </td>
               
            </tr>
          </table>
         </div>

         <div id="panel_modal_reusltado_subir_plan_cuentas"> 

         </div>

      </div>
      <div class="modal-footer">
        <center>
         <button type="button" class="btn btn-danger" data-dismiss="modal">
         <span class="glyphicon glyphicon-remove"></span>
         Cerrar </button>
        </center>

      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal_Register_plan_cuentas_select" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" style="width: 95%;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Registrar plan de cuentas </h4>
      </div>
      <div class="modal-body">
         <div id="panel_modal_registrar_plan_cuentas_select"> </div>
         <div id="panel_modal_resultado_registrar_plan_cuentas_select"> </div>
      </div>
      <div class="modal-footer">
        
        <center>
          <button type="button" id="btn_registrar" class="btn btn-info" onclick="btn_registrar_plan_cuentas_select();" > 
          <span class="glyphicon glyphicon-file"></span>
          Registrar </button>

          <button type="button" class="btn btn-danger" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>
          Cerrar </button>
        </center>

      </div>
    </div>

  </div>
</div>




<!-- Modal -->
<div id="myModal_Update_plan_cuentas_select" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Edición de plan de cuentas </h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="id_plan_cuentas_edicion" >
        <div id="panel_modal_editar_plan_cuentas_select"> </div>
        <div id="panel_modal_respuesta_editar_plan_cuentas_select"> </div>
      </div>
      <div class="modal-footer">
 
      <center>       
        <button type="button" id="btn_editar" class="btn btn-success" onclick="btn_guardar_plan_cuentas_select();" > 
         <span class="glyphicon glyphicon-floppy-saved"></span>
         Guardar 
        </button>

        <button type="button" class="btn btn-danger" data-dismiss="modal"> 
         <span class="glyphicon glyphicon-remove"></span>
         Cerrar 
        </button>
      </center>

      </div>
    </div>

  </div>
</div>


<!-- Modal -->
<div id="myModal_Delete_plan_cuentas_select" class="modal fade" role="dialog" style="padding-left: 0px;">
  <div class="modal-dialog modal-xs" style="margin-left: 25.5%;">

    <!-- Modal content-->
    <div class="modal-content" style="margin-left: 0px; padding-left: 0%;">
      <div class="modal-header" style="background:red !important; ">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Borrado de plan de cuentas  </h4>
      </div>
      <div class="modal-body">
         <input type="hidden" id="id_plan_cuentas_eliminar" >
         <div id="panel_modal_eliminar_plan_cuentas_select"> 
 
           <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" >&times;</button>
             
             <h4> <strong> ¡Esta Seguro de Eliminar el Dato! </strong> </h4> 
             Es muy importante que este seguro de la funcion

           </div>

         </div>

      </div>
      <div class="modal-footer">

      <center>       
        <button type="button" id="btn_borrar" class="btn btn-info" onclick="btn_eliminar_plan_cuentas_select();" > 
         <span class="glyphicon glyphicon-trash"></span> 
         Borrar 
        </button>
        
        <button type="button" class="btn btn-danger" data-dismiss="modal"> 
         <span class="glyphicon glyphicon-remove"></span>
         Cerrar 
        </button>
      </center>

      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal_Cuentas_Especiales" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">  Parametrizaci��n de Cuentas </h4>
      </div>
      <div class="modal-body">
          <div id="panel_cuentas_especiales" style="overflow-y: auto; max-height: 550px;"></div>
          <div id="panel_resultado_cuentas_especiales"></div>
      </div>
      <div class="modal-footer">
        <button type="button"  class="btn btn-default" onclick="btn_agregra_cts_especiales();"> Agregar </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"> Cerrar </button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal_Cuentas_Activo_Fijo" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" style="width:80%;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Cuentas de Activo Fijo </h4>
      </div>
      <div class="modal-body">
          
          <div id="panel_cuentas_activo_fijo" style="overflow-y: auto; max-height: 550px;"></div>
          <div id="panel_resultado_cuentas_activo_fijo"></div>
          
      </div>
      <div class="modal-footer">
         
        <button type="button" class="btn btn-danger" data-dismiss="modal"> Aceptar </button>
      </div>
    </div>

  </div>
</div>


<!-- Modal -->
<div id="myModal_Registro_Cuentas_Activo_Fijo" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" >

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Cuentas de Activo Fijo </h4>
      </div>
      <div class="modal-body">
          
          <div id="panel_registro_cuentas_activo_fijo">  </div>
          <div id="panel_resultado_registro_cuentas_activo_fijo"></div>
          
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" onclick="btn_guardar_cuenta_activo_fijo();" > Registrar </button> 
         <button type="button" class="btn btn-danger" data-dismiss="modal"> Cancelar </button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal_Cuentas_Retencion" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" >

    <!-- Modal content-->
    <div class="modal-content" style="margin-left: -1%; margin-right: 1%;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Cuentas de Retención </h4>
      </div>
      <div class="modal-body">
          <div id="panel_cuentas_retencion"></div>
          <div id="panel_resultado_cuentas_retencion"></div>
      </div>
      <div class="modal-footer">
        <button type="button"  class="btn btn-default" onclick="btn_agregra_cts_especiales();"> Agregar </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"> Cerrar </button>
      </div>
    </div>

  </div>
</div>