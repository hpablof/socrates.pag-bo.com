<a href="javascript:void();" onclick="cargar_datos_plan_cuentas(1);"> Listar Datos  </a>
   <table class="table table-bordered table-condensed table-hover table-striped">
     <thead>
      <tr>
        <th class="hidden-lg hidden-md hidden-sm"> </th>
        
        <th> Codigo </th> 
        <th> Nombre </th> 
        <th> Nivel </th> 
        <th> Moneda </th>  
        <th> NIT </th> 
        <th> Glosa </th> 
        <th> Cod SIAT </th> 
        <th> Cod Flujo SIAT </th> 
        <th> Cod Evolucion Pat </th> 
         
       <th class="hidden-xs"> </th>

      </tr>

     </thead>
     <tbody>
     <?php 
     foreach($plan_cuentass as $ob_plan_cuentas)
     {
        $id_plan_cuentas = $ob_plan_cuentas->id_plan_cuenta;
        $Codigo = $ob_plan_cuentas->Codigo;
        $Nombre = $ob_plan_cuentas->Nombre;
        $Nivel = $ob_plan_cuentas->Nivel;
        $Moneda = $ob_plan_cuentas->Moneda;
        $NIT = $ob_plan_cuentas->NIT;
        $Glosa = $ob_plan_cuentas->Glosa;
        $Cod_SIAT = $ob_plan_cuentas->Cod_SIAT;
        $Cod_Flujo_SIAT = $ob_plan_cuentas->Cod_Flujo_SIAT;
        $Cod_Evolucion_Pat = $ob_plan_cuentas->Cod_Evolucion_Pat;
     ?>
       <tr>

        <td class="col-xs-1 hidden-lg hidden-md hidden-sm"> <center>
             <a href="javascript:void();" onclick="btn_submenu_plan_cuentas_xs('<?php echo $id_plan_cuentas; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones-xs" id="panel_opciones_pc_xs<?php echo $id_plan_cuentas; ?>">

              <div align="right" class="panel_cerrar_botones-xs" >
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu_plan_cuentas_xs('<?php echo $id_plan_cuentas; ?>');"> X </buttom>
              </div>
              <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_examinar_plan_cuentas('<?php echo $id_plan_cuentas; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_editar_plan_cuentas('<?php echo $id_plan_cuentas; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line-xs">

               <button class="btn btn-danger btn-md" onclick="btn_borrar_plan_cuentas('<?php echo $id_plan_cuentas; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 

             </div>

          </center>
          </td>
     
          <td> <?php echo $Codigo; ?> </td>
          <td> <?php echo $Nombre; ?> </td>
          <td> <?php echo $Nivel; ?> </td>
          <td> <?php echo $Moneda; ?> </td>
          <td> <?php echo $NIT; ?> </td>
          <td> <?php echo $Glosa; ?> </td>
          <td> <?php echo $Cod_SIAT; ?> </td>
          <td> <?php echo $Cod_Flujo_SIAT; ?> </td>
          <td> <?php echo $Cod_Evolucion_Pat; ?> </td>

          <td class="col-lg-1 hidden-xs"> <center>
             <a href="javascript:void();" onclick="btn_submenu_plan_cuentas('<?php echo $id_plan_cuentas; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones" id="panel_opciones_pc_<?php echo $id_plan_cuentas; ?>">

              <div align="right" class="panel_cerrar_botones">
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu_plan_cuentas('<?php echo $id_plan_cuentas; ?>');"> x </buttom>
              </div>
              <hr class="btn_line">

               <button class="btn btn-default btn-xs" onclick="btn_examinar_plan_cuentas('<?php echo $id_plan_cuentas; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line">

               <button class="btn btn-default btn-xs" onclick="btn_editar_plan_cuentas('<?php echo $id_plan_cuentas; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line">

               <button class="btn btn-danger btn-xs" onclick="btn_borrar_plan_cuentas('<?php echo $id_plan_cuentas; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 
             
             </div>

          </center>
          </td>

       </tr>
     <?php
     } 
     ?>
     </tbody>
 
 <!-- Final del table -->
 </table>

 <?php require "paginacion_busqueda.php"; ?>

