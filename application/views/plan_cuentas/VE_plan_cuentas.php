
<div class="row">
<form id="formulario" enctype="multipart/form-data">

 <?php 
         $id_plan_cuenta = $plan_cuentass[0]->id_plan_cuenta;
         $Codigo = $plan_cuentass[0]->Codigo;
         $Nombre = $plan_cuentass[0]->Nombre;
         $Nivel = $plan_cuentass[0]->Nivel;
         $Moneda = $plan_cuentass[0]->Moneda;
        
         $NIT = $plan_cuentass[0]->NIT;
         $Glosa = $plan_cuentass[0]->Glosa;
         $Cod_SIAT = $plan_cuentass[0]->Cod_SIAT;
         $Cod_Flujo_SIAT = $plan_cuentass[0]->Cod_Flujo_SIAT;
         $Cod_Evolucion_Pat = $plan_cuentass[0]->Cod_Evolucion_Pat;
 ?>

    <!-- -------------------------------------------------------------------- -->
 
       <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">

       <input type="hidden" name="id_plan_cuenta" id="id_plan_cuenta" value="<?php echo $id_plan_cuenta; ?>"> 

       <label> Codigo </label></br> 
       <input type="text" class="form-control" name="Codigo" 
       id="Codigo" maxlength="100" onkeyup="validador_campo('codigo','panel_resp_Codigo','5');" 
       onkeypress="return valida_letras(event);" placeholder="* Codigo" value="<?php echo $Codigo; ?>" >

       <div id="panel_resp_Codigo"></div>

        <!-- Final del div panel campo -->
       </div>


	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	     <label> Nombre </label></br> 
         <input type="text" class="form-control" name="Nombre" 
         id="Nombre" maxlength="200" onkeyup="validador_campo('Nombre','panel_resp_Nombre','5');" 
         onkeypress="return valida_letras(event);" placeholder="* Nombre" value="<?php echo $Nombre; ?>" >

         <div id="panel_resp_Nombre"></div>

        <!-- Final del div panel campo -->
       </div>
 
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
 
        <label> Nivel : <span id="nivel"> <?php echo $Nivel; ?></span> </label></br> 
        <input type="text" class="form-control" name="Nivel" id="Nivel" placeholder="Nivel" value="<?php echo $Nivel; ?>">
 
        <div id="panel_resp_Nivel"></div>

    <!-- Final del div panel campo -->
    </div>
    
	<!-- -------------------------------------------------------------------- -->
 
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
        <label> Moneda : <span> <?php echo $Moneda; ?> </span> </label></br> 
        <input type="text" class="form-control" name="Moneda" id="Moneda" 
         value="<?php echo $Moneda; ?>">
 
        <div id="panel_resp_Moneda"></div>

    <!-- Final del div panel campo -->
    </div>
    
	<!-- -------------------------------------------------------------------- -->
 
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	 <label> NIT </label></br> 
     <input type="text" class="form-control" name="NIT" 
     id="NIT" maxlength="20" onkeyup="validador_campo('NIT','panel_resp_NIT','5');" 
     onkeypress="return valida_letras(event);" placeholder="* NIT" value="<?php echo $NIT; ?>" >
     <div id="panel_resp_NIT"></div>

    <!-- Final del div panel campo -->
    </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	 <label> Glosa </label></br> 
     <input type="text" class="form-control" name="Glosa" 
     id="Glosa" maxlength="200" onkeyup="validador_campo('Glosa','panel_resp_Glosa','5');" 
     onkeypress="return valida_letras(event);" placeholder="* Glosa" value="<?php echo $Glosa; ?>" >
     <div id="panel_resp_Glosa"></div>

    <!-- Final del div panel campo -->
    </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	 <label> Cod SIAT </label></br> 
     <input type="text" class="form-control" name="Cod_SIAT" 
     id="Cod_SIAT" maxlength="200" onkeyup="validador_campo('Cod_SIAT','panel_resp_Cod_SIAT','5');" 
     onkeypress="return valida_letras(event);" placeholder="* Cod SIAT" value="<?php echo $Cod_SIAT; ?>" >
     <div id="panel_resp_Cod_SIAT"></div>

    <!-- Final del div panel campo -->
    </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	 <label> Cod Flujo SIAT </label></br> 
     <input type="text" class="form-control" name="Cod_Flujo_SIAT" 
     id="Cod_Flujo_SIAT" maxlength="200" onkeyup="validador_campo('Cod_Flujo_SIAT','panel_resp_Cod_Flujo_SIAT','5');" 
     onkeypress="return valida_letras(event);" placeholder="* Cod Flujo SIAT" value="<?php echo $Cod_Flujo_SIAT; ?>" >

     <div id="panel_resp_Cod_Flujo_SIAT"></div>

    <!-- Final del div panel campo -->
    </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	 <label> Cod Evolucion Pat </label></br> 
     
     <input type="text" class="form-control" name="Cod_Evolucion_Pat" 
     id="Cod_Evolucion_Pat" maxlength="200" onkeyup="validador_campo('Cod_Evolucion_Pat','panel_resp_Cod_Evolucion_Pat','5');" 
     onkeypress="return valida_letras(event);" placeholder="* Cod Evolucion de Pat" value="<?php echo $Cod_Evolucion_Pat; ?>" >
     
     <div id="panel_resp_Cod_Evolucion_Pat"></div>

    <!-- Final del div panel campo -->
    </div>
 
  
<!-- -------------------------------------------------------------------- -->
 
</form>
<!-- Final del div row -->
</div>
 

