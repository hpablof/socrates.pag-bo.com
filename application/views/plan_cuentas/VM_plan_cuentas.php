
<!-- Modal -->
<div id="myModal_Insertar_PC" class="modal fade" role="dialog">
  <div class="modal-dialog" >

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Subir Plan de Cuentas </h4>
      </div>
      <div class="modal-body">
         
         <div id="panel_modal_subir_plan_cuentas"> 
           
           <form id="formulario_PC" enctype="multipart/form-data" >
           
             <table class="table table-bordered">
              <tr>
                <td> <input type="file" name="archivo_pc" id="archivo_pc" > </td>
                
                <td> <button type="button" class="btn btn-default btn-xs" onclick="btn_subir_archivo_pc();"> Subir </button> </td>
                
                <td> <button class="btn btn-default btn-xs"> Importar </button> </td>
                
              </tr>
             </table>

           </form>

         </div>

         <div id="panel_modal_reusltado_subir_plan_cuentas"> 

         </div>

      </div>
      <div class="modal-footer">
 
        <button type="button" class="btn btn-danger" data-dismiss="modal">
         <span class="glyphicon glyphicon-remove"></span>
         Cerrar </button>

      </div>
    </div>

  </div>
</div>


<!-- Modal -->
<div id="myModal_Register_plan_cuentas" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" style="width: 95%;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Registrar plan de cuentas </h4>
      </div>
      <div class="modal-body">
         <div id="panel_modal_registrar_plan_cuentas"> </div>
         <div id="panel_modal_resultado_registrar_plan_cuentas"> </div>
      </div>
      <div class="modal-footer">
        
        <button type="button" id="btn_registrar" class="btn btn-info" onclick="btn_registrar_plan_cuentas();" > 
         <span class="glyphicon glyphicon-file"></span>
         Registrar </button>

        <button type="button" class="btn btn-danger" data-dismiss="modal">
         <span class="glyphicon glyphicon-remove"></span>
         Cerrar </button>

      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal_View_plan_cuentas" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Examinar plan_cuentas </h4>
      </div>
      <div class="modal-body">
         <div id="panel_modal_examinar_plan_cuentas"> </div>
      </div>
      <div class="modal-footer">
 
        <button type="button" class="btn btn-danger" data-dismiss="modal"> 
         <span class="glyphicon glyphicon-remove"></span>
         Cerrar 
        </button>
      
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal_Update_plan_cuentas" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Edición de plan_cuentas </h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="id_plan_cuentas_edicion" >
        <div id="panel_modal_editar_plan_cuentas"> </div>
        <div id="panel_modal_respuesta_editar_plan_cuentas"> </div>
      </div>
      <div class="modal-footer">
        
        <button type="button" id="btn_editar" class="btn btn-success" onclick="btn_guardar_plan_cuentas();" > 
         <span class="glyphicon glyphicon-floppy-saved"></span>
         Guardar 
        </button>

        <button type="button" class="btn btn-danger" data-dismiss="modal"> 
         <span class="glyphicon glyphicon-remove"></span>
         Cerrar 
        </button>

      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal_Delete_plan_cuentas" class="modal fade" role="dialog">
  <div class="modal-dialog modal-xs">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background:red !important; ">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Borrado de plan de cuentas  </h4>
      </div>
      <div class="modal-body">
         <input type="text" id="id_plan_cuentas_eliminar" >
         <div id="panel_modal_eliminar_plan_cuentas"> 
 
           <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" >&times;</button>
             
             <h4> <strong> ¡Esta Seguro de Eliminar el Dato! </strong> </h4> 
             Es muy importante que este seguro de la funcion

           </div>

         </div>

      </div>
      <div class="modal-footer">
        
        <button type="button" id="btn_borrar" class="btn btn-info" onclick="btn_eliminar_plan_cuentas();" > 
         <span class="glyphicon glyphicon-trash"></span> 
         Borrar 
        </button>
        
        <button type="button" class="btn btn-danger" data-dismiss="modal"> 
         <span class="glyphicon glyphicon-remove"></span>
         Cerrar 
        </button>

      </div>
    </div>

  </div>
</div>


