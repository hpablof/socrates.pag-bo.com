<style type="text/css">
.form-control-sm
{

  width: 100%;
  padding: 1.3%;
 
}



</style>
<?php

if($opcion=="cuentas_especiales")
{
 
 ?>
 
 <div class="row">
 	<table class="table">
 		<tr>

 			<td class="col-lg-6"> 
 				<button class="btn btn-default btn-md" style="width: 100%;"> Guardar Cuentas Especiales </button> 
 			</td>
 			
 			<td> 
 				<button class="btn btn-default btn-md" style="width: 100%;" onclick="btn_modal_cuentas_retencion();"> Cuentas de Retencion </button>
 			</td>
 		</tr>
 	</table>

 	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="panel_cuentas_parametrizadas">
 	 
 	 <table class="table table-bordered" style="font-size: 12px;">
   <?php
      $cont_cpam = 0;
      foreach ($cuenta_param as $cp) 
      {
         $id_cparam = $cp->id_cuenta_parametrizadas;
         $cuenta_cp = $cp->cuenta_cp;
         $codigo_cp = $cp->codigo_cp;
         $plan_cuenta_cp = $cp->plan_cuenta_cp;
         $cont_cpam++;
      ?>
    
      <tr>
      <td> <?php echo $id_cparam; ?> </td>
      <td style="background: #c5f6fa; font-weight: bold;"> <?php echo  $cuenta_cp; ?></td>
      
      <td class="col-lg-2"> 
       <input type="text" class="form-control" name="codigo_<?php echo $id_cparam; ?>" id="codigo_<?php echo $id_cparam; ?>" value="<?php echo $codigo_cp; ?>" onkeyup="btn_buscar_pc_param('codigo_<?php echo $id_cparam; ?>','panel_resp_codigo_<?php echo $id_cparam; ?>','<?php echo $id_cparam; ?>');" > 
       
       <div id='panel_resp_codigo_<?php echo $id_cparam; ?>'></div>

      </td>
      
      <td> 
       <input type="text" class="form-control" name="cuenta_<?php echo $id_cparam; ?>" id="cuenta_<?php echo $id_cparam; ?>" value="<?php echo $plan_cuenta_cp; ?>" onkeyup = "btn_buscar_pc_param('cuenta_<?php echo $id_cparam; ?>','panel_resp_cuenta_<?php echo $id_cparam; ?>','<?php echo $id_cparam; ?>');" > 
       
       <div id='panel_resp_cuenta_<?php echo $id_cparam; ?>'></div>
      
      </td>
      
      <td class="col-lg-1" align="center" > <button class="btn btn-default btn-xs"> <span class="glyphicon glyphicon-search"></span> </button>
      </td>
    
      </tr>

      <?php

       if($cont_cpam==4)
       { ?>

       <tr>
         <td colspan="5"> <input type="checkbox" checked> Usar cuenta de reexpresion de ingresos y egresos </td>
       </tr>
         <?php
       }

       if($cont_cpam==6)
       { ?>

       <tr>
         <td>  </td>
         <td> Porc. IVA </td>
         <td> <input type="text" name="iva" id="iva" value="13.00" style="width: 50px;"> <label> % </label> </td>
         <td> <input type="checkbox" checked > Sumar ICE y Importes exentos en la misma cuenta de gasto </td>         
       </tr>
         <?php
       }

      /* FINAL DEL FOR DE LISTADO */ 
      }
 
   ?>

 	 </table>	
 	</div>
 </div>

 <?php
}


if($opcion=="cuentas_especiales_recargada")
{
 ?>
   <table class="table table-bordered" style="font-size: 12px;">
   <?php
      $cont_cpam = 0;
      foreach ($cuenta_param as $cp) 
      {
         $id_cparam = $cp->id_cuenta_parametrizadas;
         $cuenta_cp = $cp->cuenta_cp;
         $codigo_cp = $cp->codigo_cp;
         $plan_cuenta_cp = $cp->plan_cuenta_cp;
         $cont_cpam++;
      ?>
    
      <tr>
      <td> <?php echo $id_cparam; ?> </td>
      <td style="background: #c5f6fa; font-weight: bold;"> <?php echo  $cuenta_cp; ?></td>
      
      <td class="col-lg-2"> 
       <input type="text" class="form-control" name="codigo_<?php echo $id_cparam; ?>" id="codigo_<?php echo $id_cparam; ?>" value="<?php echo $codigo_cp; ?>" onkeyup="btn_buscar_pc_param('codigo_<?php echo $id_cparam; ?>','panel_resp_codigo_<?php echo $id_cparam; ?>','<?php echo $id_cparam; ?>');" > 
       
       <div id='panel_resp_codigo_<?php echo $id_cparam; ?>'></div>

      </td>
      
      <td> 
       <input type="text" class="form-control" name="cuenta_<?php echo $id_cparam; ?>" id="cuenta_<?php echo $id_cparam; ?>" value="<?php echo $plan_cuenta_cp; ?>" onkeyup = "btn_buscar_pc_param('cuenta_<?php echo $id_cparam; ?>','panel_resp_cuenta_<?php echo $id_cparam; ?>','<?php echo $id_cparam; ?>');" > 
       
       <div id='panel_resp_cuenta_<?php echo $id_cparam; ?>'></div>
      
      </td>
      
      <td class="col-lg-1" align="center" > <button class="btn btn-default btn-xs"> <span class="glyphicon glyphicon-search"></span> </button>
      </td>
    
      </tr>

      <?php

       if($cont_cpam==4)
       { ?>

       <tr>
         <td colspan="5"> <input type="checkbox" checked> Usar cuenta de reexpresion de ingresos y egresos </td>
       </tr>
         <?php
       }

       if($cont_cpam==6)
       { ?>

       <tr>
         <td>  </td>
         <td> Porc. IVA </td>
         <td> <input type="text" name="iva" id="iva" value="13.00" style="width: 50px;"> <label> % </label> </td>
         <td> <input type="checkbox" checked > Sumar ICE y Importes exentos en la misma cuenta de gasto </td>         
       </tr>
         <?php
       }

      /* FINAL DEL FOR DE LISTADO */ 
      }
 
   ?>

   </table> 
 <?php
}

if($opcion=="cuentas_retenciales")
{
?>
 <div class="row">

 	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 	 
 	 <table class="table table-bordered" style="font-size: 11px;">

     <tr>
    	<td>  Retenciones IUE Servicios</td>
    	<td> <input type="text" class="form-control-sm" name="" id="" value="2.1.3.007.01"> </td>
	    <td>
	 		<input type="text" class="form-control-sm" name="" id="" value="Retenciones IUE Bienes 5% Por Pagar" >
	    </td>
	 	<td class="col-lg-1" align="center" > <button class="btn btn-default btn-xs"> <span class="glyphicon glyphicon-search"></span> </button></td>
	    <td> % </td>
	 	<td>
	 		<input type="text" class="form-control-sm" name="" id="" value="5" >
	    </td>

     </tr>


     <tr>
    	<td>  Retenciones IUE Bienes </td>
    	<td> <input type="text" class="form-control-sm" name="" id="" value="2.1.3.007.02"> </td>
	    <td>
	 		<input type="text" class="form-control-sm" name="" id="" value="Retenciones IUE Servicios 12.5% Por Pagar" >
	    </td>
	 	<td class="col-lg-1" align="center" > <button class="btn btn-default btn-xs"> <span class="glyphicon glyphicon-search"></span> </button></td>
	    <td> % </td>
	 	<td>
	 		<input type="text" class="form-control-sm" name="" id="" value="12.5" >
	    </td>

     </tr>

     <tr>
    	<td>  Retenciones IT </td>
    	<td> <input type="text" class="form-control-sm" name="" id="" value="2.1.3.007.03"> </td>
	    <td>
	 		<input type="text" class="form-control-sm" name="" id="" value="Retenciones IT 3% Por Pagar" >
	    </td>
	 	<td class="col-lg-1" align="center" > <button class="btn btn-default btn-xs"> <span class="glyphicon glyphicon-search"></span> </button></td>
	    <td> % </td>
	 	<td>
	 		<input type="text" class="form-control-sm" name="" id="" value="3" >
	    </td>

     </tr>

     <tr>
    	<td>  Retenciones RC-IVA Dependiente y Viaticos </td>
    	<td> <input type="text" class="form-control-sm" name="" id="" value="2.1.3.007.05"> </td>
	    <td>
	 		<input type="text" class="form-control-sm" name="" id="" value="Retenciones RC IVA 13% Dependientes Por Pagar" >
	    </td>
	 	<td class="col-lg-1" align="center" > <button class="btn btn-default btn-xs"> <span class="glyphicon glyphicon-search"></span> </button></td>
	    <td> % </td>
	 	<td>
	 		<input type="text" class="form-control-sm" name="" id="" value="13" >
	    </td>

     </tr>

     <tr>
    	<td>  Retenciones RC-IVA Alquileres </td>
    	<td> <input type="text" class="form-control-sm" name="" id="" value="2.1.3.007.04"> </td>
	    <td>
	 		<input type="text" class="form-control-sm" name="" id="" value="Retenciones RC IVA 13% Por Pagar" >
	    </td>
	 	<td class="col-lg-1" align="center" > <button class="btn btn-default btn-xs"> <span class="glyphicon glyphicon-search"></span> </button></td>
	    <td> % </td>
	 	<td>
	 		<input type="text" class="form-control-sm" name="" id="" value="13" >
	    </td>

     </tr>

     </table>

    </div>

 <!-- FINAL DEL ROW DEL COMPONENTE -->    
 </div>

<?php
}

if($opcion=="listar_filtro_pc")
{
  //echo "id = "; echo $select_nivel;  echo "</br>";
  ?>
 <table style="font-size: 11px; width: 100%;" class="table table-bordered table-hover">
    
    <tr>
      <th onclick="btn_filtro_pc_lista(1);" style="cursor: pointer;"> 1 </br> + </th>
      <th onclick="btn_filtro_pc_lista(2);" style="cursor: pointer;"> 2 </br> + </th>
      <th onclick="btn_filtro_pc_lista(3);" style="cursor: pointer;"> 3 </br> + </th>
      <th onclick="btn_filtro_pc_lista(4);" style="cursor: pointer;"> 4 </br> + </th>

      <th onclick="btn_filtro_pc_lista(5);" style="cursor: pointer;"> 5 </br> + </th>

      <th> Codigo </th>
      <th> Plan de Cuentas </th>
      <th> Nivel </th>
      <th> Moneda </th>
       
      <th> Nit </th>
      <th> Glosa </th>
      <th> Cod Siat </th>
      <th> Cod Flujo SIAT </th>
      <th> Cod Evolucion PAT </th>
      <!-- <th> </th> -->
      <th style="width: 20px; background: #003366; text-align: center; text-transform: uppercase;"> Agre</br>gar </th>
      <th style="width: 20px; background: green; text-align: center; text-transform: uppercase;"> Modi</br>ficar </th>
      <th style="width: 20px; background: red; text-align: center; text-transform: uppercase;"> Elimi</br>nar </th>
    </tr>

  <?php
 
  if($select_nivel=="")
  {
  foreach ($plan_cuentas as $pc) 
  { 
    $id_plan_cuenta = $pc-> id_plan_cuenta;
    $Codigo = $pc-> Codigo;
    $Nombre = $pc-> Nombre;
    $Nivel = $pc-> Nivel;
    $Moneda = $pc-> Moneda;
    
    $NIT = $pc-> NIT;
    $Glosa = $pc-> Glosa;
    $Cod_SIAT = $pc-> Cod_SIAT;
    $Cod_Flujo_SIAT = $pc-> Cod_Flujo_SIAT;
    $Cod_Evolucion_Pat = $pc-> Cod_Evolucion_Pat;

    if($Nivel==1){ echo "<tr style='background: #8DD3F7;  font-weight:bold;'> "; }
    if($Nivel==2){ echo "<tr style='background: #33FF00;'> "; }
    if($Nivel==3){ echo "<tr style='background: #FFCC00;'> "; }
    if($Nivel==4){ echo "<tr style='background: #FFFF33; '> "; }

  ?>
    
          <td> - </td>
          <td> - </td>
          <td> - </td>
          <td> - </td>
          <td> - </td>

          <td style="font-weight: bold;"> <?php echo $Codigo; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Nombre; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Nivel; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Moneda; ?> </td>
 
          <td style="font-weight: bold;"> <?php echo $NIT; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Glosa; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Cod_SIAT; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Cod_Flujo_SIAT; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Cod_Evolucion_Pat; ?> </td>

         <!--
         <td style="background: white;">
            <button class="btn btn-default btn-xs" onclick="myModal_Opciones_PC_Select('<?php //echo $id_plan_cuenta_select; ?>');" > <span class="glyphicon glyphicon-align-justify"></span> </button>
          </td> -->

          <td style="background: white;  text-align: center;">
            <button class="btn btn-info btn-sm" style="background: #003366; color:white; border:1px solid #003366;" onclick="btn_nuevo_plan_cuentas_modal_select('<?php echo $id_plan_cuenta_select; ?>');" >  <span class="glyphicon glyphicon-plus"></span> </button>
          </td>

           <td style="background: white;  text-align: center;">
            <button class="btn btn-success btn-sm" style="background: green; color:white;" onclick="btn_editar_plan_cuentas_modal_select('<?php echo $id_plan_cuenta_select; ?>');">  <span class="glyphicon glyphicon-pencil"></span> </button>
          </td>

          <td style="background: white;  text-align: center;">
            <button class="btn btn-danger btn-sm" style="background: red; color:white;" onclick="btn_eliminar_plan_cuentas_modal_select('<?php echo $id_plan_cuenta_select; ?>');" > <span class="glyphicon glyphicon-trash"></span></button>
          </td>

        </tr>
   <?php                  
  }

  }
  else
  {
  foreach ($plan_cuentas as $pc) 
  { 
    $id_plan_cuenta = $pc-> id_plan_cuenta;
    $Codigo = $pc-> Codigo;
    $Nombre = $pc-> Nombre;
    $Nivel = $pc-> Nivel;
    $Moneda = $pc-> Moneda;
   
    $NIT = $pc-> NIT;
    $Glosa = $pc-> Glosa;
    $Cod_SIAT = $pc-> Cod_SIAT;
    $Cod_Flujo_SIAT = $pc-> Cod_Flujo_SIAT;
    $Cod_Evolucion_Pat = $pc-> Cod_Evolucion_Pat;

    if($Nivel==$select_nivel){

    if($Nivel==1){ echo "<tr style='background: #8DD3F7;  font-weight:bold;'> "; }
    if($Nivel==2){ echo "<tr style='background: #33FF00;'> "; }
    if($Nivel==3){ echo "<tr style='background: #FFCC00;'> "; }
    if($Nivel==4){ echo "<tr style='background: #FFFF33; '> "; }

  ?>
    
          <td> - </td>
          <td> - </td>
          <td> - </td>
          <td> - </td>
          <td> - </td>

          <td style="font-weight: bold;"> <?php echo $Codigo; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Nombre; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Nivel; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Moneda; ?> </td>
 
          <td style="font-weight: bold;"> <?php echo $NIT; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Glosa; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Cod_SIAT; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Cod_Flujo_SIAT; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Cod_Evolucion_Pat; ?> </td>

         <!--
         <td style="background: white;">
            <button class="btn btn-default btn-xs" onclick="myModal_Opciones_PC_Select('<?php //echo $id_plan_cuenta_select; ?>');" > <span class="glyphicon glyphicon-align-justify"></span> </button>
          </td> -->

          <td style="background: white;  text-align: center;">
            <button class="btn btn-info btn-sm" style="background: #003366; color:white; border:1px solid #003366;" onclick="btn_nuevo_plan_cuentas_modal_select('<?php echo $id_plan_cuenta_select; ?>');" >  <span class="glyphicon glyphicon-plus"></span> </button>
          </td>

           <td style="background: white;  text-align: center;">
            <button class="btn btn-success btn-sm" style="background: green; color:white;" onclick="btn_editar_plan_cuentas_modal_select('<?php echo $id_plan_cuenta_select; ?>');">  <span class="glyphicon glyphicon-pencil"></span> </button>
          </td>

          <td style="background: white;  text-align: center;">
            <button class="btn btn-danger btn-sm" style="background: red; color:white;" onclick="btn_eliminar_plan_cuentas_modal_select('<?php echo $id_plan_cuenta_select; ?>');" > <span class="glyphicon glyphicon-trash"></span></button>
          </td>

        </tr>
   <?php
   }                   
  }
  }
 
  ?>

   </table>
  <?php
}


if($opcion=="listar_filtro_pc_numero")
{

  ?>
     <table style="font-size: 11px; width: 100%;" class="table table-bordered table-hover">
        
        <tr>
          <th onclick="btn_filtro_pc_lista(1);" style="cursor: pointer;"> 1 </br> + </th>
          <th onclick="btn_filtro_pc_lista(2);" style="cursor: pointer;"> 2 </br> + </th>
          <th onclick="btn_filtro_pc_lista(3);" style="cursor: pointer;"> 3 </br> + </th>
          <th onclick="btn_filtro_pc_lista(4);" style="cursor: pointer;"> 4 </br> + </th>

          <th onclick="btn_filtro_pc_lista(5);" style="cursor: pointer;"> 5 </br> + </th>

          <th> Codigo </th>
          <th> Plan de Cuentas </th>
          <th> Nivel </th>
          <th> Moneda </th>
           
          <th> Nit </th>
          <th> Glosa </th>
          <th> Cod Siat </th>
          <th> Cod Flujo SIAT </th>
          <th> Cod Evolucion PAT </th>
          <!-- <th> </th> -->
          <th style="width: 20px; background: #003366; text-align: center; text-transform: uppercase;"> Agre</br>gar </th>
          <th style="width: 20px; background: green; text-align: center; text-transform: uppercase;"> Modi</br>ficar </th>
          <th style="width: 20px; background: red; text-align: center; text-transform: uppercase;"> Elimi</br>nar </th>
        </tr>

  <?php
 

  foreach ($plan_cuentas as $pc) 
  { 
    $id_plan_cuenta = $pc-> id_plan_cuenta;
    $Codigo = $pc-> Codigo;
    $Nombre = $pc-> Nombre;
    $Nivel = $pc-> Nivel;
    $Moneda = $pc-> Moneda;
   
    $NIT = $pc-> NIT;
    $Glosa = $pc-> Glosa;
    $Cod_SIAT = $pc-> Cod_SIAT;
    $Cod_Flujo_SIAT = $pc-> Cod_Flujo_SIAT;
    $Cod_Evolucion_Pat = $pc-> Cod_Evolucion_Pat;

    $primera_cad = substr($Codigo, 0, 1); 

    if($select_nivel == $primera_cad)
    { 
        if($Nivel==1){ echo "<tr style='background: #6699FF;  font-weight:bold;'> "; }
        if($Nivel==2){ echo "<tr style='background: #33FF00;'> "; }
        if($Nivel==3){ echo "<tr style='background: #FFCC00;'> "; }
        if($Nivel==4){ echo "<tr style='background: #FFFF33; '> "; }

        ?>
          <td> - </td>
          <td> - </td>
          <td> - </td>
          <td> - </td>
          <td> - </td>

          <td style="font-weight: bold;"> <?php echo $Codigo; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Nombre; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Nivel; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Moneda; ?> </td>
 
          <td style="font-weight: bold;"> <?php echo $NIT; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Glosa; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Cod_SIAT; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Cod_Flujo_SIAT; ?> </td>
          <td style="font-weight: bold;"> <?php echo $Cod_Evolucion_Pat; ?> </td>

         <!--
         <td style="background: white;">
            <button class="btn btn-default btn-xs" onclick="myModal_Opciones_PC_Select('<?php //echo $id_plan_cuenta_select; ?>');" > <span class="glyphicon glyphicon-align-justify"></span> </button>
          </td> -->

          <td style="background: white;  text-align: center;">
            <button class="btn btn-info btn-sm" style="background: #003366; color:white; border:1px solid #003366;" onclick="btn_nuevo_plan_cuentas_modal_select('<?php echo $id_plan_cuenta_select; ?>');" >  <span class="glyphicon glyphicon-plus"></span> </button>
          </td>

           <td style="background: white;  text-align: center;">
            <button class="btn btn-success btn-sm" style="background: green; color:white;" onclick="btn_editar_plan_cuentas_modal_select('<?php echo $id_plan_cuenta_select; ?>');">  <span class="glyphicon glyphicon-pencil"></span> </button>
          </td>

          <td style="background: white;  text-align: center;">
            <button class="btn btn-danger btn-sm" style="background: red; color:white;" onclick="btn_eliminar_plan_cuentas_modal_select('<?php echo $id_plan_cuenta_select; ?>');" > <span class="glyphicon glyphicon-trash"></span></button>
          </td>

        </tr>
       <?php                  
       }
    }

?>
</table>
<?php
 
}

if($opcion=="imprimir_pc")
{
  ?>
  <style type="text/css">
      
      .table
      { width: 100%; font-family: arial; 
        border-collapse: collapse;
      } 

      .table td
      { 
        border:1px solid #7f8c8d;
        border-collapse: collapse;
        color: #212529;
        padding: 1%;
        font-size: 12px;
      }

      .table th
      { 
        border:1px solid #ecf0f1;
        border-collapse: collapse;
        background: #ecf0f1;
        text-transform: uppercase;
        padding: 1%;
        font-size: 12px;
      }

  </style>
  
  <h3 style="font-weight: bold; font-family: arial; text-align: center; margin-bottom: 0.5%;"> LISTA DE PLAN DE CUENTAS </h3>
  
  <table class="table" >
    
    <tr>
 
    <th> Codigo </th>
    <th> Plan de Cuentas </th>
    <th> Nivel </th>
    <th> Moneda </th>
    <th> Nit </th>
    <th> Glosa </th>
 
    </tr>

  <?php

  foreach ($plan_cuentas as $pc) 
  { 
    $id_plan_cuenta = $pc-> id_plan_cuenta;
    $Codigo = $pc-> Codigo;
    $Nombre = $pc-> Nombre;
    $Nivel = $pc-> Nivel;
    $Moneda = $pc-> Moneda;
    $NIT = $pc-> NIT;
    $Glosa = $pc-> Glosa;
    $Cod_SIAT = $pc-> Cod_SIAT;
    $Cod_Flujo_SIAT = $pc-> Cod_Flujo_SIAT;
    $Cod_Evolucion_Pat = $pc-> Cod_Evolucion_Pat;
    ?>
    <tr>
          <td style="font-weight: bold; width: 10%; "> <?php echo $Codigo; ?> </td>
          <td style="font-weight: bold; width: 30%;"> <?php echo $Nombre; ?> </td>
          <td style="font-weight: bold; width: 10%;"> <?php echo $Nivel; ?> </td>
          <td style="font-weight: bold; width: 10%;"> <?php echo $Moneda; ?> </td>
          <td style="font-weight: bold; width: 10%;"> <?php echo $NIT; ?> </td>
          <td style="font-weight: bold; width: 20%;"> <?php echo $Glosa; ?> </td>
 
    </tr>
    <?php                  
 
    }

?>
</table>

<script type="text/javascript">
    print();
</script>
<?php

}

if($opcion=="Buscar_pc_select")
{
  // print_r($plan_cuentas);
  ?>
  <div style="width: 50%; position: absolute; border: 1px solid #bdc3c7; background: white; z-index: 2; overflow-y: auto; max-height: 350px; box-shadow: 1px 1px 10px 1px #bdc3c7; margin-top: 5px;">
  
  <table style="width: 100%;">
    <tr>
      <td style="width: 80%; padding:0.7%;"> Resultados Para : <label> <?php echo $txt_buscar; ?> </label> </td>
      
      <td style="width: 20%; text-align: right; padding:0.7%;"> <button class="btn btn-danger btn-xs" onclick="btn_eliminar_buscador_pc('<?php echo $panel_resp; ?>');"> X </button> </td>
    </tr>
    
  </table>

  <table class="table table-bordered table-striper">
    <tr>
      <th> Codigo </th>
      <th> Plan Cuenta </th>
      <th> Nivel </th>
   
    </tr>
  <?php

   foreach ($plan_cuentas as $pc) 
   {
     $id_plan_cuenta = $pc->id_plan_cuenta;
     $Codigo = $pc->Codigo;
     $Nombre = $pc->Nombre;
     $Nivel = $pc->Nivel;

    ?>
    <tr style="cursor: pointer;" onclick="btn_select_pc_pointer('<?php echo $id_pc; ?>','<?php echo $Codigo; ?>','<?php echo $Nombre; ?>','<?php echo $panel_resp; ?>' );">
      <td> <?php echo $Codigo; ?> </td>
      <td> <?php echo $Nombre; ?> </td>
      <td> <?php echo $Nivel; ?> </td>
    </tr>
    <?php

   }

  ?>
  </table>
    
  </div>
  <?php
}

?>