<?php


if($opcion=="formulario_activo_fijo")
{
    //print_r($cuentas_fijas);
?>
<div style="width:100%; text-align:right; padding-bottom:1%;">
    <button class="btn btn-info btn-md" type="button" onclick="btn_registrar_cuenta_activo_fijo();"> Nueva Cuenta </button>
</div>
<table class="table table-bordered table-striped">

<thead>
<tr>
   <th colspan="2"> CUENTA ACTIVO FIJO </th>
   <th colspan="2"> CUENTA DE DEP. ACUMULADA </th>
   <th colspan="2"> CUENTA DE DEPRECIACIÓN </th>
   <th> % </th>
   <th> </th>
   <th> </th>
</tr>
</thead>

<tbody>
<?php

    foreach($cuentas_fijas as $caf)
    {
        $id_cuenta_activo_fijo = $caf->id_cuenta_activo_fijo;
        $codigo_caf = $caf->codigo_caf;	
        $cuenta_caf = $caf->cuenta_caf;	
        $codigo_cda = $caf->codigo_cda;	
        $cuenta_cda = $caf->cuenta_cda;	
        $codigo_cdd = $caf->codigo_cdd;
        $cuenta_cdd = $caf->cuenta_cdd;
        $porcentaje_af = $caf->porcentaje_af; 
    ?>
    <tr>
        <td> <input type="text" class="form-control" id="codigo_caf_<?php echo $id_cuenta_activo_fijo; ?>" value="<?php echo $codigo_caf; ?>" onkeyup="btn_buscar_ca_fijo('codigo_caf_<?php echo $id_cuenta_activo_fijo; ?>','panel_codigo_caf_<?php echo $id_cuenta_activo_fijo; ?>','<?php echo $id_cuenta_activo_fijo; ?>');" > 
             <div id="panel_codigo_caf_<?php echo $id_cuenta_activo_fijo; ?>">  </div> 
        </td>
        <td> <input type="text" class="form-control" id="cuenta_caf_<?php echo $id_cuenta_activo_fijo; ?>" value="<?php echo $cuenta_caf; ?>" onkeyup="btn_buscar_ca_fijo('cuenta_caf_<?php echo $id_cuenta_activo_fijo; ?>','panel_cuenta_caf_<?php echo $id_cuenta_activo_fijo; ?>','<?php echo $id_cuenta_activo_fijo; ?>');" > 
             <div id="panel_cuenta_caf_<?php echo $id_cuenta_activo_fijo; ?>"> </div> 
        </td>
        <td> <input type="text" class="form-control" id="codigo_cda_<?php echo $id_cuenta_activo_fijo; ?>" value="<?php echo $codigo_cda; ?>" onkeyup="btn_buscar_ca_fijo('codigo_cda_<?php echo $id_cuenta_activo_fijo; ?>','panel_codigo_cda_<?php echo $id_cuenta_activo_fijo; ?>','<?php echo $id_cuenta_activo_fijo; ?>');" > 
             <div id="panel_codigo_cda_<?php echo $id_cuenta_activo_fijo; ?>"> </div> 
        </td>
        <td> <input type="text" class="form-control" id="cuenta_cda_<?php echo $id_cuenta_activo_fijo; ?>" value="<?php echo $cuenta_cda; ?>" onkeyup="btn_buscar_ca_fijo('cuenta_cda_<?php echo $id_cuenta_activo_fijo; ?>','panel_cuenta_cda_<?php echo $id_cuenta_activo_fijo; ?>','<?php echo $id_cuenta_activo_fijo; ?>');" > 
             <div id="panel_cuenta_cda_<?php echo $id_cuenta_activo_fijo; ?>"> </div> 
        </td>
        <td> <input type="text" class="form-control" id="codigo_cdd_<?php echo $id_cuenta_activo_fijo; ?>" value="<?php echo $codigo_cdd; ?>" onkeyup="btn_buscar_ca_fijo('codigo_cdd_<?php echo $id_cuenta_activo_fijo; ?>','panel_codigo_cdd_<?php echo $id_cuenta_activo_fijo; ?>','<?php echo $id_cuenta_activo_fijo; ?>');" > 
             <div id="panel_codigo_cdd_<?php echo $id_cuenta_activo_fijo; ?>"> </div>
        </td>
        <td> <input type="text" class="form-control" id="cuenta_cdd_<?php echo $id_cuenta_activo_fijo; ?>" value="<?php echo $cuenta_cdd; ?>" onkeyup="btn_buscar_ca_fijo('cuenta_cdd_<?php echo $id_cuenta_activo_fijo; ?>','panel_cuenta_cdd_<?php echo $id_cuenta_activo_fijo; ?>','<?php echo $id_cuenta_activo_fijo; ?>');" > 
             <div id="panel_cuenta_cdd_<?php echo $id_cuenta_activo_fijo; ?>"> </div>
        </td>
        <td> <?php echo $porcentaje_af; ?> </td>
       
        
        <td> <button class="btn btn-default btn-xs" onclick="btn_guardar_activo_fijo_select('<?php echo $id_cuenta_activo_fijo; ?>');" > Guardar </button> </td> 
        <td> <button class="btn btn-danger btn-xs" onclick="btn_eliminar_activo_fijo_select('<?php echo $id_cuenta_activo_fijo; ?>');"> Eliminar </button> </td> 
    </tr>    
    <?php
    }
    
?>

</tbody>

</table>    
<?php
}

if($opcion=="registro_cuenta_activo_fijo")
{
?>
  <div class="row">

	<!-- -------------------------------------------------------------------- -->
	   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	    <label> codigo caf </label></br> 
        <input type="text" class="form-control" name="codigo_caf" 
        id="codigo_caf" maxlength="200" onkeyup="validador_campo('codigo_caf','panel_resp_codigo_caf','');" 
        onkeypress="return valida_letras(event);" placeholder="* Escriba su codigo caf">
      
        <div id="panel_resp_codigo_caf"></div>

      <!-- Final del div panel campo -->
      </div>
  
	<!-- -------------------------------------------------------------------- -->
	   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	    <label> cuenta caf </label></br> 
        <input type="text" class="form-control" name="cuenta_caf" 
        id="cuenta_caf" maxlength="200" onkeyup="validador_campo('cuenta_caf','panel_resp_cuenta_caf','');" 
        onkeypress="return valida_letras(event);" placeholder="* Escriba su cuenta caf">
     
        <div id="panel_resp_cuenta_caf"></div>

      <!-- Final del div panel campo -->
      </div>
  
	<!-- -------------------------------------------------------------------- -->
	  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	   <label> codigo cda </label></br> 
       <input type="text" class="form-control" name="codigo_cda" 
       id="codigo_cda" maxlength="200" onkeyup="validador_campo('codigo_cda','panel_resp_codigo_cda','');" 
       onkeypress="return valida_letras(event);" placeholder="* Escriba su codigo cda">
     
       <div id="panel_resp_codigo_cda"></div>

      <!-- Final del div panel campo -->
      </div>
  
	<!-- -------------------------------------------------------------------- -->
	    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	   <label> cuenta_cda </label></br> 
      <input type="text" class="form-control" name="cuenta_cda" 
       id="cuenta_cda" maxlength="200" onkeyup="validador_campo('cuenta_cda','panel_resp_cuenta_cda','');" 
       onkeypress="return valida_letras(event);" placeholder="* Escriba su cuenta_cda">
     
       <div id="panel_resp_cuenta_cda"></div>

      <!-- Final del div panel campo -->
      </div>
  
	<!-- -------------------------------------------------------------------- -->
	  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	   <label> codigo cdd </label></br> 
       <input type="text" class="form-control" name="codigo_cdd" 
       id="codigo_cdd" maxlength="200" onkeyup="validador_campo('codigo_cdd','panel_resp_codigo_cdd','');" 
       onkeypress="return valida_letras(event);" placeholder="* Escriba su codigo cdd">
     
       <div id="panel_resp_codigo_cdd"></div>

      <!-- Final del div panel campo -->
      </div>
  
	<!-- -------------------------------------------------------------------- -->
	  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	   <label> cuenta cdd </label></br> 
       <input type="text" class="form-control" name="cuenta_cdd" 
       id="cuenta_cdd" maxlength="200" onkeyup="validador_campo('cuenta_cdd','panel_resp_cuenta_cdd','');" 
       onkeypress="return valida_letras(event);" placeholder="* Escriba su cuenta cdd">
     
       <div id="panel_resp_cuenta_cdd"></div>

      <!-- Final del div panel campo -->
      </div>
  
	<!-- -------------------------------------------------------------------- -->
	  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	   <label> porcentaje </label></br> 
       <input type="text" class="form-control" name="porcentaje_af" 
       id="porcentaje_af" maxlength="200" onkeyup="validador_campo('porcentaje_af','panel_resp_porcentaje_af','');" 
       onkeypress="return valida_letras(event);" placeholder="* Escriba su porcentaje">
     
       <div id="panel_resp_porcentaje_af"></div>

      <!-- Final del div panel campo -->
      </div>
 
  <!-- Final del div row panel -->
  </div>

<?php    
}


if($opcion=="lista_busqueda_activos_fijos")
{
   
  ?>
  <div style="width: 70%; position: absolute; border: 1px solid #bdc3c7; background: white; z-index: 2; overflow-y: auto; max-height: 350px; box-shadow: 1px 1px 10px 1px #bdc3c7; margin-top: 5px; ">
     
     <table style="width: 100%;">
        <tr>
            
          <td style="width: 20%; text-align: left; padding:0.7%;"> <button class="btn btn-danger btn-xs" onclick="btn_cerrar_panel_fijo('<?php echo $panel_activo_fijo; ?>');"> X </button> </td>
          <td style="width: 80%; padding:0.7%;"> Resultados Para : <label> <?php echo $txt_buscar; ?> </label> </td>

        </tr>
        
     </table>
  
     <table class="table table-bordered table-striper">
        <tr>
          <th> Codigo </th>
          <th> Plan Cuenta </th>
          <th> Nivel </th>
       
        </tr>
      <?php
    
       foreach ($plan_cuentas as $pc) 
       {
         $id_plan_cuenta = $pc->id_plan_cuenta;
         $Codigo = $pc->Codigo;
         $Nombre = $pc->Nombre;
         $Nivel = $pc->Nivel;
    
        ?>
        <tr style="cursor: pointer;" onclick="btn_select_pc_pointer_caf('<?php echo $Codigo; ?>','<?php echo $Nombre; ?>','<?php echo $code_id_caf; ?>','<?php echo $id_caf; ?>' );">
          <td> <?php echo $Codigo; ?> </td>
          <td> <?php echo $Nombre; ?> </td>
          <td> <?php echo $Nivel; ?> </td>
        </tr>
        <?php
    
       }
    
      ?>
      </table>
      
  </div>
  <?php
}

?>