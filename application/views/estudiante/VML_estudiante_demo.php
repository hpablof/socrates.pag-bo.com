
<div class="componente_menu" >

  <h4 class="titulo_menu" > System /estudiante /menu </h4>
  <script type="text/javascript" src="<?php echo base_url(); ?>/scripts/C_estudiante.js"></script>
 
  <div class="row">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="panel_buscador">
     
     <table class="table table-bordered table-condensed table-hover table-striped">
     <tr>
      
      <td> 
       <input type="text" class="form-control" id="txt_buscar" placeholder="* Buscar estudiante" onkeyup="buscar_datos(1);"> 
      </td>
      
      <td>
        <button class="btn btn-default btn-md" onclick="buscar_datos(1);" > 
        <span class="glyphicon glyphicon-search"></span>
        Buscar </button>
      
      </td>

      <td align="right">
       <button class="btn btn-default btn-md" onclick="btn_nuevo_cargo_usuario();" > 
       <span class="glyphicon glyphicon-file"></span>
       Nuevo estudiante </button>
      </td>

     </tr>
     
     </table>

     <div id="panel_resultado_busqueda"> </div>

   </div>

   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="panel_listado">
   <div id="panel_paginacion_datos" class="table-responsive">
   
   <table class="table table-bordered table-condensed table-hover table-striped">
     <thead>
      <tr>
        <th class="hidden-lg hidden-md hidden-sm"> </th>
    	<th> </th>
    	<th>  estudiante </th>
    	<th>  razon social </th>
    	<th>  ci / nit </th>
    	<th>  email </th>
    	<th>  celular </th>
    	<th>  tipo de pago </th>
    	<th>  cod/num </br> transferencia </th>
    	<th>  estado </th>
    	<th>  factura </th>
    	<th>  fecha </th>
 
        <th class="hidden-xs"> </th>

      </tr>

     </thead>
     <tbody>
     <?php 
     
     $cont_part=0;
     foreach($estudiantes as $est)
     {    
          $cont_part++;
     	  $id_estudiante = $est->id_estudiante;
		  $nombre_estudiante = $est-> nombre_estudiante;
		  $razon_social = $est-> razon_social;
		  $ci_nit_estudiante = $est-> ci_nit_estudiante;
		  $email_estudiante = $est-> email_estudiante;
		  $celular_estudiante = $est-> celular_estudiante;
		  $tipo_de_pago = $est-> tipo_de_pago;
		  $codigo_num_transferencia = $est->codigo_num_transferencia;
		  $fecha_est = $est->fecha_est;
		  $hora_est = $est->hora_est;
		  
     ?>
       <tr>

        <td class="col-xs-1 hidden-lg hidden-md hidden-sm"> <center>
             <a href="#" onclick="btn_submenu_xs('<?php echo $id_cargo_usuario; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones-xs" id="panel_opciones_xs<?php echo $id_cargo_usuario; ?>">

              <div align="right" class="panel_cerrar_botones-xs" >
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu_xs('<?php echo $id_cargo_usuario; ?>');"> X </buttom>
              </div>
              <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_examinar_cargo_usuario('<?php echo $id_cargo_usuario; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_editar_cargo_usuario('<?php echo $id_cargo_usuario; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line-xs">

               <button class="btn btn-danger btn-md" onclick="btn_borrar_cargo_usuario('<?php echo $id_cargo_usuario; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 

             </div>

          </center>
          </td>
     	  
     	  <td>  <?php echo $cont_part; ?> </td> 
    	  <td>  <?php echo $nombre_estudiante; ?> </td>
          <td>  <?php echo $razon_social; ?> </td>
          <td>  <?php echo $ci_nit_estudiante; ?> </td>
          <td>  <?php echo $email_estudiante; ?> </td>
          <td>  <?php echo $celular_estudiante; ?> </td>
          <td>  <?php   
 
            if($tipo_de_pago=="1"){ echo "Banco Union"; }
            if($tipo_de_pago=="2"){ echo "Banco Mercantil"; }
            if($tipo_de_pago=="3"){ echo "Banco Bisa"; }
            if($tipo_de_pago=="4"){ echo "Pago en Efectivo"; }

          ?> </td>
          <td>  <?php echo $codigo_num_transferencia; ?> </td>
          <td>  <?php echo $fecha_est; ?> </td>
          <td>  <?php echo $hora_est; ?> </td>
    
		    
          <td class="col-lg-1 hidden-xs"> <center>
             <a href="#" onclick="btn_submenu('<?php echo $id_cargo_usuario; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones" id="panel_opciones_<?php echo $id_cargo_usuario; ?>">

              <div align="right" class="panel_cerrar_botones">
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu('<?php echo $id_cargo_usuario; ?>');"> x </buttom>
              </div>
              <hr class="btn_line">

	   	         <button class="btn btn-default btn-xs" onclick="btn_examinar_cargo_usuario('<?php echo $id_cargo_usuario; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
	   	         examinar </button> 
	   	       
               <hr class="btn_line">

	   	         <button class="btn btn-default btn-xs" onclick="btn_editar_cargo_usuario('<?php echo $id_cargo_usuario; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
	   	           editar </button> 

               <hr class="btn_line">

	   	         <button class="btn btn-danger btn-xs" onclick="btn_borrar_cargo_usuario('<?php echo $id_cargo_usuario; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
	   	         borrar </button> 
             
             </div>

          </center>
	   	    </td>

       </tr>
     <?php
     } 
     ?>
     </tbody>
 
 <!-- Final del table -->
 </table>

 

 </div>
 <!-- Final del panel blanco -->
 </div>

 <!-- Final del div panel -->
 </div>



<!-- Final del div row -->
</div>

 
 

