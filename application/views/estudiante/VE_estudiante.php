
<div class="row">
<form id="formulario" enctype="multipart/form-data">

  <?php 
         $nombre_estudiante = $estudiantes[0]->nombre_estudiante;
         $razon_social = $estudiantes[0]->razon_social;
         $ci_nit_estudiante = $estudiantes[0]->ci_nit_estudiante;
         $email_estudiante = $estudiantes[0]->email_estudiante;
         $celular_estudiante = $estudiantes[0]->celular_estudiante;
         $tipo_de_pago = $estudiantes[0]->tipo_de_pago;
         $codigo_num_transferencia = $estudiantes[0]->codigo_num_transferencia;
         $curso = $estudiantes[0]->curso;
         
         $estado_estudiante = $estudiantes[0]->estado_estudiante;
		 $factura = $estudiantes[0]->factura;
		 
		 $factura = $estudiantes[0]->factura;
		 $estado_certificado = $estudiantes[0]->estado_certificado;
  ?>

	<!-- -------------------------------------------------------------------- -->
 
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="panel_campo">
        
        <label> curso : <span id="curso"> <?php echo $curso; ?></span> </label></br> 

    <!-- Final del div panel campo -->
    </div>


	<!-- -------------------------------------------------------------------- -->
 
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
     <label> participante </label></br> 
     <input type="text" class="form-control" name="nombre_estudiante" 
     id="nombre_estudiante" maxlength="200" onkeyup="validador_campo('nombre_estudiante','panel_resp_nombre_estudiante','5');" 
     onkeypress="return valida_letras(event);" placeholder="* Escriba al participante" value="<?php echo $nombre_estudiante; ?>" >
     <div id="panel_resp_nombre_estudiante"></div>

    <!-- Final del div panel campo -->
    </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	 <label> razon social </label></br> 
     <input type="text" class="form-control" name="razon_social" 
     id="razon_social" maxlength="200" onkeyup="validador_campo('razon_social','panel_resp_razon_social','5');" 
     onkeypress="return valida_letras(event);" placeholder="* Escriba su razon social" value="<?php echo $razon_social; ?>" >
     <div id="panel_resp_razon_social"></div>

    <!-- Final del div panel campo -->
    </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	 <label> ci / nit </label></br> 
     <input type="text" class="form-control" name="ci_nit_estudiante" 
     id="ci_nit_estudiante" maxlength="200" onkeyup="validador_campo('ci_nit_estudiante','panel_resp_ci_nit_estudiante','5');" 
     onkeypress="return valida_letras(event);" placeholder="* Escriba su ci / nit" value="<?php echo $ci_nit_estudiante; ?>" >
     <div id="panel_resp_ci_nit_estudiante"></div>

    <!-- Final del div panel campo -->
    </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
     <label> email </label></br> 
     <input type="text" class="form-control" name="email_estudiante" 
     id="email_estudiante" maxlength="200" onkeyup="validador_campo('email_estudiante','panel_resp_email_estudiante','5');" 
     onkeypress="return valida_letras(event);" placeholder="* Escriba su email" value="<?php echo $email_estudiante; ?>" >
     <div id="panel_resp_email_estudiante"></div>

    <!-- Final del div panel campo -->
    </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	 <label> celular </label></br> 
     <input type="text" class="form-control" name="celular_estudiante" 
     id="celular_estudiante" maxlength="200" onkeyup="validador_campo('celular_estudiante','panel_resp_celular_estudiante','5');" 
     onkeypress="return valida_letras(event);" placeholder="* Escriba su celular" value="<?php echo $celular_estudiante; ?>" >
     <div id="panel_resp_celular_estudiante"></div>

    <!-- Final del div panel campo -->
    </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
     <label> tipo de pago </label>
     <?php 
     
        if($tipo_de_pago=="1"){ echo "Banco Union"; }
        if($tipo_de_pago=="2"){ echo "Banco Mercantil"; }
        if($tipo_de_pago=="3"){ echo "Banco Bisa"; }
        if($tipo_de_pago=="4"){ echo "Pago en Efectivo"; }
        if($tipo_de_pago=="5"){ echo "Tigo Money"; }
     
     ?>
     </br> 
     
     <select class="form-control" id="tipo_de_pago" name="tipo_de_pago">
      <option value=""> Seleccione </option>
      <option value="1" > Banco Union </option>
      <option value="2" > Banco Mercantil </option>
      <option value="3" > Banco Bisa </option>
      <option value="5" > Tigo Money </option>
      <option value="4" > Pago en Efectivo </option>
     </select>

     <div id="panel_resp_tipo_de_pago"></div>

    <!-- Final del div panel campo -->
    </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
     <label> codigo / num transferencia </label></br> 
     <input type="text" class="form-control" name="codigo_num_transferencia" 
     id="codigo_num_transferencia" maxlength="200" onkeyup="validador_campo('codigo_num_transferencia','panel_resp_codigo_num_transferencia','5');" 
     onkeypress="return valida_letras(event);" placeholder="* Escriba su codigo_num_transferencia" value="<?php echo $codigo_num_transferencia; ?>" >
     <div id="panel_resp_codigo_num_transferencia"></div>

    <!-- Final del div panel campo -->
    </div>


	<!-- -------------------------------------------------------------------- -->
 
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
     <label> estado  </label>
     <?php
		     if($estado_estudiante==0){ echo "<label style='color:red;'> PENDIENTE </label>";}
		               
		     if($estado_estudiante==1){ echo "<label style='color:green;'> CANCELADO </label>";}
		     
		     if($estado_estudiante==2){ echo "<label style='color:#3498db;'> RESERVADO </label>";}
     ?>     
     </br> 
     
     <select class="form-control" id="estado_estudiante" name="estado_estudiante">
      <option value=""> Seleccione </option>
      <option value="0" > PENDIENTE </option>
      <option value="1" > CANCELADO </option>
      <option value="2" > CORTESIA </option>
     </select>
     
     <div id="panel_resp_estado_estudiante"></div>

    <!-- Final del div panel campo -->
    </div>


	<!-- -------------------------------------------------------------------- -->
 
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
     <label> estado certificado  </label>
     <?php
             
		     if($estado_certificado==0){ echo "<label style='color:red;'> PENDIENTE </label>";}
		     if($estado_certificado==1){ echo "<label style='color:green;'> ENTREGADO </label>";}
     ?>     
     </br> 
     
     <select class="form-control" id="estado_certificado" name="estado_certificado">
      <option value=""> Seleccione </option>
      <option value="0" > PENDIENTE </option>
      <option value="1" > ENTREGADO  </option>
     </select>
     
     <div id="panel_resp_estado_estudiante"></div>

    <!-- Final del div panel campo -->
    </div>
    
	<!-- -------------------------------------------------------------------- -->
 
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
     <label> facturar </label> 
     <?php
      
		 if($factura==0){ echo "<label style='color:red;'> NO </label>";}
		 if($factura==1){ echo "<label style='color:green;'> SI </label>";}

     ?>
     </br> 
     
     <select class="form-control" id="factura_estudiante" name="factura_estudiante" >
      <option value=""> Seleccione </option>
      <option value="1" > SI </option>
      <option value="0" > No </option>
     </select>
     
     <div id="panel_resp_estado_estudiante"></div>

    <!-- Final del div panel campo -->
    </div>
    
  

    
<!-- -------------------------------------------------------------------- -->
 
</form>
<!-- Final del div row -->
</div>
 

