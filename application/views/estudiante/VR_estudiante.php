<script src="https://socrates.pag-bo.com/scripts/Formulario.js"></script>

<div class="row">
 
 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel">
  <div class="row">


	<!-- -------------------------------------------------------------------- -->
	    <div class="col-lg126 col-md-12 col-sm-12 col-xs-12" id="panel_campo">
        
	   
        <label> Curso : <span id="curso"> </span> </label></br> 
        <input type="hidden" name="id_curso" id="id_curso" >
 
        <ul class="nav navbar-nav" style="width:100%; ">
         
        <li class="dropdown" style="width:100%;">
          
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="background:transparent; color:#2E4053; width:100%; border:1px solid #2E4053; padding: 1%;" id="select_curso" > Seleccione el Curso <span class="caret"></span> </a>

        <ul class="dropdown-menu" id="select-form" style="border:1px solid #bdc3c7;">
        <?php

         foreach($cursos as $curso)
         {
            $id_curso = $curso->id_curso;
            $curso_n = $curso->curso;
            $costo_curso_n = $curso->costo_curso;
            ?>
            <li> <a href="#" onclick="select('<?php echo $id_curso; ?>','<?php echo $curso_n; ?>','curso');"> <?php echo $curso_n; echo " "; echo $costo_curso_n." Bs."; ?> </a> </li> 
            <?php
          } ?>
         </ul>

         </li>
        </ul> 
         
        <div id="panel_resp_curso"></div>

    <!-- Final del div panel campo -->
    </div>
    
    
	<!-- -------------------------------------------------------------------- -->
	  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	     <label> NOMBRE COMPLETO </label></br> 
         <input type="text" class="form-control" name="nombre_estudiante" 
         id="nombre_estudiante" maxlength="200" onkeyup="validador_campo('nombre_estudiante','panel_resp_nombre_estudiante','5');" 
         onkeypress="return valida_letras(event);" placeholder="* Nombre Completo">
     
         <div id="panel_resp_nombre_estudiante"></div>

        <!-- Final del div panel campo -->
      </div>

	<!-- -------------------------------------------------------------------- -->
	  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	    <label> CI </label></br> 
        <input type="text" class="form-control" name="ci_estudiante" 
        id="ci_estudiante" maxlength="200" onkeyup="validador_campo('ci_estudiante','panel_resp_ci_estudiante','5');" 
        onkeypress="return valida_numeros(event);" placeholder="0">
     
        <div id="panel_resp_ci_estudiante"></div>

      <!-- Final del div panel campo -->
      </div>
      
	<!-- -------------------------------------------------------------------- -->
	  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	    <label> RAZON SOCIAL </label></br> 
        <input type="text" class="form-control" name="razon_social" 
        id="razon_social" maxlength="200" onkeyup="validador_campo('razon_social','panel_resp_razon_social','5');" 
        onkeypress="return valida_letras(event);" placeholder="* Razon Social">
     
        <div id="panel_resp_razon_social"></div>

      <!-- Final del div panel campo -->
      </div>
  
	<!-- -------------------------------------------------------------------- -->
	  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	     <label> NIT PARTICIPANTE </label></br> 
         <input type="text" class="form-control" name="nit_estudiante" 
         id="nit_estudiante" maxlength="200" onkeyup="validador_campo('nit_estudiante','panel_resp_nit_estudiante','5');" 
         onkeypress="return valida_letras(event);" placeholder="0">
     
         <div id="panel_resp_ci_nit_estudiante"></div>

      <!-- Final del div panel campo -->
      </div>
  
	<!-- -------------------------------------------------------------------- -->
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	   <label> EMAIL PARTICIPANTE </label></br> 
       <input type="text" class="form-control" name="email_estudiante" 
       id="email_estudiante" maxlength="200" onkeyup="validador_campo('email_estudiante','panel_resp_email_estudiante','5');" 
       onkeypress="return valida_letras(event);" placeholder="* Email">
     
       <div id="panel_resp_email_estudiante"></div>

      <!-- Final del div panel campo -->
      </div>
  
	<!-- -------------------------------------------------------------------- -->
	  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	   <label> CELULAR / WHATSAPP </label></br> 
       <input type="text" class="form-control" name="celular_estudiante" 
       id="celular_estudiante" maxlength="200" onkeyup="validador_campo('celular_estudiante','panel_resp_celular_estudiante','5');" 
       onkeypress="return valida_letras(event);" placeholder="0">
     
       <div id="panel_resp_celular_estudiante"></div>

      <!-- Final del div panel campo -->
      </div>

	<!-- -------------------------------------------------------------------- -->
	  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	   <label> IMPORTE CANCELADO </label></br> 
       <input type="text" class="form-control" name="importe_estudiante" 
       id="importe_estudiante" maxlength="200" onkeyup="validador_campo('importe_estudiante','panel_resp_importe_estudiante','5');" 
       onkeypress="return valida_numeros(event);" placeholder="0.00">
     
       <div id="panel_resp_importe_estudiante"></div>

      <!-- Final del div panel campo -->
      </div>


	<!-- -------------------------------------------------------------------- -->
	  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	   <label> SELECCIONE COMO REALIZO EL PAGO </label></br> 

       <select class="form-control" id="opcion_tipo_pago" onchange="btn_selec_codigo_transferencia();">
         
         <option value=""> Seleccione </option>
         <option value="1" > Banco Union </option>
         <option value="2" > Banco Mercantil </option>
         <option value="3" > Banco Bisa </option>
         <option value="5" > Tigo Money </option>
         <option value="4" > Pago en Efectivo </option>
                 
        </select>
        <div id="panel_resp_tipo_pago"></div>
             

      <!-- Final del div panel campo -->
      </div>
 
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
         
         <label> codigo/numero de transferencia </label>
         <input type="text" class="form-control" id="codigo_transferencia" placeholder="* numero o codigo de transferencia" autocomplete="off" style="text-transform:uppercase;" maxlength="50">
         <div id="panel_resp_codigo_transferencia"></div>
         
      </div>
         
 
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          </br>
          <center> 
           <button class="btn btn-default" onclick="btn_agregar_carrito_estudiante();" > Agregar </button> 
          </center>
      </div>

<!-- -------------------------------------------------------------------- -->

  <!-- Final del div row panel -->
  </div>

<!-- -------------------------------------------------------------------- -->  

 <!-- Final del div panel -->
 </div>

<!-- -------------------------------------------------------------------- -->

 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_listado">
    <h4 align="center" > LISTADO DE DATOS A REGISTRAR </h4>
    <div id="panel_listado_estudiante" style="overflow-y:scroll; max-height:350px;" >
      
    </div>
 </div>

 <!-- Final del div panel listado -->
 </div>

<!-- Final del div row -->
</div>
       

