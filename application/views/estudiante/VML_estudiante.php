
<script src="https://sistemasdemos.com/gratis/exportar_html_excel/FileSaver.min.js"></script>
<script src="https://sistemasdemos.com/gratis/exportar_html_excel/Blob.min.js"></script>
<script src="https://sistemasdemos.com/gratis/exportar_html_excel/xls.core.min.js"></script>
<script src="https://sistemasdemos.com/gratis/exportar_html_excel/dist/js/tableexport.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

<div class="componente_menu" >

  <h4 class="titulo_menu" > System /participante /menu </h4>
  <script type="text/javascript" src="<?php echo base_url(); ?>scripts/C_estudiante.js"></script>
   
 
  <div class="row">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="panel_buscador">
     
     <table class="table table-bordered table-condensed table-hover table-striped">
     <tr>
      
      <td> 
       <input type="text" class="form-control" id="txt_buscar" placeholder="* Buscar participante" onkeyup="buscar_datos(1);"> 
      </td>
      
      <td>
        <button class="btn btn-default btn-md" onclick="buscar_datos(1);" > 
        <span class="glyphicon glyphicon-search"></span>
        Buscar </button>
      
      </td>

      <td align="right">
       <select class="form-control" id="id_curso" name="id_curso" onchange="btn_listar_estudiantes_curso();">
           <option value=""> CURSOS </option>
           <?php
           
           foreach($cursos as $cu)
           {
               $id_curso = $cu->id_curso;
               $curso = $cu->curso;
            ?>
            <option value="<?php echo $id_curso; ?>"> <?php echo $curso; ?> </option>
            <?php
               
           }
           
           ?>
       </select>
       
      </td>
      
      <td align="right">
       <button class="btn btn-success btn-md" onclick="exportTableToExcel('tabla_participantes', 'tabla_participantes');" > 
       <span class="glyphicon glyphicon-load"></span>
       exportar excel </button>
       
      </td>

      <td align="right">
       <button class="btn btn-default btn-md" onclick="btn_modal_impresion('<?php echo $id_estudiante; ?>','<?php echo $id_curso; ?>');" > 
       <span class="glyphicon glyphicon-load"></span>
       imprimir </button>
       
      </td>

      <td align="right">
       <button class="btn btn-default btn-md" style=" color:#f39c12; border:1px solid #f39c12;" onclick="btn_opciones_impresion_certificados();" > 
       <span class="glyphicon glyphicon-load"></span>
       certificados </button>

       <div id="panel_certificados_print"> -.- </div>

      </td>
      
      <td align="right">
       <button class="btn btn-default btn-md" onclick="cargar_datos(1);" > 
       <span class="glyphicon glyphicon-load"></span>
       Recargar </button>
      </td>
      
      <td align="right">
       <button class="btn btn-default btn-md" onclick="btn_nuevo_estudiante();" > 
       <span class="glyphicon glyphicon-file"></span>
       Nuevo Participante </button>
      </td>

     </tr>
     
     </table>

     <div id="panel_resultado_busqueda"> </div>
     
     <label> Numero de Participantes : <?php echo $numrows; ?> </label>
     
   </div>
  
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="panel_listado">
   <div id="panel_paginacion_datos"  >
   
   <table class="table table-bordered table-condensed table-hover table-striped" id="tabla_participantes">
     <thead>
      <tr>
            <th class="hidden-lg hidden-md hidden-sm"> </th>
            <th> # </th>
    		<th> participante </th>
    		<th> razon social </th>
    		<th> nit </th>
    		<th> ci </th>
    		<th> email </th>
    		<th> celular </th>
    		<th> importe </th>
    		<th> tip pago </th>
    		<th> codigo num </th>
    		<th> curso </th>
    		<th> estado pago </th>
    		<th> facturar </th>
    		<th> estado certificado </th>
            <th class="hidden-xs"> </th>

      </tr>

     </thead>
     <tbody>
     <?php 
     $cont_part=0;
     foreach($estudiantes as $ob_estudiante)
     {
     	    $id_estudiante = $ob_estudiante->id_estudiante;
		    $nombre_estudiante = $ob_estudiante->nombre_estudiante;
		    $razon_social = $ob_estudiante->razon_social;
		    $ci_nit_estudiante = $ob_estudiante->ci_nit_estudiante;
		    $email_estudiante = $ob_estudiante->email_estudiante;
		    $celular_estudiante = $ob_estudiante->celular_estudiante;
		    $tipo_de_pago = $ob_estudiante->tipo_de_pago;
		    $codigo_num_transferencia = $ob_estudiante->codigo_num_transferencia;
		    $curso = $ob_estudiante->curso;
		    $id_curso = $ob_estudiante->id_curso;
		    
		    $estado_estudiante = $ob_estudiante->estado_estudiante;
		    $factura = $ob_estudiante->factura;
		    $cont_part++;
		    $estado_certificado = $ob_estudiante->estado_certificado;
		    
		    $ci_estudiante = $ob_estudiante->ci_estudiante;
		    $importe_estudiante = $ob_estudiante->importe_estudiante;
     ?>
       <tr >

        <td class="col-xs-1 hidden-lg hidden-md hidden-sm"> <center>
             <a href="#" onclick="btn_submenu_xs('<?php echo $id_estudiante; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones-xs" id="panel_opciones_xs<?php echo $id_estudiante; ?>">

              <div align="right" class="panel_cerrar_botones-xs" >
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu_xs('<?php echo $id_estudiante; ?>');"> X </buttom>
              </div>
              
              <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_modal_impresion('<?php echo $id_estudiante; ?>','<?php echo $id_curso; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-print"></span> 
               certificado </button> 
               
               
              <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_examinar_estudiante('<?php echo $id_estudiante; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_editar_estudiante('<?php echo $id_estudiante; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line-xs">

               <button class="btn btn-danger btn-md" onclick="btn_borrar_estudiante('<?php echo $id_estudiante; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 

             </div>

          </center>
          </td>
            <td style="text-transform:uppercase;" > <?php echo $cont_part; ?> </td>
		    <td class="col-lg-3" style="text-transform:uppercase;" > <?php echo $nombre_estudiante; ?> </td>
		    <td style="text-transform:uppercase;" > <?php echo $razon_social; ?> </td>
		    <td style="text-transform:uppercase;" > <?php echo $ci_nit_estudiante; ?> </td>
		    <td style="text-transform:uppercase;" > <?php echo $ci_estudiante; ?> </td>

		    <td class="col-lg-1" > <?php echo $email_estudiante; ?> </td>
		    <td style="text-transform:uppercase;" > <?php echo $celular_estudiante; ?> </td>
		    <td style="text-transform:uppercase;" > <?php echo $importe_estudiante; ?> </td>
		    <td style="text-transform:uppercase;" > <?php  

                        if($tipo_de_pago=="1"){ echo "Banco Union"; }
                        if($tipo_de_pago=="2"){ echo "Banco Mercantil"; }
                        if($tipo_de_pago=="3"){ echo "Banco Bisa"; }
                        if($tipo_de_pago=="4"){ echo "Pago en Efectivo"; }
                        if($tipo_de_pago=="5"){ echo "Tigo Money"; }
            
		    ?> </td>
		    <td style="text-transform:uppercase;" > <?php echo $codigo_num_transferencia; ?> </td>
		    <td class="col-lg-4" style="text-transform:uppercase;" > <?php echo $rest = substr($curso,0,50); ?> </td>
		    
		    <td style="text-transform:uppercase;" > <?php 
                       
                        if($estado_estudiante==0){ echo "<label style='color:red;'> PENDIENTE </label>";}
		                if($estado_estudiante==1){ echo "<label style='color:green;'> CANCELADO </label>";}
		                if($estado_estudiante==2){ echo "<label style='color:#3498db;'> CORTESIA </label>";}
       
            ?> </td>
		    <td style="text-transform:uppercase;" > <?php  
		    
		    		   if($factura==0){ echo "<label style='color:red;'> NO </label>";}
		               
		               if($factura==1){ echo "<label style='color:green;'> SI </label>";}
		    
		    ?> </td>
		    
		    <td>  <?php if($estado_certificado==0){ echo "<label style='color:red;'> PENDIENTE </label>"; } else{ echo "<label style='color:green;'> ENTREGADO </label>"; } ?> </td>
	 
 
            <td class="col-lg-1 hidden-xs"> <center>
             <a href="#" onclick="btn_submenu('<?php echo $id_estudiante; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones" id="panel_opciones_<?php echo $id_estudiante; ?>">

              <div align="right" class="panel_cerrar_botones">
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu('<?php echo $id_estudiante; ?>');"> x </buttom>
              </div>

              <hr class="btn_line-xs">

               <button class="btn btn-default btn-xs" onclick="btn_modal_impresion('<?php echo $id_estudiante; ?>','<?php echo $id_curso; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-print"></span> 
               certificado </button>
               
              <hr class="btn_line">

	   	         <button class="btn btn-default btn-xs" onclick="btn_examinar_estudiante('<?php echo $id_estudiante; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
	   	         examinar </button> 
	   	       
               <hr class="btn_line">

	   	         <button class="btn btn-default btn-xs" onclick="btn_editar_estudiante('<?php echo $id_estudiante; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
	   	           editar </button> 

               <hr class="btn_line">

	   	         <button class="btn btn-danger btn-xs" onclick="btn_borrar_estudiante('<?php echo $id_estudiante; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
	   	         borrar </button> 
             
             </div>

          </center>
	   	    </td>

       </tr>
     <?php
     } 
     ?>
     </tbody>
 
 <!-- Final del table -->
 </table>

 <?php //require "paginacion.php"; ?>

 </div>
 <!-- Final del panel blanco -->
 </div>

 <!-- Final del div panel -->
 </div>



<!-- Final del div row -->
</div>

<?php require "VM_estudiante.php"; ?>
 

