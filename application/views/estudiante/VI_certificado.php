<?php
      
 require_once 'assets/phpqrcode/phpqrcode.php';
 require_once ('assets/dompdf/autoload.inc.php');
 
 $id_estudiante = $estudiante[0]->id_estudiante;
 $participante_curso = $estudiante[0]->nombre_estudiante;
 $razon_social = $estudiante[0]->razon_social;
 $ci_nit_estudiante = $estudiante[0]->ci_nit_estudiante;
 $email_estudiante = $estudiante[0]->email_estudiante;
 
 $celular_estudiante = $estudiante[0]->celular_estudiante;
 $curso = $estudiante[0]-> curso;
 $id_curso = $estudiante[0]-> $id_curso;
 $certificado_curso = $estudiante[0]-> certificado_curso;
 
 $se_certifica_curso = $estudiante[0]-> se_certifica_curso;
 $participa_curso = $estudiante[0]-> participa_curso;
 $horas_curso = $estudiante[0]-> horas_curso;
 $fecha_del_evento = $estudiante[0]-> fecha_del_evento;
 
 $titulo_curso = $estudiante[0]-> curso;
  
         
 $qr_participante = "Curso : ".$titulo_curso." \n fecha :  ".$fecha_curso." \n Instructor : Juan Vargas Buendia \n participante : ".$participante_curso."\n razon social : ".$razon_social."\n ci/nit : ".$ci_nit_estudiante."\n email : ".$email_estudiante."\n celular : ".$celular_estudiante; 
 
 QRcode::png ($qr_participante,"assets/multimedia/qrs/participante_C".$id_curso."-P".$id_estudiante."_qr.png", "L", 5, 5) ;
 
 //$html=' <img src="https://socrates.pag-bo.com/assets/multimedia/qrs/prueba_qr.png" style="width:150px; height:150px;">';
 
   /*
  $titulo_curso;
  $detalle_curso;
  $participante_curso;
  $fecha_curso;
  */
  
 $html='

    <div style="width:100%; padding:0px;">
        <img src="https://socrates.pag-bo.com/assets/multimedia/certificado/'.$certificado_curso.'" 
        style="width:100%; height: 730px; position:absolute; z-index:1; ">

        <h2 style="z-index:1; position:absolute; margin-top:8%; margin-left:3%; font-family: Arial, Helvetica, sans-serif; font-size:40px; color: #2C303B; text-transform:uppercase; text-align:center;"> 
         '.$titulo_curso.' </h2>
         
 
        <p style="z-index:1; position:absolute; margin-top:40%; margin-left:21%; font-family: Arial, Helvetica, sans-serif; font-size:15px; color: #2C303B; text-transform:uppercase; font-weight:bold;"> 
         '.$se_certifica_curso.' </p>
         
        <p style="z-index:1; position:absolute; margin-top:45%; margin-left:21%; font-family: Arial, Helvetica, sans-serif; font-size:35px; color: #2C303B; text-transform:uppercase;"> 
         '.$participante_curso.' </p>

        <p style="z-index:1; position:absolute; margin-top:53%; margin-left:21%; font-family: Arial, Helvetica, sans-serif; font-size:15px; color: #2C303B; text-transform:uppercase; font-weight:bold;"> 
         '.$participa_curso.' </p>
         
         
        <p style="z-index:1; position:absolute; margin-top:60%; margin-left:21%; font-family: Arial, Helvetica, sans-serif; font-size:20px; color: #2C303B; text-transform:uppercase; font-weight:bold;"> 
         '.$horas_curso.' </p>


        <p style="z-index:1; position:absolute; margin-top:67%; margin-left:21%; font-family: Arial, Helvetica, sans-serif; font-size:18px; color: #2C303B; text-transform:uppercase; font-weight:bold;"> 
         '.$fecha_del_evento.' </p>
         
    
        <img src="https://socrates.pag-bo.com/assets/multimedia/qrs/participante_C'.$id_curso.'-P'.$id_estudiante.'_qr.png" 
        style="width:150px; height:150px; z-index:1; position:absolute; margin-top:22%; margin-left:82%; ">
    
    </div> 

 ';

use Dompdf\Dompdf;
$dompdf = new Dompdf(array('enable_remote' => true));
$dompdf->loadHtml($html);
$dompdf->setPaper('letter', 'landscape');

$dompdf->render();
$dompdf->stream("certificado_".$participante_curso."",array("Attachment"=>0)); 
 
?>



