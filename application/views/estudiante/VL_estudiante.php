   <table class="table table-bordered table-condensed table-hover table-striped" id="tabla_participantes">
     <thead>
      <tr>
        <th class="hidden-lg hidden-md hidden-sm"> </th>
        <th> # </th>
		<th> participante </th>
		<th> razon social </th>
		<th> nit </th>
		<th> ci </th>
		<th> email </th>
		<th> celular </th>
		<th> importe </th>
		<th> tip pago </th>
		<th> codigo num </th>
		<th> curso </th>
		<th> estado pago </th>
		<th> facturar </th>
		<th> estado certificado </th>
        <th class="hidden-xs"> </th>
      </tr>

     </thead>
     <tbody>
     <?php 
     $cont_part=0;
     foreach($estudiantes as $ob_estudiante)
     {
     	    $id_estudiante = $ob_estudiante->id_estudiante;
		    $nombre_estudiante = $ob_estudiante->nombre_estudiante;
		    $razon_social = $ob_estudiante->razon_social;
		    $ci_nit_estudiante = $ob_estudiante->ci_nit_estudiante;
		    $email_estudiante = $ob_estudiante->email_estudiante;
		    $celular_estudiante = $ob_estudiante->celular_estudiante;
		    $tipo_de_pago = $ob_estudiante->tipo_de_pago;
		    $codigo_num_transferencia = $ob_estudiante->codigo_num_transferencia;
		    $curso = $ob_estudiante->curso;
		    $id_curso = $ob_estudiante->id_curso;
		    
		    $estado_estudiante = $ob_estudiante->estado_estudiante;
		    $factura = $ob_estudiante->factura;
		    $cont_part++;
		    $estado_certificado = $ob_estudiante->estado_certificado;
		    
		    $ci_estudiante = $ob_estudiante->ci_estudiante;
		    $importe_estudiante = $ob_estudiante->importe_estudiante;
     ?>
       <tr >

        <td class="col-xs-1 hidden-lg hidden-md hidden-sm"> <center>
             <a href="#" onclick="btn_submenu_xs('<?php echo $id_estudiante; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones-xs" id="panel_opciones_xs<?php echo $id_estudiante; ?>">

              <div align="right" class="panel_cerrar_botones-xs" >
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu_xs('<?php echo $id_estudiante; ?>');"> X </buttom>
              </div>
              
              <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_modal_impresion('<?php echo $id_estudiante; ?>','<?php echo $id_curso; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-print"></span> 
               certificado </button> 
               
               
              <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_examinar_estudiante('<?php echo $id_estudiante; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_editar_estudiante('<?php echo $id_estudiante; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line-xs">

               <button class="btn btn-danger btn-md" onclick="btn_borrar_estudiante('<?php echo $id_estudiante; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 

             </div>

          </center>
          </td>
            <td style="text-transform:uppercase;" > <?php echo $cont_part; ?> </td>
		    <td class="col-lg-3" style="text-transform:uppercase;" > <?php echo $nombre_estudiante; ?> </td>
		    <td style="text-transform:uppercase;" > <?php echo $razon_social; ?> </td>
		    <td style="text-transform:uppercase;" > <?php echo $ci_nit_estudiante; ?> </td>
		    <td style="text-transform:uppercase;" > <?php echo $ci_estudiante; ?> </td>

		    <td class="col-lg-1" > <?php echo $email_estudiante; ?> </td>
		    <td style="text-transform:uppercase;" > <?php echo $celular_estudiante; ?> </td>
		    <td style="text-transform:uppercase;" > <?php echo $importe_estudiante; ?> </td>
		    <td style="text-transform:uppercase;" > <?php  

                        if($tipo_de_pago=="1"){ echo "Banco Union"; }
                        if($tipo_de_pago=="2"){ echo "Banco Mercantil"; }
                        if($tipo_de_pago=="3"){ echo "Banco Bisa"; }
                        if($tipo_de_pago=="4"){ echo "Pago en Efectivo"; }
                        if($tipo_de_pago=="5"){ echo "Tigo Money"; }
            
		    ?> </td>
		    <td style="text-transform:uppercase;" > <?php echo $codigo_num_transferencia; ?> </td>
		    <td class="col-lg-4" style="text-transform:uppercase;" > <?php echo $rest = substr($curso,0,50); ?> </td>
		    
		    <td style="text-transform:uppercase;" > <?php 
                       
                        if($estado_estudiante==0){ echo "<label style='color:red;'> PENDIENTE </label>";}
		                if($estado_estudiante==1){ echo "<label style='color:green;'> CANCELADO </label>";}
		                if($estado_estudiante==2){ echo "<label style='color:#3498db;'> CORTESIA </label>";}
       
            ?> </td>
		    <td style="text-transform:uppercase;" > <?php  
		    
		    		   if($factura==0){ echo "<label style='color:red;'> NO </label>";}
		               
		               if($factura==1){ echo "<label style='color:green;'> SI </label>";}
		    
		    ?> </td>
		    
		    <td>  <?php if($estado_certificado==0){ echo "<label style='color:red;'> PENDIENTE </label>"; } else{ echo "<label style='color:green;'> ENTREGADO </label>"; } ?> </td>
	 
 
            <td class="col-lg-1 hidden-xs"> <center>
             <a href="#" onclick="btn_submenu('<?php echo $id_estudiante; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones" id="panel_opciones_<?php echo $id_estudiante; ?>">

              <div align="right" class="panel_cerrar_botones">
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu('<?php echo $id_estudiante; ?>');"> x </buttom>
              </div>

              <hr class="btn_line-xs">

               <button class="btn btn-default btn-xs" onclick="btn_modal_impresion('<?php echo $id_estudiante; ?>','<?php echo $id_curso; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-print"></span> 
               certificado </button>
               
              <hr class="btn_line">

	   	         <button class="btn btn-default btn-xs" onclick="btn_examinar_estudiante('<?php echo $id_estudiante; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
	   	         examinar </button> 
	   	       
               <hr class="btn_line">

	   	         <button class="btn btn-default btn-xs" onclick="btn_editar_estudiante('<?php echo $id_estudiante; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
	   	           editar </button> 

               <hr class="btn_line">

	   	         <button class="btn btn-danger btn-xs" onclick="btn_borrar_estudiante('<?php echo $id_estudiante; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
	   	         borrar </button> 
             
             </div>

          </center>
	   	    </td>

       </tr>
     <?php
     } 
     ?>
     </tbody>
 
 <!-- Final del table -->
 </table>
 
 <?php // require "paginacion.php"; ?>

