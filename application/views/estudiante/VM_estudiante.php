

<!-- Modal -->
<div id="myModal_Register" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" style="width: 95%;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Registrar participante </h4>
      </div>
      <div class="modal-body">
         <div id="panel_modal_registrar"> </div>
         <div id="panel_modal_resultado_registrar"> </div>
      </div>
      <div class="modal-footer">
        
        <button type="button" id="btn_registrar" class="btn btn-info" onclick="btn_registrar_estudiante();" > 
         <span class="glyphicon glyphicon-file"></span>
         Registrar </button>

        <button type="button" class="btn btn-danger" data-dismiss="modal">
         <span class="glyphicon glyphicon-remove"></span>
         Cerrar </button>

      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal_Certificado" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md"">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Modulo Impresion </h4>
      </div>
      <div class="modal-body">
         
         <div id="panel_modal_certificado"> </div>
         
         <div id="panel_modal_resultado_certificado"> </div>
         
      </div>
      <div class="modal-footer">
        
        <center>
         <button type="button" id="btn_registrar" class="btn btn-success" onclick="btn_formulario_imprimir_participante();" > 
         <span class="glyphicon glyphicon-file"></span>
          Generar </button>

        <button type="button" class="btn btn-danger" data-dismiss="modal">
         <span class="glyphicon glyphicon-remove"></span>
         Cerrar </button>
        </center>

      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal_View" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Examinar participante </h4>
      </div>
      <div class="modal-body">
         <div id="panel_modal_examinar"> </div>
      </div>
      <div class="modal-footer">
 
        <button type="button" class="btn btn-danger" data-dismiss="modal"> 
         <span class="glyphicon glyphicon-remove"></span>
         Cerrar 
        </button>
      
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal_Update" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Edición de participante </h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="id_estudiante_edicion" >
        <div id="panel_modal_editar"> </div>
        <div id="panel_modal_respuesta_editar"> </div>
      </div>
      <div class="modal-footer">
        
        <button type="button" id="btn_editar" class="btn btn-success" onclick="btn_guardar_estudiante();" > 
         <span class="glyphicon glyphicon-floppy-saved"></span>
         Guardar 
        </button>

        <button type="button" class="btn btn-danger" data-dismiss="modal"> 
         <span class="glyphicon glyphicon-remove"></span>
         Cerrar 
        </button>

      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModal_Delete" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background:red !important; ">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Borrado de participante  </h4>
      </div>
      <div class="modal-body">
         <input type="hidden" id="id_estudiante_eliminar" >
         <div id="panel_modal_eliminar"> 
 
           <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" >&times;</button>
             
             <h4> <strong> ¡Esta Seguro de Eliminar el Dato! </strong> </h4> 
             Es muy importante que este seguro de la funcion

           </div>

         </div>

      </div>
      <div class="modal-footer">
        
        <button type="button" id="btn_borrar" class="btn btn-info" onclick="btn_eliminar_estudiante();" > 
         <span class="glyphicon glyphicon-trash"></span> 
         Borrar 
        </button>
        
        <button type="button" class="btn btn-danger" data-dismiss="modal"> 
         <span class="glyphicon glyphicon-remove"></span>
         Cerrar 
        </button>

      </div>
    </div>

  </div>
</div>



<!-- Modal -->
<div id="myModal_Impresion" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md" >

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Impresion Participante </h4>
      </div>
      <div class="modal-body">
         <div id="panel_modal_impresion"> </div>
         <div id="panel_modal_resultado_impresion"> </div>
      </div>
      <div class="modal-footer">
        
        <button type="button" id="btn_registrar" class="btn btn-info" onclick="btn_imprimir_participante_filtro();" > 
         <span class="glyphicon glyphicon-print"></span>
         Imprimir </button>

        <button type="button" class="btn btn-danger" data-dismiss="modal">
         <span class="glyphicon glyphicon-remove"></span>
         Cerrar </button>

      </div>
    </div>

  </div
  

