
<div class="row">
<form id="formulario" enctype="multipart/form-data">

	<!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
         <center>
        <label> portada </label></br> 

	      <img id="img_resp_portada" src="" style="width: 80%; height: 300px; margin-bottom: 1.5%; border:1px solid #086A87;"> </br>
	 
	      <span class="btn btn-success btn-file"> 
	      <span class="glyphicon glyphicon-picture"></span> Subir portada
	      
	      <input type="file" class="form-control" name="portada" 
	      id="portada" onchange="previsualizar();">

	      </span> </center>

	      <div id="panel_resp_portada"></div> 
 
	  <!-- Final del div panel campo -->
	  </div>

	<!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        <label> razon social </label></br> 
	     <input type="text" class="form-control" name="razon_social" 
	     id="razon_social" maxlength="200" onkeyup="validador_campo('razon_social','panel_resp_razon_social','5');" 
	     onkeypress="return valida_letras(event);" placeholder="* Escriba su razon social">
	     
	     <div id="panel_resp_razon_social"></div>

	  <!-- Final del div panel campo -->
	  </div>

	  
	<!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
          <label> descripcion </label></br> 
	      <textarea class="form-control" name="descripcion" 
	      id="descripcion" maxlength="3000" onkeyup="validador_campo('descripcion','panel_resp_descripcion','');" onkeypress="return valida_ambos(event);" placeholder="* Escriba su descripcion" ></textarea>
	 
	      <div id="panel_resp_descripcion"></div>

	  <!-- Final del div panel campo -->
	  </div>

	  
	<!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="panel_campo">
         <label> celular </label></br> 
	       <input type="text" class="form-control" name="celular" 
	       id="celular" maxlength="10" onkeyup="validador_campo('celular','panel_resp_celular','');" 
	       onkeypress="return valida_numeros(event);" placeholder="* Escriba su celular">
	       <div id="panel_resp_celular"></div>

	       <!-- Final del div panel campo -->
	    </div>
	   
	<!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="panel_campo">
         <label> whatsapp </label></br> 
	       <input type="text" class="form-control" name="whatsapp" 
	       id="whatsapp" maxlength="10" onkeyup="validador_campo('whatsapp','panel_resp_whatsapp','');" 
	       onkeypress="return valida_numeros(event);" placeholder="* Escriba su whatsapp">
	       <div id="panel_resp_whatsapp"></div>

	       <!-- Final del div panel campo -->
	    </div>
	   
	<!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        <label> direccion </label></br> 
	     <input type="text" class="form-control" name="direccion" 
	     id="direccion" maxlength="200" onkeyup="validador_campo('direccion','panel_resp_direccion','');" 
	     onkeypress="return valida_letras(event);" placeholder="* Escriba su direccion">
	     
	     <div id="panel_resp_direccion"></div>

	  <!-- Final del div panel campo -->
	  </div>

	  


	  

<!-- Final del formulario -->
</form>

<!-- Final del div row -->
</div>
       

