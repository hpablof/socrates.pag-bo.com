
   <a href="javascript:void();" onclick="cargar_datos(1);"> Listar Datos  </a>
   <table class="table table-bordered table-condensed table-hover table-striped">
     <thead>
      <tr>
        <th class="hidden-lg hidden-md hidden-sm"> </th>
        <th> razon_social </th>
        <th> descripcion </th>
        <th> celular </th>
        <th> whatsapp </th>
        <th> direccion </th>
        <th> portada </th>
       <th class="hidden-xs"> </th>

      </tr>

     </thead>
     <tbody>
     <?php 
     foreach($ajustess as $ob_ajustes)
     {
        $id_ajustes = $ob_ajustes->id_ajustes;
        $razon_social = $ob_ajustes->razon_social;
        $descripcion = $ob_ajustes->descripcion;
        $celular = $ob_ajustes->celular;
        $whatsapp = $ob_ajustes->whatsapp;
        $direccion = $ob_ajustes->direccion;
        $portada = $ob_ajustes->portada;
     ?>
       <tr>

        <td class="col-xs-1 hidden-lg hidden-md hidden-sm"> <center>
             <a href="#" onclick="btn_submenu_xs('<?php echo $id_ajustes; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones-xs" id="panel_opciones_xs<?php echo $id_ajustes; ?>">

              <div align="right" class="panel_cerrar_botones-xs" >
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu_xs('<?php echo $id_ajustes; ?>');"> X </buttom>
              </div>
              <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_examinar_ajustes('<?php echo $id_ajustes; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_editar_ajustes('<?php echo $id_ajustes; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line-xs">

               <button class="btn btn-danger btn-md" onclick="btn_borrar_ajustes('<?php echo $id_ajustes; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 

             </div>

          </center>
          </td>
     
        <td> <?php echo $razon_social; ?> </td>
        <td> <?php echo $descripcion; ?> </td>
        <td> <?php echo $celular; ?> </td>
        <td> <?php echo $whatsapp; ?> </td>
        <td> <?php echo $direccion; ?> </td>
        <td> <img src="<?php echo base_url(); ?>assets/multimedia/portadas/<?php echo $portada; ?>" style="width:50px; height:50px;" > </td>
        <td class="col-lg-1 hidden-xs"> <center>
             <a href="#" onclick="btn_submenu('<?php echo $id_ajustes; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones" id="panel_opciones_<?php echo $id_ajustes; ?>">

              <div align="right" class="panel_cerrar_botones">
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu('<?php echo $id_ajustes; ?>');"> x </buttom>
              </div>
              <hr class="btn_line">

               <button class="btn btn-default btn-xs" onclick="btn_examinar_ajustes('<?php echo $id_ajustes; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line">

               <button class="btn btn-default btn-xs" onclick="btn_editar_ajustes('<?php echo $id_ajustes; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line">

               <button class="btn btn-danger btn-xs" onclick="btn_borrar_ajustes('<?php echo $id_ajustes; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 
             
             </div>

          </center>
          </td>

       </tr>
     <?php
     } 
     ?>
     </tbody>
 
 <!-- Final del table -->
 </table>
 <?php require "paginacion_busqueda.php"; ?>

