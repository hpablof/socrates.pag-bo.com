
<div class="row">
 
 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel">
  <div class="row">

	<!-- -------------------------------------------------------------------- -->
	    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	   <label> alias </label></br> 
      <input type="text" class="form-control" name="alias" 
       id="alias" maxlength="200" onkeyup="validador_campo('alias','panel_resp_alias','5');" 
       onkeypress="return valida_letras(event);" placeholder="* Escriba su alias">
     
       <div id="panel_resp_alias"></div>

      <!-- Final del div panel campo -->
      </div>
  
	<!-- -------------------------------------------------------------------- -->
	    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	   <label> email </label></br> 
      <input type="text" class="form-control" name="email" 
       id="email" maxlength="300" onkeyup="validador_correo('email','5','panel_resp_email');" 
       onkeypress="return valida_ambos(event);" placeholder="* Escriba su email">
       
       <div id="panel_resp_email"></div>

      <!-- Final del div panel campo -->
     </div>
 
  
	<!-- -------------------------------------------------------------------- -->
	    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	   <label> password </label></br> 
      <input type="password" class="form-control" name="password" 
      id="password" maxlength="10" onkeyup="validador_campo('password','panel_resp_password','5');" 
      onkeypress="return valida_ambos(event);" placeholder="* Escriba su password">
      <div id="panel_resp_password"></div>

     <!-- Final del div panel campo -->
     </div>

  
	<!-- -------------------------------------------------------------------- -->
	    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	   
        <label> cargo : <span id="cargo_usuario"> </span> </label></br> 
        <input type="hidden" name="id_cargo_usuario" id="id_cargo_usuario" >
 
        <ul class="nav navbar-nav" style="width:100%; ">
         
        <li class="dropdown" style="width:100%;">
          
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="background:transparent; color:#2E4053; width:100%; border:1px solid #2E4053; padding: 1%;" id="select_cargo_usuario" > cargo <span class="caret"></span> </a>

        <ul class="dropdown-menu" id="select-form" style="border:1px solid #bdc3c7;">
        <?php

         foreach($cargo_usuarios as $cargo_usuario)
         {
            $id_cargo_usuario = $cargo_usuario->id_cargo_usuario;
            $cargo_usuario_n = $cargo_usuario->cargo_usuario;
            ?>
            <li> <a href="#" onclick="select('<?php echo $id_cargo_usuario; ?>','<?php echo $cargo_usuario_n; ?>','cargo_usuario');"> <?php echo $cargo_usuario_n; ?> </a> </li> 
            <?php
          } ?>
         </ul>

         </li>
        </ul> 
         
        <div id="panel_resp_cargo_usuario"></div>

    <!-- Final del div panel campo -->
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="panel_campo"></div>
    
	<!-- -------------------------------------------------------------------- -->
	    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	   <label> nombres </label></br> 
      <input type="text" class="form-control" name="nombres" 
       id="nombres" maxlength="200" onkeyup="validador_campo('nombres','panel_resp_nombres','5');" 
       onkeypress="return valida_letras(event);" placeholder="* Escriba su nombres">
     
       <div id="panel_resp_nombres"></div>

      <!-- Final del div panel campo -->
      </div>
  
	<!-- -------------------------------------------------------------------- -->
	    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	   <label> apellidos </label></br> 
      <input type="text" class="form-control" name="apellidos" 
       id="apellidos" maxlength="200" onkeyup="validador_campo('apellidos','panel_resp_apellidos','5');" 
       onkeypress="return valida_letras(event);" placeholder="* Escriba su apellidos">
     
       <div id="panel_resp_apellidos"></div>

      <!-- Final del div panel campo -->
      </div>
  
	<!-- -------------------------------------------------------------------- -->
	    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
	   <label> ci </label></br> 
      <input type="text" class="form-control" name="ci" 
       id="ci" maxlength="200" onkeyup="validador_campo('ci','panel_resp_ci','5');" 
       onkeypress="return valida_letras(event);" placeholder="* Escriba su ci">
     
       <div id="panel_resp_ci"></div>

      <!-- Final del div panel campo -->
      </div>
  



    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      </br>
      <center> 
       <button class="btn btn-default" onclick="btn_agregar_carrito_usuario();" > Agregar </button> 
      </center>
    </div>

<!-- -------------------------------------------------------------------- -->

  <!-- Final del div row panel -->
  </div>

<!-- -------------------------------------------------------------------- -->  

 <!-- Final del div panel -->
 </div>

<!-- -------------------------------------------------------------------- -->

 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_listado">
    <h4 align="center" > LISTADO DE DATOS A REGISTRAR </h4>
    <div id="panel_listado_usuario" style="overflow-y:scroll; max-height:350px;" >
      
    </div>
 </div>

 <!-- Final del div panel listado -->
 </div>

<!-- Final del div row -->
</div>
       

