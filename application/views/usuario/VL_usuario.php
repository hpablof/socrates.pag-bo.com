

   <table class="table table-bordered table-condensed table-hover table-striped">
     <thead>
      <tr>
        <th class="hidden-lg hidden-md hidden-sm"> </th>
        <th> alias </th>
        <th> email </th>
        <th> password </th>
        <th> cargo_usuario </th>
        <th> nombres </th>
        <th> apellidos </th>
        <th> ci </th>
       <th class="hidden-xs"> </th>

      </tr>

     </thead>
     <tbody>
     <?php 
     foreach($usuarios as $ob_usuario)
     {
        $id_usuario = $ob_usuario->id_usuario;
        $alias = $ob_usuario->alias;
        $email = $ob_usuario->email;
        $password = $ob_usuario->password;
        $cargo_usuario = $ob_usuario->cargo_usuario;
        $nombres = $ob_usuario->nombres;
        $apellidos = $ob_usuario->apellidos;
        $ci = $ob_usuario->ci;
     ?>
       <tr>

        <td class="col-xs-1 hidden-lg hidden-md hidden-sm"> <center>
             <a href="#" onclick="btn_submenu_xs('<?php echo $id_usuario; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones-xs" id="panel_opciones_xs<?php echo $id_usuario; ?>">

              <div align="right" class="panel_cerrar_botones-xs" >
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu_xs('<?php echo $id_usuario; ?>');"> X </buttom>
              </div>
              <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_examinar_usuario('<?php echo $id_usuario; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_editar_usuario('<?php echo $id_usuario; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line-xs">

               <button class="btn btn-danger btn-md" onclick="btn_borrar_usuario('<?php echo $id_usuario; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 

             </div>

          </center>
          </td>
     
        <td> <?php echo $alias; ?> </td>
        <td> <?php echo $email; ?> </td>
        <td> <?php echo $password; ?> </td>
        <td> <?php echo $cargo_usuario; ?> </td>
        <td> <?php echo $nombres; ?> </td>
        <td> <?php echo $apellidos; ?> </td>
        <td> <?php echo $ci; ?> </td>
        <td class="col-lg-1 hidden-xs"> <center>
             <a href="#" onclick="btn_submenu('<?php echo $id_usuario; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones" id="panel_opciones_<?php echo $id_usuario; ?>">

              <div align="right" class="panel_cerrar_botones">
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu('<?php echo $id_usuario; ?>');"> x </buttom>
              </div>
              <hr class="btn_line">

               <button class="btn btn-default btn-xs" onclick="btn_examinar_usuario('<?php echo $id_usuario; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line">

               <button class="btn btn-default btn-xs" onclick="btn_editar_usuario('<?php echo $id_usuario; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line">

               <button class="btn btn-danger btn-xs" onclick="btn_borrar_usuario('<?php echo $id_usuario; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 
             
             </div>

          </center>
          </td>

       </tr>
     <?php
     } 
     ?>
     </tbody>
 
 <!-- Final del table -->
 </table>
 <?php require "paginacion.php"; ?>

