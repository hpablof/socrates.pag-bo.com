<div class="row">
<form id="formulario" enctype="multipart/form-data">

	<!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
         <center>
          <label> portada  </label></br> 

	      <img id="img_resp_portada_curso" src="" style="width: 80%; height: 300px; margin-bottom: 1.5%; border:1px solid #086A87;"> </br>
	 
	      <span class="btn btn-success btn-file"> 
	      <span class="glyphicon glyphicon-picture"></span> Subir portada
	      
	      <input type="file" class="form-control" name="portada_curso" 
	      id="portada_curso" onchange="previsualizar();">

	      </span> </center>

	      <div id="panel_resp_portada_curso"></div> 
 

         <center>
          <label> certificado_curso </label></br> 

	      <img id="img_resp_certificado_curso" src="" style="width: 80%; height: 300px; margin-bottom: 1.5%; border:1px solid #086A87;"> </br>
	 
	      <span class="btn btn-success btn-file"> 
	      <span class="glyphicon glyphicon-picture"></span> Subir certificado
	      
	      <input type="file" class="form-control" name="certificado_curso" 
	      id="certificado_curso" onchange="previsualizar_certificado();">

	      </span> 
	     </center>

	      <div id="panel_resp_certificado_curso"></div> 

	  <!-- Final del div panel campo -->
	  </div>
	  
	  
	<!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
         <label> curso </label></br> 
	     <input type="text" class="form-control" name="curso" 
	     id="curso" maxlength="200" onkeyup="validador_campo('curso','panel_resp_curso','');" 
	     onkeypress="return valida_letras(event);" placeholder="* Escriba su curso">
	     
	     <div id="panel_resp_curso"></div>

	  <!-- Final del div panel campo -->
	  </div>

	  
	 <!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
         <label> costo </label></br> 
	     <input type="text" class="form-control" name="costo_curso" 
	     id="costo_curso" maxlength="10" onkeyup="validador_campo('costo_curso','panel_resp_costo_curso','');" 
	     onkeypress="return valida_letras(event);" placeholder="0.00">
	     
	     <div id="panel_resp_costo_curso"></div>

	  <!-- Final del div panel campo -->
	  </div>

	  
	 <!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
         <label> fecha del curso </label></br> 
	      <input type="date" class="form-control" name="fecha_curso" 
	      id="fecha_curso" maxlength="10" onkeyup="validador_campo('fecha_curso','panel_resp_fecha_curso','');" 
	      onkeypress="return valida_ambos(event);" placeholder="* Escriba su fecha_curso">
	      <div id="panel_resp_fecha_curso"></div>

	  <!-- Final del div panel campo -->
	  </div>
	  
	 <!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        <label> duración del curso </label></br> 
	     <input type="text" class="form-control" name="duracion_curso" 
	     id="duracion_curso" maxlength="200" onkeyup="validador_campo('duracion_curso','panel_resp_duracion_curso','');" 
	     onkeypress="return valida_letras(event);" placeholder="* Escriba su duracion_curso">
	     
	     <div id="panel_resp_duracion_curso"></div>

	  <!-- Final del div panel campo -->
	  </div>

	  <!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
         
         <label> descripcion </label></br> 
	     <textarea class="form-control" name="descripcion_curso" id="descripcion_curso" > </textarea>
    
         <script>
          CKEDITOR.replace('descripcion_curso');
         </script>
 
	     <div id="panel_resp_descripcion_curso"></div>

	  <!-- Final del div panel campo -->
	  </div>

	  <!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="panel_campo">
       <hr>  
	  <!-- Final del div panel campo -->
	  </div>
	  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
    	   <label>  se certificia a : </label> 
    	   
           <input type="text" class="form-control" name="se_certifica_curso" id="se_certifica_curso" placeholder="* se certifica " >
           <div id="panel_resp_se_certifica"></div>

        <!-- Final del div panel campo -->
       </div>

	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
    	   <label>  participo en el curso : </label> 
    	   
           <input type="text" class="form-control" name="participa_curso" id="participa_curso" placeholder="* participo en el curso " >
           <div id="panel_resp_participa_curso"></div>

        <!-- Final del div panel campo -->
       </div>
       
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
    	   <label> horas del curso : </label> 
    	   
           <input type="text" class="form-control" name="horas_curso" id="horas_curso" placeholder="* horas del curso " >
           <div id="panel_resp_horas_curso"></div>

        <!-- Final del div panel campo -->
       </div>

	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
    	   <label> fecha del evento : </label> 
    	   
           <input type="text" class="form-control" name="fecha_del_evento" id="fecha_del_evento" placeholder="* fecha del evento " >
           <div id="panel_resp_fecha_del_evento"></div>

        <!-- Final del div panel campo -->
       </div>
       
	  
	  

<!-- Final del formulario -->
</form>

<!-- Final del div row -->
</div>
       

