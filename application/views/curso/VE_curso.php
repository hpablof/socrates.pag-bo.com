
<div class="row">
<form id="formulario_ed" enctype="multipart/form-data">
    <?php 
    
         $portada_curso = $cursos[0]->portada_curso;
         $curso = $cursos[0]->curso;
         $descripcion_curso = $cursos[0]->descripcion_curso;
         $costo_curso = $cursos[0]->costo_curso;
         $fecha_curso = $cursos[0]->fecha_curso;
         $duracion_curso = $cursos[0]->duracion_curso;
         $certificado_curso = $cursos[0]->certificado_curso;
         $estado_curso = $cursos[0]->estado_curso;
         
         $se_certifica_curso = $cursos[0]-> se_certifica_curso;
         $participa_curso = $cursos[0]-> participa_curso;
         $horas_curso = $cursos[0]-> horas_curso;
         $fecha_del_evento = $cursos[0]-> fecha_del_evento;

    ?>
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	      <center>
              <label> portada curso </label></br> 
    
    	      <img id="img_resp_portada_curso" src="<?php echo base_url(); ?>assets/multimedia/curso/<?php echo $portada_curso; ?>" style="width: 80%; height: 300px; margin-bottom: 1.5%; border:1px solid #086A87;">
    	 
    	      <span class="btn btn-success btn-file"> 
    	      <span class="glyphicon glyphicon-picture"></span> Subir portada curso
    	      
    	      <input type="file" class="form-control" name="portada_curso" 
    	      id="portada_curso" onchange="previsualizar();">
    
    	      </span> 
	      </center>

	      <div id="panel_resp_portada_curso"></div>
	      
   <!-- ---------------------------------------------------------------------------------------------------------------------------------------- -->
	     
	      <center>
          <label> certificado curso </label></br> 

	      <img id="img_resp_certificado_curso" src="<?php echo base_url(); ?>assets/multimedia/certificado/<?php echo $certificado_curso; ?>" style="width: 80%; height: 300px; margin-bottom: 1.5%; border:1px solid #086A87;">
	 
	      <span class="btn btn-success btn-file"> 
	      <span class="glyphicon glyphicon-picture"></span> Subir certificado curso
	      
	      <input type="file" class="form-control" name="certificado_curso" 
	      id="certificado_curso" onchange="previsualizar_certificado();">

	      </span> </center>

	      <div id="panel_resp_certificado_curso"></div> 	      
	      
 
	  <!-- Final del div panel campo -->
	  </div>
	<!-- -------------------------------------------------------------------- -->
 
	  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
    	   <label> curso </label></br> 
           <input type="text" class="form-control" name="curso" 
           id="curso" maxlength="200" onkeyup="validador_campo('curso','panel_resp_curso','');" 
           onkeypress="return valida_letras(event);" placeholder="* Escriba su curso" value="<?php echo $curso; ?>" >
           <div id="panel_resp_curso"></div>

       <!-- Final del div panel campo -->
       </div>

  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
    	   <label> costo del curso </label></br> 
           <input type="text" class="form-control" name="costo_curso" 
           id="costo_curso" maxlength="200" onkeyup="validador_campo('costo_curso','panel_resp_costo_curso','');" 
           onkeypress="return valida_letras(event);" placeholder="* Escriba su costo_curso" value="<?php echo $costo_curso; ?>" >
           <div id="panel_resp_costo_curso"></div>

        <!-- Final del div panel campo -->
       </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
    	   <label> fecha del curso </label></br> 
           <input type="date" class="form-control" name="fecha_curso" 
           id="fecha_curso" maxlength="10" onkeyup="validador_campo('fecha_curso','panel_resp_fecha_curso','');" 
           onkeypress="return valida_ambos(event);" placeholder="* Escriba su fecha_curso" value="<?php echo $fecha_curso; ?>" >
           <div id="panel_resp_fecha_curso"></div>

        <!-- Final del div panel campo -->
       </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
    	   <label> duracion del curso </label></br> 
           <input type="text" class="form-control" name="duracion_curso" 
           id="duracion_curso" maxlength="200" onkeyup="validador_campo('duracion_curso','panel_resp_duracion_curso','');" 
           onkeypress="return valida_letras(event);" placeholder="* Escriba su duracion_curso" value="<?php echo $duracion_curso; ?>" >
           <div id="panel_resp_duracion_curso"></div>

        <!-- Final del div panel campo -->
       </div>
 
 
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
    	   <label> estado del curso : </label> 
    	   
    	   <?php if($estado_curso==0){ echo "<label> INACTIVO </label>"; } else { echo "<label> ACTIVO </label>"; } ?></br> 
     
           <select class="form-control" id="estado_curso" name="estado_curso">
               <option value="" > Seleccione </option>
               <option value="1" > ACTIVO </option>
               <option value="0" > INACTIVO </option>
           </select>       
           
           <div id="panel_resp_estado_curso"></div>

        <!-- Final del div panel campo -->
       </div>
       
 
	  <!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
         
         <label> descripción </label></br> 
	     <textarea class="form-control" name="descripcion_curso_ed" id="descripcion_curso_ed" ><?php echo $descripcion_curso; ?></textarea>
    
         <script>
          CKEDITOR.replace('descripcion_curso_ed');
         </script>
 
	     <div id="panel_resp_descripcion_curso_ed"></div>

	  <!-- Final del div panel campo -->
	  </div>

	  <!-- -------------------------------------------------------------------- -->

	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="panel_campo">
       <hr>  
	  <!-- Final del div panel campo -->
	  </div>
	  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
    	   <label>  se certificia a : </label> 
    	   
           <input type="text" class="form-control" name="se_certifica_curso" id="se_certifica_curso" value="<?php echo $se_certifica_curso; ?>" placeholder="* se certifica " >
           <div id="panel_resp_se_certifica"></div>

        <!-- Final del div panel campo -->
       </div>

	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
    	   <label>  participo en el curso : </label> 
    	   
           <input type="text" class="form-control" name="participa_curso" id="participa_curso" value="<?php echo $participa_curso; ?>" placeholder="* participo en el curso " >
           <div id="panel_resp_participa_curso"></div>

        <!-- Final del div panel campo -->
       </div>
       
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
    	   <label> horas del curso : </label> 
    	   
           <input type="text" class="form-control" name="horas_curso" id="horas_curso" value="<?php echo $horas_curso; ?>" placeholder="* horas del curso " >
           <div id="panel_resp_horas_curso"></div>

        <!-- Final del div panel campo -->
       </div>

	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
    	   <label> fecha del evento : </label> 
    	   
           <input type="text" class="form-control" name="fecha_del_evento" id="fecha_del_evento" value="<?php echo $fecha_del_evento; ?>" placeholder="* fecha del evento " >
           <div id="panel_resp_fecha_del_evento"></div>

        <!-- Final del div panel campo -->
       </div>
       
              
  
<!-- -------------------------------------------------------------------- -->
 
</form>
<!-- Final del div row -->
</div>
 

