<a href="javascript:void();" onclick="cargar_datos(1);"> Listar Datos  </a>
 
 <table class="table table-bordered table-condensed table-hover table-striped">
     <thead>
     
      <tr>
        <th class="hidden-lg hidden-md hidden-sm"> </th>
		<th> portada </th>
		<th> curso </th>
		<th> costo (Bs) </th>
		<th> fecha </th>
		<th> duracion </th>
		<th> certificado </th>
		<th> estado </th>
        <th class="hidden-xs"> </th>
      </tr>

     </thead>
     <tbody>
     <?php 
     foreach($cursos as $ob_curso)
     {
     	    $id_curso = $ob_curso->id_curso;
		    $portada_curso = $ob_curso->portada_curso;
		    $curso = $ob_curso->curso;
		    $descripcion_curso = $ob_curso->descripcion_curso;
		    $costo_curso = $ob_curso->costo_curso;
		    $fecha_curso = $ob_curso->fecha_curso;
		    $duracion_curso = $ob_curso->duracion_curso;
		    $certificado_curso = $ob_curso->certificado_curso;
		    $estado_curso = $ob_curso->estado_curso;
       ?>
       <tr>

        <td class="col-xs-1 hidden-lg hidden-md hidden-sm"> <center>
             <a href="#" onclick="btn_submenu_xs('<?php echo $id_curso; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones-xs" id="panel_opciones_xs<?php echo $id_curso; ?>">

              <div align="right" class="panel_cerrar_botones-xs" >
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu_xs('<?php echo $id_curso; ?>');"> X </buttom>
              </div>
              <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_examinar_curso('<?php echo $id_curso; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_editar_curso('<?php echo $id_curso; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line-xs">

               <button class="btn btn-danger btn-md" onclick="btn_borrar_curso('<?php echo $id_curso; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 

             </div>

          </center>
          </td>
          
		    <td> <img src="<?php echo base_url(); ?>assets/multimedia/curso/<?php echo $portada_curso; ?>" style="width:50px; height:50px;" > </td>
		    <td> <?php echo $curso; ?> </td>
		    <td> <?php echo $costo_curso; ?> </td>
		    <td> <?php echo $fecha_curso; ?> </td>
		    <td> <?php echo $duracion_curso; ?> </td>
		    
		    <td> <img src="<?php echo base_url(); ?>assets/multimedia/certificado/<?php echo $certificado_curso; ?>" style="width:50px; height:50px;" > </td>
            <td> <?php  if($estado_curso==0){ echo "<label style='color:red;'> INACTIVO </label>"; } else { echo "<label> ACTIVO </label>"; }  ?> </td>
            
            <td class="col-lg-1 hidden-xs"> <center>
             <a href="javascript:void();" onclick="btn_submenu('<?php echo $id_curso; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones" id="panel_opciones_<?php echo $id_curso; ?>">

              <div align="right" class="panel_cerrar_botones">
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu('<?php echo $id_curso; ?>');"> x </buttom>
              </div>
              <hr class="btn_line">

	   	         <button class="btn btn-default btn-xs" onclick="btn_examinar_curso('<?php echo $id_curso; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
	   	         examinar </button> 
	   	       
               <hr class="btn_line">

	   	         <button class="btn btn-default btn-xs" onclick="btn_editar_curso('<?php echo $id_curso; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
	   	           editar </button> 

               <hr class="btn_line">

	   	         <button class="btn btn-danger btn-xs" onclick="btn_borrar_curso('<?php echo $id_curso; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
	   	         borrar </button> 
             
             </div>

          </center>
	   	    </td>

       </tr>
     <?php
     } 
     ?>
     </tbody>
 
 <!-- Final del table -->
 </table>
 
 <?php require "paginacion_busqueda.php"; ?>

