

   <table class="table table-bordered table-condensed table-hover table-striped">
     <thead>
      <tr>
        <th class="hidden-lg hidden-md hidden-sm"> </th>
        <th> proveedor </th>
        <th> encargado </th>
        <th> telefonos </th>
        <th> celulares </th>
        <th> whatsapp </th>
        <th> direccion </th>
        <th> correo </th>
       <th class="hidden-xs"> </th>

      </tr>

     </thead>
     <tbody>
     <?php 
     foreach($proveedors as $ob_proveedor)
     {
        $id_proveedor = $ob_proveedor->id_proveedor;
        $proveedor = $ob_proveedor->proveedor;
        $encargado = $ob_proveedor->encargado;
        $telefonos = $ob_proveedor->telefonos;
        $celulares = $ob_proveedor->celulares;
        $whatsapp = $ob_proveedor->whatsapp;
        $direccion = $ob_proveedor->direccion;
        $correo = $ob_proveedor->correo;
     ?>
       <tr>

        <td class="col-xs-1 hidden-lg hidden-md hidden-sm"> <center>
             <a href="#" onclick="btn_submenu_xs('<?php echo $id_proveedor; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones-xs" id="panel_opciones_xs<?php echo $id_proveedor; ?>">

              <div align="right" class="panel_cerrar_botones-xs" >
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu_xs('<?php echo $id_proveedor; ?>');"> X </buttom>
              </div>
              <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_examinar_proveedor('<?php echo $id_proveedor; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_editar_proveedor('<?php echo $id_proveedor; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line-xs">

               <button class="btn btn-danger btn-md" onclick="btn_borrar_proveedor('<?php echo $id_proveedor; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 

             </div>

          </center>
          </td>
     
        <td> <?php echo $proveedor; ?> </td>
        <td> <?php echo $encargado; ?> </td>
        <td> <?php echo $telefonos; ?> </td>
        <td> <?php echo $celulares; ?> </td>
        <td> <?php echo $whatsapp; ?> </td>
        <td> <?php echo $direccion; ?> </td>
        <td> <?php echo $correo; ?> </td>
        <td class="col-lg-1 hidden-xs"> <center>
             <a href="#" onclick="btn_submenu('<?php echo $id_proveedor; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones" id="panel_opciones_<?php echo $id_proveedor; ?>">

              <div align="right" class="panel_cerrar_botones">
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu('<?php echo $id_proveedor; ?>');"> x </buttom>
              </div>
              <hr class="btn_line">

               <button class="btn btn-default btn-xs" onclick="btn_examinar_proveedor('<?php echo $id_proveedor; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line">

               <button class="btn btn-default btn-xs" onclick="btn_editar_proveedor('<?php echo $id_proveedor; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line">

               <button class="btn btn-danger btn-xs" onclick="btn_borrar_proveedor('<?php echo $id_proveedor; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 
             
             </div>

          </center>
          </td>

       </tr>
     <?php
     } 
     ?>
     </tbody>
 
 <!-- Final del table -->
 </table>
 <?php require "paginacion.php"; ?>

