

   <table class="table table-bordered table-condensed table-hover table-striped">
     <thead>
      <tr>
        <th class="hidden-lg hidden-md hidden-sm"> </th>
        <th> nit </th>
        <th> razon social </th>
       <th class="hidden-xs"> </th>

      </tr>

     </thead>
     <tbody>
     <?php 
     foreach($razon_socials as $ob_razon_social)
     {
        $id_razon_social = $ob_razon_social->id_razon_social;
        $nit = $ob_razon_social->nit;
        $razon_social = $ob_razon_social->razon_social;
     ?>
       <tr>

        <td class="col-xs-1 hidden-lg hidden-md hidden-sm"> <center>
             <a href="#" onclick="btn_submenu_xs('<?php echo $id_razon_social; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones-xs" id="panel_opciones_xs<?php echo $id_razon_social; ?>">

              <div align="right" class="panel_cerrar_botones-xs" >
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu_xs('<?php echo $id_razon_social; ?>');"> X </buttom>
              </div>
              <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_examinar_razon_social('<?php echo $id_razon_social; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_editar_razon_social('<?php echo $id_razon_social; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line-xs">

               <button class="btn btn-danger btn-md" onclick="btn_borrar_razon_social('<?php echo $id_razon_social; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 

             </div>

          </center>
          </td>
     
        <td> <?php echo $nit; ?> </td>
        <td> <?php echo $razon_social; ?> </td>
        <td class="col-lg-1 hidden-xs"> <center>
             <a href="#" onclick="btn_submenu('<?php echo $id_razon_social; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones" id="panel_opciones_<?php echo $id_razon_social; ?>">

              <div align="right" class="panel_cerrar_botones">
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu('<?php echo $id_razon_social; ?>');"> x </buttom>
              </div>
              <hr class="btn_line">

               <button class="btn btn-default btn-xs" onclick="btn_examinar_razon_social('<?php echo $id_razon_social; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line">

               <button class="btn btn-default btn-xs" onclick="btn_editar_razon_social('<?php echo $id_razon_social; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line">

               <button class="btn btn-danger btn-xs" onclick="btn_borrar_razon_social('<?php echo $id_razon_social; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 
             
             </div>

          </center>
          </td>

       </tr>
     <?php
     } 
     ?>
     </tbody>
 
 <!-- Final del table -->
 </table>
 <?php require "paginacion.php"; ?>

