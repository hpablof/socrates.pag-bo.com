<a href="javascript:void();" onclick="cargar_datos(1);"> Listar Datos  </a>
 
 <table class="table table-bordered table-condensed table-hover table-striped">
     <thead>
      <tr>
        <th class="hidden-lg hidden-md hidden-sm"> </th>
        <th> # </th>
        <th> cliente </th>
        <th> ci nit </th>
        <th> celular </th>
        <th> whatsapp </th>
        <th> direccion </th>
        <th> correo </th>
        <th class="hidden-xs"> </th>
      </tr>

     </thead>
     <tbody>
     <?php 
     foreach($clientes as $ob_cliente)
     {
        $id_cliente = $ob_cliente->id_cliente;
        $cliente = $ob_cliente->cliente;
        $ci_nit = $ob_cliente->ci_nit;
        $celular_cl = $ob_cliente->celular_cl;
        $whatsapp_cl = $ob_cliente->whatsapp_cl;
        $direccion_cl = $ob_cliente->direccion_cl;
        $correo_cl = $ob_cliente->correo_cl;
     ?>
       <tr>

        <td class="col-xs-1 hidden-lg hidden-md hidden-sm"> <center>
             <a href="#" onclick="btn_submenu_xs('<?php echo $id_cliente; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones-xs" id="panel_opciones_xs<?php echo $id_cliente; ?>">

              <div align="right" class="panel_cerrar_botones-xs" >
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu_xs('<?php echo $id_cliente; ?>');"> X </buttom>
              </div>
              <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_examinar_cliente('<?php echo $id_cliente; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_editar_cliente('<?php echo $id_cliente; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line-xs">

               <button class="btn btn-danger btn-md" onclick="btn_borrar_cliente('<?php echo $id_cliente; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 

             </div>

          </center>
        </td>
        
        <td> <?php echo $id_cliente; ?> </td>
        <td> <?php echo $cliente; ?> </td>
        <td> <?php echo $ci_nit; ?> </td>
        <td> <?php echo $celular_cl; ?> </td>
        <td> <?php echo $whatsapp_cl; ?> </td>
        <td> <?php echo $direccion_cl; ?> </td>
        <td> <?php echo $correo_cl; ?> </td>
        
        <td class="col-lg-1 hidden-xs"> <center>
             <a href="#" onclick="btn_submenu('<?php echo $id_cliente; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones" id="panel_opciones_<?php echo $id_cliente; ?>">

              <div align="right" class="panel_cerrar_botones">
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu('<?php echo $id_cliente; ?>');"> x </buttom>
              </div>
              <hr class="btn_line">

               <button class="btn btn-default btn-xs" onclick="btn_examinar_cliente('<?php echo $id_cliente; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line">

               <button class="btn btn-default btn-xs" onclick="btn_editar_cliente('<?php echo $id_cliente; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line">

               <button class="btn btn-danger btn-xs" onclick="btn_borrar_cliente('<?php echo $id_cliente; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 
             
             </div>

          </center>
          </td>

       </tr>
     <?php
     } 
     ?>
     </tbody>
 
 <!-- Final del table -->
 </table>
 
 <?php require "paginacion_busqueda.php"; ?>

