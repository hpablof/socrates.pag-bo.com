
<div class="row">
<form id="formulario" enctype="multipart/form-data">

 <?php 
         $cliente = $clientes[0]->cliente;
         $ci_nit = $clientes[0]->ci_nit;
         $celular_cl = $clientes[0]->celular_cl;
         $whatsapp_cl = $clientes[0]->whatsapp_cl;
         $direccion_cl = $clientes[0]->direccion_cl;
         $correo_cl = $clientes[0]->correo_cl;
         ?>
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
    	   <label> cliente </label></br> 
           <input type="text" class="form-control" name="cliente" 
           id="cliente" maxlength="200" onkeyup="validador_campo('cliente','panel_resp_cliente','');" 
           onkeypress="return valida_letras(event);" placeholder="* Escriba su cliente" value="<?php echo $cliente; ?>" >
           <div id="panel_resp_cliente"></div>

        <!-- Final del div panel campo -->
       </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
    	   <label> ci/nit </label></br> 
           <input type="text" class="form-control" name="ci_nit" 
           id="ci_nit" maxlength="200" onkeyup="validador_campo('ci_nit','panel_resp_ci_nit','');" 
           onkeypress="return valida_letras(event);" placeholder="* Escriba su ci_nit" value="<?php echo $ci_nit; ?>" >
           <div id="panel_resp_ci_nit"></div>

        <!-- Final del div panel campo -->
       </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
    	   <label> celular  </label></br> 
           <input type="text" class="form-control" name="celular_cl" 
           id="celular_cl" maxlength="200" onkeyup="validador_campo('celular_cl','panel_resp_celular_cl','');" 
           onkeypress="return valida_letras(event);" placeholder="* Escriba su celular_cl" value="<?php echo $celular_cl; ?>" >
           <div id="panel_resp_celular_cl"></div>

        <!-- Final del div panel campo -->
       </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
    	   <label> whatsapp  </label></br> 
           <input type="text" class="form-control" name="whatsapp_cl" 
           id="whatsapp_cl" maxlength="200" onkeyup="validador_campo('whatsapp_cl','panel_resp_whatsapp_cl','');" 
           onkeypress="return valida_letras(event);" placeholder="* Escriba su whatsapp_cl" value="<?php echo $whatsapp_cl; ?>" >
           
           <div id="panel_resp_whatsapp_cl"></div>

        <!-- Final del div panel campo -->
       </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
    	   <label> dirección </label></br> 

           <input type="text" class="form-control" name="direccion_cl" 
           id="direccion_cl" maxlength="200" onkeyup="validador_campo('direccion_cl','panel_resp_direccion_cl','');" 
           onkeypress="return valida_letras(event);" placeholder="* Escriba su direccion_cl" value="<?php echo $direccion_cl; ?>" >
           
           <div id="panel_resp_direccion_cl"></div>

        <!-- Final del div panel campo -->
       </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	    <label> correo </label></br> 
        <input type="text" class="form-control" name="correo_cl" 
        id="correo_cl" maxlength="200" onkeyup="validador_campo('correo_cl','panel_resp_correo_cl','');" 
        onkeypress="return valida_letras(event);" placeholder="* Escriba su correo_cl" value="<?php echo $correo_cl; ?>" >
       
        <div id="panel_resp_correo_cl"></div>

      <!-- Final del div panel campo -->
      </div>
 
  
<!-- -------------------------------------------------------------------- -->
 
</form>
<!-- Final del div row -->
</div>
 

