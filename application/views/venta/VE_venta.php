<div class="row" style="margin: 0px; padding: 0px;">
<?php

  $id_venta = $ventas[0]-> id_venta;
  $codigo_venta = $ventas[0]-> codigo_venta;
  $cliente = $ventas[0]-> cliente; 
  $ci_nit = $ventas[0]-> ci_nit;

  $total_venta = $ventas[0]-> total_venta;  
  $pago_venta = $ventas[0]-> pago_venta;  
  $cambio_venta = $ventas[0]-> cambio_venta;  
  
  $fecha_venta = $ventas[0]-> fecha_venta; 
  $hora_venta = $ventas[0]-> hora_venta; 
?>
 
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 1%; margin-top: 0px; font-family: arial;">
 
<h2 align="center" style="margin-bottom: 0px; font-weight: bold; margin-top: 0px;"> RECIBO DE VENTA N° <?php echo $id_venta; ?></h2>
<p align="center" style="margin-top: 0px; font-size: 11px; "> Comprobante de venta de productos para el consumidor </p>

<h4 align="center" style="margin-bottom: 0px; font-weight: bold;"> Codigo : <?php echo $codigo_venta; ?></h4> 

<table style="width: 100%; font-size: 12px;">

<tr>
  <td> Cliente </td>
  <td style="font-weight: bold;"> <?php echo $cliente; ?></td>
  <td> Ci/Nit </td>
  <td style="font-weight: bold;"> <?php echo $ci_nit; ?></td> 
</tr>

<hr>

<tr>
  <td> Fecha </td>
  <td> <?php echo $fecha_venta; ?></td>
  <td> Hora </td>
  <td> <?php echo $hora_venta; ?></td>
</tr>

</table>  

<hr>

<br>

<table style="width: 100%; font-size: 12px;">
  <tr>
    <th align="left"> prod </th>
    <th align="left"> cant </th>
    <th align="left"> p. venta </th>
    <th align="left"> sub-total </th>
  </tr>
<?php
  foreach ($ventas as $dv) 
  {
     $producto = $dv-> producto;
     $cantidad_dv = $dv-> cantidad_dv; 
     $precio_venta_dv = $dv-> precio_venta_dv; 
     $subtotal_dv = $dv-> subtotal_dv; 
  ?>
  <tr>
    <td> <?php echo $producto; ?> </td>
    <td> <?php echo $cantidad_dv; echo " uds. ";?> </td>
    <td> <?php echo $precio_venta_dv; ?> </td>
    <td> <?php echo $subtotal_dv; ?> </td>
  </tr>
  <?php   
  }

?>
</table>
<br> 
<hr>
</br>

<table style="width: 100%;  font-size: 12px;">

<tr>
  <th align="left"> Total  </th>
  <td colspan="2"> <?php echo $total_venta; echo " Bs.";?></td>
 
  <th align="left"> Pago  </th>
  <td colspan="2"> <?php echo $pago_venta; echo " Bs.";?></td>
 
  <th align="left"> Cambio  </th>
  <td colspan="2"> <?php echo $cambio_venta; echo " Bs.";?></td>
</tr>
</table>
<hr>
<br> </br>

</div>
<!-- -------------------------------------------------------------------- -->
 
<!-- Final del div row -->
</div>
 

