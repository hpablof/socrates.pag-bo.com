<div class="row">
 
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel">
  <div class="row">

	<!-- -------------------------------------------------------------------- -->
	    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="panel_campo">
 
        <table style="width: 100%;">
          <tr>
            <td style="width: 80%;"> <label> cliente  </label> </td>
            <td align="right"> 
                <button class="btn btn-info btn-md" onclick="btn_modal_cliente();"> + cliente </button>
            </td>
          </tr>
        </table>

        <input type="hidden" name="cliente" id="cliente" >
        <input type="hidden" name="id_cliente" id="id_cliente" >
        
        <input type="text" class="form-control" name="txt_buscar_cliente" 
         id="txt_buscar_cliente" maxlength="100" onkeyup="btn_buscar_cliente_venta();" 
         placeholder="* Busque al Cliente" autocomplete="off">
        
        <div id="panel_resp_cliente"></div>
        </br>
    <!-- Final del div panel campo -->
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        
        <label> productos </label>

        <input type="hidden" name="id_producto" id="id_producto">
        <input type="hidden" name="producto" id="producto">

        <input type="text" class="form-control" name="txt_buscar_producto" id="txt_buscar_producto" placeholder="* Buscar Producto" onkeyup="btn_buscar_productos_venta();" autocomplete="off" >
        
        <div id="panel_resp_producto"></div>
        </br>
    
    </div>

	<!-- -------------------------------------------------------------------- -->
	    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6" id="panel_campo">
        
	     <label> cantidad </label></br> 
       
       <input type="text" class="form-control" name="cantidad_venta" 
        id="cantidad_venta" maxlength="10" placeholder="0" onkeyup="btn_subtotal_venta();">
        
        <div id="panel_resp_cantidad_venta"></div>

      <!-- Final del div panel campo -->
      </div>

  
	<!-- -------------------------------------------------------------------- -->
	    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6" id="panel_campo">
        
	     <label> precio venta </label></br> 
       <input type="text" class="form-control" name="precio_venta" 
        id="precio_venta" maxlength="10" placeholder="0.00" disabled>
     
        <div id="panel_resp_precio_venta_"></div>

       <!-- Final del div panel campo -->
      </div>
  
	<!-- -------------------------------------------------------------------- -->
	    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6" id="panel_campo">
        
	     <label> subtotal </label></br> 
       <input type="text" class="form-control" name="subtotal_venta" 
       id="subtotal_venta" maxlength="10" disabled  placeholder="0.00">
     
       <div id="panel_resp_subtotal_venta"></div>

      <!-- Final del div panel campo -->
      </div>
  



    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      </br>
      <center> 
       <button class="btn btn-default" onclick="btn_agregar_carrito_detalle_venta();" > Agregar </button> 
      </center>
    </div>

<!-- -------------------------------------------------------------------- -->

  <!-- Final del div row panel -->
  </div>

<!-- -------------------------------------------------------------------- -->  

 <!-- Final del div panel -->
 </div>

<!-- -------------------------------------------------------------------- -->

 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_listado">
    <h4 align="center" > LISTADO DE PRODUCTOS A VENDER </h4>
    <div id="panel_listado_venta" style="overflow-y:scroll; max-height:350px;" >
      
    </div>
    <table class="table table-bordered">
      <tr>
        
        <td> <input type="text" class="form-control" name="total_venta" id="total_venta" placeholder="0.00" disabled> </td>
        
        <td> <input type="text" class="form-control" name="pago_venta" id="pago_venta" placeholder="0.00" onkeyup="btn_pago_venta();"> </td>
        
        <td> <input type="text" class="form-control" name="cambio_venta" id="cambio_venta" placeholder="0.00" disabled> </td>

      </tr>
    </table>
 </div>

 <!-- Final del div panel listado -->
 </div>

<!-- Final del div row -->
</div>
       

