<?php

if($opcion=="listar_clientes")
{
?>
<div style="width: 96%; max-height: 350px; overflow-y: auto; position: absolute; background: white; z-index: 2; padding: 1%; border:1px solid #bdc3c7; margin-top: 0.5%; ">
   
   <table style="width: 100%;">
   	<tr>
   		<td> Resultados para "<label> <?php echo $txt_buscar; ?> </label>" </td>
   		<td align="right"> <button class="btn btn-danger btn-md" onclick="btn_cerrar_buscador_cliente();"> Cerrar </button></td>
   	</tr>
   </table>

   <table class="table table-bordered table-condensed table-hover table-striped">
     <thead>
      <tr>
		<th> cliente </th>
		<th> ci/nit </th>
		<th> celular </th>
		<th> whatsapp </th>
		<th> direccion </th>
      </tr>

     </thead>
     
     <tbody>
     <?php 
     foreach($clientes as $ob_cliente)
     {
 	    $id_cliente = $ob_cliente->id_cliente;
	    $cliente = $ob_cliente->cliente;
	    $ci_nit = $ob_cliente->ci_nit;
	    $celular_cl = $ob_cliente->celular_cl;
	    $whatsapp_cl = $ob_cliente->whatsapp_cl;
	    $direccion_cl = $ob_cliente->direccion_cl;
	    $correo_cl = $ob_cliente->correo_cl;
     ?>
     <tr style="cursor: pointer;" onclick="btn_select_cliente_venta('<?php echo $id_cliente; ?>', '<?php echo $cliente; ?>');">

	    <td> <?php echo $cliente;      ?> </td>
	    <td> <?php echo $ci_nit;       ?> </td>
	    <td> <?php echo $celular_cl;   ?> </td>
	    <td> <?php echo $whatsapp_cl;  ?> </td>
	    <td> <?php echo $direccion_cl; ?> </td>

     </tr>
     <?php
     } 
     ?>
     </tbody>
 
   <!-- Final del table -->
   </table>

</div>

<?php 
}

if($opcion=="listar_productos")
{
?>
<div style="width: 96%; max-height: 350px; overflow-y: auto; position: absolute; background: white; z-index: 2; padding: 1%; border:1px solid #bdc3c7; margin-top: 0.5%; ">
   
   <table style="width: 100%;">
   	<tr>
   		<td> Resultados para "<label> <?php echo $txt_buscar; ?> </label>" </td>
   		<td align="right"> <button class="btn btn-danger btn-md" onclick="btn_cerrar_buscador_productos();"> Cerrar </button></td>
   	</tr>
   </table>

   <table class="table table-bordered table-condensed table-hover table-striped">
     
     <thead>
      
      <tr>
    		<th> portada </th>
        <th> producto </th>
        <th> area </th>
    		<th> cod barras </th>
    		<th> <center> cantidad </center> </th>
    		<th> <center> p. compra </center> </th>
    		<th> <center> p. venta </center> </th>
      </tr>

     </thead>

     <tbody>
     <?php 
     foreach($productos as $ob_producto)
     {
     	  $id_producto = $ob_producto->id_producto;
		    $producto = $ob_producto->producto;
		    $portada = $ob_producto->portada;
		    $codigo_bar = $ob_producto->codigo_bar;
		    $cantidad = $ob_producto->cantidad;
		    $precio_compra = $ob_producto->precio_compra;
		    $precio_venta = $ob_producto->precio_venta;
		    $descripcion = $ob_producto->descripcion;
		    $area = $ob_producto->area;
     ?>
       <tr style="cursor:pointer;" onclick="btn_select_producto('<?php echo $id_producto; ?>','<?php echo $producto; ?>','<?php echo $precio_compra; ?>','<?php echo $precio_venta; ?>');" >
  
		<td style="width: 60px; text-align: center;"> <img src="<?php echo base_url(); ?>assets/multimedia/portadas/<?php echo $portada; ?>" style="width:50px; height:50px;" > </td>
        <td> <?php echo $producto; ?> </td>
        <td> <label> <?php echo $area; ?> <label> </td>
		<td> <?php echo $codigo_bar; ?> </td>
		<td align="center" > <label> <?php echo $cantidad; ?> </label></td>
		<td align="center" > <?php echo $precio_compra; ?> </td>
		<td align="center" > <?php echo $precio_venta; ?> </td>
 
       </tr>
     <?php
     } 
     ?>
     </tbody>
 
 <!-- Final del table -->
 </table>

</div> 
<?php

}


if($opcion=="recibo_venta")
{

  $id_venta = $ventas[0]-> id_venta;
  $codigo_venta = $ventas[0]-> codigo_venta;
  $cliente = $ventas[0]-> cliente; 
  $ci_nit = $ventas[0]-> ci_nit;

  $total_venta = $ventas[0]-> total_venta;  
  $pago_venta = $ventas[0]-> pago_venta;  
  $cambio_venta = $ventas[0]-> cambio_venta;  
  
  $fecha_venta = $ventas[0]-> fecha_venta; 
  $hora_venta = $ventas[0]-> hora_venta; 
?>

<div style="width: 33.3%; float: left; padding: 1%;">
	
</div>

<div style="width: 33.3%; float: left; border: 1px solid black; padding: 1%; font-family: arial;">
 
<h2 align="center" style="margin-bottom: 0px; font-weight: bold;"> RECIBO DE VENTA N° <?php echo $id_venta; ?></h2>
<p align="center" style="margin-top: 0px; font-size: 11px; "> Comprobante de venta de productos para el consumidor </p>

<h4 align="center" style="margin-bottom: 0px; font-weight: bold;"> Codigo : <?php echo $codigo_venta; ?></h4>	

<table style="width: 100%; font-size: 12px;">

<tr>
	<td> Cliente </td>
	<td style="font-weight: bold;"> <?php echo $cliente; ?></td>
	<td> Ci/Nit </td>
	<td style="font-weight: bold;"> <?php echo $ci_nit; ?></td>	
</tr>

<hr>

<tr>
	<td> Fecha </td>
	<td> <?php echo $fecha_venta; ?></td>
	<td> Hora </td>
	<td> <?php echo $hora_venta; ?></td>
</tr>

</table>	

<hr>

<br>

<table style="width: 100%; font-size: 12px;">
	<tr>
		<th align="left"> prod </th>
		<th align="left"> cant </th>
		<th align="left"> p. venta </th>
		<th align="left"> sub-total </th>
	</tr>
<?php
  foreach ($ventas as $dv) 
  {
     $producto = $dv-> producto;
     $cantidad_dv = $dv-> cantidad_dv; 
     $precio_venta_dv = $dv-> precio_venta_dv; 
     $subtotal_dv = $dv-> subtotal_dv; 
  ?>
  <tr>
  	<td> <?php echo $producto; ?> </td>
  	<td> <?php echo $cantidad_dv; echo " uds. ";?> </td>
  	<td> <?php echo $precio_venta_dv; ?> </td>
  	<td> <?php echo $subtotal_dv; ?> </td>
  </tr>
  <?php	  
  }

?>
</table>
<br> 
<hr>
</br>

<table style="width: 100%;  font-size: 12px;">

<tr>
	<th align="left"> Total  </th>
	<td colspan="2"> <?php echo $total_venta; echo " Bs.";?></td>
 
	<th align="left"> Pago  </th>
	<td colspan="2"> <?php echo $pago_venta; echo " Bs.";?></td>
 
	<th align="left"> Cambio  </th>
	<td colspan="2"> <?php echo $cambio_venta; echo " Bs.";?></td>
</tr>
</table>
<hr>
<br> </br>

</div>

<script type="text/javascript">
	print();
</script>

<?php
}

if($opcion=="mostrar_cliente")
{
?>
  <div class="row">

  <!-- -------------------------------------------------------------------- -->
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
       <label> cliente </label></br> 
       <input type="text" class="form-control" name="cliente_reg" 
       id="cliente_reg" maxlength="200" onkeyup="validador_campo('cliente','panel_resp_cliente','');" 
       onkeypress="return valida_letras(event);" placeholder="* Nombre Completo">
     
       <div id="panel_resp_cliente"></div>

      <!-- Final del div panel campo -->
      </div>
  
  <!-- -------------------------------------------------------------------- -->
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
       <label> ci/nit </label></br> 
       <input type="text" class="form-control" name="ci_nit" 
       id="ci_nit" maxlength="200" onkeyup="validador_campo('ci_nit','panel_resp_ci_nit','');" 
       onkeypress="return valida_letras(event);" placeholder="* Ci/Nit">
     
       <div id="panel_resp_ci_nit"></div>

      <!-- Final del div panel campo -->
      </div>
  
  <!-- -------------------------------------------------------------------- -->
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
       <label> celular </label></br> 
       <input type="text" class="form-control" name="celular_cl" 
       id="celular_cl" maxlength="20" onkeyup="validador_campo('celular_cl','panel_resp_celular_cl','');" 
       onkeypress="return valida_letras(event);" placeholder="* Celular">
     
       <div id="panel_resp_celular_cl"></div>

      <!-- Final del div panel campo -->
      </div>
  
  <!-- -------------------------------------------------------------------- -->
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
       <label> whatsapp </label></br> 
       <input type="text" class="form-control" name="whatsapp_cl" 
       id="whatsapp_cl" maxlength="200" onkeyup="validador_campo('whatsapp_cl','panel_resp_whatsapp_cl','');" 
       onkeypress="return valida_letras(event);" placeholder="* Whatsapp">
     
       <div id="panel_resp_whatsapp_cl"></div>

      <!-- Final del div panel campo -->
      </div>
  
  <!-- -------------------------------------------------------------------- -->
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
       <label> direccion </label></br> 
       <input type="text" class="form-control" name="direccion_cl" 
       id="direccion_cl" maxlength="200" onkeyup="validador_campo('direccion_cl','panel_resp_direccion_cl','');" 
       onkeypress="return valida_letras(event);" placeholder="* Dirección">
     
       <div id="panel_resp_direccion_cl"></div>

      <!-- Final del div panel campo -->
      </div>
  
  <!-- -------------------------------------------------------------------- -->
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
       <label> correo </label></br> 
       <input type="text" class="form-control" name="correo_cl" 
       id="correo_cl" maxlength="200" onkeyup="validador_campo('correo_cl','panel_resp_correo_cl','');" 
       onkeypress="return valida_letras(event);" placeholder="* Correo">
     
       <div id="panel_resp_correo_cl"></div>

      <!-- Final del div panel campo -->
      </div>
 
<!-- -------------------------------------------------------------------- -->

  <!-- Final del div row panel -->
  </div>


<?php  
}

?>