<a href="javascript:void();" onclick="cargar_datos(1);"> Listar Datos  </a>

   <table class="table table-bordered table-condensed table-hover table-striped">
     <thead>
      
      <tr>
        <th class="hidden-lg hidden-md hidden-sm"> </th>
        <th> # </th>
        <th> cliente </th>
        <th> total venta </th>
        <th> pago venta </th>
        <th> cambio venta </th>
        <th> fecha </th>
        <th> hora </th>
        <th class="hidden-xs"> </th>
      </tr>

     </thead>
     <tbody>
     <?php 
     foreach($ventas as $ob_venta)
     {
        $id_venta = $ob_venta->id_venta;
        $codigo_venta = $ob_venta->codigo_venta;
        $total_venta = $ob_venta->total_venta;
        $pago_venta = $ob_venta->pago_venta;
        $cambio_venta = $ob_venta->cambio_venta;
        $fecha_venta = $ob_venta->fecha_venta;
        $hora_venta = $ob_venta->hora_venta;

        $id_cliente = $ob_venta->id_cliente;
        $cliente = $ob_venta->cliente; 

     ?>
       <tr>

        <td class="col-xs-1 hidden-lg hidden-md hidden-sm"> <center>
             <a href="#" onclick="btn_submenu_xs('<?php echo $id_venta; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones-xs" id="panel_opciones_xs<?php echo $id_venta; ?>">

              <div align="right" class="panel_cerrar_botones-xs" >
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu_xs('<?php echo $id_venta; ?>');"> X </buttom>
              </div>
              <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_examinar_venta('<?php echo $id_venta; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_editar_venta('<?php echo $id_venta; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line-xs">

               <button class="btn btn-danger btn-md" onclick="btn_borrar_venta('<?php echo $id_venta; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 

             </div>

          </center>
          </td>
        <td> <?php echo $id_venta; ?> </td>
        <td> <?php echo $cliente; ?> </td>
        <td> <?php echo $total_venta; ?> </td>
        <td> <?php echo $pago_venta; ?> </td>
        <td> <?php echo $cambio_venta; ?> </td>

        <td> <?php echo $fecha_venta; ?> </td>
        <td> <?php echo $hora_venta; ?> </td>
        
        <td class="col-lg-1 hidden-xs"> <center>
             <a href="#" onclick="btn_submenu('<?php echo $id_venta; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones" id="panel_opciones_<?php echo $id_venta; ?>">

              <div align="right" class="panel_cerrar_botones">
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu('<?php echo $id_venta; ?>');"> x </buttom>
              </div>
              <hr class="btn_line">

               <button class="btn btn-default btn-xs" onclick="btn_examinar_venta('<?php echo $id_venta; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line">

               <button class="btn btn-default btn-xs" onclick="btn_editar_venta('<?php echo $id_venta; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line">

               <button class="btn btn-danger btn-xs" onclick="btn_borrar_venta('<?php echo $codigo_venta; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 
             
             </div>

          </center>
          </td>

       </tr>
     <?php
     } 
     ?>
     </tbody>
 
 <!-- Final del table -->
 </table>


<?php require "paginacion_busqueda.php"; ?>

