<?php

if($opcion=="listar_proveedor")
{	 
?>
<div style="width: 96%; max-height: 350px; overflow-y: auto; position: absolute; background: white; z-index: 2; padding: 1%; border:1px solid #bdc3c7; margin-top: 0.5%; ">
   
   <table style="width: 100%;">
   	<tr>
   		<td> Resultados para "<label> <?php echo $txt_buscar; ?> </label>" </td>
   		<td align="right"> <button class="btn btn-danger btn-md" onclick="btn_cerrar_buscador_proveedor();"> Cerrar </button></td>
   	</tr>
   </table>

   <table class="table table-bordered table-condensed table-hover table-striped">
     <thead>
      <tr>
        <th> proveedor </th>
        <th> encargado </th>
        <th> whatsapp </th>
        <th> celulares </th>
      </tr>

     </thead>
     <tbody>
     <?php 
     foreach($proveedors as $ob_proveedor)
     {
 	    $id_proveedor = $ob_proveedor->id_proveedor;
	    $proveedor = $ob_proveedor->proveedor;
	    $encargado = $ob_proveedor->encargado;
	    $telefonos = $ob_proveedor->telefonos;
	    $celulares = $ob_proveedor->celulares;
	    $whatsapp = $ob_proveedor->whatsapp;
	    $direccion = $ob_proveedor->direccion;
	    $correo = $ob_proveedor->correo;
     ?>
       <tr style="cursor:pointer;" onclick="btn_select_proveedor('<?php echo $id_proveedor; ?>','<?php echo $proveedor; ?>');">

	    <td> <?php echo $proveedor; ?> </td>
	    <td> <?php echo $encargado; ?> </td>
	    <td> <?php echo $whatsapp;  ?> </td>
	    <td> <?php echo $celulares; ?> </td>

       </tr>
     <?php
     } 
     ?>
     </tbody>
 
 <!-- Final del table -->
 </table>

</div>
<?php
}

if($opcion=="listar_productos")
{
?>
<div style="width: 96%; max-height: 350px; overflow-y: auto; position: absolute; background: white; z-index: 2; padding: 1%; border:1px solid #bdc3c7; margin-top: 0.5%; ">
   
   <table style="width: 100%;">
   	<tr>
   		<td> Resultados para "<label> <?php echo $txt_buscar; ?> </label>" </td>
   		<td align="right"> <button class="btn btn-danger btn-md" onclick="btn_cerrar_buscador_productos();"> Cerrar </button></td>
   	</tr>
   </table>

   <table class="table table-bordered table-condensed table-hover table-striped">
     
     <thead>
      
      <tr>
		<th> portada </th>
        <th> producto </th>
        <th> area </th>
		<th> cod barras </th>
		<th> <center> cantidad </center> </th>
		<th> <center> p. compra </center> </th>
		<th> <center> p. venta </center> </th>
      </tr>

     </thead>

     <tbody>
     <?php 
     foreach($productos as $ob_producto)
     {
     	    $id_producto = $ob_producto->id_producto;
		    $producto = $ob_producto->producto;
		    $portada = $ob_producto->portada;
		    $codigo_bar = $ob_producto->codigo_bar;
		    $cantidad = $ob_producto->cantidad;
		    $precio_compra = $ob_producto->precio_compra;
		    $precio_venta = $ob_producto->precio_venta;
		    $descripcion = $ob_producto->descripcion;
		    $area = $ob_producto->area;
     ?>
       <tr style="cursor:pointer;" onclick="btn_select_producto('<?php echo $id_producto; ?>','<?php echo $producto; ?>','<?php echo $precio_compra; ?>','<?php echo $precio_venta; ?>');" >
  
		<td style="width: 60px; text-align: center;"> <img src="<?php echo base_url(); ?>assets/multimedia/portadas/<?php echo $portada; ?>" style="width:50px; height:50px;" > </td>
        <td> <?php echo $producto; ?> </td>
        <td> <label> <?php echo $area; ?> <label> </td>
		<td> <?php echo $codigo_bar; ?> </td>
		<td align="center" > <label> <?php echo $cantidad; ?> </label></td>
		<td align="center" > <?php echo $precio_compra; ?> </td>
		<td align="center" > <?php echo $precio_venta; ?> </td>
 
       </tr>
     <?php
     } 
     ?>
     </tbody>
 
 <!-- Final del table -->
 </table>

</div> 
<?php

}

// ----------------------------------------------------------------------------------------------------

if($opcion=="mostrar_proveedor")
{  
?>
 
  <div class="row">

  <!-- -------------------------------------------------------------------- -->
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="panel_campo">
        
      <label> proveedor </label></br> 
      <input type="text" class="form-control" name="proveedor_reg" 
       id="proveedor_reg" maxlength="200" onkeyup="validador_campo('proveedor','panel_resp_proveedor','');" 
       onkeypress="return valida_letras(event);" placeholder="* Proveedor">
     
       <div id="panel_resp_proveedor"></div>

      <!-- Final del div panel campo -->
      </div>
  
  <!-- -------------------------------------------------------------------- -->
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
     <label> encargado </label></br> 
      <input type="text" class="form-control" name="encargado" 
       id="encargado" maxlength="200" onkeyup="validador_campo('encargado','panel_resp_encargado','');" 
       onkeypress="return valida_letras(event);" placeholder="* Encargado">
     
       <div id="panel_resp_encargado"></div>

      <!-- Final del div panel campo -->
      </div>
  
  <!-- -------------------------------------------------------------------- -->
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
     <label> telefonos </label></br> 
      <input type="text" class="form-control" name="telefonos" 
       id="telefonos" maxlength="200" onkeyup="validador_campo('telefonos','panel_resp_telefonos','');" 
       onkeypress="return valida_letras(event);" placeholder="* Telefonos">
     
       <div id="panel_resp_telefonos"></div>

      <!-- Final del div panel campo -->
      </div>
  
  <!-- -------------------------------------------------------------------- -->
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
     <label> celulares </label></br> 
      <input type="text" class="form-control" name="celulares" 
       id="celulares" maxlength="200" onkeyup="validador_campo('celulares','panel_resp_celulares','');" 
       onkeypress="return valida_letras(event);" placeholder="* Celulares">
     
       <div id="panel_resp_celulares"></div>

      <!-- Final del div panel campo -->
      </div>
  
  <!-- -------------------------------------------------------------------- -->
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        
     <label> whatsapp </label></br> 
      <input type="text" class="form-control" name="whatsapp" 
       id="whatsapp" maxlength="200" onkeyup="validador_campo('whatsapp','panel_resp_whatsapp','');" 
       onkeypress="return valida_letras(event);" placeholder="* Whatsapp">
     
       <div id="panel_resp_whatsapp"></div>

      <!-- Final del div panel campo -->
      </div>
  
  <!-- -------------------------------------------------------------------- -->
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="panel_campo">
        
      <label> direccion </label></br> 
      <input type="text" class="form-control" name="direccion" 
       id="direccion" maxlength="200" onkeyup="validador_campo('direccion','panel_resp_direccion','');" 
       onkeypress="return valida_letras(event);" placeholder="* Direccion">
     
       <div id="panel_resp_direccion"></div>

      <!-- Final del div panel campo -->
      </div>
  
  <!-- -------------------------------------------------------------------- -->
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="panel_campo">
        
      <label> correo </label></br> 
      <input type="text" class="form-control" name="correo" 
       id="correo" maxlength="200" onkeyup="validador_campo('correo','panel_resp_correo','');" 
       onkeypress="return valida_letras(event);" placeholder="* Correo">
     
       <div id="panel_resp_correo"></div>

      <!-- Final del div panel campo -->
      </div>
  
<!-- -------------------------------------------------------------------- -->

<!-- Final del div row panel -->
</div>

 
<?php
}


if($opcion=="mostrar_producto")
{
?>

<div class="row">
<form id="formulario" enctype="multipart/form-data">
   
  <!-- -------------------------------------------------------------------- -->

    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
         <center>
        <label> portada </label></br> 

        <img id="img_resp_portada" src="" style="width: 80%; height: 300px; margin-bottom: 1.5%; border:1px solid #086A87;"> </br>
   
        <span class="btn btn-success btn-file"> 
        <span class="glyphicon glyphicon-picture"></span> Subir portada
        
        <input type="file" class="form-control" name="portada" 
        id="portada" onchange="previsualizar();">

        </span> </center>

        <div id="panel_resp_portada"></div> 
 
    <!-- Final del div panel campo -->
    </div>

  <!-- -------------------------------------------------------------------- -->

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id="panel_campo">
        <label> producto </label></br> 
       <input type="text" class="form-control" name="producto" 
       id="producto" maxlength="200" onkeyup="validador_campo('producto','panel_resp_producto','');" 
       onkeypress="return valida_letras(event);" placeholder="* Escriba el Producto">
       
       <div id="panel_resp_producto"></div>

    <!-- Final del div panel campo -->
    </div>

  <!-- -------------------------------------------------------------------- -->

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id="panel_campo">
         
        <label> area : <span id="area"> </span> </label></br> 
        <input type="hidden" name="id_area" id="id_area" >
 
        <ul class="nav navbar-nav" style="width:100%; ">
         
        <li class="dropdown" style="width:100%;">
          
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="background:transparent; color:#2E4053; width:100%; border:1px solid #2E4053; padding: 1%;" id="area" > area <span class="caret"></span> </a>

        <ul class="dropdown-menu" id="select-form" style="border:1px solid #2E4053;">
        <?php

         foreach($areas as $area)
         {
            $id_area = $area->id_area;
            $area_n = $area->area;
            ?>
            <li> <a href="#" onclick="select('<?php echo $id_area; ?>','<?php echo $area_n; ?>','area');"> <?php echo $area_n; ?> </a> </li> 
            <?php
          } ?>
         </ul>

         </li>
        </ul> 
         
        <div id="panel_resp_area"></div>

    <!-- Final del div panel campo -->
    </div>

  <!-- -------------------------------------------------------------------- -->

    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo"> </div>  
    
  <!-- -------------------------------------------------------------------- -->

    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" id="panel_campo">
        <label> codigo de barras </label></br> 
       <input type="text" class="form-control" name="codigo_bar" 
       id="codigo_bar" maxlength="200" onkeyup="validador_campo('codigo_bar','panel_resp_codigo_bar','');" 
       onkeypress="return valida_letras(event);" placeholder="* Codigo de Barras">
       
       <div id="panel_resp_codigo_bar"></div>

    <!-- Final del div panel campo -->
    </div>

    
  <!-- -------------------------------------------------------------------- -->

    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" id="panel_campo">
         <label> cantidad </label></br> 
        <input type="text" class="form-control" name="cantidad" 
         id="cantidad" maxlength="10" onkeyup="validador_campo('cantidad','panel_resp_cantidad','');" 
         onkeypress="return valida_numeros(event);" placeholder="* Cantidad">
         <div id="panel_resp_cantidad"></div>

         <!-- Final del div panel campo -->
    </div>
     
  <!-- -------------------------------------------------------------------- -->

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id="panel_campo">
        <label> precio de compra </label></br> 
       <input type="text" class="form-control" name="precio_compra" 
       id="precio_compra" maxlength="10" onkeyup="validador_campo('precio_compra','panel_resp_precio_compra','');" 
       onkeypress="return valida_letras(event);" placeholder="* 0.00">
       
       <div id="panel_resp_precio_compra"></div>

    <!-- Final del div panel campo -->
    </div>

    
  <!-- -------------------------------------------------------------------- -->

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id="panel_campo">
       <label> precio de venta </label></br> 
       <input type="text" class="form-control" name="precio_venta" 
       id="precio_venta" maxlength="10" onkeyup="validador_campo('precio_venta','panel_resp_precio_venta','');" 
       onkeypress="return valida_letras(event);" placeholder="* 0.00">
       
       <div id="panel_resp_precio_venta"></div>

    <!-- Final del div panel campo -->
    </div>

    
  <!-- -------------------------------------------------------------------- -->

    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="panel_campo">
        <label> descripcion </label></br> 
        <textarea class="form-control" name="descripcion" 
        id="descripcion" maxlength="3000" onkeyup="validador_campo('descripcion','panel_resp_descripcion','');" onkeypress="return valida_ambos(event);" placeholder="* Escriba la descripcion" ></textarea>
   
        <div id="panel_resp_descripcion"></div>

    <!-- Final del div panel campo -->
    </div>
 
<!-- Final del formulario -->
</form>

<!-- Final del div row -->
</div>


<?php
}

?>