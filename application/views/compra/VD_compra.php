
<div class="row" style="padding: 0px; margin: 0px;">
 
        <?php 

         $proveedor = $compras[0]->proveedor;
         $total_compra = $compras[0]->total_compra;
         $pago_compra = $compras[0]->pago_compra;
         $cambio_compra = $compras[0]->cambio_compra;
         $fecha_compra = $compras[0]->fecha_compra;
         $hora_compra = $compras[0]->hora_compra;

         ?>
	<!-- -------------------------------------------------------------------- -->

	   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="panel_campo" style="padding: 0px; margin: 0px;">
        
	    <h3 align="center" style="padding: 0px; margin: 0px;"> Detalle de Compras </h3>
	    <p align="center" style="padding: 0px; margin-top: 0px;"> lista de todos los producto comprados </p>

	    <table class="table table-bordered">
	    	<tr>
	    		<td> Proveedor </td>
	    		<td> <?php echo $proveedor; ?> </td>

	    		<td> Fecha </td>
	    		<td> <?php echo $fecha_compra; ?> </td>
	    		<td> Hora </td>
	    		<td> <?php echo $hora_compra; ?> </td>

	    	</tr>
	    </table>  

	    <table class="table table-bordered">
	    	 <tr>
	    	 	<th> Producto </th>
	    	 	<th> Cantidad </th>
	    	 	<th> Precio Compra </th>
	    	 	<th> Precio Venta </th>
	    	 	<th> Subtotal Compra </th>
	    	 </tr>
         <?php

		  foreach ($compras as $dc) 
		  { 
			$producto = $dc->producto;
			$cantidad_dc = $dc->cantidad_dc;
			$precio_dc = $dc->precio_dc;
			$pecio_venta_dc = $dc->precio_venta_dc;
			$subtotal_dc = $dc->subtotal_dc;
		  ?>
           <tr>
           	<td> <?php echo $producto; ?> </td>
           	<td> <?php echo $cantidad_dc; ?> </td>
           	<td> <?php echo $precio_dc; ?> </td>
           	<td> <?php echo $pecio_venta_dc; ?> </td>
           	<td> <?php echo $subtotal_dc; ?> </td>
           </tr>
		  <?php

		  }
         ?>

	    </table>  


	    <table class="table table-bordered">
	    	<tr>
	    		<td> Total </td>
	    		<td> <?php echo $total_compra; ?> </td>

	    		<td> Pago </td>
	    		<td> <?php echo $pago_compra; ?> </td>
	    		
	    		<td> Cambio </td>
	    		<td> <?php echo $cambio_compra; ?> </td>

	    	</tr>
	    </table>  

    
 

	   <!-- Final del div panel campo -->
	   </div>
	 
	  
<!-- -------------------------------------------------------------------- -->
 
<!-- Final del div row -->
</div>
 

