<table class="table table-bordered table-condensed table-hover table-striped">
     <thead>
      <tr>
        <th class="hidden-lg hidden-md hidden-sm"> </th>
        <th> # </th>
        <th> proveedor </th>
        <th> detalle </th>
        <th> total </th>
        <th> pago </th>
        <th> cambio </th>
        <th> fecha </th>
        <th> hora </th>
        <th class="hidden-xs"> </th>

      </tr>

     </thead>
     <tbody>
     <?php 
     foreach($compras as $obcompra)
     {
        $id_compra = $obcompra->id_compra;
        $codigo_compra = $obcompra->codigo_compra;
        $proveedor = $obcompra->proveedor;
        $total = $obcompra->total_compra;
        $pago = $obcompra->pago_compra;
        $cambio = $obcompra->cambio_compra;
        $fecha = $obcompra->fecha_compra;
        $hora = $obcompra->hora_compra;
         
     ?>
       <tr>

        <td class="col-xs-1 hidden-lg hidden-md hidden-sm"> <center>
             <a href="javascript:void();" onclick="btn_submenu_xs('<?php echo $id_compra; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones-xs" id="panel_opciones_xs<?php echo $id_compra; ?>">

              <div align="right" class="panel_cerrar_botones-xs" >
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu_xs('<?php echo $id_compra; ?>');"> X </buttom>
              </div>
              <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_examinar_compra('<?php echo $id_compra; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_editar_compra('<?php echo $id_compra; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line-xs">

               <button class="btn btn-danger btn-md" onclick="btn_borrar_compra('<?php echo $codigo_compra; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 

             </div>

          </center>
          </td>
        <td> <?php echo $id_compra; ?> </td>
        <td> <?php echo $proveedor; ?> </td>
        <td> - </td>
        <td> <?php echo $total; ?> </td>
        <td> <?php echo $pago; ?> </td>
        <td> <?php echo $cambio; ?> </td>
        <td> <?php echo $fecha; ?> </td>
        <td> <?php echo $hora; ?> </td>
        
        <td class="col-lg-1 hidden-xs"> <center>
             <a href="javascript:void();" onclick="btn_submenu('<?php echo $id_compra; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones" id="panel_opciones_<?php echo $id_compra; ?>">

              <div align="right" class="panel_cerrar_botones">
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu('<?php echo $id_compra; ?>');"> x </buttom>
              </div>
              <hr class="btn_line">

               <button class="btn btn-default btn-xs" onclick="btn_examinar_compra('<?php echo $id_compra; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line">

               <button class="btn btn-default btn-xs" onclick="btn_editar_compra('<?php echo $id_compra; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line">

               <button class="btn btn-danger btn-xs" onclick="btn_borrar_compra('<?php echo $codigo_compra; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 
             
             </div>

          </center>
          </td>

       </tr>
     <?php
     } 
     ?>
     </tbody>
 
 <!-- Final del table -->
 </table>
 
 <?php require "paginacion.php"; ?>

