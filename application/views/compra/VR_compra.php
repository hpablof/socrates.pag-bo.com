<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

		<div class="row">

	      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 
	          <table style="width: 100%;">
	          	<tr>
	          		<td style="width: 80%;"> <label> Proveedor </label> </td>
	          		<td align="right"> 
                    <button class="btn btn-info btn-md" onclick="btn_modal_proveedor();"> + proveedor </button>
	          		</td>
	          	</tr>
	          </table>

	          <input type="hidden" name="id_proveedor" id="id_proveedor">
	          <input type="hidden" name="proveedor" id="proveedor">

	          <input type="text" class="form-control" name="txt_buscar_proveedor" id="txt_buscar_proveedor" placeholder="* Buscar Proveedor" onkeyup="btn_buscar_proveedor();" autocomplete="off">
	          
	          <div id="panel_resp_proveedor"></div>
              </br>
	      
	      </div>

	      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 
	          <table style="width: 100%;">
	          	<tr>
	          		<td style="width: 80%;"> <label> Productos </label> </td>
	          		<td align="right"> 
                    <button class="btn btn-success btn-md" onclick="btn_modal_producto();"> + productos </button>
	          		</td>
	          	</tr>
	          </table>

	          <input type="hidden" name="id_producto" id="id_producto">
	          <input type="hidden" name="producto" id="producto">

	          <input type="text" class="form-control" name="txt_buscar_producto" id="txt_buscar_producto" placeholder="* Buscar Producto" onkeyup="btn_buscar_productos();" autocomplete="off" >
	          
	          <div id="panel_resp_producto"></div>
	          </br>
	      
	      </div>

	      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
	          
	          <label> fecha vencimiento </label>
 
	          <input type="date" class="form-control" name="fecha_vencimiento" id="fecha_vencimiento">
	          
	          <div id="panel_resp_fecha_vencimiento"></div>
	      
	      </div>

	      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
	          
	          <label> cantidad </label>
 
	          <input type="text" class="form-control" name="cantidad" id="cantidad" placeholder="0" autocomplete="off" onkeyup="btn_subtotal_compra();">
	          
	          <div id="panel_resp_cantidad"></div>
	      
	      </div>


	      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
	          
	          <label> precio compra </label>
 
	          <input type="text" class="form-control" name="precio_compra" id="precio_compra" placeholder="0.00" autocomplete="off" onkeyup="btn_subtotal_compra();">
	          
	          <div id="panel_resp_precio_compra"></div>
	      
	      </div>

	      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
	          
	          <label> precio venta </label>
 
	          <input type="text" class="form-control" name="precio_venta" id="precio_venta" placeholder="0.00" autocomplete="off" onkeyup="btn_subtotal_compra();">
	          
	          <div id="panel_resp_precio_venta"></div>
	      
	      </div>

	      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
	          
	          <label> subtotal </label>
 
	          <input type="text" class="form-control" name="subtotal" id="subtotal" placeholder="0.00" autocomplete="off" readonly>
	          
	          <div id="panel_resp_subtotal" ></div>
	      
	      </div>

	      
	      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

              <hr>
	          <center> 

	          	<button class="btn btn-info btn-md" onclick="btn_agregar_carrito_compra();"> <span class=""></span> Agregar </button>

	          </center>
	          
	          <div id="panel_resp_carrito" ></div>
	      
	      </div>


	    </div> 	
		
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h4 align="center"> <label> LISTA DE COMPRAS </label> </h4>
		<div id="panel_listado_compra">
			
		</div>

		<table class="table">
			<tr>
				<td> <input type="text" name="total_compra" id="total_compra" class="form-control" disabled placeholder="0.00"> </td>

			    <td> <input type="text" name="pago_compra" id="pago_compra" class="form-control" placeholder="0.00" onkeyup="btn_cambio_compra();"> </td>

			    <td> <input type="text" name="cambio_compra" id="cambio_compra" class="form-control" disabled placeholder="0.00"> </td>

			</tr>
		</table>
		
	</div>
</div>