
<div class="row">
<form id="formulario" enctype="multipart/form-data">

 <?php 
         $producto = $productos[0]->producto;
         $portada = $productos[0]->portada;
         $codigo_bar = $productos[0]->codigo_bar;
         $cantidad = $productos[0]->cantidad;
         $precio_compra = $productos[0]->precio_compra;
         $precio_venta = $productos[0]->precio_venta;
         $descripcion = $productos[0]->descripcion;
         $area = $productos[0]->area;
         ?>
	<!-- -------------------------------------------------------------------- -->

  <!-- -------------------------------------------------------------------- -->
 
     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
      <center>
        <label> portada </label></br> 

        <img id="img_resp_portada" src="<?php echo base_url(); ?>assets/multimedia/portadas/<?php echo $portada; ?>" style="width: 80%; height: 300px; margin-bottom: 1.5%; border:1px solid #086A87;">
   
        <span class="btn btn-success btn-file"> 
        <span class="glyphicon glyphicon-picture"></span> Subir portada
        
        <input type="file" class="form-control" name="portada" 
        id="portada" onchange="previsualizar();">

        </span> </center>

        <div id="panel_resp_portada"></div> 
 
     <!-- Final del div panel campo -->
     </div>

	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	   <label> producto </label></br> 
     <input type="text" class="form-control" name="producto" 
     id="producto" maxlength="200" onkeyup="validador_campo('producto','panel_resp_producto','');" 
     onkeypress="return valida_letras(event);" placeholder="* Escriba su producto" value="<?php echo $producto; ?>" >
     <div id="panel_resp_producto"></div>

     <!-- Final del div panel campo -->
     </div>

  <!-- -------------------------------------------------------------------- -->
 
     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
 
        <label> area : <span id="area"> <?php echo $area; ?></span> </label></br> 
        <input type="hidden" name="id_area" id="id_area" >
 
        <ul class="nav navbar-nav" style="width:100%; ">
         
        <li class="dropdown" style="width:100%;">
          
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="background:transparent; color:#2E4053; width:100%; border:1px solid #2E4053; padding: 1%;" id="select_area" > area <span class="caret"></span> </a>

        <ul class="dropdown-menu" id="select-form" style="border:1px solid #bdc3c7;">
        <?php

         foreach($areas as $area)
         {
            $id_area = $area->id_area;
            $area_n = $area->area;
            ?>
            <li> <a href="#" onclick="select('<?php echo $id_area; ?>','<?php echo $area_n; ?>','area');"> <?php echo $area_n; ?> </a> </li> 
            <?php
          } ?>
         </ul>

         </li>
        </ul> 
         
        <div id="panel_resp_area"></div>

     <!-- Final del div panel campo -->
     </div>


	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id="panel_campo">
        
	    <label> codigo de barras </label></br> 
      <input type="text" class="form-control" name="codigo_bar" 
      id="codigo_bar" maxlength="200" onkeyup="validador_campo('codigo_bar','panel_resp_codigo_bar','');" 
      onkeypress="return valida_letras(event);" placeholder="* Escriba su codigo_bar" value="<?php echo $codigo_bar; ?>" >
      <div id="panel_resp_codigo_bar"></div>

     <!-- Final del div panel campo -->
     </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id="panel_campo">
        
	    <label> cantidad </label></br> 
      <input type="text" class="form-control" name="cantidad" 
      id="cantidad" maxlength="10" onkeyup="validador_campo('cantidad','panel_resp_cantidad','');" 
      onkeypress="return valida_numeros(event);" placeholder="* Escriba su cantidad" value="<?php echo $cantidad; ?>" >
      <div id="panel_resp_cantidad"></div>

     <!-- Final del div panel campo -->
     </div>

  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id="panel_campo">
        
	   <label> precio de compra </label></br> 
     <input type="text" class="form-control" name="precio_compra" 
     id="precio_compra" maxlength="200" onkeyup="validador_campo('precio_compra','panel_resp_precio_compra','');" 
     onkeypress="return valida_letras(event);" placeholder="* 0.00" value="<?php echo $precio_compra; ?>" >
     <div id="panel_resp_precio_compra"></div>

        <!-- Final del div panel campo -->
       </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id="panel_campo">
        
	   <label> precio de venta </label></br> 
     <input type="text" class="form-control" name="precio_venta" 
     id="precio_venta" maxlength="200" onkeyup="validador_campo('precio_venta','panel_resp_precio_venta','');" 
     onkeypress="return valida_letras(event);" placeholder="* 0.00" value="<?php echo $precio_venta; ?>" >
     <div id="panel_resp_precio_venta"></div>

     <!-- Final del div panel campo -->
     </div>
 
  
	<!-- -------------------------------------------------------------------- -->
 
	   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="panel_campo">
        
	    <label> descripcion </label></br> 
      <textarea class="form-control" name="descripcion" 
      id="descripcion" maxlength="3000" onkeyup="validador_campo('descripcion','panel_resp_descripcion','');" onkeypress="return valida_ambos(event);" placeholder="* Escriba su descripcion" value="<?php echo $descripcion; ?>" ></textarea>
 
      <div id="panel_resp_descripcion"></div>

     <!-- Final del div panel campo -->
     </div>

  

    
<!-- -------------------------------------------------------------------- -->
 
</form>
<!-- Final del div row -->
</div>
 

