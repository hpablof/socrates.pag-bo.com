
<div class="componente_menu" >

  <h4 class="titulo_menu" > System /producto /menu </h4>
  <script type="text/javascript" src="<?php echo base_url(); ?>/scripts/C_producto.js"></script>
 
  <div class="row">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="panel_buscador">
     
     <table class="table table-bordered table-condensed table-hover table-striped">
     <tr>
      
      <td> 
       <input type="text" class="form-control" id="txt_buscar" placeholder="* Buscar producto" onkeyup="buscar_datos(1);"> 
      </td>
      
      <td>
        <button class="btn btn-default btn-md" onclick="buscar_datos(1);" > 
        <span class="glyphicon glyphicon-search"></span>
        Buscar </button>
      
      </td>

      <td align="right">
       <button class="btn btn-default btn-md" onclick="btn_nuevo_producto();" > 
       <span class="glyphicon glyphicon-file"></span>
       Nuevo producto </button>
      </td>

     </tr>
     
     </table>

     <div id="panel_resultado_busqueda"> </div>

   </div>

   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="panel_listado">
   <div id="panel_paginacion_datos" class="table-responsive">
   
   <table class="table table-bordered table-condensed table-hover table-striped">
     <thead>
      <tr>
        <th class="hidden-lg hidden-md hidden-sm"> </th>
    		<th> portada </th>
        <th> producto </th>
        <th> area </th>
    		<th> cod barras </th>
    		<th> <center> cantidad </center> </th>
    		<th> <center> p. compra </center> </th>
    		<th> <center> p. venta </center> </th>
        <th class="hidden-xs"> </th>
      </tr>

     </thead>
     <tbody>
     <?php 
     foreach($productos as $ob_producto)
     {
     	  $id_producto = $ob_producto->id_producto;
		    $producto = $ob_producto->producto;
		    $portada = $ob_producto->portada;
		    $codigo_bar = $ob_producto->codigo_bar;
		    $cantidad = $ob_producto->cantidad;
		    $precio_compra = $ob_producto->precio_compra;
		    $precio_venta = $ob_producto->precio_venta;
		    $descripcion = $ob_producto->descripcion;
		    $area = $ob_producto->area;
     ?>
       <tr>

        <td class="col-xs-1 hidden-lg hidden-md hidden-sm"> <center>
             <a href="javascript:void();" onclick="btn_submenu_xs('<?php echo $id_producto; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones-xs" id="panel_opciones_xs<?php echo $id_producto; ?>">

              <div align="right" class="panel_cerrar_botones-xs" >
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu_xs('<?php echo $id_producto; ?>');"> X </buttom>
              </div>
              <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_examinar_producto('<?php echo $id_producto; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
               examinar </button> 
             
               <hr class="btn_line-xs">

               <button class="btn btn-default btn-md" onclick="btn_editar_producto('<?php echo $id_producto; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
                 editar </button> 

               <hr class="btn_line-xs">

               <button class="btn btn-danger btn-md" onclick="btn_borrar_producto('<?php echo $id_producto; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
               borrar </button> 

             </div>

          </center>
          </td>
     
		    
		    <td style="width: 60px; text-align: center;"> <img src="<?php echo base_url(); ?>assets/multimedia/portadas/<?php echo $portada; ?>" style="width:50px; height:50px;" > </td>
        <td> <?php echo $producto; ?> </td>
        <td> <label> <?php echo $area; ?> <label> </td>
		    <td> <?php echo $codigo_bar; ?> </td>
		    <td align="center" > <label> <?php echo $cantidad; ?> </label></td>
		    <td align="center" > <?php echo $precio_compra; ?> </td>
		    <td align="center" > <?php echo $precio_venta; ?> </td>
 
        <td class="col-lg-1 hidden-xs"> <center>
             <a href="javascript:void();" onclick="btn_submenu('<?php echo $id_producto; ?>');"> <span class="glyphicon glyphicon-align-justify"> </span></a>
                
             <div class="btn_opciones" id="panel_opciones_<?php echo $id_producto; ?>">

              <div align="right" class="panel_cerrar_botones">
               <buttom class="btn btn-danger btn-xs" onclick="btn_submenu('<?php echo $id_producto; ?>');"> x </buttom>
              </div>
              <hr class="btn_line">

	   	         <button class="btn btn-default btn-xs" onclick="btn_examinar_producto('<?php echo $id_producto; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-eye-open"></span> 
	   	         examinar </button> 
	   	       
               <hr class="btn_line">

	   	         <button class="btn btn-default btn-xs" onclick="btn_editar_producto('<?php echo $id_producto; ?>');" style="width: 100%;" > 
                 <span class="glyphicon glyphicon-pencil"></span> 
	   	           editar </button> 

               <hr class="btn_line">

	   	         <button class="btn btn-danger btn-xs" onclick="btn_borrar_producto('<?php echo $id_producto; ?>');" style="width: 100%;" > 
               <span class="glyphicon glyphicon-trash"></span> 
	   	         borrar </button> 
             
             </div>

          </center>
	   	    </td>

       </tr>
     <?php
     } 
     ?>
     </tbody>
 
 <!-- Final del table -->
 </table>

 <?php require "paginacion.php"; ?>

 </div>
 <!-- Final del panel blanco -->
 </div>

 <!-- Final del div panel -->
 </div>



<!-- Final del div row -->
</div>

<?php require "VM_producto.php"; ?>
 

