
/* CONTROLADOR DE AREA DEL SISTEMA */
var url_p = "https://socrates.pag-bo.com/";
var lista = [];


 function cargar_datos_nivel_pc(page)
 {  
    var ob = {page:page};
    $("#panel_resultado_busqueda").html("");

    $.ajax({
        type: "POST",
        url: url_p+"Cnivel_pc/paginacion",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos_nivel_pc").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos_nivel_pc").html(data);

        }
      });
}


 function buscar_datos_nivel_pc(page)
 {  
    var txt_buscar = $("#txt_buscar_nivel_pc").val();

    var cadena = " Resultado para : <label> "+txt_buscar+" </label> ";
    $("#panel_resultado_busqueda").html(cadena);
    
    var ob = { page:page, txt_buscar:txt_buscar };

    $.ajax({
        type: "POST",
        url: url_p+"Cnivel_pc/paginacion_find",
        data: ob,
        beforeSend: function(objeto)
        {
          $("#panel_paginacion_datos_nivel_pc").html("<div class='cargando'> </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos_nivel_pc").html(data);

        }
      });
}



function nivel_pc_o(nivel_pc, nombre_pc)
{ 
   this.nivel_pc = nivel_pc;
   this.nombre_pc = nombre_pc; 
}

function btn_nuevo_nivel_pc()
{
  $("#myModal_Register_nivel_pc").modal("show");
 
  var ob = "";
  $.ajax({
      type: "POST",
      url: url_p+"Cnivel_pc/R_nivel_pc",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar_nivel_pc").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar_nivel_pc").html(data);
      }
     });

}

function btn_agregar_carrito_nivel_pc()
{ 
  var nivel_pc = $("#nivel_pc").val();
  var nombre_pc = $("#nombre_pc").val(); 
  
  if(nivel_pc!= "" && nombre_pc!="")
  {
  
  var obj_nivel_pc = new nivel_pc_o(nivel_pc, nombre_pc);

  lista.push(obj_nivel_pc);
  btn_mostrar_datos_nivel_pc();
  }

  else {
  
       if (nivel_pc =="")
       {  
           $("#nivel_pc").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de nivel_pc </label>";
           $("#panel_resp_nivel_pc").html(res);  
       }

       
  //Final de la codicional else
  }

 //Final de la fucion
}

function btn_mostrar_datos_nivel_pc()
{
  var cadena = "<table class='table table-bordered table-condensed table-striped table-hover'>";
  
  cadena = cadena + "<tr>"+"<th> nivel pc </th>"+"<th> nombre pc </th>"+"<th> opciones </th>"; 

  cadena = cadena +"</tr>";

  for(var i = 0 ; i<lista.length; i++)
  {
    var nivel_pc = lista[i].nivel_pc; 
    var nombre_pc = lista[i].nombre_pc;

    cadena = cadena + "<tr>"+
    "<td>"+ nivel_pc +"</td>"+
    "<td>"+ nombre_pc +"</td>"+
    "<td class='col-lg-1' > <center> <button class='btn btn-danger btn-xs' onclick='btn_eliminar_item("+i+");' > X </button> </center> </td><tr>"; 
   }

   cadena = cadena +" </table> ";

   $("#panel_listado_nivel_pc").html(cadena);  
 
} 

function btn_eliminar_item(contador)
{
  lista.splice( contador , 1);
  btn_mostrar_datos_nivel_pc();
}

function btn_registrar_nivel_pc()
{  
   var id_usuario = $("#id_usuario_session").val(); 
   var lista_area = JSON.stringify(lista);
   var ob = {lista : lista_area, id_usuario:id_usuario };

  $.ajax({
      type: "POST",
      url: url_p+"Cnivel_pc/G_nivel_pc",
      data: ob,
      beforeSend: function(objeto)
      {
        $("#panel_modal_resultado_registrar_nivel_pc").html("<div class='cargando'>  </div>");
        //$("#btn_registrar").prop("disabled", true);
      },
      success:function(data)
      { 
        //$("#btn_registrar").prop("disabled", false);   
        $("#panel_modal_resultado_registrar_nivel_pc").html(data);
      }
  });
}

function btn_examinar_nivel_pc(id_nivel_pc)
{
   

  $("#myModal_View_nivel_pc").modal("show");

  var ob = {id_nivel_pc : id_nivel_pc};
  $.ajax({
      type: "POST",
      url: url_p+"Cnivel_pc/VD_nivel_pc",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_examinar_nivel_pc").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_examinar_nivel_pc").html(data);
      }
     });

}

function btn_editar_nivel_pc(id_nivel_pc)
{
  $("#myModal_Update_nivel_pc").modal("show");
  $("#id_nivel_pc_edicion").val(id_nivel_pc);
  
  var ob = {id_nivel_pc : id_nivel_pc};
  $.ajax({
      type: "POST",
      url: url_p+"Cnivel_pc/VE_nivel_pc",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_editar_nivel_pc").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_editar_nivel_pc").html(data);
      }
     });

}

function btn_guardar_nivel_pc()
{
  var id_nivel_pc = $("#id_nivel_pc_edicion").val();
  var nivel_pc = $("#nivel_pc").val();
  var ob = { id_nivel_pc : id_nivel_pc, nivel_pc:nivel_pc }; 

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_nivel_pc", id_nivel_pc);

  $.ajax({
   url: url_p+"Cnivel_pc/VU_nivel_pc",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){

     $("#btn_editar").prop("disabled", true);
     $("#panel_modal_respuesta_editar_nivel_pc").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_editar").prop("disabled", false);  
     $("#panel_modal_respuesta_editar_nivel_pc").html(data);
   
   }
 }); 

}

function btn_borrar_nivel_pc(id_nivel_pc)
{
  $("#myModal_Delete_nivel_pc").modal("show");
  $("#id_nivel_pc_eliminar").val(id_nivel_pc);
}

function btn_eliminar_nivel_pc()
{
  var id_nivel_pc = $("#id_nivel_pc_eliminar").val();
  var ob = { id_nivel_pc : id_nivel_pc };
  
  $.ajax({
      type: "POST",
      url: url_p+"Cnivel_pc/VB_nivel_pc",
      data: ob,
      beforeSend: function(objeto){

        $("#btn_borrar").prop("disabled", true);
        $("#panel_modal_eliminar_nivel_pc").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        $("#btn_borrar").prop("disabled", false);   
        $("#panel_modal_eliminar_nivel_pc").html(data);
        
        setTimeout(function(){ $("#panel_modal_eliminar").html(""); },1000);
        setTimeout(function(){ $("#myModal_Delete_nivel_pc").modal("hide"); },2000);
        setTimeout(function(){ cargar_datos_nivel_pc(1); },3000);
      }
  });

}


function select(id,dato,area)
{   
  $("#id_"+area).val(id);
  $("#"+area).html(dato);
}





