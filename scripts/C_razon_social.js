
/* CONTROLADOR DE AREA DEL SISTEMA */
var url_p = "https://socrates.pag-bo.com/";
var lista = [];


 function cargar_datos(page)
 {  
    var ob = {page:page};
    $("#panel_resultado_busqueda").html("");

    $.ajax({
        type: "POST",
        url: url_p+"Crazon_social/paginacion",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}


 function buscar_datos(page)
 {  
    var txt_buscar = $("#txt_buscar").val();

    var cadena = " Resultado para : <label> "+txt_buscar+" </label> ";
    $("#panel_resultado_busqueda").html(cadena);
    
    var ob = { page:page,txt_buscar : txt_buscar };

    $.ajax({
        type: "POST",
        url: url_p+"Crazon_social/paginacion_find",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}

function razon_social_o( nit,razon_social )
{ 
 this.nit = nit; 
   this.razon_social = razon_social; 
  
}

function btn_nuevo_razon_social()
{
  $("#myModal_Register").modal("show");
 
  var ob = "";
  $.ajax({
      type: "POST",
      url: url_p+"Crazon_social/R_razon_social",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar").html(data);
      }
     });
}

function btn_agregar_carrito_razon_social()
{ 
    var nit = $("#nit").val();
    var razon_social = $("#razon_social").val(); 
  
  if(nit != "" && razon_social != "" )
  {
  
  var obj_razon_social = new razon_social_o( nit,  razon_social );

  lista.push(obj_razon_social);
  btn_mostrar_datos_razon_social();
  }

  else {
  
       if (nit =="")
       {  
           $("#nit").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de nit </label>";
           $("#panel_resp_nit").html(res);  
       }

       
       if (razon_social =="")
       {  
           $("#razon_social").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de razon_social </label>";
           $("#panel_resp_razon_social").html(res);  
       }

       
  //Final de la codicional else
  }

 //Final de la fucion
}

function btn_mostrar_datos_razon_social()
{
  var cadena = "<table class='table table-bordered table-condensed table-striped table-hover'>";
  
  cadena = cadena + "<tr>"+ "<th> nit </th> " +  "<th> razon_social </th> " +  "<th> Opciones </th>"; 

  cadena = cadena +"</tr>";

  for(var i = 0 ; i<lista.length; i++)
  {
    nit = lista[i].nit; 
    razon_social = lista[i].razon_social; 
      cadena = cadena + "<tr>"+
    "<td>"+ nit +"</td>"+
    "<td>"+ razon_social +"</td>"+ " <td  class='col-lg-1' > <center> <button class='btn btn-danger btn-xs' onclick='btn_eliminar_item("+i+");' > X </button> </center> </td><tr>"; 
   }

   cadena = cadena +" </table> ";

   $("#panel_listado_razon_social").html(cadena);  
 
} 

function btn_eliminar_item(contador)
{
  lista.splice( contador , 1);
  btn_mostrar_datos_razon_social();
}

function btn_registrar_razon_social()
{ 
  var id_usuario = $("#id_usuario_session").val(); 

   var lista_area = JSON.stringify(lista);
   var ob = {lista : lista_area, id_usuario:id_usuario };

  
  $.ajax({
      type: "POST",
      url: url_p+"Crazon_social/G_razon_social",
      data: ob,
      beforeSend: function(objeto){

        $("#panel_modal_resultado_registrar").html("<div class='cargando'>  </div>");
        $("#btn_registrar").prop("disabled", true);

      },
      success:function(data)
      { 
        $("#btn_registrar").prop("disabled", false);   
        $("#panel_modal_resultado_registrar").html(data);
        
        setTimeout(function(){ $("#panel_modal_resultado_registrar").html(""); },1000);
        setTimeout(function(){ $("#myModal_Register").modal("hide"); },2000);
        setTimeout(function(){ location.reload(); },3000);
      }
  });
}

function btn_examinar_razon_social(id_razon_social)
{
  $("#myModal_View").modal("show");

  var ob = {id_razon_social : id_razon_social};
  $.ajax({
      type: "POST",
      url: url_p+"Crazon_social/VD_razon_social",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_examinar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_examinar").html(data);
      }
     });

}

function btn_editar_razon_social(id_razon_social)
{
  $("#myModal_Update").modal("show");
  $("#id_razon_social_edicion").val(id_razon_social);
  
  var ob = {id_razon_social : id_razon_social};
  $.ajax({
      type: "POST",
      url: url_p+"Crazon_social/VE_razon_social",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_editar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_editar").html(data);
      }
     });

}

function btn_guardar_razon_social()
{

  var id_razon_social = $("#id_razon_social_edicion").val();

  var nit = $("#nit").val();
  var razon_social = $("#razon_social").val();
  var ob = { id_razon_social : id_razon_social, nit:nit, razon_social:razon_social }; 

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_razon_social", id_razon_social);

  $.ajax({
   url: url_p+"Crazon_social/VU_razon_social",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){

     $("#btn_editar").prop("disabled", true);
     $("#panel_modal_respuesta_editar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_editar").prop("disabled", false);  
     $("#panel_modal_respuesta_editar").html(data);
   
   }
 }); 

}

function btn_borrar_razon_social(id_razon_social)
{
  $("#myModal_Delete").modal("show");
  $("#id_razon_social_eliminar").val(id_razon_social);

}

function btn_eliminar_razon_social()
{
  var id_razon_social = $("#id_razon_social_eliminar").val();

  var ob = { id_razon_social : id_razon_social };
  
  $.ajax({
      type: "POST",
      url: url_p+"Crazon_social/VB_razon_social",
      data: ob,
      beforeSend: function(objeto){

        $("#btn_borrar").prop("disabled", true);
        $("#panel_modal_eliminar").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        $("#btn_borrar").prop("disabled", false);   
        $("#panel_modal_eliminar").html(data);
        
        setTimeout(function(){ $("#panel_modal_eliminar").html(""); },1000);
        setTimeout(function(){ $("#myModal_Delete").modal("show"); },2000);
        setTimeout(function(){ location.reload(); },1000);
      }
  });

}


var estado = 0;

function btn_submenu(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_"+id).css("display","none");
  }
  }

}

function btn_submenu_xs(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_xs"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_xs"+id).css("display","none");
  }
  }

}


function select(id,dato,area)
{   
  $("#id_"+area).val(id);
  $("#"+area).html(dato);
}


function validar_nit(nit,panel_nit)
{
  var nit_ver = $("#"+nit).val(); 
  var ob = { nit_ver : nit_ver };
  
  $.ajax({
      type: "POST",
      url: url_p+"Crazon_social/Verificar_razon_social",
      data: ob,
      beforeSend: function(objeto)
      {
        $("#"+panel_nit).html("<div class='cargando'>  </div>");
      },
      success:function(data)
      { 
        $("#"+panel_nit).html(data);
      }
  });
}


function btn_subir_razones_sociales()
{
  $("#myModal_Upload_RS").modal('show');

  var ob = "";
  
  $.ajax({
      type: "POST",
      url: url_p+"Crazon_social/Upload_razon_social",
      data: ob,
      beforeSend: function(objeto)
      {
        $("#panel_modal_subida_rs").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      { 
        $("#panel_modal_subida_rs").html(data);
      }
  });
}


function btn_subir_rs_db()
{
  var id_usuario = $("#id_usuario_session").val(); 
  var formData = new FormData($("#formulario_RS")[0]);
  formData.append("id_usuario", id_usuario);
 
  $.ajax({
   url: url_p+"Crazon_social/Subir_razon_social",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto)
   {
     $("#panel_resp_upload_rs").html("<div class='cargando'>  </div>");
   },
   success: function(data)
   { 
     $("#panel_resp_upload_rs").html(data);
   
   }
 }); 

}