
/* CONTROLADOR DE AREA DEL SISTEMA */
var url_p = "https://socrates.pag-bo.com/";
var lista = [];


 function cargar_datos(page)
 {  
    var ob = {page:page};
    $("#panel_resultado_busqueda").html("");

    $.ajax({
        type: "POST",
        url: url_p+"Ccargo_usuario/paginacion",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}


 function buscar_datos(page)
 {  
    var txt_buscar = $("#txt_buscar").val();

    var cadena = " Resultado para : <label> "+txt_buscar+" </label> ";
    $("#panel_resultado_busqueda").html(cadena);
    
    var ob = {page:page,txt_buscar : txt_buscar };

    $.ajax({
        type: "POST",
        url: url_p+"Ccargo_usuario/paginacion_find",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}

function cargo_usuario_o( cargo_usuario )
{ 
 this.cargo_usuario = cargo_usuario; 
  
}

function btn_nuevo_cargo_usuario()
{
  $("#myModal_Register").modal("show");
 
  var ob = "";
  $.ajax({
      type: "POST",
      url: url_p+"Ccargo_usuario/R_cargo_usuario",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar").html(data);
      }
     });
}

function btn_agregar_carrito_cargo_usuario()
{ 
    var cargo_usuario = $("#cargo_usuario").val(); 
  
  if(cargo_usuario != "" )
  {
  
  var obj_cargo_usuario = new cargo_usuario_o( cargo_usuario );

  lista.push(obj_cargo_usuario);
  btn_mostrar_datos_cargo_usuario();
  }

  else {
  
       if (cargo_usuario =="")
       {  
           $("#cargo_usuario").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de cargo_usuario </label>";
           $("#panel_resp_cargo_usuario").html(res);  
       }

       
  //Final de la codicional else
  }

 //Final de la fucion
}

function btn_mostrar_datos_cargo_usuario()
{
  var cadena = "<table class='table table-bordered table-condensed table-striped table-hover'>";
  
  cadena = cadena + "<tr>"+ "<th> cargo_usuario </th> " +  "<th> Opciones </th>"; 

  cadena = cadena +"</tr>";

  for(var i = 0 ; i<lista.length; i++)
  {
    cargo_usuario = lista[i].cargo_usuario; 
      cadena = cadena + "<tr>"+
    "<td>"+ cargo_usuario +"</td>"+ " <td  class='col-lg-1' > <center> <button class='btn btn-danger btn-xs' onclick='btn_eliminar_item("+i+");' > X </button> </center> </td><tr>"; 
   }

   cadena = cadena +" </table> ";

   $("#panel_listado_cargo_usuario").html(cadena);  
 
} 

function btn_eliminar_item(contador)
{
  lista.splice( contador , 1);
  btn_mostrar_datos_cargo_usuario();
}

function btn_registrar_cargo_usuario()
{ 
  var id_usuario = $("#id_usuario_session").val(); 

   var cargo_usuario = $("#cargo_usuario").val();

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_usuario", id_usuario);

  $.ajax({
   url: url_p+"Ccargo_usuario/G_cargo_usuario",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){
     
     $("#btn_registrar").prop("disabled", true); 
     $("#panel_modal_resultado_registrar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_registrar").prop("disabled", false);  
     $("#panel_modal_resultado_registrar").html(data);
 
   }
 });  


}

function btn_examinar_cargo_usuario(id_cargo_usuario)
{
  $("#myModal_View").modal("show");

  var ob = {id_cargo_usuario : id_cargo_usuario};
  $.ajax({
      type: "POST",
      url: url_p+"Ccargo_usuario/VD_cargo_usuario",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_examinar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_examinar").html(data);
      }
     });

}

function btn_editar_cargo_usuario(id_cargo_usuario)
{
  $("#myModal_Update").modal("show");
  $("#id_cargo_usuario_edicion").val(id_cargo_usuario);
  
  var ob = {id_cargo_usuario : id_cargo_usuario};
  $.ajax({
      type: "POST",
      url: url_p+"Ccargo_usuario/VE_cargo_usuario",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_editar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_editar").html(data);
      }
     });

}

function btn_guardar_cargo_usuario()
{

  var id_cargo_usuario = $("#id_cargo_usuario_edicion").val();

  var cargo_usuario = $("#cargo_usuario").val();
  var ob = { id_cargo_usuario : id_cargo_usuario, cargo_usuario:cargo_usuario }; 

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_cargo_usuario", id_cargo_usuario);

  $.ajax({
   url: url_p+"Ccargo_usuario/VU_cargo_usuario",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){

     $("#btn_editar").prop("disabled", true);
     $("#panel_modal_respuesta_editar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_editar").prop("disabled", false);  
     $("#panel_modal_respuesta_editar").html(data);
   
   }
 }); 

}

function btn_borrar_cargo_usuario(id_cargo_usuario)
{
  $("#myModal_Delete").modal("show");
  $("#id_cargo_usuario_eliminar").val(id_cargo_usuario);

}

function btn_eliminar_cargo_usuario()
{
  var id_cargo_usuario = $("#id_cargo_usuario_eliminar").val();

  var ob = { id_cargo_usuario : id_cargo_usuario };
  
  $.ajax({
      type: "POST",
      url: url_p+"Ccargo_usuario/VB_cargo_usuario",
      data: ob,
      beforeSend: function(objeto){

        $("#btn_borrar").prop("disabled", true);
        $("#panel_modal_eliminar").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        $("#btn_borrar").prop("disabled", false);   
        $("#panel_modal_eliminar").html(data);
        
        setTimeout(function(){ $("#panel_modal_eliminar").html(""); },1000);
        setTimeout(function(){ $("#myModal_Delete").modal("show"); },2000);
        setTimeout(function(){ location.reload(); },1000);
      }
  });

}


var estado = 0;

function btn_submenu(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_"+id).css("display","none");
  }
  }

}

function btn_submenu_xs(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_xs"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_xs"+id).css("display","none");
  }
  }

}


function select(id,dato,area)
{   
  $("#id_"+area).val(id);
  $("#"+area).html(dato);
}


