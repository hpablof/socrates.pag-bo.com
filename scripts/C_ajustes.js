
/* CONTROLADOR DE AREA DEL SISTEMA */
var url_p = "https://socrates.pag-bo.com/";
var lista = [];


 function cargar_datos(page)
 {  
    var ob = {page:page};
    $("#panel_resultado_busqueda").html("");

    $.ajax({
        type: "POST",
        url: url_p+"Cajustes/paginacion",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}


 function buscar_datos(page)
 {  
    var txt_buscar = $("#txt_buscar").val();

    var cadena = " Resultado para : <label> "+txt_buscar+" </label> ";
    $("#panel_resultado_busqueda").html(cadena);
    
    var ob = {page:page,txt_buscar : txt_buscar };

    $.ajax({
        type: "POST",
        url: url_p+"Cajustes/paginacion_find",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}

function ajustes_o( razon_social,descripcion,celular,whatsapp,direccion,portada )
{ 
 this.razon_social = razon_social; 
   this.descripcion = descripcion; 
   this.celular = celular; 
   this.whatsapp = whatsapp; 
   this.direccion = direccion; 
   this.portada = portada; 
  
}

function btn_nuevo_ajustes()
{
  $("#myModal_Register").modal("show");
 
  var ob = "";
  $.ajax({
      type: "POST",
      url: url_p+"Cajustes/R_ajustes",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar").html(data);
      }
     });
}

function btn_agregar_carrito_ajustes()
{ 
    var razon_social = $("#razon_social").val();
    var descripcion = $("#descripcion").val();
    var celular = $("#celular").val();
    var whatsapp = $("#whatsapp").val();
    var direccion = $("#direccion").val();
    var portada = $("#portada").val(); 
  
  if(razon_social != "" && descripcion != "" && celular != "" && whatsapp != "" && direccion != "" && portada != "" )
  {
  
  var obj_ajustes = new ajustes_o( razon_social,  descripcion,  celular,  whatsapp,  direccion,  portada );

  lista.push(obj_ajustes);
  btn_mostrar_datos_ajustes();
  }

  else {
  
       if (razon_social =="")
       {  
           $("#razon_social").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de razon_social </label>";
           $("#panel_resp_razon_social").html(res);  
       }

       
       if (descripcion =="")
       {  
           $("#descripcion").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de descripcion </label>";
           $("#panel_resp_descripcion").html(res);  
       }

       
       if (celular =="")
       {  
           $("#celular").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de celular </label>";
           $("#panel_resp_celular").html(res);  
       }

       
       if (whatsapp =="")
       {  
           $("#whatsapp").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de whatsapp </label>";
           $("#panel_resp_whatsapp").html(res);  
       }

       
       if (direccion =="")
       {  
           $("#direccion").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de direccion </label>";
           $("#panel_resp_direccion").html(res);  
       }

       
       if (portada =="")
       {  
           $("#portada").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de portada </label>";
           $("#panel_resp_portada").html(res);  
       }

       
  //Final de la codicional else
  }

 //Final de la fucion
}

function btn_mostrar_datos_ajustes()
{
  var cadena = "<table class='table table-bordered table-condensed table-striped table-hover'>";
  
  cadena = cadena + "<tr>"+ "<th> razon_social </th> " +  "<th> descripcion </th> " +  "<th> celular </th> " +  "<th> whatsapp </th> " +  "<th> direccion </th> " +  "<th> portada </th> " +  "<th> Opciones </th>"; 

  cadena = cadena +"</tr>";

  for(var i = 0 ; i<lista.length; i++)
  {
    razon_social = lista[i].razon_social; 
    descripcion = lista[i].descripcion; 
    celular = lista[i].celular; 
    whatsapp = lista[i].whatsapp; 
    direccion = lista[i].direccion; 
    portada = lista[i].portada; 
      cadena = cadena + "<tr>"+
    "<td>"+ razon_social +"</td>"+
    "<td>"+ descripcion +"</td>"+
    "<td>"+ celular +"</td>"+
    "<td>"+ whatsapp +"</td>"+
    "<td>"+ direccion +"</td>"+
    "<td>"+ portada +"</td>"+ " <td  class='col-lg-1' > <center> <button class='btn btn-danger btn-xs' onclick='btn_eliminar_item("+i+");' > X </button> </center> </td><tr>"; 
   }

   cadena = cadena +" </table> ";

   $("#panel_listado_ajustes").html(cadena);  
 
} 

function btn_eliminar_item(contador)
{
  lista.splice( contador , 1);
  btn_mostrar_datos_ajustes();
}

function btn_registrar_ajustes()
{ 
  var id_usuario = $("#id_usuario_session").val(); 

   var razon_social = $("#razon_social").val();
   var descripcion = $("#descripcion").val();
   var celular = $("#celular").val();
   var whatsapp = $("#whatsapp").val();
   var direccion = $("#direccion").val();
   var portada = $("#portada").val();

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_usuario", id_usuario);

  $.ajax({
   url: url_p+"Cajustes/G_ajustes",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){
     
     $("#btn_registrar").prop("disabled", true); 
     $("#panel_modal_resultado_registrar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_registrar").prop("disabled", false);  
     $("#panel_modal_resultado_registrar").html(data);
 
   }
 });  


}

function btn_examinar_ajustes(id_ajustes)
{
  $("#myModal_View").modal("show");

  var ob = {id_ajustes : id_ajustes};
  $.ajax({
      type: "POST",
      url: url_p+"Cajustes/VD_ajustes",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_examinar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_examinar").html(data);
      }
     });

}

function btn_editar_ajustes(id_ajustes)
{
  $("#myModal_Update").modal("show");
  $("#id_ajustes_edicion").val(id_ajustes);
  
  var ob = {id_ajustes : id_ajustes};
  $.ajax({
      type: "POST",
      url: url_p+"Cajustes/VE_ajustes",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_editar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_editar").html(data);
      }
     });

}

function btn_guardar_ajustes()
{

  var id_ajustes = $("#id_ajustes_edicion").val();

  var razon_social = $("#razon_social").val();
  var descripcion = $("#descripcion").val();
  var celular = $("#celular").val();
  var whatsapp = $("#whatsapp").val();
  var direccion = $("#direccion").val();
  var portada = $("#portada").val();
  var ob = { id_ajustes : id_ajustes, razon_social:razon_social, descripcion:descripcion, celular:celular, whatsapp:whatsapp, direccion:direccion, portada:portada }; 

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_ajustes", id_ajustes);

  $.ajax({
   url: url_p+"Cajustes/VU_ajustes",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){

     $("#btn_editar").prop("disabled", true);
     $("#panel_modal_respuesta_editar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_editar").prop("disabled", false);  
     $("#panel_modal_respuesta_editar").html(data);
   
   }
 }); 

}

function btn_borrar_ajustes(id_ajustes)
{
  $("#myModal_Delete").modal("show");
  $("#id_ajustes_eliminar").val(id_ajustes);

}

function btn_eliminar_ajustes()
{
  var id_ajustes = $("#id_ajustes_eliminar").val();

  var ob = { id_ajustes : id_ajustes };
  
  $.ajax({
      type: "POST",
      url: url_p+"Cajustes/VB_ajustes",
      data: ob,
      beforeSend: function(objeto){

        $("#btn_borrar").prop("disabled", true);
        $("#panel_modal_eliminar").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        $("#btn_borrar").prop("disabled", false);   
        $("#panel_modal_eliminar").html(data);
        
        setTimeout(function(){ $("#panel_modal_eliminar").html(""); },1000);
        setTimeout(function(){ $("#myModal_Delete").modal("hide"); },2000);
        setTimeout(function(){ location.reload(); },3000);
      }
  });

}


     function previsualizar()
     {
        readImage();

        $("#img_resp_portada").css("width","80%");
        $("#img_resp_portada").css("height","200");
        $("#img_resp_portada").html("<br></br>");
     }

     function readImage () {
      $("#img_resp_portada").html("<div class='cargando'></div>");
      var input = document.getElementById("portada");
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $("#img_resp_portada").attr("src", e.target.result); // Renderizamos la imagen
        }
        reader.readAsDataURL(input.files[0]);
      }
     }

    
var estado = 0;

function btn_submenu(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_"+id).css("display","none");
  }
  }

}

function btn_submenu_xs(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_xs"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_xs"+id).css("display","none");
  }
  }

}


function select(id,dato,area)
{   
  $("#id_"+area).val(id);
  $("#"+area).html(dato);
}


