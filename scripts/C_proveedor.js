/* CONTROLADOR DE AREA DEL SISTEMA */
 var lista = [];

 function cargar_datos(page)
 {  
    var id_empresa = $("#id_empresa_select").val();
    var id_gestion = $("#id_gestion_select").val();
    
    var ob = { page:page, id_empresa:id_empresa, id_gestion:id_gestion };
    
    $("#panel_resultado_busqueda").html("");

    $.ajax({
        type: "POST",
        url: url_p+"Cproveedor/paginacion",
        data: ob,
        beforeSend: function(objeto)
        {
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data)
        {
          $("#panel_paginacion_datos").html(data);
        }
    });
}


 function buscar_datos(page)
 {  
    var txt_buscar = $("#txt_buscar").val();
    var id_empresa = $("#id_empresa_select").val();
    var id_gestion = $("#id_gestion_select").val();

    var cadena = " Resultado para : <label> "+txt_buscar+" </label> ";
    $("#panel_resultado_busqueda").html(cadena);
    
    var ob = { page:page, txt_buscar:txt_buscar, id_empresa:id_empresa, id_gestion:id_gestion };

    $.ajax({
        type: "POST",
        url: url_p+"Cproveedor/paginacion_find",
        data: ob,
        beforeSend: function(objeto)
        {
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data)
        {
          $("#panel_paginacion_datos").html(data);
        }
    });
}

function proveedor_o( proveedor,encargado,telefonos,celulares,whatsapp,direccion,correo )
{ 
   this.proveedor = proveedor; 
   this.encargado = encargado; 
   this.telefonos = telefonos; 
   this.celulares = celulares; 
   this.whatsapp = whatsapp; 
   this.direccion = direccion; 
   this.correo = correo;  
}

function btn_nuevo_proveedor()
{
  $("#myModal_Register").modal("show");
 
  var ob = "";
  $.ajax({
      type: "POST",
      url: url_p+"Cproveedor/R_proveedor",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar").html(data);
      }
     });
}

function btn_agregar_carrito_proveedor()
{ 
    var proveedor = $("#proveedor").val();
    var encargado = $("#encargado").val();
    var telefonos = $("#telefonos").val();
    var celulares = $("#celulares").val();
    var whatsapp = $("#whatsapp").val();
    var direccion = $("#direccion").val();
    var correo = $("#correo").val(); 
  
  if(proveedor != "" && encargado != "" && telefonos != "" && celulares != "" && whatsapp != "" && direccion != "" && correo != "" )
  {
  
  var obj_proveedor = new proveedor_o( proveedor,  encargado,  telefonos,  celulares,  whatsapp,  direccion,  correo );

  lista.push(obj_proveedor);
  btn_mostrar_datos_proveedor();
  }

  else {
  
       if (proveedor =="")
       {  
           $("#proveedor").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de proveedor </label>";
           $("#panel_resp_proveedor").html(res);  
       }

       
       if (encargado =="")
       {  
           $("#encargado").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de encargado </label>";
           $("#panel_resp_encargado").html(res);  
       }

       
       if (telefonos =="")
       {  
           $("#telefonos").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de telefonos </label>";
           $("#panel_resp_telefonos").html(res);  
       }

       
       if (celulares =="")
       {  
           $("#celulares").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de celulares </label>";
           $("#panel_resp_celulares").html(res);  
       }

       
       if (whatsapp =="")
       {  
           $("#whatsapp").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de whatsapp </label>";
           $("#panel_resp_whatsapp").html(res);  
       }

       
       if (direccion =="")
       {  
           $("#direccion").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de direccion </label>";
           $("#panel_resp_direccion").html(res);  
       }

       
       if (correo =="")
       {  
           $("#correo").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de correo </label>";
           $("#panel_resp_correo").html(res);  
       }

       
  //Final de la codicional else
  }

 //Final de la fucion
}

function btn_mostrar_datos_proveedor()
{
  var cadena = "<table class='table table-bordered table-condensed table-striped table-hover'>";
  
  cadena = cadena + "<tr>"+ "<th> proveedor </th> " +  "<th> encargado </th> " +  "<th> telefonos </th> " +  "<th> celulares </th> " +  "<th> whatsapp </th> " +  "<th> direccion </th> " +  "<th> correo </th> " +  "<th> Opciones </th>"; 

  cadena = cadena +"</tr>";

  for(var i = 0 ; i<lista.length; i++)
  {
    proveedor = lista[i].proveedor; 
    encargado = lista[i].encargado; 
    telefonos = lista[i].telefonos; 
    celulares = lista[i].celulares; 
    whatsapp = lista[i].whatsapp; 
    direccion = lista[i].direccion; 
    correo = lista[i].correo; 
      cadena = cadena + "<tr>"+
    "<td>"+ proveedor +"</td>"+
    "<td>"+ encargado +"</td>"+
    "<td>"+ telefonos +"</td>"+
    "<td>"+ celulares +"</td>"+
    "<td>"+ whatsapp +"</td>"+
    "<td>"+ direccion +"</td>"+
    "<td>"+ correo +"</td>"+ " <td  class='col-lg-1' > <center> <button class='btn btn-danger btn-xs' onclick='btn_eliminar_item("+i+");' > X </button> </center> </td><tr>"; 
   }

   cadena = cadena +" </table> ";

   $("#panel_listado_proveedor").html(cadena);  
 
} 

function btn_eliminar_item(contador)
{
  lista.splice( contador , 1);
  btn_mostrar_datos_proveedor();
}

function btn_registrar_proveedor()
{ 
   var id_usuario = $("#id_usuario_session").val(); 
   var id_empresa = $("#id_empresa_select").val();
   var id_gestion = $("#id_gestion_select").val();

   var lista_area = JSON.stringify(lista);
   var ob = {lista : lista_area, id_usuario:id_usuario, id_empresa:id_empresa,
   id_gestion:id_gestion };

  
  $.ajax({
      type: "POST",
      url: url_p+"Cproveedor/G_proveedor",
      data: ob,
      beforeSend: function(objeto){

        $("#panel_modal_resultado_registrar").html("<div class='cargando'>  </div>");
        //$("#btn_registrar").prop("disabled", true);

      },
      success:function(data)
      { 
        //$("#btn_registrar").prop("disabled", false);   
        $("#panel_modal_resultado_registrar").html(data);

      }
  });
}

function btn_examinar_proveedor(id_proveedor)
{
  $("#myModal_View").modal("show");

  var ob = {id_proveedor : id_proveedor};
  $.ajax({
      type: "POST",
      url: url_p+"Cproveedor/VD_proveedor",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_examinar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_examinar").html(data);
      }
     });

}

function btn_editar_proveedor(id_proveedor)
{
  $("#myModal_Update").modal("show");
  $("#id_proveedor_edicion").val(id_proveedor);
  
  var ob = {id_proveedor : id_proveedor};
  $.ajax({
      type: "POST",
      url: url_p+"Cproveedor/VE_proveedor",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_editar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_editar").html(data);
      }
     });

}

function btn_guardar_proveedor()
{

  var id_proveedor = $("#id_proveedor_edicion").val();

  var proveedor = $("#proveedor").val();
  var encargado = $("#encargado").val();
  var telefonos = $("#telefonos").val();
  var celulares = $("#celulares").val();
  var whatsapp = $("#whatsapp").val();
  var direccion = $("#direccion").val();
  var correo = $("#correo").val();
  var ob = { id_proveedor : id_proveedor, proveedor:proveedor, encargado:encargado, telefonos:telefonos, celulares:celulares, whatsapp:whatsapp, direccion:direccion, correo:correo }; 

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_proveedor", id_proveedor);

  $.ajax({
   url: url_p+"Cproveedor/VU_proveedor",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){

     $("#btn_editar").prop("disabled", true);
     $("#panel_modal_respuesta_editar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_editar").prop("disabled", false);  
     $("#panel_modal_respuesta_editar").html(data);
   
   }
 }); 

}

function btn_borrar_proveedor(id_proveedor)
{
  $("#myModal_Delete").modal("show");
  $("#id_proveedor_eliminar").val(id_proveedor);

}

function btn_eliminar_proveedor()
{
  var id_proveedor = $("#id_proveedor_eliminar").val();

  var ob = { id_proveedor : id_proveedor };
  
  $.ajax({
      type: "POST",
      url: url_p+"Cproveedor/VB_proveedor",
      data: ob,
      beforeSend: function(objeto){

        $("#btn_borrar").prop("disabled", true);
        $("#panel_modal_eliminar").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        $("#btn_borrar").prop("disabled", false);   
        $("#panel_modal_eliminar").html(data);
        
        setTimeout(function(){ $("#panel_modal_eliminar").html(""); },1000);
        setTimeout(function(){ $("#myModal_Delete").modal("show"); },2000);
        setTimeout(function(){ location.reload(); },1000);
      }
  });

}


var estado = 0;

function btn_submenu(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_"+id).css("display","none");
  }
  }

}

function btn_submenu_xs(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_xs"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_xs"+id).css("display","none");
  }
  }

}


function select(id,dato,area)
{   
  $("#id_"+area).val(id);
  $("#"+area).html(dato);
}


