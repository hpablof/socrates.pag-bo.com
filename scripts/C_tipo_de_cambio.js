/* CONTROLADOR DE AREA DEL SISTEMA */
var url_p = "https://socrates.pag-bo.com/";
var lista = [];

 function cargar_datos(page)
 {  
    var ob = {page:page};
    $("#panel_resultado_busqueda").html("");

    $.ajax({
        type: "POST",
        url: url_p+"Ctipo_de_cambio/paginacion",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}


 function buscar_datos(page)
 {  
    var txt_buscar = $("#txt_buscar").val();

    var cadena = " Resultado para : <label> "+txt_buscar+" </label> ";
    $("#panel_resultado_busqueda").html(cadena);
    
    var ob = {page:page,txt_buscar : txt_buscar };

    $.ajax({
        type: "POST",
        url: url_p+"Ctipo_de_cambio/paginacion_find",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}

function tipo_de_cambio_o( fecha_tc,dolar_tc,ufv_tc )
{ 
   this.fecha_tc = fecha_tc; 
   this.dolar_tc = dolar_tc; 
   this.ufv_tc = ufv_tc; 
  
}

function btn_nuevo_tipo_de_cambio()
{
  $("#myModal_Register").modal("show");
 
  var ob = "";
  $.ajax({
      type: "POST",
      url: url_p+"Ctipo_de_cambio/R_tipo_de_cambio",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar").html(data);
      }
     });
}

function btn_agregar_carrito_tipo_de_cambio()
{ 
    var fecha_tc = $("#fecha_tc").val();
    var dolar_tc = $("#dolar_tc").val();
    var ufv_tc = $("#ufv_tc").val(); 
  
  if(fecha_tc != "" && dolar_tc != "" && ufv_tc != "" )
  {
  
  var obj_tipo_de_cambio = new tipo_de_cambio_o( fecha_tc,  dolar_tc,  ufv_tc );

  lista.push(obj_tipo_de_cambio);
  btn_mostrar_datos_tipo_de_cambio();
  }

  else {
  
       if (fecha_tc =="")
       {  
           $("#fecha_tc").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de fecha_tc </label>";
           $("#panel_resp_fecha_tc").html(res);  
       }

       
       if (dolar_tc =="")
       {  
           $("#dolar_tc").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de dolar_tc </label>";
           $("#panel_resp_dolar_tc").html(res);  
       }

       
       if (ufv_tc =="")
       {  
           $("#ufv_tc").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de ufv_tc </label>";
           $("#panel_resp_ufv_tc").html(res);  
       }

       
  //Final de la codicional else
  }

 //Final de la fucion
}

function btn_mostrar_datos_tipo_de_cambio()
{
  var cadena = "<table class='table table-bordered table-condensed table-striped table-hover'>";
  
  cadena = cadena + "<tr>"+ "<th> fecha tc </th> " +  
  "<th> dolar tc </th> " +  "<th> ufv tc </th> " +  "<th> Opciones </th>"; 

  cadena = cadena +"</tr>";

  for(var i = 0 ; i<lista.length; i++)
  {
    var fecha_tc = lista[i].fecha_tc; 
    var dolar_tc = lista[i].dolar_tc; 
    var ufv_tc = lista[i].ufv_tc; 

    var cadena = cadena + "<tr>"+
    "<td>"+ fecha_tc +"</td>"+
    "<td>"+ dolar_tc +"</td>"+
    "<td>"+ ufv_tc +"</td>"+ " <td  class='col-lg-1' > <center> <button class='btn btn-danger btn-xs' onclick='btn_eliminar_item("+i+");' > X </button> </center> </td><tr>"; 
   }

   cadena = cadena +" </table> ";

   $("#panel_listado_tipo_de_cambio").html(cadena);  
 
} 

function btn_eliminar_item(contador)
{
  lista.splice( contador, 1);
  btn_mostrar_datos_tipo_de_cambio();
}

function btn_registrar_tipo_de_cambio()
{ 
  var id_usuario = $("#id_usuario_session").val(); 

   var lista_area = JSON.stringify(lista);
   var ob = {lista : lista_area, id_usuario:id_usuario };

  
  $.ajax({
      type: "POST",
      url: url_p+"Ctipo_de_cambio/G_tipo_de_cambio",
      data: ob,
      beforeSend: function(objeto){

        $("#panel_modal_resultado_registrar").html("<div class='cargando'>  </div>");
        $("#btn_registrar").prop("disabled", true);

      },
      success:function(data)
      { 
        $("#btn_registrar").prop("disabled", false);   
        $("#panel_modal_resultado_registrar").html(data);
        
        setTimeout(function(){ $("#panel_modal_resultado_registrar").html(""); },1000);
        setTimeout(function(){ $("#myModal_Register").modal("hide"); },2000);
        setTimeout(function(){ location.reload(); },3000);
      }
  });
}

function btn_examinar_tipo_de_cambio(id_tipo_de_cambio)
{
  $("#myModal_View").modal("show");

  var ob = {id_tipo_de_cambio : id_tipo_de_cambio};
  $.ajax({
      type: "POST",
      url: url_p+"Ctipo_de_cambio/VD_tipo_de_cambio",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_examinar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_examinar").html(data);
      }
     });

}

function btn_editar_tipo_de_cambio(id_tipo_de_cambio)
{
  $("#myModal_Update").modal("show");
  $("#id_tipo_de_cambio_edicion").val(id_tipo_de_cambio);
  
  var ob = {id_tipo_de_cambio : id_tipo_de_cambio};
  $.ajax({
      type: "POST",
      url: url_p+"Ctipo_de_cambio/VE_tipo_de_cambio",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_editar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_editar").html(data);
      }
     });

}

function btn_guardar_tipo_de_cambio()
{

  var id_tipo_de_cambio = $("#id_tipo_de_cambio_edicion").val();

  var fecha_tc = $("#fecha_tc").val();
  var dolar_tc = $("#dolar_tc").val();
  var ufv_tc = $("#ufv_tc").val();
  var ob = { id_tipo_de_cambio : id_tipo_de_cambio, fecha_tc:fecha_tc, dolar_tc:dolar_tc, ufv_tc:ufv_tc }; 

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_tipo_de_cambio", id_tipo_de_cambio);

  $.ajax({
   url: url_p+"Ctipo_de_cambio/VU_tipo_de_cambio",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){

     $("#btn_editar").prop("disabled", true);
     $("#panel_modal_respuesta_editar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_editar").prop("disabled", false);  
     $("#panel_modal_respuesta_editar").html(data);
   
   }
 }); 

}

function btn_borrar_tipo_de_cambio(id_tipo_de_cambio)
{
  $("#myModal_Delete").modal("show");
  $("#id_tipo_de_cambio_eliminar").val(id_tipo_de_cambio);

}

function btn_eliminar_tipo_de_cambio()
{
  var id_tipo_de_cambio = $("#id_tipo_de_cambio_eliminar").val();

  var ob = { id_tipo_de_cambio : id_tipo_de_cambio };
  
  $.ajax({
      type: "POST",
      url: url_p+"Ctipo_de_cambio/VB_tipo_de_cambio",
      data: ob,
      beforeSend: function(objeto){

        $("#btn_borrar").prop("disabled", true);
        $("#panel_modal_eliminar").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        $("#btn_borrar").prop("disabled", false);   
        $("#panel_modal_eliminar").html(data);
        
        setTimeout(function(){ $("#panel_modal_eliminar").html(""); },1000);
        setTimeout(function(){ $("#myModal_Delete").modal("show"); },2000);
        setTimeout(function(){ location.reload(); },1000);
      }
  });

}


var estado = 0;

function btn_submenu(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_"+id).css("display","none");
  }
  }

}

function btn_submenu_xs(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_xs"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_xs"+id).css("display","none");
  }
  }

}


function select(id,dato,area)
{   
  $("#id_"+area).val(id);
  $("#"+area).html(dato);
}


