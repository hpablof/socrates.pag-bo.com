/* CONTROLADOR DE AREA DEL SISTEMA */
var url_p = "https://socrates.pag-bo.com/";
var lista = [];


 function cargar_datos(page)
 {  
    var ob = {page:page};
    $("#panel_resultado_busqueda").html("");

    $.ajax({
        type: "POST",
        url: url_p+"Cusuario/paginacion",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}


 function buscar_datos(page)
 {  
    var txt_buscar = $("#txt_buscar").val();

    var cadena = " Resultado para : <label> "+txt_buscar+" </label> ";
    $("#panel_resultado_busqueda").html(cadena);
    
    var ob = {page:page,txt_buscar : txt_buscar };

    $.ajax({
        type: "POST",
        url: url_p+"Cusuario/paginacion_find",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}

function usuario_o( alias,email,password,id_cargo_usuario,cargo_usuario,nombres,apellidos,ci )
{ 
   this.alias = alias; 
   this.email = email; 
   this.password = password; 
   this.id_cargo_usuario = id_cargo_usuario; 
   this.cargo_usuario = cargo_usuario;
   this.nombres = nombres; 
   this.apellidos = apellidos; 
   this.ci = ci; 
  
}

function btn_nuevo_usuario()
{
  $("#myModal_Register").modal("show");
 
  var ob = "";
  $.ajax({
      type: "POST",
      url: url_p+"Cusuario/R_usuario",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar").html(data);
      }
     });
}

function btn_agregar_carrito_usuario()
{ 
    var alias = $("#alias").val();
    var email = $("#email").val();
    var password = $("#password").val();
    var id_cargo_usuario = $("#id_cargo_usuario").val();
    var cargo_usuario = $("#cargo_usuario").text();
    var nombres = $("#nombres").val();
    var apellidos = $("#apellidos").val();
    var ci = $("#ci").val(); 
 
  if(alias != "" && email != "" && password != "" && id_cargo_usuario != "" && nombres != "" && apellidos != "" && ci != "" )
  {
  
     var obj_usuario = new usuario_o (alias,email,password,id_cargo_usuario,
                                      cargo_usuario,nombres,apellidos,ci);

     lista.push(obj_usuario);

     $("#alias").val("");
     $("#email").val("");
     $("#password").val("");
     $("#nombres").val("");
     $("#apellidos").val("");
     $("#ci").val(""); 

     btn_mostrar_datos_usuario();
  }

  else {
  
       if (alias =="")
       {  
           $("#alias").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de alias </label>";
           $("#panel_resp_alias").html(res);  
       }

       
       if (email =="")
       {  
           $("#email").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de email </label>";
           $("#panel_resp_email").html(res);  
       }

       
       if (password =="")
       {  
           $("#password").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de password </label>";
           $("#panel_resp_password").html(res);  
       }

       
       if (id_cargo_usuario =="")
       {  
           $("#id_cargo_usuario").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de cargo_usuario </label>";
           $("#panel_resp_id_cargo_usuario").html(res);  
       }

       
       if (nombres =="")
       {  
           $("#nombres").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de nombres </label>";
           $("#panel_resp_nombres").html(res);  
       }

       
       if (apellidos =="")
       {  
           $("#apellidos").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de apellidos </label>";
           $("#panel_resp_apellidos").html(res);  
       }

       
       if (ci =="")
       {  
           $("#ci").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de ci </label>";
           $("#panel_resp_ci").html(res);  
       }

       
  //Final de la codicional else
  }

 //Final de la fucion
}

function btn_mostrar_datos_usuario()
{
  var cadena = "<table class='table table-bordered table-condensed table-striped table-hover'>";
  
  cadena = cadena + "<tr>"+ "<th>alias</th>"+"<th>email</th>"+"<th>password</th>"+
  "<th>cargo</th>"+"<th>nombres</th>"+"<th>apellidos</th>"+"<th>ci</th>"+"<th> Opciones </th>"; 

  cadena = cadena +"</tr>";

  for(var i = 0 ; i<lista.length; i++)
  {
    alias = lista[i].alias; 
    email = lista[i].email; 
    password = lista[i].password; 
    cargo_usuario = lista[i].cargo_usuario; 
    nombres = lista[i].nombres; 
    apellidos = lista[i].apellidos; 
    ci = lista[i].ci; 
      cadena = cadena + "<tr>"+
    "<td>"+ alias +"</td>"+
    "<td>"+ email +"</td>"+
    "<td>"+ password +"</td>"+
    "<td>"+ cargo_usuario +"</td>"+
    "<td>"+ nombres +"</td>"+
    "<td>"+ apellidos +"</td>"+
    "<td>"+ ci +"</td>"+ " <td  class='col-lg-1' > <center> <button class='btn btn-danger btn-xs' onclick='btn_eliminar_item("+i+");' > X </button> </center> </td><tr>"; 
   }

   cadena = cadena +" </table> ";

   $("#panel_listado_usuario").html(cadena);  
 
} 

function btn_eliminar_item(contador)
{
  lista.splice( contador , 1);
  btn_mostrar_datos_usuario();
}

function btn_registrar_usuario()
{ 
   var id_usuario = $("#id_usuario_session").val(); 

   var lista_area = JSON.stringify(lista);
   var ob = {lista : lista_area, id_usuario:id_usuario };

  
  $.ajax({
      type: "POST",
      url: url_p+"Cusuario/G_usuario",
      data: ob,
      beforeSend: function(objeto){

        $("#panel_modal_resultado_registrar").html("<div class='cargando'>  </div>");
        $("#btn_registrar").prop("disabled", true);

      },
      success:function(data)
      { 
        $("#btn_registrar").prop("disabled", false);   
        $("#panel_modal_resultado_registrar").html(data);
        
      }
  });
}

function btn_examinar_usuario(id_usuario)
{
  $("#myModal_View").modal("show");

  var ob = {id_usuario : id_usuario};
  $.ajax({
      type: "POST",
      url: url_p+"Cusuario/VD_usuario",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_examinar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_examinar").html(data);
      }
     });

}

function btn_editar_usuario(id_usuario)
{
  $("#myModal_Update").modal("show");
  $("#id_usuario_edicion").val(id_usuario);
  
  var ob = {id_usuario : id_usuario};
  $.ajax({
      type: "POST",
      url: url_p+"Cusuario/VE_usuario",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_editar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_editar").html(data);
      }
     });

}

function btn_guardar_usuario()
{

  var id_usuario = $("#id_usuario_edicion").val();

  var alias = $("#alias").val();
  var email = $("#email").val();
  var password = $("#password").val();
  var id_cargo_usuario = $("#id_cargo_usuario").val();
  var nombres = $("#nombres").val();
  var apellidos = $("#apellidos").val();
  var ci = $("#ci").val();
  var ob = { id_usuario : id_usuario, alias:alias, email:email, password:password, id_cargo_usuario:id_cargo_usuario, nombres:nombres, apellidos:apellidos, ci:ci }; 

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_usuario", id_usuario);

  $.ajax({
   url: url_p+"Cusuario/VU_usuario",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){

     $("#btn_editar").prop("disabled", true);
     $("#panel_modal_respuesta_editar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_editar").prop("disabled", false);  
     $("#panel_modal_respuesta_editar").html(data);
   
   }
 }); 

}

function btn_borrar_usuario(id_usuario)
{
  $("#myModal_Delete").modal("show");
  $("#id_usuario_eliminar").val(id_usuario);

}

function btn_eliminar_usuario()
{
  var id_usuario = $("#id_usuario_eliminar").val();

  var ob = { id_usuario : id_usuario };
  
  $.ajax({
      type: "POST",
      url: url_p+"Cusuario/VB_usuario",
      data: ob,
      beforeSend: function(objeto){

        $("#btn_borrar").prop("disabled", true);
        $("#panel_modal_eliminar").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        $("#btn_borrar").prop("disabled", false);   
        $("#panel_modal_eliminar").html(data);
        
        setTimeout(function(){ $("#panel_modal_eliminar").html(""); },1000);
        setTimeout(function(){ $("#myModal_Delete").modal("show"); },2000);
        setTimeout(function(){ location.reload(); },1000);
      }
  });

}


var estado = 0;

function btn_submenu(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_"+id).css("display","none");
  }
  }

}

function btn_submenu_xs(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_xs"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_xs"+id).css("display","none");
  }
  }

}


function select(id,dato,area)
{   
  $("#id_"+area).val(id);
  $("#"+area).html(dato);
}


