
/* CONTROLADOR DE AREA DEL SISTEMA */
var url_p = "https://socrates.pag-bo.com/";
var lista = [];


 function cargar_datos(page)
 {  
    var ob = {page:page};
    $("#panel_resultado_busqueda").html("");

    $.ajax({
        type: "POST",
        url: url_p+"Ccuenta_parametrizadas/paginacion",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}


 function buscar_datos(page)
 {  
    var txt_buscar = $("#txt_buscar").val();

    var cadena = " Resultado para : <label> "+txt_buscar+" </label> ";
    $("#panel_resultado_busqueda").html(cadena);
    
    var ob = {page:page,txt_buscar : txt_buscar };

    $.ajax({
        type: "POST",
        url: url_p+"Ccuenta_parametrizadas/paginacion_find",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}

function cuenta_parametrizadas_o( cuenta_cp,codigo_cp,plan_cuenta_cp )
{ 
 this.cuenta_cp = cuenta_cp; 
   this.codigo_cp = codigo_cp; 
   this.plan_cuenta_cp = plan_cuenta_cp; 
  
}

function btn_nuevo_cuenta_parametrizadas()
{
  $("#myModal_Register").modal("show");
 
  var ob = "";
  $.ajax({
      type: "POST",
      url: url_p+"Ccuenta_parametrizadas/R_cuenta_parametrizadas",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar").html(data);
      }
     });
}

function btn_agregar_carrito_cuenta_parametrizadas()
{ 
    var cuenta_cp = $("#cuenta_cp").val();
    var codigo_cp = $("#codigo_cp").val();
    var plan_cuenta_cp = $("#plan_cuenta_cp").val(); 
  
  if(cuenta_cp != "" && codigo_cp != "" && plan_cuenta_cp != "" )
  {
  
  var obj_cuenta_parametrizadas = new cuenta_parametrizadas_o( cuenta_cp,  codigo_cp,  plan_cuenta_cp );

  lista.push(obj_cuenta_parametrizadas);
  btn_mostrar_datos_cuenta_parametrizadas();
  }

  else {
  
       if (cuenta_cp =="")
       {  
           $("#cuenta_cp").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de cuenta_cp </label>";
           $("#panel_resp_cuenta_cp").html(res);  
       }

       
       if (codigo_cp =="")
       {  
           $("#codigo_cp").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de codigo_cp </label>";
           $("#panel_resp_codigo_cp").html(res);  
       }

       
       if (plan_cuenta_cp =="")
       {  
           $("#plan_cuenta_cp").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de plan_cuenta_cp </label>";
           $("#panel_resp_plan_cuenta_cp").html(res);  
       }

       
  //Final de la codicional else
  }

 //Final de la fucion
}

function btn_mostrar_datos_cuenta_parametrizadas()
{
  var cadena = "<table class='table table-bordered table-condensed table-striped table-hover'>";
  
  cadena = cadena + "<tr>"+ "<th> cuenta_cp </th> " +  "<th> codigo_cp </th> " +  "<th> plan_cuenta_cp </th> " +  "<th> Opciones </th>"; 

  cadena = cadena +"</tr>";

  for(var i = 0 ; i<lista.length; i++)
  {
    cuenta_cp = lista[i].cuenta_cp; 
    codigo_cp = lista[i].codigo_cp; 
    plan_cuenta_cp = lista[i].plan_cuenta_cp; 
      cadena = cadena + "<tr>"+
    "<td>"+ cuenta_cp +"</td>"+
    "<td>"+ codigo_cp +"</td>"+
    "<td>"+ plan_cuenta_cp +"</td>"+ " <td  class='col-lg-1' > <center> <button class='btn btn-danger btn-xs' onclick='btn_eliminar_item("+i+");' > X </button> </center> </td><tr>"; 
   }

   cadena = cadena +" </table> ";

   $("#panel_listado_cuenta_parametrizadas").html(cadena);  
 
} 

function btn_eliminar_item(contador)
{
  lista.splice( contador , 1);
  btn_mostrar_datos_cuenta_parametrizadas();
}

function btn_registrar_cuenta_parametrizadas()
{ 
  var id_usuario = $("#id_usuario_session").val(); 

   var cuenta_cp = $("#cuenta_cp").val();
   var codigo_cp = $("#codigo_cp").val();
   var plan_cuenta_cp = $("#plan_cuenta_cp").val();

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_usuario", id_usuario);

  $.ajax({
   url: url_p+"Ccuenta_parametrizadas/G_cuenta_parametrizadas",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){
     
     $("#btn_registrar").prop("disabled", true); 
     $("#panel_modal_resultado_registrar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_registrar").prop("disabled", false);  
     $("#panel_modal_resultado_registrar").html(data);
 
   }
 });  


}

function btn_examinar_cuenta_parametrizadas(id_cuenta_parametrizadas)
{
  $("#myModal_View").modal("show");

  var ob = {id_cuenta_parametrizadas : id_cuenta_parametrizadas};
  $.ajax({
      type: "POST",
      url: url_p+"Ccuenta_parametrizadas/VD_cuenta_parametrizadas",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_examinar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_examinar").html(data);
      }
     });

}

function btn_editar_cuenta_parametrizadas(id_cuenta_parametrizadas)
{
  $("#myModal_Update").modal("show");
  $("#id_cuenta_parametrizadas_edicion").val(id_cuenta_parametrizadas);
  
  var ob = {id_cuenta_parametrizadas : id_cuenta_parametrizadas};
  $.ajax({
      type: "POST",
      url: url_p+"Ccuenta_parametrizadas/VE_cuenta_parametrizadas",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_editar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_editar").html(data);
      }
     });

}

function btn_guardar_cuenta_parametrizadas()
{

  var id_cuenta_parametrizadas = $("#id_cuenta_parametrizadas_edicion").val();

  var cuenta_cp = $("#cuenta_cp").val();
  var codigo_cp = $("#codigo_cp").val();
  var plan_cuenta_cp = $("#plan_cuenta_cp").val();
  var ob = { id_cuenta_parametrizadas : id_cuenta_parametrizadas, cuenta_cp:cuenta_cp, codigo_cp:codigo_cp, plan_cuenta_cp:plan_cuenta_cp }; 

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_cuenta_parametrizadas", id_cuenta_parametrizadas);

  $.ajax({
   url: url_p+"Ccuenta_parametrizadas/VU_cuenta_parametrizadas",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){

     $("#btn_editar").prop("disabled", true);
     $("#panel_modal_respuesta_editar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_editar").prop("disabled", false);  
     $("#panel_modal_respuesta_editar").html(data);
   
   }
 }); 

}

function btn_borrar_cuenta_parametrizadas(id_cuenta_parametrizadas)
{
  $("#myModal_Delete").modal("show");
  $("#id_cuenta_parametrizadas_eliminar").val(id_cuenta_parametrizadas);

}

function btn_eliminar_cuenta_parametrizadas()
{
  var id_cuenta_parametrizadas = $("#id_cuenta_parametrizadas_eliminar").val();

  var ob = { id_cuenta_parametrizadas : id_cuenta_parametrizadas };
  
  $.ajax({
      type: "POST",
      url: url_p+"Ccuenta_parametrizadas/VB_cuenta_parametrizadas",
      data: ob,
      beforeSend: function(objeto){

        $("#btn_borrar").prop("disabled", true);
        $("#panel_modal_eliminar").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        $("#btn_borrar").prop("disabled", false);   
        $("#panel_modal_eliminar").html(data);
        
        setTimeout(function(){ $("#panel_modal_eliminar").html(""); },1000);
        setTimeout(function(){ $("#myModal_Delete").modal("show"); },2000);
        setTimeout(function(){ location.reload(); },1000);
      }
  });

}


var estado = 0;

function btn_submenu(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_"+id).css("display","none");
  }
  }

}

function btn_submenu_xs(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_xs"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_xs"+id).css("display","none");
  }
  }

}


function select(id,dato,area)
{   
  $("#id_"+area).val(id);
  $("#"+area).html(dato);
}


