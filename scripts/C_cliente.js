
/* CONTROLADOR DE AREA DEL SISTEMA */
var lista = [];


 function cargar_datos(page)
 {  
    var id_empresa = $("#id_empresa_select").val();
    var id_gestion = $("#id_gestion_select").val();

    var ob = { page:page, id_empresa:id_empresa, id_gestion:id_gestion };
    
    $("#panel_resultado_busqueda").html("");

    $.ajax({
        type: "POST",
        url: url_p+"Ccliente/paginacion",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}


 function buscar_datos(page)
 {  
    var txt_buscar = $("#txt_buscar").val();
    var id_empresa = $("#id_empresa_select").val();
    var id_gestion = $("#id_gestion_select").val();

    var cadena = " Resultado para : <label> "+txt_buscar+" </label> ";
    $("#panel_resultado_busqueda").html(cadena);
    
    var ob = {page:page,txt_buscar : txt_buscar, id_empresa:id_empresa, id_gestion:id_gestion };

    $.ajax({
        type: "POST",
        url: url_p+"Ccliente/paginacion_find",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}

function cliente_o( cliente,ci_nit,celular_cl,whatsapp_cl,direccion_cl,correo_cl )
{ 
   this.cliente = cliente; 
   this.ci_nit = ci_nit; 
   this.celular_cl = celular_cl; 
   this.whatsapp_cl = whatsapp_cl; 
   this.direccion_cl = direccion_cl; 
   this.correo_cl = correo_cl; 
  
}

function btn_nuevo_cliente()
{
  $("#myModal_Register").modal("show");
 
  var ob = "";
  $.ajax({
      type: "POST",
      url: url_p+"Ccliente/R_cliente",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar").html(data);
      }
     });
}

function btn_agregar_carrito_cliente()
{ 
    var cliente = $("#cliente").val();
    var ci_nit = $("#ci_nit").val();
    var celular_cl = $("#celular_cl").val();
    var whatsapp_cl = $("#whatsapp_cl").val();
    var direccion_cl = $("#direccion_cl").val();
    var correo_cl = $("#correo_cl").val(); 
  
  if(cliente != "" && ci_nit != "" && celular_cl != "" && whatsapp_cl != "" && direccion_cl != "" && correo_cl != "" )
  {
  
  var obj_cliente = new cliente_o( cliente,  ci_nit,  celular_cl,  whatsapp_cl,  direccion_cl,  correo_cl );

  lista.push(obj_cliente);
  btn_mostrar_datos_cliente();
  }

  else {
  
       if (cliente =="")
       {  
           $("#cliente").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de cliente </label>";
           $("#panel_resp_cliente").html(res);  
       }

       
       if (ci_nit =="")
       {  
           $("#ci_nit").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de ci_nit </label>";
           $("#panel_resp_ci_nit").html(res);  
       }

       
       if (celular_cl =="")
       {  
           $("#celular_cl").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de celular_cl </label>";
           $("#panel_resp_celular_cl").html(res);  
       }

       
       if (whatsapp_cl =="")
       {  
           $("#whatsapp_cl").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de whatsapp_cl </label>";
           $("#panel_resp_whatsapp_cl").html(res);  
       }

       
       if (direccion_cl =="")
       {  
           $("#direccion_cl").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de direccion_cl </label>";
           $("#panel_resp_direccion_cl").html(res);  
       }

       
       if (correo_cl =="")
       {  
           $("#correo_cl").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de correo_cl </label>";
           $("#panel_resp_correo_cl").html(res);  
       }

       
  //Final de la codicional else
  }

 //Final de la fucion
}

function btn_mostrar_datos_cliente()
{
  var cadena = "<table class='table table-bordered table-condensed table-striped table-hover'>";
  
  cadena = cadena + "<tr>"+ "<th> cliente </th> "+"<th>ci/nit</th>"+"<th>celular</th>"+
  "<th> whatsapp </th>"+"<th> direccion </th>"+"<th> correo </th>"+"<th> Opciones </th>"; 

  cadena = cadena +"</tr>";

  for(var i = 0 ; i<lista.length; i++)
  {
    cliente = lista[i].cliente; 
    ci_nit = lista[i].ci_nit; 
    celular_cl = lista[i].celular_cl; 
    whatsapp_cl = lista[i].whatsapp_cl; 
    direccion_cl = lista[i].direccion_cl; 
    correo_cl = lista[i].correo_cl; 
      cadena = cadena + "<tr>"+
    "<td>"+ cliente +"</td>"+
    "<td>"+ ci_nit +"</td>"+
    "<td>"+ celular_cl +"</td>"+
    "<td>"+ whatsapp_cl +"</td>"+
    "<td>"+ direccion_cl +"</td>"+
    "<td>"+ correo_cl +"</td>"+ " <td  class='col-lg-1' > <center> <button class='btn btn-danger btn-xs' onclick='btn_eliminar_item("+i+");' > X </button> </center> </td><tr>"; 
   }

   cadena = cadena +" </table> ";

   $("#panel_listado_cliente").html(cadena);  
 
} 

function btn_eliminar_item(contador)
{
  lista.splice(contador, 1);
  btn_mostrar_datos_cliente();
}

function btn_registrar_cliente()
{ 
  var id_usuario = $("#id_usuario_session").val(); 
  var id_empresa = $("#id_empresa_select").val();
  var id_gestion = $("#id_gestion_select").val();

  var lista_area = JSON.stringify(lista);
  var ob = { lista : lista_area, id_usuario:id_usuario, id_empresa:id_empresa, id_gestion:id_gestion };

  $.ajax({
      type: "POST",
      url: url_p+"Ccliente/G_cliente",
      data: ob,
      beforeSend: function(objeto){

        $("#panel_modal_resultado_registrar").html("<div class='cargando'>  </div>");
        //$("#btn_registrar").prop("disabled", true);

      },
      success:function(data)
      { 
        //$("#btn_registrar").prop("disabled", false);   
        $("#panel_modal_resultado_registrar").html(data);
        
         
      }
  });
}

function btn_examinar_cliente(id_cliente)
{
  $("#myModal_View").modal("show");

  var ob = {id_cliente : id_cliente};
  $.ajax({
      type: "POST",
      url: url_p+"Ccliente/VD_cliente",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_examinar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_examinar").html(data);
      }
     });
}

function btn_editar_cliente(id_cliente)
{
  $("#myModal_Update").modal("show");
  $("#id_cliente_edicion").val(id_cliente);
  
  var ob = {id_cliente : id_cliente};
  $.ajax({
      type: "POST",
      url: url_p+"Ccliente/VE_cliente",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_editar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_editar").html(data);
      }
     });

}

function btn_guardar_cliente()
{
  var id_cliente = $("#id_cliente_edicion").val();

  var cliente = $("#cliente").val();
  var ci_nit = $("#ci_nit").val();
  var celular_cl = $("#celular_cl").val();
  var whatsapp_cl = $("#whatsapp_cl").val();
  var direccion_cl = $("#direccion_cl").val();
  var correo_cl = $("#correo_cl").val();
  var ob = { id_cliente : id_cliente, cliente:cliente, ci_nit:ci_nit, celular_cl:celular_cl, whatsapp_cl:whatsapp_cl, direccion_cl:direccion_cl, correo_cl:correo_cl }; 

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_cliente", id_cliente);

  $.ajax({
   url: url_p+"Ccliente/VU_cliente",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){

     $("#btn_editar").prop("disabled", true);
     $("#panel_modal_respuesta_editar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_editar").prop("disabled", false);  
     $("#panel_modal_respuesta_editar").html(data);
   
   }
 }); 

}

function btn_borrar_cliente(id_cliente)
{ 

  $("#myModal_Delete").modal("show");
  $("#id_cliente_eliminar").val(id_cliente);

}

function btn_eliminar_cliente()
{
  var id_cliente = $("#id_cliente_eliminar").val();

  var ob = { id_cliente : id_cliente };
  
  $.ajax({
      type: "POST",
      url: url_p+"Ccliente/VB_cliente",
      data: ob,
      beforeSend: function(objeto){

        $("#btn_borrar").prop("disabled", true);
        $("#panel_modal_eliminar").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        $("#btn_borrar").prop("disabled", false);   
        $("#panel_modal_eliminar").html(data);
        
        setTimeout(function(){ $("#panel_modal_eliminar").html(""); },1000);
        setTimeout(function(){ $("#myModal_Delete").modal("show"); },2000);
        setTimeout(function(){ cargar_datos(1); },1000);
      }
  });

}


var estado = 0;

function btn_submenu(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_"+id).css("display","none");
  }
  }

}

function btn_submenu_xs(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_xs"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_xs"+id).css("display","none");
  }
  }

}


function select(id,dato,area)
{   
  $("#id_"+area).val(id);
  $("#"+area).html(dato);
}


