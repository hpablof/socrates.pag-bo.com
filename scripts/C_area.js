/* CONTROLADOR DE AREA DEL SISTEMA */
var lista = [];

 function cargar_datos(page)
 {  
    var id_empresa = $("#id_empresa_select").val();
    var id_gestion = $("#id_gestion_select").val();
    
    var ob = { page:page, id_empresa: id_empresa, id_gestion:id_gestion };
    $("#panel_resultado_busqueda").html("");

    $.ajax({
        type: "POST",
        url: url_p+"Carea/paginacion",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data)
        {
          $("#panel_paginacion_datos").html(data);
        }
    });
}


 function buscar_datos(page)
 {  
    var txt_buscar = $("#txt_buscar").val();

    var cadena = " Resultado para : <label> "+txt_buscar+" </label> ";
    $("#panel_resultado_busqueda").html(cadena);

    var id_empresa = $("#id_empresa_select").val();
    var id_gestion = $("#id_gestion_select").val();
 
    var ob = {page:page,txt_buscar : txt_buscar, id_empresa: id_empresa, id_gestion:id_gestion };

    $.ajax({
        type: "POST",
        url: url_p+"Carea/paginacion_find",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data)
        {
          $("#panel_paginacion_datos").html(data);
        }
      });
}

function area_o( area )
{ 
 this.area = area; 
}

function btn_nuevo_area()
{
  $("#myModal_Register").modal("show");
 
  var ob = "";
  
  $.ajax({
      type: "POST",
      url: url_p+"Carea/R_area",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar").html(data);
      }
     });
}

function btn_agregar_carrito_area()
{ 
  var area = $("#area").val(); 
  
  if(area != "" )
  {
    var obj_area = new area_o( area );
    lista.push(obj_area);
    btn_mostrar_datos_area();
    $("#area").val(""); 
    $("#area").focus(); 
  }

  else {
  
       if (area =="")
       {  
           $("#area").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de area </label>";
           $("#panel_resp_area").html(res);  
       }

       
  //Final de la codicional else
  }

 //Final de la fucion
}

function btn_mostrar_datos_area()
{
  var cadena = "<table class='table table-bordered table-condensed table-striped table-hover'>";
  
  cadena = cadena + "<tr>"+ "<th> area </th> " +  "<th> Opciones </th>"; 

  cadena = cadena +"</tr>";

  for(var i = 0 ; i<lista.length; i++)
  {
    area = lista[i].area; 
      cadena = cadena + "<tr>"+
    "<td>"+ area +"</td>"+ " <td  class='col-lg-1' > <center> <button class='btn btn-danger btn-xs' onclick='btn_eliminar_item("+i+");' > X </button> </center> </td><tr>"; 
   }

   cadena = cadena +" </table> ";

   $("#panel_listado_area").html(cadena);  
 
} 

function btn_eliminar_item(contador)
{
  lista.splice( contador , 1);
  btn_mostrar_datos_area();
}

function btn_registrar_area()
{ 
   var id_usuario = $("#id_usuario_session").val(); 
   var id_empresa = $("#id_empresa_select").val();
   var id_gestion = $("#id_gestion_select").val();

   var lista_area = JSON.stringify(lista);
   var ob = {lista : lista_area, id_usuario:id_usuario, id_empresa:id_empresa, 
   id_gestion:id_gestion };

  
  $.ajax({
      type: "POST",
      url: url_p+"Carea/G_area",
      data: ob,
      beforeSend: function(objeto){

        $("#panel_modal_resultado_registrar").html("<div class='cargando'>  </div>");
        //$("#btn_registrar").prop("disabled", true);

      },
      success:function(data)
      { 
        //$("#btn_registrar").prop("disabled", false);   
        $("#panel_modal_resultado_registrar").html(data);
      }
  });
}

function btn_examinar_area(id_area)
{
  $("#myModal_View").modal("show");

  var ob = {id_area : id_area};
  $.ajax({
      type: "POST",
      url: url_p+"Carea/VD_area",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_examinar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_examinar").html(data);
      }
     });

}

function btn_editar_area(id_area)
{
  $("#myModal_Update").modal("show");
  $("#id_area_edicion").val(id_area);
  
  var ob = {id_area : id_area};
  $.ajax({
      type: "POST",
      url: url_p+"Carea/VE_area",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_editar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_editar").html(data);
      }
     });

}

function btn_guardar_area()
{

  var id_area = $("#id_area_edicion").val();

  var area = $("#area").val();
  var ob = { id_area : id_area, area:area }; 

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_area", id_area);

  $.ajax({
   url: url_p+"Carea/VU_area",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){

     $("#btn_editar").prop("disabled", true);
     $("#panel_modal_respuesta_editar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_editar").prop("disabled", false);  
     $("#panel_modal_respuesta_editar").html(data);
   
   }
 }); 

}

function btn_borrar_area(id_area)
{
  $("#myModal_Delete").modal("show");
  $("#id_area_eliminar").val(id_area);

}

function btn_eliminar_area()
{
  var id_area = $("#id_area_eliminar").val();

  var ob = { id_area : id_area };
  
  $.ajax({
      type: "POST",
      url: url_p+"Carea/VB_area",
      data: ob,
      beforeSend: function(objeto){

        $("#btn_borrar").prop("disabled", true);
        $("#panel_modal_eliminar").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        $("#btn_borrar").prop("disabled", false);   
        $("#panel_modal_eliminar").html(data);
        
        setTimeout(function(){ $("#panel_modal_eliminar").html(""); },1000);
        setTimeout(function(){ $("#myModal_Delete").modal("show"); },2000);
        setTimeout(function(){ location.reload(); },1000);
      }
  });

}


var estado = 0;

function btn_submenu(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_"+id).css("display","none");
  }
  }

}

function btn_submenu_xs(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_xs"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_xs"+id).css("display","none");
  }
  }

}


function select(id,dato,area)
{   
  $("#id_"+area).val(id);
  $("#"+area).html(dato);
}


