
/* CONTROLADOR DE AREA DEL SISTEMA */
 var lista = [];

 function cargar_datos(page)
 {  
    var id_empresa = $("#id_empresa_select").val();
    var id_gestion = $("#id_gestion_select").val();

    var ob = { page:page, id_empresa:id_empresa, id_gestion:id_gestion };

    $("#panel_resultado_busqueda").html("");

    $.ajax({
        type: "POST",
        url: url_p+"Ccompra/paginacion",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}


 function buscar_datos(page)
 {  
    var txt_buscar = $("#txt_buscar").val();
    var id_empresa = $("#id_empresa_select").val();
    var id_gestion = $("#id_gestion_select").val();

    var cadena = " Resultado para : <label> "+txt_buscar+" </label> ";
    $("#panel_resultado_busqueda").html(cadena);
    
    var ob = { page:page, txt_buscar:txt_buscar, id_empresa:id_empresa, id_gestion:id_gestion };

    $.ajax({
        type: "POST",
        url: url_p+"Ccompra/paginacion_find",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}

function compra_o(id_producto,producto,cantidad,precio_compra,precio_venta,
subtotal_compra, fecha_vencimiento)
{ 
   this.id_producto = id_producto; 
   this.producto = producto; 
   this.cantidad = cantidad; 
   this.precio_compra = precio_compra; 
   this.precio_venta = precio_venta; 
   this.subtotal_compra = subtotal_compra;
   this.fecha_vencimiento = fecha_vencimiento; 
  
}

function btn_nuevo_compra()
{
  $("#myModal_Register").modal("show");
 
  var ob = "";
  $.ajax({
      type: "POST",
      url: url_p+"Ccompra/R_compra",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar").html(data);
      }
     });
}

function btn_agregar_carrito_compra()
{ 
    var id_producto = $("#id_producto").val();
    var producto = $("#producto").val();
    var cantidad = $("#cantidad").val();
    var precio_compra = $("#precio_compra").val();
    var precio_venta = $("#precio_venta").val();
    var subtotal_compra = $("#subtotal").val(); 
    var fecha_vencimiento = $("#fecha_vencimiento").val(); 
 
    var obj_compra = new compra_o(id_producto,producto,cantidad,precio_compra,precio_venta,
    subtotal_compra, fecha_vencimiento);

    lista.push(obj_compra);
    btn_mostrar_datos_compra();
  
 
 //Final de la fucion
}

function btn_mostrar_datos_compra()
{
  var cadena = "<table class='table table-bordered table-condensed table-striped table-hover'>";
  
  cadena = cadena + "<tr>"+ "<th> producto </th> "+"<th> cantidad </th>"+
  "<th> precio compra </th>"+"<th> precio venta </th>"+"<th> fecha venc </th> "+
  "<th> subtotal </th>"+"<th> Opciones </th>"; 

  cadena = cadena +"</tr>";

  var total_compra = 0;

  for(var i = 0 ; i<lista.length; i++)
  {
     
    var producto = lista[i].producto;
    var cantidad = lista[i].cantidad; 
    var precio_compra = lista[i].precio_compra; 
    var precio_venta = lista[i].precio_venta; 
    var subtotal_compra = lista[i].subtotal_compra;
    var fecha_vencimiento = lista[i].fecha_vencimiento; 

    total_compra = parseFloat(total_compra) + parseFloat(subtotal_compra); 
    
    var cadena = cadena + "<tr>"+
    "<td>"+ producto +"</td>"+
    "<td>"+ cantidad +"</td>"+
    "<td>"+ precio_compra +"</td>"+
    "<td>"+ precio_venta +"</td>"+
    "<td>"+ fecha_vencimiento +"</td>"+
    "<td>"+ subtotal_compra +"</td>"+ 
    
    "<td  class='col-lg-1' > <center> <button class='btn btn-danger btn-xs' onclick='btn_eliminar_item("+i+");' > X </button> </center> </td><tr>"; 
   }
   
   cadena = cadena +" </table> ";

   total_compra = total_compra.toFixed(2);
 
   $("#panel_listado_compra").html(cadena); 
   $("#total_compra").val(total_compra); 
 
} 

function btn_eliminar_item(contador)
{
  lista.splice( contador , 1);
  btn_mostrar_datos_compra();
}

function btn_registrar_compra()
{ 
   var id_usuario = $("#id_usuario_session").val();
   var id_empresa = $("#id_empresa_select").val();
   var id_gestion = $("#id_gestion_select").val(); 
   var id_proveedor = $("#id_proveedor").val();

  var total_compra = $("#total_compra").val();
  var pago_compra = $("#pago_compra").val();
  var cambio_compra = $("#cambio_compra").val();

 
   var lista_area = JSON.stringify(lista);
   var ob = { lista:lista_area, id_usuario:id_usuario, id_empresa:id_empresa,
   id_gestion:id_gestion, id_proveedor:id_proveedor,total_compra:total_compra, 
   pago_compra:pago_compra, cambio_compra:cambio_compra };

  
  $.ajax({
      type: "POST",
      url: url_p+"Ccompra/G_compra",
      data: ob,
      beforeSend: function(objeto){

        $("#panel_modal_resultado_registrar").html("<div class='cargando'>  </div>");
        //$("#btn_registrar").prop("disabled", true);

      },
      success:function(data)
      { 
        //$("#btn_registrar").prop("disabled", false);   
        $("#panel_modal_resultado_registrar").html(data);

      }
  });
}

function btn_examinar_compra(id_compra)
{
  $("#myModal_View").modal("show");

  var ob = {id_compra : id_compra};
  $.ajax({
      type: "POST",
      url: url_p+"Ccompra/VD_compra",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_examinar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_examinar").html(data);
      }
     });

}

function btn_editar_compra(id_compra)
{
  $("#myModal_Update").modal("show");
  $("#id_compra_edicion").val(id_compra);
  
  var ob = {id_compra : id_compra};
  $.ajax({
      type: "POST",
      url: url_p+"Ccompra/VE_compra",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_editar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_editar").html(data);
      }
     });

}

function btn_guardar_compra()
{

  var id_compra = $("#id_compra_edicion").val();

  var id_producto = $("#id_producto").val();
  var codigo_compra = $("#codigo_compra").val();
  var cantidad_dc = $("#cantidad_dc").val();
  var precio_dc = $("#precio_dc").val();
  var precio_venta_dc = $("#precio_venta_dc").val();
  var subtotal_dc = $("#subtotal_dc").val();
  
  var ob = { id_compra : id_compra, id_producto:id_producto, codigo_compra:codigo_compra, cantidad_dc:cantidad_dc, precio_dc:precio_dc, precio_venta_dc:precio_venta_dc, subtotal_dc:subtotal_dc }; 

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_compra", id_compra);

  $.ajax({
   url: url_p+"Ccompra/VU_compra",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){

     $("#btn_editar").prop("disabled", true);
     $("#panel_modal_respuesta_editar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_editar").prop("disabled", false);  
     $("#panel_modal_respuesta_editar").html(data);
   
   }
 }); 

}

function btn_borrar_compra(codigo_compra)
{
  $("#myModal_Delete").modal("show");
  $("#id_compra_eliminar").val(codigo_compra);

}

function btn_eliminar_detalle_compra()
{
  var codigo_compra = $("#id_compra_eliminar").val();

  var ob = { codigo_compra : codigo_compra };
  
  $.ajax({
      type: "POST",
      url: url_p+"Ccompra/VB_compra",
      data: ob,
      beforeSend: function(objeto){

        $("#btn_borrar").prop("disabled", true);
        $("#panel_modal_eliminar").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        $("#btn_borrar").prop("disabled", false);   
        $("#panel_modal_eliminar").html(data);
        
        setTimeout(function(){ $("#panel_modal_eliminar").html(""); },1000);
        setTimeout(function(){ $("#myModal_Delete").modal("show"); },2000);
        setTimeout(function(){ location.reload(); },1000);
      }
  });

}


var estado = 0;

function btn_submenu(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_"+id).css("display","none");
  }
  }

}

function btn_submenu_xs(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_xs"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_xs"+id).css("display","none");
  }
  }

}


function select(id,dato,area)
{   
  $("#id_"+area).val(id);
  $("#"+area).html(dato);
}

function btn_buscar_proveedor()
{
  var txt_buscar = $("#txt_buscar_proveedor").val();
  var id_empresa = $("#id_empresa_select").val();
  var id_gestion = $("#id_gestion_select").val();

  var ob = { txt_buscar : txt_buscar,id_empresa:id_empresa, id_gestion:id_gestion };
  
  $.ajax({
      type: "POST",
      url: url_p+"Ccompra/VB_proveedor",
      data: ob,
      beforeSend: function(objeto)
      {
        $("#panel_resp_proveedor").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      { 
        $("#panel_resp_proveedor").html(data);
      }
  });
}


function btn_cerrar_buscador_proveedor()
{
  $("#panel_resp_proveedor").html("");
  $("#txt_buscar_proveedor").val("");
  $("#id_proveedor").val("");
  $("#proveedor").val("");
}

function btn_select_proveedor(id_proveedor,proveedor)
{
  $("#txt_buscar_proveedor").val(proveedor);
  $("#id_proveedor").val(id_proveedor);
  $("#proveedor").val(proveedor); 
  $("#panel_resp_proveedor").html("");
}


function btn_buscar_productos()
{
  var txt_buscar = $("#txt_buscar_producto").val();
  var id_empresa = $("#id_empresa_select").val();
  var id_gestion = $("#id_gestion_select").val();

  var ob = { txt_buscar:txt_buscar, id_empresa:id_empresa, id_gestion:id_gestion};
  
  $.ajax({
      type: "POST",
      url: url_p+"Ccompra/VB_producto",
      data: ob,
      beforeSend: function(objeto)
      {
        $("#panel_resp_producto").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      { 
        $("#panel_resp_producto").html(data);
      }
  });
}

function btn_cerrar_buscador_productos()
{
  $("#panel_resp_producto").html("");
  $("#txt_buscar_producto").val("");
  $("#id_producto").val("");
  $("#producto").val("");
}

function btn_select_producto(id_producto,producto,precio_compra,precio_venta)
{
  $("#txt_buscar_producto").val(producto);
  $("#id_producto").val(id_producto);
  $("#producto").val(producto); 
  $("#precio_compra").val(precio_compra);
  $("#precio_venta").val(precio_venta);

  $("#panel_resp_producto").html("");
}

function btn_subtotal_compra()
{
  var precio_compra = $("#precio_compra").val();
  var cantidad = $("#cantidad").val();

  if(precio_compra==""){ precio_compra=0; }
  if(cantidad==""){ cantidad=0; }

  //alert(precio_venta+" - "+cantidad);
  
  var suma = cantidad*precio_compra;
  suma = suma.toFixed(2);

  $("#subtotal").val(suma);

}

function btn_cambio_compra()
{
  var total_compra = $("#total_compra").val();
  var pago_compra = $("#pago_compra").val();

  if(total_compra==""){ total_compra = 0; };
  if(pago_compra==""){ pago_compra = 0; };

  var resta = parseFloat(pago_compra) - parseFloat(total_compra);
  resta = resta.toFixed(2);

  if(resta<0){ resta=0; }
  $("#cambio_compra").val(resta); 
}


function btn_modal_proveedor()
{
  $("#myModal_Register_Proveedor").modal("show");
 
  var ob = "";
  $.ajax({
      type: "POST",
      url: url_p+"Ccompra/R_proveedor",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar_Proveedor").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar_Proveedor").html(data);
      }
     }); 
}

function btn_registrar_proveedor()
{
  var id_empresa = $("#id_empresa_select").val();
  var id_gestion = $("#id_gestion_select").val();
  var id_usuario = $("#id_usuario_session").val();

  var proveedor = $("#proveedor_reg").val();
  var encargado = $("#encargado").val();
  var telefonos = $("#telefonos").val();
  var celulares = $("#celulares").val();
  var whatsapp = $("#whatsapp").val();
  var direccion = $("#direccion").val();
  var correo = $("#correo").val();

  var ob = {
  id_empresa : id_empresa,
  id_gestion : id_gestion,
  id_usuario : id_usuario,
  proveedor : proveedor,
  encargado : encargado,
  telefonos : telefonos,
  celulares : celulares,
  whatsapp : whatsapp,
  direccion : direccion,
  correo : correo 
  };
  
  $.ajax({
      type: "POST",
      url: url_p+"Ccompra/G_proveedor",
      data: ob,
      beforeSend: function(objeto)
      {
        $("#panel_modal_resultado_registrar_Proveedor").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_resultado_registrar_Proveedor").html(data);
      }
     }); 

}

function previsualizar()
{
  readImage();

  $("#img_resp_portada").css("width","80%");
  $("#img_resp_portada").css("height","300");
  $("#img_resp_portada").html("<br></br>");
}

function readImage () 
{
  $("#img_resp_portada").html("<div class='cargando'></div>");
  var input = document.getElementById("portada");
  if (input.files && input.files[0]) 
  {
    var reader = new FileReader();
    reader.onload = function (e) {
    $("#img_resp_portada").attr("src", e.target.result); // Renderizamos la imagen
    }
    reader.readAsDataURL(input.files[0]);
  }

}

function btn_modal_producto()
{

  $("#myModal_Register_Producto").modal("show");
 
  var id_empresa = $("#id_empresa_select").val();
  var id_gestion = $("#id_gestion_select").val();
  var id_usuario = $("#id_usuario_session").val();

  var ob = 
  {
    id_empresa : id_empresa,
    id_gestion : id_gestion,
    id_usuario : id_usuario
  };

  $.ajax({
      type: "POST",
      url: url_p+"Ccompra/R_producto",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar_Producto").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar_Producto").html(data);
      }
  }); 

}

function btn_registrar_producto()
{
   var id_usuario = $("#id_usuario_session").val(); 
   var id_empresa = $("#id_empresa_select").val();
   var id_gestion = $("#id_gestion_select").val();

   var formData = new FormData($("#formulario")[0]);
   formData.append("id_usuario", id_usuario);
   formData.append("id_empresa", id_empresa);
   formData.append("id_gestion", id_gestion);

  $.ajax({
   url: url_p+"Ccompra/G_producto",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){
     
     //$("#btn_registrar").prop("disabled", true); 
     $("#panel_modal_resultado_registrar_Producto").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     //$("#btn_registrar").prop("disabled", false);  
     $("#panel_modal_resultado_registrar_Producto").html(data);
 
   }
 });  
}
