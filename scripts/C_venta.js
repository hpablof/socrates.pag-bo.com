 /* CONTROLADOR DE AREA DEL SISTEMA */
 var lista = [];

 function cargar_datos(page)
 {  
    var id_empresa = $("#id_empresa_select").val();
    var id_gestion = $("#id_gestion_select").val();

    var ob = { page:page, id_empresa:id_empresa, id_gestion:id_gestion };

    $("#panel_resultado_busqueda").html("");

    $.ajax({
        type: "POST",
        url: url_p+"Cventa/paginacion",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
 }


 function buscar_datos(page)
 {  
    var txt_buscar = $("#txt_buscar").val();
    var id_empresa = $("#id_empresa_select").val();
    var id_gestion = $("#id_gestion_select").val();

    var cadena = " Resultado para : <label> "+txt_buscar+" </label> ";
    $("#panel_resultado_busqueda").html(cadena);

    var ob = {page:page,txt_buscar : txt_buscar, id_empresa:id_empresa, id_gestion:id_gestion };

    $.ajax({
        type: "POST",
        url: url_p+"Cventa/paginacion_find",
        data: ob,
        beforeSend: function(objeto)
        {
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data)
        {
          $("#panel_paginacion_datos").html(data);
        }
      });
}

function venta_o(producto,id_producto,cantidad_venta,precio_venta,subtotal_venta)
{ 
   this.producto = producto;
   this.id_producto = id_producto; 
   this.cantidad_venta = cantidad_venta; 
   this.precio_venta = precio_venta; 
   this.subtotal_venta = subtotal_venta; 
}

function btn_nueva_venta()
{
  $("#myModal_Register").modal("show");
 
  var ob = "";
  $.ajax({
      type: "POST",
      url: url_p+"Cventa/R_detalle_venta",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar").html(data);
      }
     });
}

function btn_agregar_carrito_detalle_venta()
{ 
  var producto = $("#producto").val();
  var id_producto = $("#id_producto").val();
  var cantidad_venta = $("#cantidad_venta").val();
  var precio_venta = $("#precio_venta").val();
  var subtotal_venta = $("#subtotal_venta").val(); 
 
  var obj_venta = new venta_o(producto,id_producto,cantidad_venta,precio_venta,subtotal_venta);

  lista.push(obj_venta);
  btn_mostrar_datos_venta();
 
 //Final de la fucion
}

function btn_mostrar_datos_venta()
{
  var cadena = "<table class='table table-bordered table-condensed table-striped table-hover'>";
  
  cadena = cadena + "<tr>"+ "<th>producto</th>"+"<th>cantidad</th>"+
  "<th> p venta </th>"+"<th>subtotal</th>"+"<th> Opciones </th>"; 

  cadena = cadena +"</tr>";

  var total_venta = 0;

  for(var i = 0 ; i<lista.length; i++)
  {
    var producto = lista[i].producto; 
    var cantidad_venta = lista[i].cantidad_venta; 
    var precio_venta = lista[i].precio_venta; 
    var subtotal_venta = lista[i].subtotal_venta; 

    total_venta = parseFloat(total_venta) + parseFloat(subtotal_venta);
    
    var cadena = cadena + "<tr>"+
    "<td>"+ producto +"</td>"+
    "<td>"+ cantidad_venta +"</td>"+
    "<td>"+ precio_venta +"</td>"+
    "<td>"+ subtotal_venta +"</td>"+ 
    "<td  class='col-lg-1' > <center> <button class='btn btn-danger btn-xs' onclick='btn_eliminar_item("+i+");' > X </button> </center> </td><tr>"; 
   
   }

   total_venta = total_venta.toFixed(2);
   cadena = cadena +" </table> ";

   $("#panel_listado_venta").html(cadena);  
   $("#total_venta").val(total_venta);
 
} 

function btn_eliminar_item(contador)
{
  lista.splice( contador , 1);
  btn_mostrar_datos_venta();
}

function btn_registrar_venta()
{ 
   var id_empresa = $("#id_empresa_select").val();
   var id_gestion = $("#id_gestion_select").val();
   var id_usuario = $("#id_usuario_session").val(); 
   var id_cliente = $("#id_cliente").val();

   var total_venta = $("#total_venta").val();
   var pago_venta = $("#pago_venta").val();
   var cambio_venta = $("#cambio_venta").val();

   var lista_area = JSON.stringify(lista);

   var ob = {lista : lista_area, id_usuario:id_usuario, id_empresa:id_empresa,
   id_gestion:id_gestion, id_cliente:id_cliente, total_venta:total_venta, 
   pago_venta:pago_venta, cambio_venta:cambio_venta };

  
   $.ajax({
      type: "POST",
      url: url_p+"Cventa/G_venta",
      data: ob,
      beforeSend: function(objeto){

        $("#panel_modal_resultado_registrar").html("<div class='cargando'>  </div>");
        //$("#btn_registrar").prop("disabled", true);

      },
      success:function(data)
      { 
        //$("#btn_registrar").prop("disabled", false);   
        $("#panel_modal_resultado_registrar").html(data);
      }
  });
}

function btn_examinar_venta(id_venta)
{
  $("#myModal_View").modal("show");

  var ob = {id_venta : id_venta};
  $.ajax({
      type: "POST",
      url: url_p+"Cventa/VD_venta",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_examinar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_examinar").html(data);
      }
     });

}

function btn_editar_venta(id_venta)
{
  $("#myModal_Update").modal("show");
  $("#id_venta_edicion").val(id_venta);
  
  var ob = {id_venta : id_venta};
  $.ajax({
      type: "POST",
      url: url_p+"Cventa/VE_detalle_venta",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_editar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_editar").html(data);
      }
     });

}

function btn_guardar_detalle_venta()
{
  var id_detalle_venta = $("#id_detalle_venta_edicion").val();

  var id_producto = $("#id_producto").val();
  var cantidad_dv = $("#cantidad_dv").val();
  var precio_venta_dv = $("#precio_venta_dv").val();
  var subtotal_dv = $("#subtotal_dv").val();
  var ob = { id_detalle_venta : id_detalle_venta, id_producto:id_producto, cantidad_dv:cantidad_dv, precio_venta_dv:precio_venta_dv, subtotal_dv:subtotal_dv }; 

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_detalle_venta", id_detalle_venta);

  $.ajax({
   url: url_p+"Cdetalle_venta/VU_detalle_venta",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){

     $("#btn_editar").prop("disabled", true);
     $("#panel_modal_respuesta_editar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_editar").prop("disabled", false);  
     $("#panel_modal_respuesta_editar").html(data);
   
   }
 }); 

}

function btn_borrar_venta(id_venta)
{
  $("#myModal_Delete").modal("show");
  $("#id_venta_eliminar").val(id_venta);

}

function btn_eliminar_venta()
{
  var codigo_venta = $("#id_venta_eliminar").val();

  var ob = { codigo_venta : codigo_venta };
  
  $.ajax({
      type: "POST",
      url: url_p+"Cventa/VB_venta",
      data: ob,
      beforeSend: function(objeto){

        $("#btn_borrar").prop("disabled", true);
        $("#panel_modal_eliminar").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        $("#btn_borrar").prop("disabled", false);   
        $("#panel_modal_eliminar").html(data);
 
      }
  });

}


var estado = 0;

function btn_submenu(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_"+id).css("display","none");
  }
  }

}

function btn_submenu_xs(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_xs"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_xs"+id).css("display","none");
  }
  }

}


function select(id,dato,area)
{   
  $("#id_"+area).val(id);
  $("#"+area).html(dato);
}

function btn_buscar_cliente_venta()
{
  var id_empresa = $("#id_empresa_select").val();
  var id_gestion = $("#id_gestion_select").val();
  var txt_buscar = $("#txt_buscar_cliente").val();

  var ob = { txt_buscar:txt_buscar, id_empresa:id_empresa, id_gestion:id_gestion };
  
  $.ajax({
      type: "POST",
      url: url_p+"Cventa/VB_cliente",
      data: ob,
      beforeSend: function(objeto)
      {
        $("#panel_resp_cliente").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        $("#panel_resp_cliente").html(data);
      }
  });

}

function btn_cerrar_buscador_cliente()
{
  $("#id_cliente").val("");
  $("#cliente").val("");
  $("#panel_resp_cliente").html("");

}


function btn_select_cliente_venta(id_cliente,cliente)
{
  $("#id_cliente").val(id_cliente);
  $("#txt_buscar_cliente").val(cliente);
  $("#cliente").val(cliente);
  $("#panel_resp_cliente").html("");

}

function btn_buscar_productos_venta()
{
  var txt_buscar = $("#txt_buscar_producto").val();
  var id_empresa = $("#id_empresa_select").val();
  var id_gestion = $("#id_gestion_select").val();

  var ob = { txt_buscar:txt_buscar, id_empresa:id_empresa, id_gestion:id_gestion};
  
  $.ajax({
      type: "POST",
      url: url_p+"Cventa/VB_producto_venta",
      data: ob,
      beforeSend: function(objeto)
      {
        $("#panel_resp_producto").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      { 
        $("#panel_resp_producto").html(data);
      }
  });
}

function btn_cerrar_buscador_productos()
{
  $("#panel_resp_producto").html("");
  $("#txt_buscar_producto").val("");
  $("#id_producto").val("");
  $("#producto").val("");
}

function btn_select_producto(id_producto,producto,precio_compra,precio_venta)
{
  $("#txt_buscar_producto").val(producto);
  $("#id_producto").val(id_producto);
  $("#producto").val(producto); 
  $("#precio_compra").val(precio_compra);
  $("#precio_venta").val(precio_venta);

  $("#panel_resp_producto").html("");
  btn_subtotal_venta();
}

function btn_subtotal_venta()
{
  var precio_venta = $("#precio_venta").val();
  var cantidad = $("#cantidad_venta").val();

  if(precio_venta==""){ precio_venta=0; }
  if(cantidad==""){ cantidad=0; }

  var suma = cantidad*precio_venta;
  suma = suma.toFixed(2);

  $("#subtotal_venta").val(suma);

}


function btn_pago_venta()
{
  var total_venta = $("#total_venta").val();
  var pago_venta = $("#pago_venta").val();

  if(total_venta==""){ total_venta = 0; };
  if(pago_venta==""){ pago_venta = 0; };

  var resta = parseFloat(pago_venta) - parseFloat(total_venta);
  resta = resta.toFixed(2);

  if(resta<0){ resta=0; }
  $("#cambio_venta").val(resta); 

}


function btn_imprimir_venta(codigo_venta)
{

  var url_recibo = url_p+"Cventa/recibo_venta?codigo_venta="+codigo_venta;
 
  var w = 720;
  var h = 720;
  var title = "Recivo Pago de Mensualidad";

  var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left; 
  var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top; width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width; height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height; 
  var left = ((width / 2) - (w / 2)) + dualScreenLeft; 
  var top = ((height / 2) - (h / 2)) + dualScreenTop; 

  var newWindow = window.open(url_recibo, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

  if (window.focus) { newWindow.focus(); }

}

/* FUNCIONES DE CLIENTES */

function btn_modal_cliente()
{
  $("#myModal_Register_Cliente").modal("show");
 
  var ob = "";

  $.ajax({
      type: "POST",
      url: url_p+"Cventa/R_cliente",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar_cliente").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar_cliente").html(data);
      }
  }); 

}

function btn_registrar_cliente()
{
  var id_empresa = $("#id_empresa_select").val();
  var id_gestion = $("#id_gestion_select").val();
  var id_usuario = $("#id_usuario_session").val();

  var cliente = $("#cliente_reg").val();
  var ci_nit = $("#ci_nit").val();
  var celular_cl = $("#celular_cl").val();
  var whatsapp_cl = $("#whatsapp_cl").val();
  var direccion_cl = $("#direccion_cl").val();
  var correo_cl = $("#correo_cl").val();

  var ob = 
  {
    id_empresa   : id_empresa,
    id_gestion   : id_gestion,
    id_usuario   : id_usuario,

    cliente      : cliente,
    ci_nit       : ci_nit,
    celular_cl   : celular_cl,
    whatsapp_cl  : whatsapp_cl,
    direccion_cl : direccion_cl,
    correo_cl    : correo_cl
  };

  $.ajax({
      type: "POST",
      url: url_p+"Cventa/G_cliente",
      data: ob,
      beforeSend: function(objeto)
      {
        $("#panel_modal_resultado_registrar_cliente").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_resultado_registrar_cliente").html(data);
      }
  }); 

}