  function btn_selec_codigo_transferencia()
  {  
      var tipo_pago = $("#opcion_tipo_pago").val();
 
      if(tipo_pago == 4)
      {
         
        $("#codigo_transferencia").prop('disabled', true); 
        $("#codigo_transferencia").val('pago en efectivo');
      }
      
      if(tipo_pago == 5)
      { 
        
        $("#codigo_transferencia").prop('disabled', true); 
        $("#codigo_transferencia").val('pago por tigo money');
      }
      
      if(tipo_pago<4)
      {
        $("#codigo_transferencia").val('');
        $("#codigo_transferencia").prop('disabled', false);   
      } 
      

  }
  
  function btn_registrar_estudiante_curso()
  {
     var nombre_completo = $("#nombre_completo").val();
     var id_curso = $("#id_curso").val();
     var razon_social = $("#razon_social").val();
     var email = $("#email").val();
     var celular = $("#celular").val();
     var ci_nit = $("#ci_nit").val();
     var ci = $("#ci").val();
     var opcion_tipo_pago = $("#opcion_tipo_pago").val();
     var codigo_transferencia = $("#codigo_transferencia").val();
     var importe = $("#importe").val();
     
    if(nombre_completo!="" && razon_social!="" && celular!="" && ci_nit!="" && email!="" && opcion_tipo_pago!="" && codigo_transferencia!="")
    {
     var ob = { nombre_completo :nombre_completo, razon_social:razon_social, email:email, celular:celular, ci_nit:ci_nit, ci:ci, 
     opcion_tipo_pago:opcion_tipo_pago, codigo_transferencia:codigo_transferencia, id_curso : id_curso, importe : importe };
    
     $.ajax({
        type: "POST",
        url: "https://socrates.pag-bo.com/Formulario/Registrar",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_resp_formulario_curso").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_resp_formulario_curso").html(data);

        }
      }); 
      
    }
    else
    {
 
       if ( nombre_completo=="")
       {  
           $("#nombre_completo").focus();
           var res ="<label style='color:red;'> Debe llenar el campo </label>";
           $("#panel_resp_nombre_completo").html(res);  
       }
       
       if ( razon_social=="")
       {  
           $("#razon_social").focus();
           var res ="<label style='color:red;'> Debe llenar el campo </label>";
           $("#panel_resp_razon_social").html(res);  
       }

       if ( email=="")
       {  
           $("#email").focus();
           var res ="<label style='color:red;'> Debe llenar el campo </label>";
           $("#panel_resp_email").html(res);  
       }

       if ( celular=="")
       {  
           $("#celular").focus();
           var res ="<label style='color:red;'> Debe llenar el numero </label>";
           $("#panel_resp_celular").html(res);  
       }
       
       
       if ( ci_nit=="")
       {  
           $("#ci_nit").focus();
           var res ="<label style='color:red;'> Debe llenar el campo </label>";
           $("#panel_resp_ci_nit").html(res);  
       }

       if ( opcion_tipo_pago=="")
       {  
           $("#opcion_tipo_pago").focus();
           var res ="<label style='color:red;'> Debe llenar el campo </label>";
           $("#panel_resp_tipo_pago").html(res);  
       }
       
       if ( codigo_transferencia=="")
       {  
           $("#codigo_transferencia").focus();
           var res ="<label style='color:red;'> Debe llenar el campo </label>";
           $("#panel_resp_codigo_transferencia").html(res);  
       }
       
       
    }
        

  }
  
  
  
  
  