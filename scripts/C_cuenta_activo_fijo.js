
/* CONTROLADOR DE AREA DEL SISTEMA */
var url_p = "https://socrates.pag-bo.com/";
var lista = [];


 function cargar_datos(page)
 {  
    var ob = {page:page};
    $("#panel_resultado_busqueda").html("");

    $.ajax({
        type: "POST",
        url: url_p+"Ccuenta_activo_fijo/paginacion",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}


 function buscar_datos(page)
 {  
    var txt_buscar = $("#txt_buscar").val();

    var cadena = " Resultado para : <label> "+txt_buscar+" </label> ";
    $("#panel_resultado_busqueda").html(cadena);
    
    var ob = {page:page,txt_buscar : txt_buscar };

    $.ajax({
        type: "POST",
        url: url_p+"Ccuenta_activo_fijo/paginacion_find",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}

function cuenta_activo_fijo_o( codigo_caf,cuenta_caf,codigo_cda,cuenta_cda,codigo_cdd,cuenta_cdd,porcentaje_af )
{ 
 this.codigo_caf = codigo_caf; 
   this.cuenta_caf = cuenta_caf; 
   this.codigo_cda = codigo_cda; 
   this.cuenta_cda = cuenta_cda; 
   this.codigo_cdd = codigo_cdd; 
   this.cuenta_cdd = cuenta_cdd; 
   this.porcentaje_af = porcentaje_af; 
  
}

function btn_nuevo_cuenta_activo_fijo()
{
  $("#myModal_Register").modal("show");
 
  var ob = "";
  $.ajax({
      type: "POST",
      url: url_p+"Ccuenta_activo_fijo/R_cuenta_activo_fijo",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar").html(data);
      }
     });
}

function btn_agregar_carrito_cuenta_activo_fijo()
{ 
    var codigo_caf = $("#codigo_caf").val();
    var cuenta_caf = $("#cuenta_caf").val();
    var codigo_cda = $("#codigo_cda").val();
    var cuenta_cda = $("#cuenta_cda").val();
    var codigo_cdd = $("#codigo_cdd").val();
    var cuenta_cdd = $("#cuenta_cdd").val();
    var porcentaje_af = $("#porcentaje_af").val(); 
  
  if(codigo_caf != "" && cuenta_caf != "" && codigo_cda != "" && cuenta_cda != "" && codigo_cdd != "" && cuenta_cdd != "" && porcentaje_af != "" )
  {
  
  var obj_cuenta_activo_fijo = new cuenta_activo_fijo_o( codigo_caf,  cuenta_caf,  codigo_cda,  cuenta_cda,  codigo_cdd,  cuenta_cdd,  porcentaje_af );

  lista.push(obj_cuenta_activo_fijo);
  btn_mostrar_datos_cuenta_activo_fijo();
  }

  else {
  
       if (codigo_caf =="")
       {  
           $("#codigo_caf").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de codigo_caf </label>";
           $("#panel_resp_codigo_caf").html(res);  
       }

       
       if (cuenta_caf =="")
       {  
           $("#cuenta_caf").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de cuenta_caf </label>";
           $("#panel_resp_cuenta_caf").html(res);  
       }

       
       if (codigo_cda =="")
       {  
           $("#codigo_cda").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de codigo_cda </label>";
           $("#panel_resp_codigo_cda").html(res);  
       }

       
       if (cuenta_cda =="")
       {  
           $("#cuenta_cda").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de cuenta_cda </label>";
           $("#panel_resp_cuenta_cda").html(res);  
       }

       
       if (codigo_cdd =="")
       {  
           $("#codigo_cdd").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de codigo_cdd </label>";
           $("#panel_resp_codigo_cdd").html(res);  
       }

       
       if (cuenta_cdd =="")
       {  
           $("#cuenta_cdd").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de cuenta_cdd </label>";
           $("#panel_resp_cuenta_cdd").html(res);  
       }

       
       if (porcentaje_af =="")
       {  
           $("#porcentaje_af").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de porcentaje_af </label>";
           $("#panel_resp_porcentaje_af").html(res);  
       }

       
  //Final de la codicional else
  }

 //Final de la fucion
}

function btn_mostrar_datos_cuenta_activo_fijo()
{
  var cadena = "<table class='table table-bordered table-condensed table-striped table-hover'>";
  
  cadena = cadena + "<tr>"+ "<th> codigo_caf </th> " +  "<th> cuenta_caf </th> " +  "<th> codigo_cda </th> " +  "<th> cuenta_cda </th> " +  "<th> codigo_cdd </th> " +  "<th> cuenta_cdd </th> " +  "<th> porcentaje_af </th> " +  "<th> Opciones </th>"; 

  cadena = cadena +"</tr>";

  for(var i = 0 ; i<lista.length; i++)
  {
    codigo_caf = lista[i].codigo_caf; 
    cuenta_caf = lista[i].cuenta_caf; 
    codigo_cda = lista[i].codigo_cda; 
    cuenta_cda = lista[i].cuenta_cda; 
    codigo_cdd = lista[i].codigo_cdd; 
    cuenta_cdd = lista[i].cuenta_cdd; 
    porcentaje_af = lista[i].porcentaje_af; 
      cadena = cadena + "<tr>"+
    "<td>"+ codigo_caf +"</td>"+
    "<td>"+ cuenta_caf +"</td>"+
    "<td>"+ codigo_cda +"</td>"+
    "<td>"+ cuenta_cda +"</td>"+
    "<td>"+ codigo_cdd +"</td>"+
    "<td>"+ cuenta_cdd +"</td>"+
    "<td>"+ porcentaje_af +"</td>"+ " <td  class='col-lg-1' > <center> <button class='btn btn-danger btn-xs' onclick='btn_eliminar_item("+i+");' > X </button> </center> </td><tr>"; 
   }

   cadena = cadena +" </table> ";

   $("#panel_listado_cuenta_activo_fijo").html(cadena);  
 
} 

function btn_eliminar_item(contador)
{
  lista.splice( contador , 1);
  btn_mostrar_datos_cuenta_activo_fijo();
}

function btn_registrar_cuenta_activo_fijo()
{ 
  var id_usuario = $("#id_usuario_session").val(); 

   var lista_area = JSON.stringify(lista);
   var ob = {lista : lista_area, id_usuario:id_usuario };

  
  $.ajax({
      type: "POST",
      url: url_p+"Ccuenta_activo_fijo/G_cuenta_activo_fijo",
      data: ob,
      beforeSend: function(objeto){

        $("#panel_modal_resultado_registrar").html("<div class='cargando'>  </div>");
        $("#btn_registrar").prop("disabled", true);

      },
      success:function(data)
      { 
        $("#btn_registrar").prop("disabled", false);   
        $("#panel_modal_resultado_registrar").html(data);
        
        setTimeout(function(){ $("#panel_modal_resultado_registrar").html(""); },1000);
        setTimeout(function(){ $("#myModal_Register").modal("hide"); },2000);
        setTimeout(function(){ location.reload(); },3000);
      }
  });
}

function btn_examinar_cuenta_activo_fijo(id_cuenta_activo_fijo)
{
  $("#myModal_View").modal("show");

  var ob = {id_cuenta_activo_fijo : id_cuenta_activo_fijo};
  $.ajax({
      type: "POST",
      url: url_p+"Ccuenta_activo_fijo/VD_cuenta_activo_fijo",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_examinar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_examinar").html(data);
      }
     });

}

function btn_editar_cuenta_activo_fijo(id_cuenta_activo_fijo)
{
  $("#myModal_Update").modal("show");
  $("#id_cuenta_activo_fijo_edicion").val(id_cuenta_activo_fijo);
  
  var ob = {id_cuenta_activo_fijo : id_cuenta_activo_fijo};
  $.ajax({
      type: "POST",
      url: url_p+"Ccuenta_activo_fijo/VE_cuenta_activo_fijo",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_editar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_editar").html(data);
      }
     });

}

function btn_guardar_cuenta_activo_fijo()
{

  var id_cuenta_activo_fijo = $("#id_cuenta_activo_fijo_edicion").val();

  var codigo_caf = $("#codigo_caf").val();
  var cuenta_caf = $("#cuenta_caf").val();
  var codigo_cda = $("#codigo_cda").val();
  var cuenta_cda = $("#cuenta_cda").val();
  var codigo_cdd = $("#codigo_cdd").val();
  var cuenta_cdd = $("#cuenta_cdd").val();
  var porcentaje_af = $("#porcentaje_af").val();
  var ob = { id_cuenta_activo_fijo : id_cuenta_activo_fijo, codigo_caf:codigo_caf, cuenta_caf:cuenta_caf, codigo_cda:codigo_cda, cuenta_cda:cuenta_cda, codigo_cdd:codigo_cdd, cuenta_cdd:cuenta_cdd, porcentaje_af:porcentaje_af }; 

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_cuenta_activo_fijo", id_cuenta_activo_fijo);

  $.ajax({
   url: url_p+"Ccuenta_activo_fijo/VU_cuenta_activo_fijo",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){

     $("#btn_editar").prop("disabled", true);
     $("#panel_modal_respuesta_editar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_editar").prop("disabled", false);  
     $("#panel_modal_respuesta_editar").html(data);
   
   }
 }); 

}

function btn_borrar_cuenta_activo_fijo(id_cuenta_activo_fijo)
{
  $("#myModal_Delete").modal("show");
  $("#id_cuenta_activo_fijo_eliminar").val(id_cuenta_activo_fijo);

}

function btn_eliminar_cuenta_activo_fijo()
{
  var id_cuenta_activo_fijo = $("#id_cuenta_activo_fijo_eliminar").val();

  var ob = { id_cuenta_activo_fijo : id_cuenta_activo_fijo };
  
  $.ajax({
      type: "POST",
      url: url_p+"Ccuenta_activo_fijo/VB_cuenta_activo_fijo",
      data: ob,
      beforeSend: function(objeto){

        $("#btn_borrar").prop("disabled", true);
        $("#panel_modal_eliminar").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        $("#btn_borrar").prop("disabled", false);   
        $("#panel_modal_eliminar").html(data);
        
        setTimeout(function(){ $("#panel_modal_eliminar").html(""); },1000);
        setTimeout(function(){ $("#myModal_Delete").modal("show"); },2000);
        setTimeout(function(){ location.reload(); },1000);
      }
  });

}


var estado = 0;

function btn_submenu(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_"+id).css("display","none");
  }
  }

}

function btn_submenu_xs(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_xs"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_xs"+id).css("display","none");
  }
  }

}


function select(id,dato,area)
{   
  $("#id_"+area).val(id);
  $("#"+area).html(dato);
}


