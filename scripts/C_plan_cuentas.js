 /* CONTROLADOR DE AREA DEL SISTEMA */
 var url_p = "https://socrates.pag-bo.com/";
 var lista = [];
 
 function cargar_datos_plan_cuentas(page)
 {  
    var ob = {page:page};
    $("#panel_resultado_busqueda").html("");

    $.ajax({
        type: "POST",
        url: url_p+"Cplan_cuentas/paginacion",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos_plan_cuentas").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos_plan_cuentas").html(data);

        }
      });
 }

 function buscar_datos_plan_cuentas(page)
 {  
    var txt_buscar = $("#txt_buscar_plan_cuentas").val();

    var cadena = " Resultado para : <label> "+txt_buscar+" </label> ";
    $("#panel_resultado_busqueda_plan_cuentas").html(cadena);
    
    var ob = {page:page,txt_buscar : txt_buscar };

    $.ajax({
        type: "POST",
        url: url_p+"Cplan_cuentas/paginacion_find",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos_plan_cuentas").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos_plan_cuentas").html(data);

        }
      });
}

function plan_cuentas_o(Codigo, Nombre, Nivel, Moneda, NIT, Glosa, Cod_SIAT, Cod_Flujo_SIAT
,Cod_Evolucion_Pat)
{ 
    this.Codigo = Codigo;
    this.Nombre = Nombre;
    this.Nivel = Nivel;
    this.Moneda = Moneda;
    this.NIT = NIT;
    this.Glosa = Glosa;
    this.Cod_SIAT = Cod_SIAT;
    this.Cod_Flujo_SIAT = Cod_Flujo_SIAT;
    this.Cod_Evolucion_Pat = Cod_Evolucion_Pat; 
}

function btn_nuevo_plan_cuentas_modal()
{
  $("#myModal_Register_plan_cuentas").modal("show");
  
  lista = [];
  var ob = "";
  $.ajax({
      type: "POST",
      url: url_p+"Cplan_cuentas/R_plan_cuentas",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar_plan_cuentas").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar_plan_cuentas").html(data);
      }
     }); 
  
}


function btn_agregar_carrito_plan_cuentas()
{ 
    var Codigo = $("#Codigo").val();
    var Nombre = $("#Nombre").val();
    var Nivel = $("#Nivel").val();
    var Moneda = $("#Moneda").val();
    
    var NIT = $("#NIT").val();
    var Glosa = $("#Glosa").val();
    var Cod_SIAT = $("#Cod_SIAT").val();
    var Cod_Flujo_SIAT = $("#Cod_Flujo_SIAT").val();
    var Cod_Evolucion_Pat = $("#Cod_Evolucion_Pat").val(); 
 
    var obj_plan_cuentas = new plan_cuentas_o(Codigo,Nombre,Nivel,Moneda,NIT,Glosa,Cod_SIAT,
    Cod_Flujo_SIAT,Cod_Evolucion_Pat);
 
    lista.push(obj_plan_cuentas);

    btn_mostrar_datos_plan_cuentas(); 
    
 //Final de la fucion
}

function btn_mostrar_datos_plan_cuentas()
{
  var cadena = "<table class='table table-bordered table-condensed table-striped table-hover'>";
  
  cadena = cadena + 
  "<tr>"+
  "<th> Codigo </th>"+
  "<th> Nombre </th>"+
  "<th> Nivel </th>"+
  "<th> Moneda </th>"+
  "<th> NIT </th>"+
  "<th> Glosa </th>"+
  "<th> Cod SIAT </th>"+
  "<th> Cod Flujo SIAT </th>"+
  "<th> Cod Evolucion Pat </th>"+
  "<th> Opciones </th>"; 

  cadena = cadena +"</tr>";

  for(var i = 0 ; i<lista.length; i++)
  {
    var Codigo = lista[i].Codigo;
    var Nivel = lista[i].Nivel;
    var Nombre = lista[i].Nombre;
    var Moneda = lista[i].Moneda;
    var NIT = lista[i].NIT;
    var Glosa = lista[i].Glosa;
    var Cod_SIAT = lista[i].Cod_SIAT;
    var Cod_Flujo_SIAT = lista[i].Cod_Flujo_SIAT;
    var Cod_Evolucion_Pat = lista[i].Cod_Evolucion_Pat;
    
    cadena = cadena + "<tr>"+
    "<td>"+Codigo+"</td>"+
    "<td>"+Nombre+"</td>"+
    "<td>"+Nivel+"</td>"+
    "<td>"+Moneda+"</td>"+
    "<td>"+NIT+"</td>"+
    "<td>"+Glosa+"</td>"+
    "<td>"+Cod_SIAT+"</td>"+
    "<td>"+Cod_Flujo_SIAT+"</td>"+
    "<td>"+Cod_Evolucion_Pat+"</td>"+
    "<td  class='col-lg-1' > <center> <button class='btn btn-danger btn-xs' onclick='btn_eliminar_item("+i+");' > X </button> </center> </td><tr>"; 
   }

   cadena = cadena +" </table>";

   $("#panel_listado_plan_cuentas_carrito").html(cadena);  
 
} 
 
function btn_eliminar_item(contador)
{
  lista.splice( contador , 1);
  btn_mostrar_datos_plan_cuentas();
}

function btn_registrar_plan_cuentas()
{ 
   var id_usuario = $("#id_usuario_session").val(); 

   var lista_area = JSON.stringify(lista);
   var ob = {lista : lista_area, id_usuario:id_usuario };

  $.ajax({
      type: "POST",
      url: url_p+"Cplan_cuentas/G_plan_cuentas",
      data: ob,
      beforeSend: function(objeto)
      {
        $("#panel_modal_resultado_registrar_plan_cuentas").html("<div class='cargando'>  </div>");
        //$("#btn_registrar").prop("disabled", true);
      },
      success:function(data)
      { 
        //$("#btn_registrar").prop("disabled", false);   
        $("#panel_modal_resultado_registrar_plan_cuentas").html(data);
      }
  });
}

function btn_examinar_plan_cuentas(id_plan_cuentas)
{
  $("#myModal_View_plan_cuentas").modal("show");

  var ob = {id_plan_cuentas : id_plan_cuentas};
  $.ajax({
      type: "POST",
      url: url_p+"Cplan_cuentas/VD_plan_cuentas",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_examinar_plan_cuentas").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_examinar_plan_cuentas").html(data);
      }
     });

}

function btn_editar_plan_cuentas(id_plan_cuentas)
{
  $("#myModal_Update_plan_cuentas").modal("show");
  $("#id_plan_cuentas_edicion").val(id_plan_cuentas);
  
  var ob = {id_plan_cuentas : id_plan_cuentas};
  $.ajax({
      type: "POST",
      url: url_p+"Cplan_cuentas/VE_plan_cuentas",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_editar_plan_cuentas").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_editar_plan_cuentas").html(data);
      }
     });

}

function btn_guardar_plan_cuentas()
{
  var id_plan_cuentas = $("#id_plan_cuentas_edicion").val();
 
  var formData = new FormData($("#formulario")[0]);
  formData.append("id_plan_cuentas", id_plan_cuentas);

  $.ajax({
   url: url_p+"Cplan_cuentas/VU_plan_cuentas",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){

     $("#btn_editar").prop("disabled", true);
     $("#panel_modal_respuesta_editar_plan_cuentas").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_editar").prop("disabled", false);  
     $("#panel_modal_respuesta_editar_plan_cuentas").html(data);
   
   }
 }); 

}

function btn_borrar_plan_cuentas(id_plan_cuentas)
{
  $("#myModal_Delete_plan_cuentas").modal("show");
  $("#id_plan_cuentas_eliminar").val(id_plan_cuentas);

}

function btn_eliminar_plan_cuentas()
{
  var id_plan_cuentas = $("#id_plan_cuentas_eliminar").val();

  var ob = { id_plan_cuentas : id_plan_cuentas };
  
  $.ajax({
      type: "POST",
      url: url_p+"Cplan_cuentas/VB_plan_cuentas",
      data: ob,
      beforeSend: function(objeto){

        $("#btn_borrar").prop("disabled", true);
        $("#panel_modal_eliminar_plan_cuentas").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        $("#btn_borrar").prop("disabled", false);   
        $("#panel_modal_eliminar_plan_cuentas").html(data);
        
        setTimeout(function(){ $("#panel_modal_eliminar_plan_cuentas").html(""); },1000);
        setTimeout(function(){ $("#myModal_Delete_plan_cuentas").modal("hide"); },2000);
        setTimeout(function(){ cargar_datos_plan_cuentas(1); },3000);
      }
  });

}

var estado = 0;
function btn_submenu_plan_cuentas(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_pc_"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_pc_"+id).css("display","none");
  }
  }

}

function btn_submenu_plan_cuentas_xs(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_pc_xs"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_pc_xs"+id).css("display","none");
  }
  }

}


function select(id,dato,area)
{   
  $("#id_"+area).val(id);
  $("#"+area).html(dato);
}
 
/* FUNCIONES DE LAS CUENTAS ESPECIALES */

function btn_modal_cts_especiales()
{ 
  $("#myModal_Cuentas_Especiales").modal("show");

  var ob = "";
  
  $.ajax({
      type: "POST",
      url: url_p+"Cplan_cuentas/VR_cts_especiales",
      data: ob,
      beforeSend: function(objeto){

        //$("#btn_borrar").prop("disabled", true);
        $("#panel_cuentas_especiales").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        //$("#btn_borrar").prop("disabled", false);   
        $("#panel_cuentas_especiales").html(data);

      }
  });

}

function btn_modal_cuentas_retencion()
{
  $("#myModal_Cuentas_Retencion").modal("show");
 
  var ob = "";
  
  $.ajax({
      type: "POST",
      url: url_p+"Cplan_cuentas/VR_cts_retenciones",
      data: ob,
      beforeSend: function(objeto)
      {
        //$("#btn_borrar").prop("disabled", true);
        $("#panel_resultado_cuentas_retencion").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        //$("#btn_borrar").prop("disabled", false);   
        $("#panel_resultado_cuentas_retencion").html(data);

      }
  });

}

function btn_importar_pc_modal()
{
  $("#myModal_Insertar_PC").modal("show");
}

function btn_subir_archivo_pc()
{

  var formData = new FormData($("#formulario_PC")[0]);
 
  $.ajax({
   url: url_p+"Cplan_cuentas/Subir_plan_cuentas",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto)
   {
     //$("#btn_editar").prop("disabled", true);
     $("#panel_modal_reusltado_subir_plan_cuentas").html("<div class='cargando'>  </div>");
   },
   success: function(data)
   { 
     //$("#btn_editar").prop("disabled", false); 
     var cadena =
     '<p class="transparent"> espacio </p>'+
     '<div class="alert alert-info alert-dismissable">'+
     '<button type="button" class="close">&times;</button>'+
     '<h4><strong>¡Registro Correcto!</strong></h4> '+
     'Informacion Almacenada Correctamente'+
     '</div>';

     $("#panel_modal_reusltado_subir_plan_cuentas").html(cadena);
   
   }
 }); 

}

function btn_filtro_pc()
{
  var select_nivel = $("#select_niveles").val();
  var ob = { select_nivel:select_nivel };

  if(select_nivel=="")
  {

      $.ajax({
          type: "POST",
          url: url_p+"Cplan_cuentas/VFiltro_plan_cuentas",
          data: ob,
          beforeSend: function(objeto)
          {
            //$("#btn_borrar").prop("disabled", true);
            $("#panel_listar_pc_filtro").html("<div class='cargando'>  </div>");

          },
          success:function(data)
          { 
            //$("#btn_borrar").prop("disabled", false);   
            $("#panel_listar_pc_filtro").html(data);

          }
      });
  }

  else{
  
      $.ajax({
          type: "POST",
          url: url_p+"Cplan_cuentas/VFiltro_plan_cuentas_numeros",
          data: ob,
          beforeSend: function(objeto)
          {
            //$("#btn_borrar").prop("disabled", true);
            $("#panel_listar_pc_filtro").html("<div class='cargando'>  </div>");

          },
          success:function(data)
          { 
            //$("#btn_borrar").prop("disabled", false);   
            $("#panel_listar_pc_filtro").html(data);

          }
      });
  }

}

function btn_filtro_pc_lista(select_nivel)
{
  var ob = { select_nivel:select_nivel };
  
  $.ajax({
      type: "POST",
      url: url_p+"Cplan_cuentas/VFiltro_plan_cuentas",
      data: ob,
      beforeSend: function(objeto)
      {
        //$("#btn_borrar").prop("disabled", true);
        $("#panel_listar_pc_filtro").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        //$("#btn_borrar").prop("disabled", false);   
        $("#panel_listar_pc_filtro").html(data);

      }
  });

}

function myModal_Opciones_PC_Select(id_plan_cuenta)
{
  $("#id_plan_cuenta_select").val(id_plan_cuenta);
  $("#myModal_Opciones_PC").modal("show");
}

function btn_imprimir_pc()
{
  var url_recibo = url_p+"Cplan_cuentas/VImprimir_PC";
 
  var w = 720;
  var h = 720;
  var title = "Imprimir Plan de Cuentas";

  var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left; 
  var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top; width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width; height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height; 
  var left = ((width / 2) - (w / 2)) + dualScreenLeft; 
  var top = ((height / 2) - (h / 2)) + dualScreenTop; 

  var newWindow = window.open(url_recibo, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

  if (window.focus) { newWindow.focus(); }
 
}


function btn_nuevo_plan_cuentas_modal_select(id_plan_cuenta)
{
  $("#myModal_Register_plan_cuentas_select").modal("show");
  //var id_plan_cuenta = $("#id_plan_cuenta_select").val();
  
  lista = [];
  var ob = {id_plan_cuenta:id_plan_cuenta};
  
  $.ajax({
      type: "POST",
      url: url_p+"Cplan_cuentas/R_plan_cuentas_select",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar_plan_cuentas_select").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar_plan_cuentas_select").html(data);
      }
  }); 

}

function btn_registrar_plan_cuentas_select()
{
   var id_usuario = $("#id_usuario_session").val(); 
   
   var lista_area = JSON.stringify(lista);
   var ob = {lista : lista_area, id_usuario:id_usuario };

  $.ajax({
      type: "POST",
      url: url_p+"Cplan_cuentas/G_plan_cuentas_select",
      data: ob,
      beforeSend: function(objeto)
      {
        $("#panel_modal_resultado_registrar_plan_cuentas_select").html("<div class='cargando'>  </div>");
        //$("#btn_registrar").prop("disabled", true);
      },
      success:function(data)
      { 
        //$("#btn_registrar").prop("disabled", false);   
        $("#panel_modal_resultado_registrar_plan_cuentas_select").html(data);
      }
  });

}

function btn_editar_plan_cuentas_modal_select(id_plan_cuentas)
{

  $("#myModal_Update_plan_cuentas_select").modal("show");
  $("#id_plan_cuentas_edicion").val(id_plan_cuentas);
  
  //var id_plan_cuentas = $("#id_plan_cuenta_select").val();
  var ob = {id_plan_cuentas : id_plan_cuentas};
  
  $.ajax({
      type: "POST",
      url: url_p+"Cplan_cuentas/VE_plan_cuentas",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_editar_plan_cuentas_select").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_editar_plan_cuentas_select").html(data);
      }
     });

}

function btn_guardar_plan_cuentas_select()
{

  var id_plan_cuentas = $("#id_plan_cuentas_edicion").val();

  var nombre = $("#nombre").val();
  var id_nivel = $("#id_nivel").val();
  var id_moneda = $("#id_moneda").val();
  var nit = $("#nit").val();
  var glosa = $("#glosa").val();
  var cod_siat = $("#cod_siat").val();
  var cod_flujo_siat = $("#cod_flujo_siat").val();
  var cod_evolucion_pat = $("#cod_evolucion_pat").val();
  
  var ob = { id_plan_cuentas : id_plan_cuentas, nombre:nombre, id_nivel:id_nivel, id_moneda:id_moneda, nit:nit, glosa:glosa, cod_siat:cod_siat, cod_flujo_siat:cod_flujo_siat, cod_evolucion_pat:cod_evolucion_pat }; 

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_plan_cuentas", id_plan_cuentas);

  $.ajax({
   url: url_p+"Cplan_cuentas/VU_plan_cuentas_select",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){

     //$("#btn_editar").prop("disabled", true);
     $("#panel_modal_respuesta_editar_plan_cuentas_select").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     //$("#btn_editar").prop("disabled", false);  
     $("#panel_modal_respuesta_editar_plan_cuentas_select").html(data);
   
   }
 }); 

}

function btn_eliminar_plan_cuentas_modal_select(id_plan_cuentas)
{
  $("#myModal_Delete_plan_cuentas_select").modal("show");
  $("#id_plan_cuentas_eliminar").val(id_plan_cuentas);
}

function btn_eliminar_plan_cuentas_select()
{
  var id_plan_cuentas = $("#id_plan_cuentas_eliminar").val();

  var ob = { id_plan_cuentas : id_plan_cuentas };
  
  $.ajax({
      type: "POST",
      url: url_p+"Cplan_cuentas/VB_plan_cuentas",
      data: ob,
      beforeSend: function(objeto){

        $("#btn_borrar").prop("disabled", true);
        $("#panel_modal_eliminar_plan_cuentas_select").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        $("#btn_borrar").prop("disabled", false);   
        $("#panel_modal_eliminar_plan_cuentas_select").html(data);
        
        setTimeout(function(){ $("#panel_modal_eliminar_plan_cuentas_select").html(""); },1000);
        setTimeout(function(){ $("#myModal_Delete_plan_cuentas_select").modal("hide"); },2000);
        setTimeout(function(){ btn_menu_plan_cuentas_asignacion(); },2800);
         
      }
  }); 
}

/* Validadores de codigo de cuentas */
function valida_codigo_plan_cuentas()
{
  var Codigo = $("#Codigo").val();
  var ob = { Codigo : Codigo };
  
  $.ajax({
      type: "POST",
      url: url_p+"Cplan_cuentas/Verificar_Codigo_PC",
      data: ob,
      beforeSend: function(objeto)
      {
        $("#panel_resp_Codigo").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      { 

        $("#panel_resp_Codigo").html(data);
     
      }
  }); 

}

/* Buscar PC */
 
function btn_buscar_pc_param(txt_buscar, panel_resp, id_pc)
{
  var txt_buscar = $("#"+txt_buscar).val(); 
  var ob = { id_pc : id_pc,  txt_buscar:txt_buscar, panel_resp:panel_resp };
  
  $.ajax({
      type: "POST",
      url: url_p+"Cplan_cuentas/Buscar_PC_select",
      data: ob,
      beforeSend: function(objeto)
      {
        $("#"+panel_resp).html("<div class='cargando'>  </div>");
      },
      success:function(data)
      { 
        $("#"+panel_resp).html(data);
      }
  });   

}

function btn_select_pc_pointer(id_pc,Codigo,Nombre, panel_resp)
{
  var ob = { id_pc : id_pc, Codigo:Codigo, Nombre:Nombre, panel_resp:panel_resp };
  
  $.ajax({
      type: "POST",
      url: url_p+"Cplan_cuentas/Asignar_pc_select",
      data: ob,
      beforeSend: function(objeto)
      {
        $("#"+panel_resp).html("<div class='cargando'>  </div>");
      },
      success:function(data)
      { 
        $("#"+panel_resp).html(data);
      }
  });  
}

function btn_recargar_cuentas_parametrizadas()
{
  var ob = "";
  
  $.ajax({
      type: "POST",
      url: url_p+"Cplan_cuentas/Recargar_cts_param",
      data: ob,
      beforeSend: function(objeto)
      {
        $("#panel_cuentas_parametrizadas").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      { 
        $("#panel_cuentas_parametrizadas").html(data);
      }
  });  
}

function btn_eliminar_buscador_pc(panel_resp)
{
  $("#"+panel_resp).html("");
}

function btn_validar_nivel()
{
  //alert("nivel 1");
  var nivel_s = $("#Nivel").val();
  nivel_s = parseInt(nivel_s);

  var codigo_s = $("#Codigo").val();
  var primerdig = codigo_s.charAt(0);
  //alert(primerdig);
 
  if(nivel_s>=5)
  {
    //alert("habilitar NIT");
    $("#NIT").prop( "disabled", false);
    $("#Moneda").prop( "disabled", false);
    $("#Glosa").prop( "disabled", false);

    $("#Cod_SIAT").prop( "disabled", false);
    $("#Cod_Flujo_SIAT").prop( "disabled", false);
 
  }

  if(nivel_s<5)
  {
    //alert("bloquear NIT");
    $("#NIT").val("");
    $("#NIT").prop( "disabled", true);
    $("#Moneda").val("");
    $("#Moneda").prop( "disabled", true);
  }


  if(nivel_s==3)
  {
    //alert("bloquear NIT");
    $("#Glosa").val("");
    $("#Glosa").prop( "disabled", false);
     
  }


  if(nivel_s==1 || nivel_s==2 || nivel_s==4)
  {
    //alert("bloquear NIT");
    $("#Glosa").val("");
    $("#Glosa").prop( "disabled", true);
     
  }

  if(nivel_s==4)
  {
    $("#Cod_SIAT").prop( "disabled", false);
    $("#Cod_Flujo_SIAT").prop( "disabled", false);
  }

  if(nivel_s==1 || nivel_s==2 || nivel_s==3)
  {
    $("#Cod_SIAT").val("");
    $("#Cod_SIAT").prop( "disabled", true);
    
    $("#Cod_Flujo_SIAT").val("");
    $("#Cod_Flujo_SIAT").prop( "disabled", true);
  }

  if(nivel_s==5 && primerdig==1)
  {
     $("#Glosa").val("Bs.");
  }

  if(nivel_s==5 && primerdig==2)
  {
     $("#Glosa").val("Bs.");
  }

  if(nivel_s<5 && primerdig==2)
  {
     $("#Glosa").val("");
     $("#Glosa").prop( "disabled", true);
  }

  if(nivel_s<5 && primerdig==1)
  {
     $("#Glosa").val("");
     $("#Glosa").prop( "disabled", true);
  }


  if(nivel_s==5 && primerdig==4 || primerdig==5)
  {
     $("#Glosa").val("UFV");
  }  

  if(nivel_s==5 && primerdig==1 || primerdig==2 || primerdig==3)
  {
     $("#Glosa").val("Bs");
  }  


  if (nivel_s==1)
  {
     $("#Codigo").attr({ "maxlength" : 1 });
  }

  if (nivel_s==2)
  {
     $("#Codigo").attr({ "maxlength" : 3 });
  }

  if (nivel_s==3)
  {
     $("#Codigo").attr({ "maxlength" : 5 });
  }

  if (nivel_s==4)
  {
     $("#Codigo").attr({ "maxlength" : 9 });
  }  

  if (nivel_s==5)
  {
     $("#Codigo").attr({ "maxlength" : 12 });
  } 

}


function btn_modal_cts_activo_fijo()
{
   $("#myModal_Cuentas_Activo_Fijo").modal("show");
 
   var ob = "";
  
   $.ajax({
      type: "POST",
      url: url_p+"Cplan_cuentas/V_cuenta_activo_fijo",
      data: ob,
      beforeSend: function(objeto)
      {
        $("#panel_cuentas_activo_fijo").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      { 
        $("#panel_cuentas_activo_fijo").html(data);
      }
   });
  
}

function btn_registrar_cuenta_activo_fijo()
{
   $("#myModal_Registro_Cuentas_Activo_Fijo").modal("show");

   var ob = "";
  
   $.ajax({
      type: "POST",
      url: url_p+"Cplan_cuentas/VR_cuenta_activo_fijo",
      data: ob,
      beforeSend: function(objeto)
      {
        $("#panel_registro_cuentas_activo_fijo").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      { 
        $("#panel_registro_cuentas_activo_fijo").html(data);
      }
   });
   
}

function btn_guardar_cuenta_activo_fijo()
{
    var id_usuario = $("#id_usuario_session").val(); 
    var codigo_caf = $("#codigo_caf").val();
    var cuenta_caf = $("#cuenta_caf").val();
    var codigo_cda = $("#codigo_cda").val();
    var cuenta_cda = $("#cuenta_cda").val();
    var codigo_cdd = $("#codigo_cdd").val();
    var cuenta_cdd = $("#cuenta_cdd").val();
    var porcentaje_af = $("#porcentaje_af").val();

   var ob = {
        codigo_caf : codigo_caf,
        cuenta_caf : cuenta_caf,
        codigo_cda : codigo_cda,
        cuenta_cda : cuenta_cda,
        codigo_cdd : codigo_cdd,
        cuenta_cdd : cuenta_cdd,
        porcentaje_af : porcentaje_af,
        id_usuario : id_usuario
   };
  
   $.ajax({
      type: "POST",
      url: url_p+"Cplan_cuentas/VG_cuenta_activo_fijo",
      data: ob,
      beforeSend: function(objeto)
      {
        $("#panel_resultado_registro_cuentas_activo_fijo").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      { 
        $("#panel_resultado_registro_cuentas_activo_fijo").html(data);
      }
   });
   
}


function btn_buscar_ca_fijo(code_id_caf, panel_activo_fijo,id_caf)
{
    var txt_buscar = $("#"+code_id_caf).val();
    var ob = { txt_buscar:txt_buscar, code_id_caf:code_id_caf, panel_activo_fijo:panel_activo_fijo, id_caf:id_caf };

    $.ajax({
      type: "POST",
      url: url_p+"Cplan_cuentas/VBuscar_cuenta_activo_fijo",
      data: ob,
      beforeSend: function(objeto)
      {
        $("#"+panel_activo_fijo).html("<div class='cargando'>  </div>");
      },
      success:function(data)
      { 
        $("#"+panel_activo_fijo).html(data);
      }
    });
   
}

function btn_cerrar_panel_fijo(panel_activo_fijo)
{
    $("#"+panel_activo_fijo).html("");
      
}

function btn_select_pc_pointer_caf(id_pc,Codigo,Nombre,code_id_caf,id_activo_fijo)
{
    
}







