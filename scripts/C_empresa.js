
/* CONTROLADOR DE AREA DEL SISTEMA */
var url_p = "https://socrates.pag-bo.com/";
var lista = [];


 function cargar_datos(page)
 {  
    var ob = {page:page};
    $("#panel_resultado_busqueda").html("");

    $.ajax({
        type: "POST",
        url: url_p+"Cempresa/paginacion",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}


 function buscar_datos(page)
 {  
    var txt_buscar = $("#txt_buscar").val();

    var cadena = " Resultado para : <label> "+txt_buscar+" </label> ";
    $("#panel_resultado_busqueda").html(cadena);
    
    var ob = {page:page,txt_buscar : txt_buscar };

    $.ajax({
        type: "POST",
        url: url_p+"Cempresa/paginacion_find",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}

function empresa_o( logo,razon_social,encargado,id_rubro,nit_empresa,ci_encargado,id_departamento,email,password,direccion,telefono,celular,whatsapp )
{ 
   this.logo = logo; 
   this.razon_social = razon_social; 
   this.encargado = encargado; 
   this.id_rubro = id_rubro; 
   this.nit_empresa = nit_empresa; 
   this.ci_encargado = ci_encargado; 
   this.id_departamento = id_departamento; 
   this.email = email; 
   this.password = password; 
   this.direccion = direccion; 
   this.telefono = telefono; 
   this.celular = celular; 
   this.whatsapp = whatsapp; 
  
}

function btn_nuevo_empresa()
{
  $("#myModal_Register").modal("show");
 
  var ob = "";
  $.ajax({
      type: "POST",
      url: url_p+"Cempresa/R_empresa",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar").html(data);
      }
     });
}

function btn_agregar_carrito_empresa()
{ 
    var logo = $("#logo").val();
    var razon_social = $("#razon_social").val();
    var encargado = $("#encargado").val();
    var id_rubro = $("#id_rubro").val();
    var nit_empresa = $("#nit_empresa").val();
    var ci_encargado = $("#ci_encargado").val();
    var id_departamento = $("#id_departamento").val();
    var email = $("#email").val();
    var password = $("#password").val();
    var direccion = $("#direccion").val();
    var telefono = $("#telefono").val();
    var celular = $("#celular").val();
    var whatsapp = $("#whatsapp").val(); 
  
  if(logo != "" && razon_social != "" && encargado != "" && id_rubro != "" && nit_empresa != "" && ci_encargado != "" && id_departamento != "" && email != "" && password != "" && direccion != "" && telefono != "" && celular != "" && whatsapp != "" )
  {
  
  var obj_empresa = new empresa_o( logo,  razon_social,  encargado,  id_rubro,  nit_empresa,  ci_encargado,  id_departamento,  email,  password,  direccion,  telefono,  celular,  whatsapp );

  lista.push(obj_empresa);
  btn_mostrar_datos_empresa();
  }

  else {
  
       if (logo =="")
       {  
           $("#logo").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de logo </label>";
           $("#panel_resp_logo").html(res);  
       }

       
       if (razon_social =="")
       {  
           $("#razon_social").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de razon_social </label>";
           $("#panel_resp_razon_social").html(res);  
       }

       
       if (encargado =="")
       {  
           $("#encargado").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de encargado </label>";
           $("#panel_resp_encargado").html(res);  
       }

       
       if (id_rubro =="")
       {  
           $("#id_rubro").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de rubro </label>";
           $("#panel_resp_id_rubro").html(res);  
       }

       
       if (nit_empresa =="")
       {  
           $("#nit_empresa").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de nit_empresa </label>";
           $("#panel_resp_nit_empresa").html(res);  
       }

       
       if (ci_encargado =="")
       {  
           $("#ci_encargado").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de ci_encargado </label>";
           $("#panel_resp_ci_encargado").html(res);  
       }

       
       if (id_departamento =="")
       {  
           $("#id_departamento").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de departamento </label>";
           $("#panel_resp_id_departamento").html(res);  
       }

       
       if (email =="")
       {  
           $("#email").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de email </label>";
           $("#panel_resp_email").html(res);  
       }

       
       if (password =="")
       {  
           $("#password").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de password </label>";
           $("#panel_resp_password").html(res);  
       }

       
       if (direccion =="")
       {  
           $("#direccion").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de direccion </label>";
           $("#panel_resp_direccion").html(res);  
       }

       
       if (telefono =="")
       {  
           $("#telefono").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de telefono </label>";
           $("#panel_resp_telefono").html(res);  
       }

       
       if (celular =="")
       {  
           $("#celular").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de celular </label>";
           $("#panel_resp_celular").html(res);  
       }

       
       if (whatsapp =="")
       {  
           $("#whatsapp").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de whatsapp </label>";
           $("#panel_resp_whatsapp").html(res);  
       }

       
  //Final de la codicional else
  }

 //Final de la fucion
}

function btn_mostrar_datos_empresa()
{
  var cadena = "<table class='table table-bordered table-condensed table-striped table-hover'>";
  
  cadena = cadena + "<tr>"+ "<th> logo </th> " +  "<th> razon_social </th> " +  "<th> encargado </th> " +  "<th> rubro </th> " +  "<th> nit_empresa </th> " +  "<th> ci_encargado </th> " +  "<th> departamento </th> " +  "<th> email </th> " +  "<th> password </th> " +  "<th> direccion </th> " +  "<th> telefono </th> " +  "<th> celular </th> " +  "<th> whatsapp </th> " +  "<th> Opciones </th>"; 

  cadena = cadena +"</tr>";

  for(var i = 0 ; i<lista.length; i++)
  {
    logo = lista[i].logo; 
    razon_social = lista[i].razon_social; 
    encargado = lista[i].encargado; 
    rubro = lista[i].rubro; 
    nit_empresa = lista[i].nit_empresa; 
    ci_encargado = lista[i].ci_encargado; 
    departamento = lista[i].departamento; 
    email = lista[i].email; 
    password = lista[i].password; 
    direccion = lista[i].direccion; 
    telefono = lista[i].telefono; 
    celular = lista[i].celular; 
    whatsapp = lista[i].whatsapp; 
      cadena = cadena + "<tr>"+
    "<td>"+ logo +"</td>"+
    "<td>"+ razon_social +"</td>"+
    "<td>"+ encargado +"</td>"+
    "<td>"+ rubro +"</td>"+
    "<td>"+ nit_empresa +"</td>"+
    "<td>"+ ci_encargado +"</td>"+
    "<td>"+ departamento +"</td>"+
    "<td>"+ email +"</td>"+
    "<td>"+ password +"</td>"+
    "<td>"+ direccion +"</td>"+
    "<td>"+ telefono +"</td>"+
    "<td>"+ celular +"</td>"+
    "<td>"+ whatsapp +"</td>"+ " <td  class='col-lg-1' > <center> <button class='btn btn-danger btn-xs' onclick='btn_eliminar_item("+i+");' > X </button> </center> </td><tr>"; 
   }

   cadena = cadena +" </table> ";

   $("#panel_listado_empresa").html(cadena);  
 
} 

function btn_eliminar_item(contador)
{
  lista.splice( contador , 1);
  btn_mostrar_datos_empresa();
}

function btn_registrar_empresa()
{ 
   var id_usuario = $("#id_usuario_session").val(); 

   var logo = $("#logo").val();
   var razon_social = $("#razon_social").val();
   var encargado = $("#encargado").val();
   var id_rubro = $("#idrubro").val();
   var nit_empresa = $("#nit_empresa").val();
   var ci_encargado = $("#ci_encargado").val();
   var id_departamento = $("#iddepartamento").val();
   var email = $("#email").val();
   var password = $("#password").val();
   var direccion = $("#direccion").val();
   var telefono = $("#telefono").val();
   var celular = $("#celular").val();
   var whatsapp = $("#whatsapp").val();

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_usuario", id_usuario);

  $.ajax({
   url: url_p+"Cempresa/G_empresa",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){
     
     $("#btn_registrar").prop("disabled", true); 
     $("#panel_modal_resultado_registrar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_registrar").prop("disabled", false);  
     $("#panel_modal_resultado_registrar").html(data);
   }
 });  

}

function btn_examinar_empresa(id_empresa)
{
  $("#myModal_View").modal("show");

  var ob = {id_empresa : id_empresa};
  $.ajax({
      type: "POST",
      url: url_p+"Cempresa/VD_empresa",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_examinar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_examinar").html(data);
      }
     });

}

function btn_editar_empresa(id_empresa)
{
  $("#myModal_Update").modal("show");
  $("#id_empresa_edicion").val(id_empresa);
  
  var ob = {id_empresa : id_empresa};
  $.ajax({
      type: "POST",
      url: url_p+"Cempresa/VE_empresa",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_editar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_editar").html(data);
      }
     });

}

function btn_guardar_empresa()
{
  var id_empresa = $("#id_empresa_edicion").val();

  var logo = $("#logo").val();
  var razon_social = $("#razon_social").val();
  var encargado = $("#encargado").val();
  var id_rubro = $("#id_rubro").val();
  var nit_empresa = $("#nit_empresa").val();
  var ci_encargado = $("#ci_encargado").val();
  var id_departamento = $("#id_departamento").val();
  var email = $("#email").val();
  var password = $("#password").val();
  var direccion = $("#direccion").val();
  var telefono = $("#telefono").val();
  var celular = $("#celular").val();
  var whatsapp = $("#whatsapp").val();
  
  var ob = { id_empresa : id_empresa, logo:logo, razon_social:razon_social, encargado:encargado, id_rubro:id_rubro, nit_empresa:nit_empresa, ci_encargado:ci_encargado, id_departamento:id_departamento, email:email, password:password, direccion:direccion, telefono:telefono, celular:celular, whatsapp:whatsapp }; 

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_empresa", id_empresa);

  $.ajax({
   url: url_p+"Cempresa/VU_empresa",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){

     $("#btn_editar").prop("disabled", true);
     $("#panel_modal_respuesta_editar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_editar").prop("disabled", false);  
     $("#panel_modal_respuesta_editar").html(data);
   
   }
 }); 

}

function btn_borrar_empresa(id_empresa)
{
  $("#myModal_Delete").modal("show");
  $("#id_empresa_eliminar").val(id_empresa);

}

function btn_eliminar_empresa()
{
  var id_empresa = $("#id_empresa_eliminar").val();

  var ob = { id_empresa : id_empresa };
  
  $.ajax({
      type: "POST",
      url: url_p+"Cempresa/VB_empresa",
      data: ob,
      beforeSend: function(objeto){

        $("#btn_borrar").prop("disabled", true);
        $("#panel_modal_eliminar").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        $("#btn_borrar").prop("disabled", false);   
        $("#panel_modal_eliminar").html(data);
        
        setTimeout(function(){ $("#panel_modal_eliminar").html(""); },1000);
        setTimeout(function(){ $("#myModal_Delete").modal("hide"); },2000);
        setTimeout(function(){ location.reload(); },3000);
      }
  });

}


function previsualizar()
{
  readImage();

  $("#img_resp_logo").css("width","80%");
  $("#img_resp_logo").css("height","350");
  $("#img_resp_logo").html("<br></br>");
}

function readImage () {
$("#img_resp_logo").html("<div class='cargando'></div>");
var input = document.getElementById("logo");
if (input.files && input.files[0]) {
  var reader = new FileReader();
  reader.onload = function (e) {
      $("#img_resp_logo").attr("src", e.target.result); // Renderizamos la imagen
  }
  reader.readAsDataURL(input.files[0]);
}
}
  
var estado = 0;

function btn_submenu(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_"+id).css("display","none");
  }
  }

}

function btn_submenu_xs(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_xs"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_xs"+id).css("display","none");
  }
  }

}


function select(id,dato,area)
{   
  $("#id_"+area).val(id);
  $("#"+area).html(dato);
}

function btn_asignar_empresa(id_empresa,razon_social)
{ 
  alert("id emp = "+id_empresa+" ->  "+razon_social );
  
  $("#empresa_select").html("'"+razon_social+"'");
  $("#id_empresa_select").val(id_empresa);
  
}



