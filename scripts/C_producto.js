/* CONTROLADOR DE AREA DEL SISTEMA */
 
var lista = [];


 function cargar_datos(page)
 {  
    var id_empresa = $("#id_empresa_select").val();
    var id_gestion = $("#id_gestion_select").val();

    var ob = { page:page, id_empresa:id_empresa, id_gestion:id_gestion };
    $("#panel_resultado_busqueda").html("");
 
    $.ajax({
        type: "POST",
        url: url_p+"Cproducto/paginacion",
        data: ob,
        beforeSend: function(objeto)
        {
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data)
        {
          $("#panel_paginacion_datos").html(data);
        }
      });
}


 function buscar_datos(page)
 {  
    var txt_buscar = $("#txt_buscar").val();

    var cadena = " Resultado para : <label> "+txt_buscar+" </label> ";
    $("#panel_resultado_busqueda").html(cadena);

    var id_empresa = $("#id_empresa_select").val();
    var id_gestion = $("#id_gestion_select").val();
    
    var ob = { page:page, txt_buscar:txt_buscar, id_empresa:id_empresa, id_gestion:id_gestion };

    $.ajax({
        type: "POST",
        url: url_p+"Cproducto/paginacion_find",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}

function producto_o( producto,portada,codigo_bar,cantidad,precio_compra,precio_venta,descripcion,id_area )
{ 
   this.producto = producto; 
   this.portada = portada; 
   this.codigo_bar = codigo_bar; 
   this.cantidad = cantidad; 
   this.precio_compra = precio_compra; 
   this.precio_venta = precio_venta; 
   this.descripcion = descripcion; 
   this.id_area = id_area; 
}

function btn_nuevo_producto()
{
  $("#myModal_Register").modal("show");
  var id_empresa = $("#id_empresa_select").val();
  var id_gestion = $("#id_gestion_select").val();

  var ob = { id_empresa:id_empresa, id_gestion:id_gestion }; 
  
  $.ajax({
      type: "POST",
      url: url_p+"Cproducto/R_producto",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar").html(data);
      }
     });
}

function btn_agregar_carrito_producto()
{ 
    var producto = $("#producto").val();
    var portada = $("#portada").val();
    var codigo_bar = $("#codigo_bar").val();
    var cantidad = $("#cantidad").val();
    var precio_compra = $("#precio_compra").val();
    var precio_venta = $("#precio_venta").val();
    var descripcion = $("#descripcion").val();
    var id_area = $("#id_area").val(); 
  
  if(producto != "" && portada != "" && codigo_bar != "" && cantidad != "" && precio_compra != "" && precio_venta != "" && descripcion != "" && id_area != "" )
  {
  
  var obj_producto = new producto_o( producto,  portada,  codigo_bar,  cantidad,  precio_compra,  precio_venta,  descripcion,  id_area );

  lista.push(obj_producto);
  btn_mostrar_datos_producto();
  }

  else {
  
       if (producto =="")
       {  
           $("#producto").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de producto </label>";
           $("#panel_resp_producto").html(res);  
       }

       
       if (portada =="")
       {  
           $("#portada").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de portada </label>";
           $("#panel_resp_portada").html(res);  
       }

       
       if (codigo_bar =="")
       {  
           $("#codigo_bar").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de codigo_bar </label>";
           $("#panel_resp_codigo_bar").html(res);  
       }

       
       if (cantidad =="")
       {  
           $("#cantidad").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de cantidad </label>";
           $("#panel_resp_cantidad").html(res);  
       }

       
       if (precio_compra =="")
       {  
           $("#precio_compra").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de precio_compra </label>";
           $("#panel_resp_precio_compra").html(res);  
       }

       
       if (precio_venta =="")
       {  
           $("#precio_venta").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de precio_venta </label>";
           $("#panel_resp_precio_venta").html(res);  
       }

       
       if (descripcion =="")
       {  
           $("#descripcion").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de descripcion </label>";
           $("#panel_resp_descripcion").html(res);  
       }

       
       if (id_area =="")
       {  
           $("#id_area").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de area </label>";
           $("#panel_resp_id_area").html(res);  
       }

       
  //Final de la codicional else
  }

 //Final de la fucion
}

function btn_mostrar_datos_producto()
{
  var cadena = "<table class='table table-bordered table-condensed table-striped table-hover'>";
  
  cadena = cadena + "<tr>"+ "<th> producto </th> " +  "<th> portada </th> " +  "<th> codigo_bar </th> " +  "<th> cantidad </th> " +  "<th> precio_compra </th> " +  "<th> precio_venta </th> " +  "<th> descripcion </th> " +  "<th> area </th> " +  "<th> Opciones </th>"; 

  cadena = cadena +"</tr>";

  for(var i = 0 ; i<lista.length; i++)
  {
    producto = lista[i].producto; 
    portada = lista[i].portada; 
    codigo_bar = lista[i].codigo_bar; 
    cantidad = lista[i].cantidad; 
    precio_compra = lista[i].precio_compra; 
    precio_venta = lista[i].precio_venta; 
    descripcion = lista[i].descripcion; 
    area = lista[i].area; 
      cadena = cadena + "<tr>"+
    "<td>"+ producto +"</td>"+
    "<td>"+ portada +"</td>"+
    "<td>"+ codigo_bar +"</td>"+
    "<td>"+ cantidad +"</td>"+
    "<td>"+ precio_compra +"</td>"+
    "<td>"+ precio_venta +"</td>"+
    "<td>"+ descripcion +"</td>"+
    "<td>"+ area +"</td>"+ " <td  class='col-lg-1' > <center> <button class='btn btn-danger btn-xs' onclick='btn_eliminar_item("+i+");' > X </button> </center> </td><tr>"; 
   }

   cadena = cadena +" </table> ";

   $("#panel_listado_producto").html(cadena);  
 
} 

function btn_eliminar_item(contador)
{
  lista.splice( contador , 1);
  btn_mostrar_datos_producto();
}

function btn_registrar_producto()
{ 
   var id_usuario = $("#id_usuario_session").val(); 

   var producto = $("#producto").val();
   var portada = $("#portada").val();
   var codigo_bar = $("#codigo_bar").val();
   var cantidad = $("#cantidad").val();
   var precio_compra = $("#precio_compra").val();
   var precio_venta = $("#precio_venta").val();
   var descripcion = $("#descripcion").val();
   var id_area = $("#id_area").val();

   var id_empresa = $("#id_empresa_select").val();
   var id_gestion = $("#id_gestion_select").val();

   var formData = new FormData($("#formulario")[0]);
   formData.append("id_usuario", id_usuario);
   formData.append("id_empresa", id_empresa);
   formData.append("id_gestion", id_gestion);

  $.ajax({
   url: url_p+"Cproducto/G_producto",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){
     
     //$("#btn_registrar").prop("disabled", true); 
     $("#panel_modal_resultado_registrar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     //$("#btn_registrar").prop("disabled", false);  
     $("#panel_modal_resultado_registrar").html(data);
 
   }
 });  


}

function btn_examinar_producto(id_producto)
{
  $("#myModal_View").modal("show");

  var ob = {id_producto : id_producto};
  $.ajax({
      type: "POST",
      url: url_p+"Cproducto/VD_producto",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_examinar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_examinar").html(data);
      }
     });

}

function btn_editar_producto(id_producto)
{
  $("#myModal_Update").modal("show");
  $("#id_producto_edicion").val(id_producto);
  var id_empresa = $("#id_empresa_select").val();

  var ob = {id_producto : id_producto, id_empresa:id_empresa };
  
  $.ajax({
      type: "POST",
      url: url_p+"Cproducto/VE_producto",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_editar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_editar").html(data);
      }
     });

}

function btn_guardar_producto()
{

  var id_producto = $("#id_producto_edicion").val();

  var producto = $("#producto").val();
  var portada = $("#portada").val();
  var codigo_bar = $("#codigo_bar").val();
  var cantidad = $("#cantidad").val();
  var precio_compra = $("#precio_compra").val();
  var precio_venta = $("#precio_venta").val();
  var descripcion = $("#descripcion").val();
  var id_area = $("#id_area").val();
  var ob = { id_producto : id_producto, producto:producto, portada:portada, codigo_bar:codigo_bar, cantidad:cantidad, precio_compra:precio_compra, precio_venta:precio_venta, descripcion:descripcion, id_area:id_area }; 

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_producto", id_producto);

  $.ajax({
   url: url_p+"Cproducto/VU_producto",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){

     $("#btn_editar").prop("disabled", true);
     $("#panel_modal_respuesta_editar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_editar").prop("disabled", false);  
     $("#panel_modal_respuesta_editar").html(data);
   
   }
 }); 

}

function btn_borrar_producto(id_producto)
{
  $("#myModal_Delete").modal("show");
  $("#id_producto_eliminar").val(id_producto);

}

function btn_eliminar_producto()
{
  var id_producto = $("#id_producto_eliminar").val();

  var ob = { id_producto : id_producto };
  
  $.ajax({
      type: "POST",
      url: url_p+"Cproducto/VB_producto",
      data: ob,
      beforeSend: function(objeto){

        $("#btn_borrar").prop("disabled", true);
        $("#panel_modal_eliminar").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        $("#btn_borrar").prop("disabled", false);   
        $("#panel_modal_eliminar").html(data);
        
      }
  });

}


function previsualizar()
{
  readImage();

  $("#img_resp_portada").css("width","80%");
  $("#img_resp_portada").css("height","300");
  $("#img_resp_portada").html("<br></br>");
}

function readImage () 
{
  $("#img_resp_portada").html("<div class='cargando'></div>");
  var input = document.getElementById("portada");
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $("#img_resp_portada").attr("src", e.target.result); // Renderizamos la imagen
  }
  reader.readAsDataURL(input.files[0]);
  }
}
  
var estado = 0;

function btn_submenu(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_"+id).css("display","none");
  }
  }

}

function btn_submenu_xs(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_xs"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_xs"+id).css("display","none");
  }
  }

}


function select(id,dato,area)
{   
  $("#id_"+area).val(id);
  $("#"+area).html(dato);
}


