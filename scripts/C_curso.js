
/* CONTROLADOR DE AREA DEL SISTEMA */
var lista = [];


 function cargar_datos(page)
 {  
    var ob = {page:page};
    $("#panel_resultado_busqueda").html("");

    $.ajax({
        type: "POST",
        url: url_p+"Ccurso/paginacion",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}


 function buscar_datos(page)
 {  
    var txt_buscar = $("#txt_buscar").val();

    var cadena = " Resultado para : <label> "+txt_buscar+" </label> ";
    $("#panel_resultado_busqueda").html(cadena);
    
    var ob = {page:page,txt_buscar : txt_buscar };

    $.ajax({
        type: "POST",
        url: url_p+"Ccurso/paginacion_find",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}

function curso_o( portada_curso,curso,descripcion_curso,costo_curso,fecha_curso,duracion_curso,certificado_curso )
{ 
 this.portada_curso = portada_curso; 
   this.curso = curso; 
   this.descripcion_curso = descripcion_curso; 
   this.costo_curso = costo_curso; 
   this.fecha_curso = fecha_curso; 
   this.duracion_curso = duracion_curso; 
   this.certificado_curso = certificado_curso; 
  
}

function btn_nuevo_curso()
{
  $("#myModal_Register").modal("show");
 
  var ob = "";
  $.ajax({
      type: "POST",
      url: url_p+"Ccurso/R_curso",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar").html(data);
      }
     });
}

function btn_agregar_carrito_curso()
{ 
    var portada_curso = $("#portada_curso").val();
    var curso = $("#curso").val();
    var descripcion_curso = $("#descripcion_curso").val();
    var costo_curso = $("#costo_curso").val();
    var fecha_curso = $("#fecha_curso").val();
    var duracion_curso = $("#duracion_curso").val();
    var certificado_curso = $("#certificado_curso").val(); 
  
  if(portada_curso != "" && curso != "" && descripcion_curso != "" && costo_curso != "" && fecha_curso != "" && duracion_curso != "" && certificado_curso != "" )
  {
  
  var obj_curso = new curso_o( portada_curso,  curso,  descripcion_curso,  costo_curso,  fecha_curso,  duracion_curso,  certificado_curso );

  lista.push(obj_curso);
  btn_mostrar_datos_curso();
  }

  else {
  
       if (portada_curso =="")
       {  
           $("#portada_curso").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de portada_curso </label>";
           $("#panel_resp_portada_curso").html(res);  
       }

       
       if (curso =="")
       {  
           $("#curso").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de curso </label>";
           $("#panel_resp_curso").html(res);  
       }

       
       if (descripcion_curso =="")
       {  
           $("#descripcion_curso").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de descripcion_curso </label>";
           $("#panel_resp_descripcion_curso").html(res);  
       }

       
       if (costo_curso =="")
       {  
           $("#costo_curso").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de costo_curso </label>";
           $("#panel_resp_costo_curso").html(res);  
       }

       
       if (fecha_curso =="")
       {  
           $("#fecha_curso").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de fecha_curso </label>";
           $("#panel_resp_fecha_curso").html(res);  
       }

       
       if (duracion_curso =="")
       {  
           $("#duracion_curso").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de duracion_curso </label>";
           $("#panel_resp_duracion_curso").html(res);  
       }

       
       if (certificado_curso =="")
       {  
           $("#certificado_curso").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de certificado_curso </label>";
           $("#panel_resp_certificado_curso").html(res);  
       }

       
  //Final de la codicional else
  }

 //Final de la fucion
}

function btn_mostrar_datos_curso()
{
  var cadena = "<table class='table table-bordered table-condensed table-striped table-hover'>";
  
  cadena = cadena + "<tr>"+ "<th> portada_curso </th> " +  "<th> curso </th> " +  "<th> descripcion_curso </th> " +  "<th> costo_curso </th> " +  "<th> fecha_curso </th> " +  "<th> duracion_curso </th> " +  "<th> certificado_curso </th> " +  "<th> Opciones </th>"; 

  cadena = cadena +"</tr>";

  for(var i = 0 ; i<lista.length; i++)
  {
    portada_curso = lista[i].portada_curso; 
    curso = lista[i].curso; 
    descripcion_curso = lista[i].descripcion_curso; 
    costo_curso = lista[i].costo_curso; 
    fecha_curso = lista[i].fecha_curso; 
    duracion_curso = lista[i].duracion_curso; 
    certificado_curso = lista[i].certificado_curso; 
      cadena = cadena + "<tr>"+
    "<td>"+ portada_curso +"</td>"+
    "<td>"+ curso +"</td>"+
    "<td>"+ descripcion_curso +"</td>"+
    "<td>"+ costo_curso +"</td>"+
    "<td>"+ fecha_curso +"</td>"+
    "<td>"+ duracion_curso +"</td>"+
    "<td>"+ certificado_curso +"</td>"+ " <td  class='col-lg-1' > <center> <button class='btn btn-danger btn-xs' onclick='btn_eliminar_item("+i+");' > X </button> </center> </td><tr>"; 
   }

   cadena = cadena +" </table> ";

   $("#panel_listado_curso").html(cadena);  
 
} 

function btn_eliminar_item(contador)
{
  lista.splice( contador , 1);
  btn_mostrar_datos_curso();
}

function btn_registrar_curso()
{ 
   var id_usuario = $("#id_usuario_session").val(); 

   var portada_curso = $("#portada_curso").val();
   var curso = $("#curso").val();
    
   var costo_curso = $("#costo_curso").val();
   var fecha_curso = $("#fecha_curso").val();
   var duracion_curso = $("#duracion_curso").val();
   var certificado_curso = $("#certificado_curso").val();
   
   var descripcion_curso = CKEDITOR.instances.descripcion_curso.getData();

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_usuario", id_usuario);
  formData.append("descripcion_curso", descripcion_curso);

  $.ajax({
   url: url_p+"Ccurso/G_curso",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){
     
     $("#btn_registrar").prop("disabled", true); 
     $("#panel_modal_resultado_registrar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_registrar").prop("disabled", false);  
     $("#panel_modal_resultado_registrar").html(data);
 
   }
 });  


}

function btn_examinar_curso(id_curso)
{
  $("#myModal_View").modal("show");

  var ob = {id_curso : id_curso};
  $.ajax({
      type: "POST",
      url: url_p+"Ccurso/VD_curso",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_examinar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_examinar").html(data);
      }
     });

}

function btn_editar_curso(id_curso)
{
  $("#myModal_Update").modal("show");
  $("#id_curso_edicion").val(id_curso);
  
  var ob = {id_curso : id_curso};
  $.ajax({
      type: "POST",
      url: url_p+"Ccurso/VE_curso",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_editar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_editar").html(data);
      }
     });

}

function btn_guardar_curso()
{
  var id_curso = $("#id_curso_edicion").val();
  var portada_curso = $("#portada_curso").val();
  var curso = $("#curso").val();
  
  var descripcion_curso = CKEDITOR.instances.descripcion_curso_ed.getData();
  var costo_curso = $("#costo_curso").val();
  var fecha_curso = $("#fecha_curso").val();
  var duracion_curso = $("#duracion_curso").val();
  var certificado_curso = $("#certificado_curso").val();
  
  var ob = { id_curso : id_curso, portada_curso:portada_curso, curso:curso, descripcion_curso:descripcion_curso, costo_curso:costo_curso, fecha_curso:fecha_curso, duracion_curso:duracion_curso, certificado_curso:certificado_curso }; 

  var formData = new FormData($("#formulario_ed")[0]);
  formData.append("id_curso", id_curso);
  formData.append("descripcion_curso", descripcion_curso);

  $.ajax({
   url: url_p+"Ccurso/VU_curso",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){

     $("#btn_editar").prop("disabled", true);
     $("#panel_modal_respuesta_editar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_editar").prop("disabled", false);  
     $("#panel_modal_respuesta_editar").html(data);
   
   }
 }); 
 

}

function btn_borrar_curso(id_curso)
{
  $("#myModal_Delete").modal("show");
  $("#id_curso_eliminar").val(id_curso);

}

function btn_eliminar_curso()
{
  var id_curso = $("#id_curso_eliminar").val();

  var ob = { id_curso : id_curso };
  
  $.ajax({
      type: "POST",
      url: url_p+"Ccurso/VB_curso",
      data: ob,
      beforeSend: function(objeto){

        $("#btn_borrar").prop("disabled", true);
        $("#panel_modal_eliminar").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        $("#btn_borrar").prop("disabled", false);   
        $("#panel_modal_eliminar").html(data);
        
        setTimeout(function(){ $("#panel_modal_eliminar").html(""); },1000);
        setTimeout(function(){ $("#myModal_Delete").modal("hide"); },2000);
        setTimeout(function(){ cargar_datos(1); },3000);
      }
  });

}


     function previsualizar()
     {
        readImage();

        $("#img_resp_portada_curso").css("width","80%");
        $("#img_resp_portada_curso").css("height","300");
        $("#img_resp_portada_curso").html("<br></br>");
     }

     function readImage() 
     {
      $("#img_resp_portada_curso").html("<div class='cargando'></div>");
      var input = document.getElementById("portada_curso");
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $("#img_resp_portada_curso").attr("src", e.target.result); // Renderizamos la imagen
        }
        reader.readAsDataURL(input.files[0]);
      }
     }

    
     function previsualizar_certificado()
     {
        readImage_certificado();

        $("#img_resp_certificado_curso").css("width","80%");
        $("#img_resp_certificado_curso").css("height","300");
        $("#img_resp_certificado_curso").html("<br></br>");
     }

     function readImage_certificado() 
     {
      $("#img_resp_certificado_curso").html("<div class='cargando'></div>");
      var input = document.getElementById("certificado_curso");
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $("#img_resp_certificado_curso").attr("src", e.target.result); // Renderizamos la imagen
        }
        reader.readAsDataURL(input.files[0]);
      }
     }
 
    
    
    var estado = 0;
    
    function btn_submenu(id)
    {
      if(estado==0)
      { //alert(id+" - "+estado);
     
        $("#panel_opciones_"+id).css("display","block");
        estado++;
      }
    
      else{
      if(estado>0)
      { //alert(id+" - "+estado);
        estado=0;
        
        $("#panel_opciones_"+id).css("display","none");
      }
      }
    
    }
    
    function btn_submenu_xs(id)
    {
      if(estado==0)
      { //alert(id+" - "+estado);
     
        $("#panel_opciones_xs"+id).css("display","block");
        estado++;
      }
    
      else{
      if(estado>0)
      { //alert(id+" - "+estado);
        estado=0;
        
        $("#panel_opciones_xs"+id).css("display","none");
      }
      }
    
    }
    
    
    function select(id,dato,area)
    {   
      $("#id_"+area).val(id);
      $("#"+area).html(dato);
    }
    


