
/* CONTROLADOR DE AREA DEL SISTEMA */
var url_p = "https://socrates.pag-bo.com/";
var lista = [];


 function cargar_datos_moneda(page)
 {  
    var ob = {page:page};
    $("#panel_resultado_busqueda").html("");

    $.ajax({
        type: "POST",
        url: url_p+"Cmoneda/paginacion",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}


 function buscar_datos_moneda(page)
 {  
    var txt_buscar = $("#txt_buscar").val();

    var cadena = " Resultado para : <label> "+txt_buscar+" </label> ";
    $("#panel_resultado_busqueda").html(cadena);
    
    var ob = {page:page,txt_buscar : txt_buscar };

    $.ajax({
        type: "POST",
        url: url_p+"Cmoneda/paginacion_find",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}

function moneda_o(moneda)
{ 
 this.moneda = moneda; 
}

function btn_nuevo_moneda()
{
  
  $("#myModal_Register_moneda").modal("show");
  var ob = "";
  $.ajax({
      type: "POST",
      url: url_p+"Cmoneda/R_moneda",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar_moneda").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar_moneda").html(data);
      }
  });

}

function btn_agregar_carrito_moneda()
{ 
  var moneda = $("#moneda").val(); 
  
  if(moneda != "" )
  {
    var obj_moneda = new moneda_o( moneda );
    lista.push(obj_moneda);
    btn_mostrar_datos_moneda();
  }

  else {
  
       if (moneda =="")
       {  
           $("#moneda").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de moneda </label>";
           $("#panel_resp_moneda").html(res);  
       }

       
  //Final de la codicional else
  }

 //Final de la fucion
}

function btn_mostrar_datos_moneda()
{
  var cadena = "<table class='table table-bordered table-condensed table-striped table-hover'>";
  
  cadena = cadena + "<tr>"+ "<th> moneda </th> " +  "<th> Opciones </th>"; 

  cadena = cadena +"</tr>";

  for(var i = 0 ; i<lista.length; i++)
  {
    moneda = lista[i].moneda; 
      cadena = cadena + "<tr>"+
    "<td>"+ moneda +"</td>"+ " <td  class='col-lg-1' > <center> <button class='btn btn-danger btn-xs' onclick='btn_eliminar_item("+i+");' > X </button> </center> </td><tr>"; 
   }

   cadena = cadena +" </table> ";

   $("#panel_listado_moneda").html(cadena);  
 
} 

function btn_eliminar_item(contador)
{
  lista.splice( contador , 1);
  btn_mostrar_datos_moneda();
}

function btn_registrar_moneda()
{ 
  var id_usuario = $("#id_usuario_session").val(); 

   var lista_area = JSON.stringify(lista);
   var ob = {lista : lista_area, id_usuario:id_usuario };

  
  $.ajax({
      type: "POST",
      url: url_p+"Cmoneda/G_moneda",
      data: ob,
      beforeSend: function(objeto){

        $("#panel_modal_resultado_registrar_moneda").html("<div class='cargando'>  </div>");
        $("#btn_registrar").prop("disabled", true);

      },
      success:function(data)
      { 
        $("#btn_registrar").prop("disabled", false);   
        $("#panel_modal_resultado_registrar_moneda").html(data);
 
      }
  });
}

function btn_examinar_moneda(id_moneda)
{
  $("#myModal_View_moneda").modal("show");

  var ob = {id_moneda : id_moneda};
  $.ajax({
      type: "POST",
      url: url_p+"Cmoneda/VD_moneda",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_examinar_moneda").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_examinar_moneda").html(data);
      }
     });

}

function btn_editar_moneda(id_moneda)
{
  $("#myModal_Update_moneda").modal("show");
  $("#id_moneda_edicion").val(id_moneda);
  
  var ob = {id_moneda : id_moneda};
  $.ajax({
      type: "POST",
      url: url_p+"Cmoneda/VE_moneda",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_editar_moneda").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_editar_moneda").html(data);
      }
     });

}

function btn_guardar_moneda()
{

  var id_moneda = $("#id_moneda_edicion").val();

  var moneda = $("#moneda").val();
  var ob = { id_moneda : id_moneda, moneda:moneda }; 

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_moneda", id_moneda);

  $.ajax({
   url: url_p+"Cmoneda/VU_moneda",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){

     $("#btn_editar").prop("disabled", true);
     $("#panel_modal_respuesta_editar_moneda").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_editar").prop("disabled", false);  
     $("#panel_modal_respuesta_editar_moneda").html(data);
   
   }
 }); 

}

function btn_borrar_moneda(id_moneda)
{
  $("#myModal_Delete_moneda").modal("show");
  $("#id_moneda_eliminar").val(id_moneda);

}

function btn_eliminar_moneda()
{
  var id_moneda = $("#id_moneda_eliminar").val();

  var ob = { id_moneda : id_moneda };
  
  $.ajax({
      type: "POST",
      url: url_p+"Cmoneda/VB_moneda",
      data: ob,
      beforeSend: function(objeto){

        $("#btn_borrar").prop("disabled", true);
        $("#panel_modal_eliminar_moneda").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        $("#btn_borrar").prop("disabled", false);   
        $("#panel_modal_eliminar_moneda").html(data);
        
        setTimeout(function(){ $("#panel_modal_eliminar_moneda").html(""); },1000);
        setTimeout(function(){ $("#myModal_Delete_moneda").modal("hide"); },2000);
        setTimeout(function(){ cargar_datos_moneda(1); },3000);
      }
  });

}


var estado = 0;

function btn_submenu(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_"+id).css("display","none");
  }
  }

}

function btn_submenu_xs(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_xs"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_xs"+id).css("display","none");
  }
  }

}


function select(id,dato,area)
{   
  $("#id_"+area).val(id);
  $("#"+area).html(dato);
}


