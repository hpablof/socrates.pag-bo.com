/* FUNCIONES DE MENU DEL SISTEMA */

function btn_ampliar(modal)
{
   var modal_ancho = $("#"+modal).width(); 
   
   if(modal_ancho>1000)
   {
      $("#"+modal).css('width','50%');
      $("#"+modal).css('height','720px');

   }
   if(modal_ancho<700)
   {
      $("#"+modal).css('width','100%');
      $("#"+modal).css('height','720px');
   }
   
}

function btn_ocultar(modal)
{
$("#"+modal).css('display','none');

}

function btn_mostrar(modal)
{
  $("#"+modal).css('display','block');
}

/* FUNCIONES DE LAS AREAS */
function btn_menu_moneda()
{
    $("#modal_moneda").css('display','block');
    
    var page=1;
    var ob = {page:page};
    
    $.ajax({
        type: "POST",
        url: url_p+"Cmoneda/",
        data: ob,
        beforeSend: function(objeto){
          $("#contenedor_moneda").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#contenedor_moneda").html(data);

        }
      });
 
}
 

function btn_menu_nits_empresas()
{
    $("#menu_nits_empresas").css('display','block');
 
    var modal_select_actual = $("#modal_select_actual").val();
    // $("#"+modal_select_actual).css('display','none');
    $("#"+modal_select_actual).html('');
    $("#modal_select_actual").val('menu_nits_empresas');
    
    var page=1;
    var ob = {page:page};
    
    $.ajax({
        type: "POST",
        url: url_p+"Crazon_social/",
        data: ob,
        beforeSend: function(objeto)
        {
          $("#contenedor_nits_empresas").html("<div class='cargando'>  </div>");
        },
        success:function(data)
        {
          $("#contenedor_nits_empresas").html(data);
        }
      });
}

function btn_menu_rubro()
{
    $("#menu_rubro").css('display','block');
 
    var modal_select_actual = $("#modal_select_actual").val();
    // $("#"+modal_select_actual).css('display','none');
    $("#"+modal_select_actual).html('');
    $("#modal_select_actual").val('menu_rubro');
    
    var page=1;
    var ob = {page:page};
    
    $.ajax({
        type: "POST",
        url: url_p+"Crubro/",
        data: ob,
        beforeSend: function(objeto)
        {
          $("#contenedor_rubro").html("<div class='cargando'>  </div>");
        },
        success:function(data)
        {
          $("#contenedor_rubro").html(data);
        }
      });
}


<!-- ->


function btn_menu_nivel_pc()
{
    $("#menu_niveles").css('display','block');
 
    var modal_select_actual = $("#modal_select_actual").val();
    // $("#"+modal_select_actual).css('display','none');
    $("#"+modal_select_actual).html('');
    $("#modal_select_actual").val('menu_niveles');
    
    var page=1;
    var ob = {page:page};
    
    $.ajax({
        type: "POST",
        url: url_p+"Cnivel_pc/",
        data: ob,
        beforeSend: function(objeto){
          $("#contenedor_niveles").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#contenedor_niveles").html(data);

        }
      });
}


function btn_menu_plan_cuentas()
{
    $("#menu_plan_de_cuentas").css('display','block');
 
    var page=1;
    var ob = {page:page};
    
    $.ajax({
        type: "POST",
        url: url_p+"Cplan_cuentas/",
        data: ob,
        beforeSend: function(objeto){
          $("#contenedor_plan_de_cuentas").html("<div class='cargando'>  </div>");
        },
        success:function(data)
        {
          $("#contenedor_plan_de_cuentas").html(data);

        }
      }); 
}


function btn_menu_plan_cuentas_asignacion()
{
    var modal_select_actual = $("#modal_select_actual").val();
    
    $("#"+modal_select_actual).css('display','none');
    
    $("#menu_plan_de_cuentas").css('display','block');
    $("#modal_select_actual").val('menu_plan_de_cuentas');
    
    var page=1;
    var ob = {page:page};
    
    $.ajax({
        type: "POST",
        url: url_p+"Cplan_cuentas/VPC_asignacion",
        data: ob,
        beforeSend: function(objeto){
          $("#contenedor_plan_de_cuentas").html("<div class='cargando'>  </div>");
        },
        success:function(data)
        {
          $("#contenedor_plan_de_cuentas").html(data);

        }
      }); 
}

function btn_menu_monedas()
{
    $("#menu_moneda").css('display','block');
 
    var modal_select_actual = $("#modal_select_actual").val();
    // $("#"+modal_select_actual).css('display','none');
    $("#"+modal_select_actual).html('');
    $("#modal_select_actual").val('menu_moneda');
    
    var page=1;
    var ob = {page:page};
    
    $.ajax({
        type: "POST",
        url: url_p+"Cmoneda/",
        data: ob,
        beforeSend: function(objeto){
          $("#contenedor_monedas").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#contenedor_monedas").html(data);

        }
      });
    
}

function btn_menu_parametrizacion()
{
    $("#menu_parametrizacion").css('display','block');
 
    var modal_select_actual = $("#modal_select_actual").val();
    $("#"+modal_select_actual).css('display','none');
    $("#modal_select_actual").val('menu_parametrizacion');    
}


/* AREA DE INVENTARIO */

function btn_empresa_menu()
{
    
    
    $("#menu_empresa").css('display','block');
 
    var modal_select_actual = $("#modal_select_actual").val();
    // $("#"+modal_select_actual).css('display','none');
    $("#"+modal_select_actual).html('');
    $("#modal_select_actual").val('menu_empresa');
     
    var page=1;
    var ob = {page:page};
    
    $.ajax({
        type: "POST",
        url: url_p+"Cempresa/",
        data: ob,
        beforeSend: function(objeto){
          $("#contenedor_empresas").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#contenedor_empresas").html(data);

        }
      });
      
}

function btn_productos_menu()
{
    $("#menu_productos").css('display','block');
 
    var modal_select_actual = $("#modal_select_actual").val();
    // $("#"+modal_select_actual).css('display','none');
    $("#"+modal_select_actual).html('');
    $("#modal_select_actual").val('menu_productos');
    
    var page=1;
    var id_empresa = $("#id_empresa_select").val();
    var id_gestion = $("#id_gestion_select").val();
 
    var ob = { page:page, id_empresa:id_empresa, id_gestion:id_gestion };
    
    $.ajax({
        type: "POST",
        url: url_p+"Cproducto/",
        data: ob,
        beforeSend: function(objeto){
          $("#contenedor_productos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#contenedor_productos").html(data);

        }
      });
      
}


function btn_area_productos_menu()
{
    $("#menu_area_productos").css('display','block');
 
    var modal_select_actual = $("#modal_select_actual").val();
    // $("#"+modal_select_actual).css('display','none');
    $("#"+modal_select_actual).html('');
    $("#modal_select_aarea").val('menu_area_productos');
    
    var page=1;
    var id_empresa = $("#id_empresa_select").val();
    var id_gestion = $("#id_gestion_select").val();
    
    //alert(url_p+" - "+id_empresa+" - "+id_gestion);

    var ob = { page:page, id_empresa:id_empresa, id_gestion:id_gestion };
    
    $.ajax({
        type: "POST",
        url: url_p+"Carea/",
        data: ob,
        beforeSend: function(objeto)
        {
          $("#contenedor_area_productos").html("<div class='cargando'>  </div>");
        },
        success:function(data)
        {
          $("#contenedor_area_productos").html(data);
        }
    });    
}


function btn_rubros_menu()
{
    $("#menu_rubro").css('display','block');
 
    var modal_select_actual = $("#modal_select_actual").val();
    // $("#"+modal_select_actual).css('display','none');
    $("#"+modal_select_actual).html('');
    $("#modal_select_actual").val('menu_rubro');
    
    var page=1;
    var id_empresa = $("#id_empresa_select").val();
    var id_gestion = $("#id_gestion_select").val();

    var ob = { page:page, id_empresa:id_empresa, id_gestion:id_gestion };
    
    $.ajax({
        type: "POST",
        url: url_p+"Crubro/",
        data: ob,
        beforeSend: function(objeto){
          $("#contenedor_rubro").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#contenedor_rubro").html(data);

        }
      });   
}

function btn_proveedor_menu()
{
    $("#menu_proveedor").css('display','block');
 
    var modal_select_actual = $("#modal_select_actual").val();
    // $("#"+modal_select_actual).css('display','none');
    $("#"+modal_select_actual).html('');
    $("#modal_select_actual").val('menu_proveedor');
    
    var page=1;
    var id_empresa = $("#id_empresa_select").val();
    var id_gestion = $("#id_gestion_select").val();
    
    //alert(id_empresa+" - "+id_gestion);
 
    var ob = { page:page, id_empresa:id_empresa, id_gestion:id_gestion };
    
    $.ajax({
        type: "POST",
        url: url_p+"Cproveedor/",
        data: ob,
        beforeSend: function(objeto){
          $("#contenedor_proveedor").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#contenedor_proveedor").html(data);

        }
      });   
}

function btn_compra_menu()
{
    $("#menu_compra").css('display','block');
 
    var modal_select_actual = $("#modal_select_actual").val();
    // $("#"+modal_select_actual).css('display','none');
    $("#"+modal_select_actual).html('');
    $("#modal_select_actual").val('menu_compra');
    
    var page=1;
    var id_empresa = $("#id_empresa_select").val();
    var id_gestion = $("#id_gestion_select").val();

    var ob = { page:page, id_empresa:id_empresa, id_gestion:id_gestion };
    
    $.ajax({
        type: "POST",
        url: url_p+"Ccompra/",
        data: ob,
        beforeSend: function(objeto){
          $("#contenedor_compra").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#contenedor_compra").html(data);

        }
      });   
}

function btn_cliente_menu()
{
    $("#menu_cliente").css('display','block');
 
    var modal_select_actual = $("#modal_select_actual").val();
    // $("#"+modal_select_actual).css('display','none');
    $("#"+modal_select_actual).html('');
    $("#modal_select_actual").val('menu_cliente');
    
    var page=1;
    var id_empresa = $("#id_empresa_select").val();
    var id_gestion = $("#id_gestion_select").val();

    var ob = { page:page, id_empresa:id_empresa, id_gestion:id_gestion };
    
    $.ajax({
        type: "POST",
        url: url_p+"Ccliente/",
        data: ob,
        beforeSend: function(objeto){
          $("#contenedor_cliente").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#contenedor_cliente").html(data);

        }
      });   
}

function btn_venta_menu()
{
    $("#menu_venta").css('display','block');
 
    var modal_select_actual = $("#modal_select_actual").val();
    // $("#"+modal_select_actual).css('display','none');
    $("#"+modal_select_actual).html('');
    $("#modal_select_actual").val('menu_venta');
    
    var page=1;
    var id_empresa = $("#id_empresa_select").val();
    var id_gestion = $("#id_gestion_select").val();

    var ob = { page:page, id_empresa:id_empresa, id_gestion:id_gestion };
    
    $.ajax({
        type: "POST",
        url: url_p+"Cventa/",
        data: ob,
        beforeSend: function(objeto){
          $("#contenedor_venta").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#contenedor_venta").html(data);

        }
      });   
}

function btn_menu_estudiantes()
{

    $("#menu_participante").css('display','block');
 
    var modal_select_actual = $("#modal_select_actual").val();
    // $("#"+modal_select_actual).css('display','none');
    $("#"+modal_select_actual).html('');
    $("#modal_select_actual").val('menu_participante');
    
    var page=1;
    var id_empresa = $("#id_empresa_select").val();
    var id_gestion = $("#id_gestion_select").val();

    var ob = { page:page, id_empresa:id_empresa, id_gestion:id_gestion };
    
    $.ajax({
        type: "POST",
        url: url_p+"Cestudiante/",
        data: ob,
        beforeSend: function(objeto){
          $("#contenedor_participante").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#contenedor_participante").html(data);

        }
      });  
      
}

function btn_menu_curso()
{

    $("#menu_curso").css('display','block');
 
    var modal_select_actual = $("#modal_select_actual").val();
    // $("#"+modal_select_actual).css('display','none');
    $("#"+modal_select_actual).html('');
    $("#modal_select_actual").val('menu_curso');
    
    var page=1;
     
    var ob = { page:page};
    
    $.ajax({
        type: "POST",
        url: url_p+"Ccurso/",
        data: ob,
        beforeSend: function(objeto){
          $("#contenedor_curso").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#contenedor_curso").html(data);

        }
      });  
      
}