/* CONTROLADOR DE AREA DEL SISTEMA */
var lista = [];

 function cargar_datos(page)
 {  
    var ob = {page:page};
    $("#panel_resultado_busqueda").html("");

    $.ajax({
        type: "POST",
        url: url_p+"Cestudiante/paginacion",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}


 function buscar_datos(page)
 {  
    var txt_buscar = $("#txt_buscar").val();

    var cadena = " Resultado para : <label> "+txt_buscar+" </label> ";
    $("#panel_resultado_busqueda").html(cadena);
    
    var ob = {page:page,txt_buscar : txt_buscar };

    $.ajax({
        type: "POST",
        url: url_p+"Cestudiante/paginacion_find",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
      });
}

function estudiante_o( nombre_estudiante,razon_social,ci_nit_estudiante,email_estudiante,celular_estudiante,tipo_de_pago,codigo_num_transferencia,id_curso )
{ 
 this.nombre_estudiante = nombre_estudiante; 
   this.razon_social = razon_social; 
   this.ci_nit_estudiante = ci_nit_estudiante; 
   this.email_estudiante = email_estudiante; 
   this.celular_estudiante = celular_estudiante; 
   this.tipo_de_pago = tipo_de_pago; 
   this.codigo_num_transferencia = codigo_num_transferencia; 
   this.id_curso = id_curso; 
  
}

function btn_nuevo_estudiante()
{
  $("#myModal_Register").modal("show");
 
  var ob = "";
  $.ajax({
      type: "POST",
      url: url_p+"Cestudiante/R_estudiante",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_registrar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_registrar").html(data);
      }
     });
}

function btn_agregar_carrito_estudiante()
{ 
    var nombre_estudiante = $("#nombre_estudiante").val();
    var razon_social = $("#razon_social").val();
    var ci_nit_estudiante = $("#ci_nit_estudiante").val();
    var email_estudiante = $("#email_estudiante").val();
    var celular_estudiante = $("#celular_estudiante").val();
    var tipo_de_pago = $("#tipo_de_pago").val();
    var codigo_num_transferencia = $("#codigo_num_transferencia").val();
    var id_curso = $("#id_curso").val(); 
  
  if(nombre_estudiante != "" && razon_social != "" && ci_nit_estudiante != "" && email_estudiante != "" && celular_estudiante != "" && tipo_de_pago != "" && codigo_num_transferencia != "" && id_curso != "" )
  {
  
  var obj_estudiante = new estudiante_o( nombre_estudiante,  razon_social,  ci_nit_estudiante,  email_estudiante,  celular_estudiante,  tipo_de_pago,  codigo_num_transferencia,  id_curso );

  lista.push(obj_estudiante);
  btn_mostrar_datos_estudiante();
  }

  else {
  
       if (nombre_estudiante =="")
       {  
           $("#nombre_estudiante").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de nombre_estudiante </label>";
           $("#panel_resp_nombre_estudiante").html(res);  
       }

       
       if (razon_social =="")
       {  
           $("#razon_social").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de razon_social </label>";
           $("#panel_resp_razon_social").html(res);  
       }

       
       if (ci_nit_estudiante =="")
       {  
           $("#ci_nit_estudiante").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de ci_nit_estudiante </label>";
           $("#panel_resp_ci_nit_estudiante").html(res);  
       }

       
       if (email_estudiante =="")
       {  
           $("#email_estudiante").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de email_estudiante </label>";
           $("#panel_resp_email_estudiante").html(res);  
       }

       
       if (celular_estudiante =="")
       {  
           $("#celular_estudiante").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de celular_estudiante </label>";
           $("#panel_resp_celular_estudiante").html(res);  
       }

       
       if (tipo_de_pago =="")
       {  
           $("#tipo_de_pago").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de tipo_de_pago </label>";
           $("#panel_resp_tipo_de_pago").html(res);  
       }

       
       if (codigo_num_transferencia =="")
       {  
           $("#codigo_num_transferencia").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de codigo_num_transferencia </label>";
           $("#panel_resp_codigo_num_transferencia").html(res);  
       }

       
       if (id_curso =="")
       {  
           $("#id_curso").focus();
           var res ="<label style='color:red;'> Debe llenar el campo de curso </label>";
           $("#panel_resp_id_curso").html(res);  
       }

       
  //Final de la codicional else
  }

 //Final de la fucion
}

function btn_mostrar_datos_estudiante()
{
  var cadena = "<table class='table table-bordered table-condensed table-striped table-hover'>";
  
  cadena = cadena + "<tr>"+ "<th> nombre_estudiante </th> " +  "<th> razon_social </th> " +  "<th> ci_nit_estudiante </th> " +  "<th> email_estudiante </th> " +  "<th> celular_estudiante </th> " +  "<th> tipo_de_pago </th> " +  "<th> codigo_num_transferencia </th> " +  "<th> curso </th> " +  "<th> Opciones </th>"; 

  cadena = cadena +"</tr>";

  for(var i = 0 ; i<lista.length; i++)
  {
    nombre_estudiante = lista[i].nombre_estudiante; 
    razon_social = lista[i].razon_social; 
    ci_nit_estudiante = lista[i].ci_nit_estudiante; 
    email_estudiante = lista[i].email_estudiante; 
    celular_estudiante = lista[i].celular_estudiante; 
    tipo_de_pago = lista[i].tipo_de_pago; 
    codigo_num_transferencia = lista[i].codigo_num_transferencia; 
    curso = lista[i].curso; 
      cadena = cadena + "<tr>"+
    "<td>"+ nombre_estudiante +"</td>"+
    "<td>"+ razon_social +"</td>"+
    "<td>"+ ci_nit_estudiante +"</td>"+
    "<td>"+ email_estudiante +"</td>"+
    "<td>"+ celular_estudiante +"</td>"+
    "<td>"+ tipo_de_pago +"</td>"+
    "<td>"+ codigo_num_transferencia +"</td>"+
    "<td>"+ curso +"</td>"+ " <td  class='col-lg-1' > <center> <button class='btn btn-danger btn-xs' onclick='btn_eliminar_item("+i+");' > X </button> </center> </td><tr>"; 
   }

   cadena = cadena +" </table> ";

   $("#panel_listado_estudiante").html(cadena);  
 
} 

function btn_eliminar_item(contador)
{
  lista.splice( contador , 1);
  btn_mostrar_datos_estudiante();
}

function btn_registrar_estudiante()
{ 
  var id_usuario = $("#id_usuario_session").val(); 

   var lista_area = JSON.stringify(lista);
   var ob = {lista : lista_area, id_usuario:id_usuario };

  
  $.ajax({
      type: "POST",
      url: url_p+"Cestudiante/G_estudiante",
      data: ob,
      beforeSend: function(objeto){

        $("#panel_modal_resultado_registrar").html("<div class='cargando'>  </div>");
        $("#btn_registrar").prop("disabled", true);

      },
      success:function(data)
      { 
        $("#btn_registrar").prop("disabled", false);   
        $("#panel_modal_resultado_registrar").html(data);
        
 
      }
  });
}

function btn_examinar_estudiante(id_estudiante)
{
  $("#myModal_View").modal("show");

  var ob = {id_estudiante : id_estudiante};
  $.ajax({
      type: "POST",
      url: url_p+"Cestudiante/VD_estudiante",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_examinar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_examinar").html(data);
      }
     });

}

function btn_editar_estudiante(id_estudiante)
{
  $("#myModal_Update").modal("show");
  $("#id_estudiante_edicion").val(id_estudiante);
  
  var ob = {id_estudiante : id_estudiante};
  $.ajax({
      type: "POST",
      url: url_p+"Cestudiante/VE_estudiante",
      data: ob,
      beforeSend: function(objeto){
        $("#panel_modal_editar").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      {   
        $("#panel_modal_editar").html(data);
      }
     });

}

function btn_guardar_estudiante()
{

  var id_estudiante = $("#id_estudiante_edicion").val();

  var nombre_estudiante = $("#nombre_estudiante").val();
  var razon_social = $("#razon_social").val();
  var ci_nit_estudiante = $("#ci_nit_estudiante").val();
  var email_estudiante = $("#email_estudiante").val();
  var celular_estudiante = $("#celular_estudiante").val();
  var tipo_de_pago = $("#tipo_de_pago").val();
  var codigo_num_transferencia = $("#codigo_num_transferencia").val();
  var id_curso = $("#id_curso").val();
  var ob = { id_estudiante : id_estudiante, nombre_estudiante:nombre_estudiante, razon_social:razon_social, ci_nit_estudiante:ci_nit_estudiante, email_estudiante:email_estudiante, celular_estudiante:celular_estudiante, tipo_de_pago:tipo_de_pago, codigo_num_transferencia:codigo_num_transferencia, id_curso:id_curso }; 

  var formData = new FormData($("#formulario")[0]);
  formData.append("id_estudiante", id_estudiante);

  $.ajax({
   url: url_p+"Cestudiante/VU_estudiante",
   type: "POST",
   dataType: "html",
   data: formData,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function(objeto){

     $("#btn_editar").prop("disabled", true);
     $("#panel_modal_respuesta_editar").html("<div class='cargando'>  </div>");

   },
   success: function(data)
   { 
     $("#btn_editar").prop("disabled", false);  
     $("#panel_modal_respuesta_editar").html(data);
   
   }
 }); 

}

function btn_borrar_estudiante(id_estudiante)
{
  $("#myModal_Delete").modal("show");
  $("#id_estudiante_eliminar").val(id_estudiante);

}

function btn_eliminar_estudiante()
{
  var id_estudiante = $("#id_estudiante_eliminar").val();

  var ob = { id_estudiante : id_estudiante };
  
  $.ajax({
      type: "POST",
      url: url_p+"Cestudiante/VB_estudiante",
      data: ob,
      beforeSend: function(objeto){

        $("#btn_borrar").prop("disabled", true);
        $("#panel_modal_eliminar").html("<div class='cargando'>  </div>");

      },
      success:function(data)
      { 
        $("#btn_borrar").prop("disabled", false);   
        $("#panel_modal_eliminar").html(data);
        
        setTimeout(function(){ $("#panel_modal_eliminar").html(""); },1000);
        setTimeout(function(){ $("#myModal_Delete").modal("hide"); },2000);
        setTimeout(function(){ cargar_datos(1); },3000);
      }
  });

}


var estado = 0;

function btn_submenu(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_"+id).css("display","none");
  }
  }

}

function btn_submenu_xs(id)
{
  if(estado==0)
  { //alert(id+" - "+estado);
 
    $("#panel_opciones_xs"+id).css("display","block");
    estado++;
  }

  else{
  if(estado>0)
  { //alert(id+" - "+estado);
    estado=0;
    
    $("#panel_opciones_xs"+id).css("display","none");
  }
  }

}


function select(id,dato,area)
{   
  $("#id_"+area).val(id);
  $("#"+area).html(dato);
}


/*OPCIONES DE IMPRESIONES LISTADO DE ESTUDIANTES */

function btn_opciones_impresion()
{
  $("#myModal_Impresion").modal("show");
 
  var ob = "";
  
  $.ajax({
      type: "POST",
      url: url_p+"Cestudiante/VO_impresion_estudiante",
      data: ob,
      beforeSend: function(objeto)
      {
        $("#panel_modal_impresion").html("<div class='cargando'>  </div>");
      },
      success:function(data)
      { 
        $("#panel_modal_impresion").html(data);
      }
  });
  
}

function btn_certificado_estudiante(id_estudiante)
{
  var url_cert = url_p+"Cestudiante/VO_certificado_estudiante?ID="+id_estudiante;
  window.open(url_cert, '_blank');
}

function btn_listar_estudiantes_curso()
{   var page = 1;
    var id_curso = $("#id_curso").val();
    var ob = { page:page, id_curso:id_curso };

    $.ajax({
        type: "POST",
        url: url_p+"Cestudiante/Filtro_Cursos",
        data: ob,
        beforeSend: function(objeto){
          $("#panel_paginacion_datos").html("<div class='cargando'>  </div>");
        },
        success:function(data){
         
          $("#panel_paginacion_datos").html(data);

        }
    });
}

function btn_modal_impresion(id_estudiante,id_curso)
{
  /*
    $("#myModal_Certificado").modal("show");
 
    var ob = { id_estudiante:id_estudiante, id_curso : id_curso };

    $.ajax({
        type: "POST",
        url: url_p+"Cestudiante/Formulario_impresion",
        data: ob,
        beforeSend: function(objeto)
        {
          $("#panel_modal_certificado").html("<div class='cargando'>  </div>");
        },
        success:function(data)
        {
          $("#panel_modal_certificado").html(data);
        }
    });
    
  */
  
  var url_cert = url_p+"Cestudiante/VO_certificado_estudiante?IDE="+id_estudiante+"&IDC="+id_curso;
  
  window.open(url_cert, '_blank');
}

function btn_formulario_imprimir_participante()
{
    var id_curso = $("#id_curso").val();
    var id_estudiante = $("#id_estudiante").val();
    var titulo_curso = $("#titulo_curso").val();
    var detalle_curso = $("#detalle_curso").val();
    var participante_curso = $("#participante_curso").val();
    var fecha_curso = $("#fecha_curso").val();


    var url_cert = url_p+"Cestudiante/VO_certificado_estudiante?IDE="+id_estudiante+
    "&IDC="+id_curso+"&TC="+titulo_curso+"&DC="+detalle_curso+"&PC="+participante_curso+
    "&FC="+fecha_curso;
  
    window.open(url_cert, '_blank');
  
}

function btn_opciones_impresion_certificados()
{
    var id_curso = $("#id_curso").val();
    var ob = { id_curso : id_curso };

    $.ajax({
        type: "POST",
        url: url_p+"Cestudiante/Impresion_Certificados",
        data: ob,
        beforeSend: function(objeto)
        {
          $("#panel_certificados_print").html("<div class='cargando'>  </div>");
        },
        success:function(data)
        {
          $("#panel_certificados_print").html(data);
        }
    });
    
}




